/**
 Custom module for site-specific javascript functions
 **/

var Custom = function()
{
	// private functions & variables

	var handleSpinners = function()
	{
		$('#slide-spinner-home').spinner();
		$('#results-spinner-coming').spinner();
		$('#results-spinner-available').spinner();
		$('#results-spinner-listings').spinner();
		$('#results-spinner-map').spinner();
		$('#setting_spinner_gated_views').spinner();
	}

	var handleDatetimePicker = function()
	{

		$(".form_date").datepicker({
			autoclose: true,
			format: "MM dd, yyyy",
			pickerPosition: "bottom-left",
			startDate: "+0d",
			endDate: "+1y"
		});

		$(".form_time").timepicker({
			autoclose: true,
			minuteStep: 30
		});

		$(".form_datetime").datetimepicker({
			autoclose: true,
			format: "MM dd, yyyy hh:ii",
			pickerPosition: "bottom-left",
			startDate: "-30d",
			endDate: "+1y"
		});

		$('body').removeClass("modal-open"); // fix bug when inline picker is used in modal
	}

	var togglePortletCollapse = function()
	{
		$('.portlet-title > .tools > a').on('click', function(event)
		{
			event.stopImmediatePropagation();

			$(this).parents('.portlet-title').trigger('click');
		});

		$('.portlet-title').on('click', function()
		{
			$portlet_title = $(this);
			$portlet_body = $(this).next();
			$collapse_btn = $(this).children('.tools').children('a');

			if($collapse_btn.length > 0)
			{
				if($collapse_btn.hasClass('expand'))
				{
					$collapse_btn.removeClass('expand');
					$collapse_btn.addClass('collapse');
					$portlet_body.removeClass('display-hide');
				}
				else
				{
					$collapse_btn.removeClass('collapse');
					$collapse_btn.addClass('expand');
					$portlet_body.addClass('display-hide');
				}
			}

		});
	}

	var initFavoriteReportTable = function()
	{
		var table = $('#favorite_report');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{
					"width": "55%",
					"targets": [0]
				},
				{
					"width": "15%",
					"targets": [1]
				},
				{
					"width": "15%",
					"targets": [2]
				},
				{
					"width": "15%",
					"targets": [3]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#favorite_report__wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown
	}

	var initShowingReportTable = function()
	{
		var table = $('#showing_report');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{
					"width": "25%",
					"targets": [0]
				},
				{
					"width": "30%",
					"targets": [1]
				},
				{
					"width": "15%",
					"targets": [2]
				},
				{
					"width": "15%",
					"targets": [3]
				},
				{
					"width": "15%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#showing_report__wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown
	}

	var initContactReportTable = function()
	{
		var table = $('#contact_report');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 20,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{
					"width": "15%",
					"targets": [0]
				},
				{
					"width": "15%",
					"targets": [1]
				},
				{
					"width": "15%",
					"targets": [2]
				},
				{
					"width": "40%",
					"targets": [3]
				},
				{
					"width": "15%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#contact_report__wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown
	}

	var initPagesHomeSliderTable = function()
	{
		function restoreRow(oTable, nRow)
		{
			var aData = oTable.fnGetData(nRow);
			var jqTds = $('>td', nRow);

			for(var i = 0, iLen = jqTds.length; i < iLen; i++)
			{
				oTable.fnUpdate(aData[i], nRow, i, false);
			}

			oTable.fnDraw();
		}

		function editRow(oTable, nRow)
		{
			var aData = oTable.fnGetData(nRow);
			var jqTds = $('>td', nRow);

			jqTds[0].innerHTML = '<input id="text_order" type="text" class="form-control" value="' + aData[1] + '">';
			jqTds[1].innerHTML =
				'<div class="fileinput fileinput-new" data-provides="fileinput">\n' +
				'	<div class="fileinput-new thumbnail" style="max-width: 200px; max-height: 150px;">\n' +
				'		' + aData[2] + '\n' +
				'	</div>\n' +
				'	<div class="fileinput-preview fileinput-exists thumbnail"\n' +
				'	   style="max-width: 200px; max-height: 150px;"></div>\n' +
				'	<div>\n' +
				'		<span class="btn default btn-file">\n' +
				'			<span class="fileinput-new"> Select image </span>\n' +
				'			<span class="fileinput-exists"> Change image</span>\n' +
				'			<input type="file" id="slider_pictures_upload" name="slider_pictures_upload">\n' +
				'        <span class="fileinput-filename display-hide"></span>\n' +
				'		</span>\n' +
				'	</div>\n' +
				'</div>';
			jqTds[2].innerHTML = '<input id="text_title" type="text" class="form-control" value="' + aData[3] + '">';
			jqTds[3].innerHTML = '<textarea id="text_caption" class="form-control" rows="3">' + aData[4] + '</textarea>';
			jqTds[4].innerHTML =
				'<a class="btn default btn-xs green edit" href=""><i class="fa fa-download"></i> Save </a>\n' +
				'<a class="btn default btn-xs red cancel" href=""><i class="fa fa-times-circle"></i> Cancel </a>';
		}

		function saveRow(oTable, nRow)
		{
			var aData = oTable.fnGetData(nRow);

			var slideID = aData[0];
			var txtOrder = '', image = '', imageEdit = '', imageName = '', txtTitle = '', txtCaption = '';
			if($('#text_order', nRow).length > 0)
			{
				txtOrder = $('#text_order', nRow).val();
			}
			if($('.fileinput-exists > img', nRow).length > 0)
			{
				image = $('.fileinput-exists > img', nRow).attr("src");
			}
			if($('.fileinput-new.thumbnail > img', nRow).length > 0)
			{
				imageEdit = $('.fileinput-new.thumbnail > img', nRow).attr("src");
			}
			if($('.fileinput-filename', nRow).length > 0)
			{
				imageName = $('.fileinput-filename', nRow).text();
			}
			if($('#text_title', nRow).length > 0)
			{
				txtTitle = $('#text_title', nRow).val();
			}
			if($('#text_caption', nRow).length > 0)
			{
				txtCaption = $('#text_caption', nRow).val();
			}
			var sliderFlash = $('#pages_home_slider_flash');

			// Display our loading, please wait message
			if(image != null && imageEdit == null)
			{
				sliderFlash.attr("class", "alert alert-info");
				sliderFlash.text("Uploading image, please wait...");
				sliderFlash.show(500);
			}
			else
			{
				sliderFlash.attr("class", "alert alert-info");
				sliderFlash.text("Saving slide information, please wait...");
				sliderFlash.show(500);
			}

			var imageSize = 0;

			if(image != null && imageEdit == null)
			{
				imageSize = image.length;

				if(imageSize > 1500000)
				{
					sliderFlash.attr("class", "alert alert-danger");
					sliderFlash.text("Image too large! The maximum file size for images is 1.5 MB!");
					$('a.cancel', nRow).trigger('click');
					sliderFlash.delay(3000).hide(500);
					return;
				}
				if(imageSize < 1)
				{
					sliderFlash.attr("class", "alert alert-danger");
					sliderFlash.text("Image not found! You must select an image to add a new slide!");
					$('a.cancel', nRow).trigger('click');
					sliderFlash.delay(3000).hide(500);
					return;
				}
			}

			// Call our ajax handler to save the edits to the db
			// We don't have an actual form to process, so we will need to
			// create our data package manually
			var ajaxUrl = "/admin/pages/";
			ajaxUrl += (slideID != 0) ? "update/slider/" : "add/slider/";

			var formData = new FormData();

			formData.append("picture_id", slideID);
			formData.append("order", txtOrder);
			formData.append("image", image);
			formData.append("image_name", imageName);
			formData.append("title", txtTitle);
			formData.append("caption", txtCaption);

			$.ajax({
				url: ajaxUrl,
				type: "POST",
				data: formData,
				processData: false,
				contentType: false
			})
				.done(function(data)
				{
					// Ajax call success
					console.log(data);
					var response = $.parseJSON(data);
					var status = response["status"];
					var message = response["message"];
					var new_picture_id = response["new_picture_id"];

					if(status == "success")
					{
						if(new_picture_id != 0)
						{
							oTable.fnUpdate(new_picture_id, nRow, 0, false);
						}
						oTable.fnUpdate(txtOrder, nRow, 1, false);
						if(image.length > 0)
						{
							oTable.fnUpdate('<img class="slide-thumb" src="' + image + '">', nRow, 2, false);
						}
						oTable.fnUpdate(txtTitle, nRow, 3, false);
						oTable.fnUpdate(txtCaption, nRow, 4, false);
						oTable.fnUpdate(
							'<a class="btn default btn-xs green edit" href=""><i class="fa fa-edit"></i> Edit </a>\n' +
							'<a class="btn default btn-xs red delete" href=""><i class="fa fa-trash-o"></i> Delete </a>', nRow,
							5, false);
						oTable.fnDraw();

						sliderFlash.attr("class", "alert alert-success");
						sliderFlash.text("Successfully updated slider image.");
						sliderFlash.delay(1500).hide(500);
					}
					else
					{
						sliderFlash.attr("class", "alert alert-danger");
						sliderFlash.text("Unable to upload slider image at this time.");
						sliderFlash.delay(3000).hide(500);
					}
				})
				.fail(function()
				{
					sliderFlash.attr("class", "alert alert-danger");
					sliderFlash.text("Unable to upload slider image at this time.");
					sliderFlash.delay(3000).hide(500);
				});
		}

		function deleteRow(oTable, nRow)
		{
			var aData = oTable.fnGetData(nRow);

			var slideID = aData[0];

			// Call our ajax handler to delete the current slide
			var ajaxUrl = "/admin/pages/delete/slider/";

			var formData = new FormData();

			formData.append("picture_id", slideID);

			$.ajax({
				url: ajaxUrl,
				type: "POST",
				data: formData,
				processData: false,
				contentType: false
			})
				.done(function()
				{
					oTable.fnDeleteRow(nRow);

					alert("Successfully deleted slider image!");
				})
				.fail(function()
				{
					alert("Unable to delete slider image at this time.");
				});
		}

		var table = $('#slider_pictures');

		var oTable = table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					"searchable": false,
					"visible": false,
					'targets': [0]
				},
				{
					"createdCell": function(td, cellData, rowData, row, col)
					{
						$(td).css("text-align", "center");
					},
					"width": "5%",
					"targets": [1]
				},
				{
					"width": "20%",
					"targets": [2]
				},
				{
					"width": "15%",
					"targets": [3]
				},
				{
					"width": "45%",
					"targets": [4]
				},
				{
					"orderable": false,
					"searchable": false,
					"width": "15%",
					"targets": [5]
				}
			],
			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#slider_pictures_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		var nEditing = null;
		var nNew = false;

		$('#slider_pictures_add').click(function(e)
		{
			e.preventDefault();

			if(nNew && nEditing)
			{
				if(confirm("Previous row not saved. Do you want to save it ?"))
				{
					saveRow(oTable, nEditing); // save
					$(nEditing).find("td:first").html("Untitled");
					nEditing = null;
					nNew = false;

				}
				else
				{
					oTable.fnDeleteRow(nEditing); // cancel
					nEditing = null;
					nNew = false;

					return;
				}
			}

			var aiNew = oTable.fnAddData(['0',
				'',
				'<img class="slide-thumb" src="/includes/img/no-image.png">',
				'',
				'',
				'']);
			var nRow = oTable.fnGetNodes(aiNew[0]);
			editRow(oTable, nRow);
			nEditing = nRow;
			nNew = true;
		});

		table.on('click', '.delete', function(e)
		{
			e.preventDefault();

			if(confirm("Are you sure to delete this slide?") == false)
			{
				return;
			}

			var nRow = $(this).parents('tr')[0];
			deleteRow(oTable, nRow);
			nEditing = null;
			nNew = false;
		});

		table.on('click', '.cancel', function(e)
		{
			e.preventDefault();

			if(nNew)
			{
				oTable.fnDeleteRow(nEditing);
				nEditing = null;
				nNew = false;
			}
			else
			{
				restoreRow(oTable, nEditing);
				nEditing = null;
				nNew = false;
			}
		});

		table.on('click', '.edit', function(e)
		{
			e.preventDefault();

			/* Get the row as a parent of the link that was clicked on */
			var nRow = $(this).parents('tr')[0];

			if(nEditing !== null && nEditing != nRow)
			{
				/* Currently editing - but not this row - restore the old before continuing to edit mode */
				restoreRow(oTable, nEditing);
				editRow(oTable, nRow);
				nEditing = nRow;
			}
			else if(nEditing == nRow && this.innerHTML.indexOf("Save") > -1)
			{
				/* Editing this row and want to save it */
				saveRow(oTable, nEditing);
				nEditing = null;
			}
			else
			{
				/* No edit in progress - let's start one */
				editRow(oTable, nRow);
				nEditing = nRow;
			}
		});

	}

	var initPropertyListTable = function()
	{
		var table = $('#prop_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 15,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [4]
				},
				{
					"searchable": false,
					"targets": [4]
				},
				{
					"width": "20%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "20%",
					"targets": [2]
				},
				{
					"width": "20%",
					"targets": [3]
				},
				{
					"width": "20%",
					"targets": [4]
				}
			],
			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#prop_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this property?") == false)
			{
				return false;
			}
		});
	}

	var initUserListTable = function()
	{

		var table = $('#user_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [5]
				},
				{
					"searchable": false,
					"targets": [5]
				},
				{
					"width": "15%",
					"targets": [0]
				},
				{
					"width": "15%",
					"targets": [1]
				},
				{
					"width": "30%",
					"targets": [2]
				},
				{
					"width": "10%",
					"targets": [3]
				},
				{
					"width": "10%",
					"targets": [4]
				},
				{
					"width": "20%",
					"targets": [5]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#user_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this user?") == false)
			{
				return false;
			}
		});
	}

	var initTeamListTable = function()
	{

		var table = $('#team_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [4]
				},
				{
					"searchable": false,
					"targets": [4]
				},
				{
					"width": "10%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "35%",
					"targets": [2]
				},
				{
					"width": "10%",
					"targets": [3]
				},
				{
					"width": "25%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#team_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this team?") == false)
			{
				return false;
			}
		});
	}

	var initOfficeListTable = function()
	{

		var table = $('#office_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [4]
				},
				{
					"searchable": false,
					"targets": [4]
				},
				{
					"width": "10%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "35%",
					"targets": [2]
				},
				{
					"width": "10%",
					"targets": [3]
				},
				{
					"width": "25%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#office_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this office?") == false)
			{
				return false;
			}
		});
	}

	var initCompanyListTable = function()
	{
		var table = $('#company_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [4]
				},
				{
					"searchable": false,
					"targets": [4]
				},
				{
					"width": "10%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "35%",
					"targets": [2]
				},
				{
					"width": "10%",
					"targets": [3]
				},
				{
					"width": "25%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#company_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this company?") == false)
			{
				return false;
			}
		});
	}

	var initAclListTable = function()
	{
		var table = $('#acl_list');

		table.dataTable({
			"lengthMenu": [
				[1, 5, 15, -1],
				[1, 5, 15, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [3]
				},
				{
					"searchable": false,
					"targets": [3]
				},
				{
					"width": "20%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "20%",
					"targets": [2]
				},
				{
					"width": "20%",
					"targets": [3]
				},
				{
					"width": "20%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#acl_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this access right?") == false)
			{
				return false;
			}
		});
	}

	var initVendorListTable = function()
	{

		var table = $('#vendor_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 5,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{
					"width": "25%",
					"targets": [0]
				},
				{
					"width": "30%",
					"targets": [1]
				},
				{
					"width": "25%",
					"targets": [2]
				},
				{
					"width": "20%",
					"targets": [3],
					"searchable": false,
					'orderable': false
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#vendor_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this vendor?") == false)
			{
				return false;
			}
		});
	}

	var initSearchesListTable = function()
	{

		var table = $('#searches_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 15,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [4]
				},
				{
					"searchable": false,
					"targets": [4]
				},
				{
					"width": "20%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "20%",
					"targets": [2]
				},
				{
					"width": "20%",
					"targets": [3]
				},
				{
					"width": "20%",
					"targets": [4]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#searches_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this search?") == false)
			{
				return false;
			}
		});
	}

	var initNewsListTable = function()
	{

		var table = $('#news_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 15,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [3]
				},
				{
					"searchable": false,
					"targets": [3]
				},
				{
					"width": "40%",
					"targets": [0]
				},
				{
					"width": "20%",
					"targets": [1]
				},
				{
					"width": "20%",
					"targets": [2]
				},
				{
					"width": "20%",
					"targets": [3]
				}
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#news_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure to delete this news article?") == false)
			{
				return false;
			}
		});
	}

	var initTestimonialListTable = function()
	{

		var table = $('#testimonial_list');

		table.dataTable({
			"lengthMenu": [
				[5, 15, 20, -1],
				[5, 15, 20, "All"] // change per page values here
			],
			// set the initial value
			"autoWidth": false,
			"pageLength": 15,
			"language": {
				"lengthMenu": "_MENU_ records per page",
				"paging": {
					"previous": "Prev",
					"next": "Next"
				}
			},
			"columnDefs": [
				{  // set default column settings
					'orderable': false,
					'targets': [3]
				},
				{
					"searchable": false,
					"targets": [3]
				},
				{
					"width": "25%",
					"targets": [0]
				},
				{
					"width": "25%",
					"targets": [1]
				},
				{
					"width": "25%",
					"targets": [2]
				},
				{
					"width": "25%",
					"targets": [3]
				},
			],

			"order": [
				[0, "asc"]
			] // set first column as a default sort by asc
		});

		var tableWrapper = jQuery('#testimonial_list_wrapper');

		tableWrapper.find('.dataTables_length select').select2({
			minimumResultsForSearch: -1
		}); // initialize select2 dropdown

		table.on('click', '.delete', function(e)
		{
			if(confirm("Are you sure you want to delete this testimonial?") == false)
			{
				return false;
			}
		});
	}

	var initPagesHomeButtons = function()
	{
		var pagesHomeCollapse = $('#pages_home_collapse');
		var pagesHomeForm = $('#pages_home_form');

		var pagesHomeSliderType = $('#slide_type');
		var pagesHomeSlider = $('#pages_home_slider_portlet');

		var savePagesHome = $('#pages_home_save');
		var cancelPagesHome = $('#pages_home_cancel');

		pagesHomeSliderType.on('change', function()
		{
			if($(this).val() == "featured")
			{
				pagesHomeSlider.addClass('display-hide');
			}
			else if($(this).val() == "custom")
			{
				pagesHomeSlider.removeClass('display-hide');
			}
		});
		savePagesHome.on('click', function()
		{
			pagesHomeForm.submit();
		});

		cancelPagesHome.on('click', function()
		{
			pagesHomeForm.trigger('reset');
			pagesHomeCollapse.trigger('click');
		});
	}

	var initPagesComingButtons = function()
	{
		var pagesComingCollapse = $('#pages_coming_collapse');
		var pagesComingForm = $('#pages_coming_form');

		var savePagesComing = $('#pages_coming_save');
		var cancelPagesComing = $('#pages_coming_cancel');

		savePagesComing.on('click', function()
		{
			pagesComingForm.submit();
		});

		cancelPagesComing.on('click', function()
		{
			pagesComingForm.trigger('reset');
			pagesComingCollapse.trigger('click');
		});
	}

	var initPagesAvailableButtons = function()
	{
		var pagesAvailableCollapse = $('#pages_available_collapse');
		var pagesAvailableForm = $('#pages_available_form');

		var savePagesAvailable = $('#pages_available_save');
		var cancelPagesAvailable = $('#pages_available_cancel');

		savePagesAvailable.on('click', function()
		{
			pagesAvailableForm.submit();
		});

		cancelPagesAvailable.on('click', function()
		{
			pagesAvailableForm.trigger('reset');
			pagesAvailableCollapse.trigger('click');
		});
	}

	var initPagesListingsButtons = function()
	{
		var pagesListingsCollapse = $('#pages_listings_collapse');
		var pagesListingsForm = $('#pages_listings_form');

		var savePagesListings = $('#pages_listings_save');
		var cancelPagesListings = $('#pages_listings_cancel');

		savePagesListings.on('click', function()
		{
			pagesListingsForm.submit();
		});

		cancelPagesListings.on('click', function()
		{
			pagesListingsForm.trigger('reset');
			pagesListingsCollapse.trigger('click');
		});
	}

	var initPagesMapButtons = function()
	{
		var pagesMapCollapse = $('#pages_map_collapse');
		var pagesMapForm = $('#pages_map_form');

		var savePagesMap = $('#pages_map_save');
		var cancelPagesMap = $('#pages_map_cancel');

		savePagesMap.on('click', function()
		{
			pagesMapForm.submit();
		});

		cancelPagesMap.on('click', function()
		{
			pagesMapForm.trigger('reset');
			pagesMapCollapse.trigger('click');
		});
	}

	var initPagesDetailsButtons = function()
	{
		var pagesDetailsCollapse = $('#pages_details_collapse');
		var pagesDetailsForm = $('#pages_details_form');

		var savePagesDetails = $('#pages_details_save');
		var cancelPagesDetails = $('#pages_details_cancel');

		savePagesDetails.on('click', function()
		{
			pagesDetailsForm.submit();
		});

		cancelPagesDetails.on('click', function()
		{
			pagesDetailsForm.trigger('reset');
			pagesDetailsCollapse.trigger('click');
		});
	}

	var initPagesCommunitiesButtons = function()
	{
		var pagesCommunitiesCollapse = $('#pages_communities_collapse');
		var pagesCommunitiesForm = $('#pages_communities_form');

		var savePagesCommunities = $('#pages_communities_save');
		var cancelPagesCommunities = $('#pages_communities_cancel');

		savePagesCommunities.on('click', function()
		{
			pagesCommunitiesForm.submit();
		});

		cancelPagesCommunities.on('click', function()
		{
			pagesCommunitiesForm.trigger('reset');
			pagesCommunitiesCollapse.trigger('click');
		});
	}

	var initPagesAboutButtons = function()
	{
		var pagesAboutCollapse = $('#pages_about_collapse');
		var pagesAboutForm = $('#pages_about_form');

		var savePagesAbout = $('#pages_about_save');
		var cancelPagesAbout = $('#pages_about_cancel');

		savePagesAbout.on('click', function()
		{
			pagesAboutForm.submit();
		});

		cancelPagesAbout.on('click', function()
		{
			pagesAboutForm.trigger('reset');
			pagesAboutCollapse.trigger('click');
		});
	}

	var initPropertyButtons = function()
	{
		var propertyInfoForm = $('#prop_info_form');

		var saveProperty = $('#prop_info_save');
		var cancelProperty = $('#prop_info_cancel');

		saveProperty.on('click', function()
		{
			propertyInfoForm.submit();
		});

		cancelProperty.on('click', function()
		{
			propertyInfoForm.trigger('reset');
			window.location.href = "/admin/properties/";
		});
	}

	var initUsersButtons = function()
	{
		var userInfoForm = $('#user_info_form');

		var saveUser = $('#user_info_save');
		var cancelUser = $('#user_info_cancel');

		saveUser.on('click', function()
		{
			userInfoForm.submit();
		});

		cancelUser.on('click', function()
		{
			userInfoForm.trigger('reset');
			window.location.href = "/admin/users/";
		});
	}

	var initTeamButtons = function()
	{
		var teamInfoForm = $('#team_info_form');

		var saveTeam = $('#team_info_save');
		var cancelTeam = $('#team_info_cancel');

		saveTeam.on('click', function()
		{
			teamInfoForm.submit();
		});

		cancelTeam.on('click', function()
		{
			teamInfoForm.trigger('reset');
			window.location.href = "/admin/users/";
		});
	}

	var initOfficeButtons = function()
	{
		var officeInfoForm = $('#office_info_form');

		var saveOffice = $('#office_info_save');
		var cancelOffice = $('#office_info_cancel');

		saveOffice.on('click', function()
		{
			officeInfoForm.submit();
		});

		cancelOffice.on('click', function()
		{
			officeInfoForm.trigger('reset');
			window.location.href = "/admin/users/";
		});
	}

	var initCompanyButtons = function()
	{
		var companyInfoForm = $('company_info_form');

		var saveCompany = $('#company_info_save');
		var cancelCompany = $('#company_info_cancel');

		saveCompany.on('click', function()
		{
			companyInfoForm.submit();
		});

		cancelCompany.on('click', function()
		{
			companyInfoForm.trigger('reset');
			window.location.href = "/admin/users/";
		});
	}

	var initAclButtons = function()
	{
		var aclInfoForm = $('acl_info_form');

		var saveAcl = $('#acl_info_save');
		var cancelAcl = $('#acl_info_cancel');

		saveAcl.on('click', function()
		{
			aclInfoForm.submit();
		});

		cancelAcl.on('click', function()
		{
			aclInfoForm.trigger('reset');
			window.location.href = "/admin/users/";
		});
	}

	var initIntegrationButtons = function()
	{
		var bombbombForm = $('#bombbomb_form');
		var bombbombFlash = $('#bombbomb_flash');
		var saveBombbomb = $('#bombbomb_save');
		var cancelBombbomb = $('#bombbomb_cancel');
		var verifyBombbomb = $('#bombbomb_verify');

		cancelBombbomb.on('click', function()
		{
			bombbombForm.trigger('reset');
		});

		verifyBombbomb.on('click', function()
		{
			$.ajax({
				type: "post",
				url: "/admin/integration/bombbomb/",
				data: bombbombForm.serialize()
			})
				.done(function(data)
				{
					// Ajax call success
					//console.log(data);
					var response = $.parseJSON(data);
					var status = response["status"];
					var message = response["message"];
					var lists = response["lists"];

					if(status == "success")
					{
						$('#bombbomb_verify_note').removeClass('verify-warning');
						$('#bombbomb_verify_note').addClass('verify-success');
						$('#bombbomb_verify_note').text(message);

						$('#bombbomb_list_id').html(lists);
					}
					else
					{
						$('#bombbomb_verify_note').removeClass('verify-success');
						$('#bombbomb_verify_note').addClass('verify-warning');
						$('#bombbomb_verify_note').text(message);
					}
				})
				.fail(function(jqXHR, status, error)
				{
					console.log(error);
				});
		});

		if(bombbombFlash.length > 0 && bombbombFlash.hasClass('alert'))
		{
			bombbombFlash.hide().slideDown(1000).delay(2000).slideUp(1000);
		}

		var rewForm = $('#rew_form');
		var rewFlash = $('#rew_flash');
		var saveREW = $('#rew_save');
		var cancelREW = $('#rew_cancel');
		var verifyREW = $('#rew_verify');

		cancelREW.on('click', function()
		{
			rewForm.trigger('reset');
		});

		verifyREW.on('click', function()
		{
			$.ajax({
				type: "post",
				url: "/admin/integration/rew/",
				data: rewForm.serialize()
			})
				.done(function(data)
				{
					// Ajax call success
					//console.log(data);
					var response = $.parseJSON(data);
					var status = response["status"];
					var message = response["message"];
					var lists = response["lists"];

					if(status == "success")
					{
						$('#rew_verify_note').removeClass('verify-warning');
						$('#rew_verify_note').addClass('verify-success');
						$('#rew_verify_note').text(message);

						$('#rew_list_id').html(lists);
					}
					else
					{
						$('#rew_verify_note').removeClass('verify-success');
						$('#rew_verify_note').addClass('verify-warning');
						$('#rew_verify_note').text(message);
					}
				})
				.fail(function(jqXHR, status, error)
				{
					console.log(error);
				});
		});

		if(rewFlash.length > 0 && rewFlash.hasClass('alert'))
		{
			rewFlash.hide().slideDown(1000).delay(2000).slideUp(1000);
		}
	}

	var initSearchesButtons = function()
	{
		var searchInfoForm = $('#search_info_form');

		var saveSearch = $('#search_info_save');
		var cancelSearch = $('#search_info_cancel');

		saveSearch.on('click', function()
		{
			searchInfoForm.submit();
		});

		cancelSearch.on('click', function()
		{
			searchInfoForm.trigger('reset');
			window.location.href = "/admin/searches/";
		});
	}

	var initNewsButtons = function()
	{
		var newsInfoForm = $('#news_info_form');

		var saveNews = $('#news_info_save');
		var cancelNews = $('#news_info_cancel');

		saveNews.on('click', function()
		{
			newsInfoForm.submit();
		});

		cancelNews.on('click', function()
		{
			newsInfoForm.trigger('reset');
			window.location.href = "/admin/news/";
		});
	}

	var initTestimonialButtons = function()
	{
		var testimonialInfoForm = $('#testimonial_info_form');

		var saveTestimonial = $('#testimonial_info_save');
		var cancelTestimonial = $('#testimonial_info_cancel');

		var companySelect = $('#testimonial_info_company');
		var officeSelect = $('#testimonial_info_office');

		saveTestimonial.on('click', function()
		{
			testimonialInfoForm.submit();
		});

		cancelTestimonial.on('click', function()
		{
			testimonialInfoForm.trigger('reset');
			window.location.href = "/admin/testimonials/";
		});

	}

	var initCharts = function()
	{
		if(!jQuery.plot)
		{
			return;
		}

		function showChartTooltip(x, y, xValue, yValue)
		{
			$('<div id="tooltip" class="chart-tooltip">' + yValue + '<\/div>').css({
				position: 'absolute',
				display: 'none',
				top: y - 40,
				left: x - 40,
				border: '0px solid #ccc',
				padding: '2px 6px',
				'background-color': '#fff'
			}).appendTo("body").fadeIn(200);
		}

		var data = [];
		var totalPoints = 250;

		if($('#site_statistics').size() != 0)
		{

			$('#site_statistics_loading').hide();
			$('#site_statistics_content').show();

			var plot_statistics = $.plot($("#site_statistics"),
				[{
					data: site_visits,
					lines: {
						fill: 0.6,
						lineWidth: 0
					},
					color: ['#f89f9f']
				}, {
					data: site_visits,
					points: {
						show: true,
						fill: true,
						radius: 5,
						fillColor: "#f89f9f",
						lineWidth: 3
					},
					color: '#fff',
					shadowSize: 0
				}],

				{
					xaxis: {
						tickLength: 0,
						tickDecimals: 0,
						mode: "categories",
						min: 0,
						font: {
							lineHeight: 14,
							style: "normal",
							variant: "small-caps",
							color: "#6F7B8A"
						}
					},
					yaxis: {
						ticks: 5,
						tickDecimals: 0,
						tickColor: "#eee",
						font: {
							lineHeight: 14,
							style: "normal",
							variant: "small-caps",
							color: "#6F7B8A"
						}
					},
					grid: {
						hoverable: true,
						clickable: true,
						tickColor: "#eee",
						borderColor: "#eee",
						borderWidth: 1
					}
				});

			var previousPoint = null;
			$("#site_statistics").bind("plothover", function(event, pos, item)
			{
				$("#x").text(pos.x.toFixed(2));
				$("#y").text(pos.y.toFixed(2));
				if(item)
				{
					if(previousPoint != item.dataIndex)
					{
						previousPoint = item.dataIndex;

						$("#tooltip").remove();
						var x = item.datapoint[0].toFixed(2),
							 y = item.datapoint[1].toFixed(2);

						showChartTooltip(item.pageX, item.pageY, item.datapoint[0], item.datapoint[1] + ' visits');
					}
				}
				else
				{
					$("#tooltip").remove();
					previousPoint = null;
				}
			});
		}
	}

	var initYouTubeVideoGallery = function()
	{
		$('ul.youtube-video-gallery').youtubeVideoGallery({
			plugin: 'fancybox',
			thumbWidth: 225,
			assetFolder: '/includes/admin/plugins/youtube-video-gallery/',
			playButton: 'play-button-blue@40.png'
		});
	}

	var initJQVMAP = function()
	{
		var showMap = function(name)
		{
			jQuery('.vmaps').hide();
			jQuery('#vmap_' + name).show();
		}

		var setMap = function(name)
		{
			var data = {
				map: 'usa_en',
				backgroundColor: null,
				borderColor: '#333333',
				borderOpacity: 0.5,
				borderWidth: 1,
				color: '#c6c6c6',
				enableZoom: true,
				hoverColor: '#c9dfaf',
				hoverOpacity: null,
				values: site_locations,
				normalizeFunction: 'linear',
				scaleColors: ['#b6da93', '#909cae'],
				selectedColor: '',
				selectedRegion: null,
				showTooltip: true,
				onLabelShow: function(event, label, code)
				{
					label.text("Visits: " + site_locations[code]);
				}
			};

			data.map = name + '_en';
			var map = jQuery('#vmap_' + name);
			if(!map)
			{
				return;
			}
			map.width(map.parent().parent().width());
			map.show();
			map.vectorMap(data);
			map.hide();

		}

		if($('#vmap_usa').length > 0)
		{
			setMap("usa");
			showMap("usa");

			$('#region_statistics_loading').hide();
			$('#region_statistics_content').show();
		}
	}

	var initFancybox = function()
	{
		$('.fancybox').fancybox({
			fitToView: false,
			wrapCSS: 'fancyboxTemplates',
			parent: 'div.page-container'
		});
	}

	var initHtmlEditor = function()
	{
		if ($('.summernote').length > 0) {
			$('.summernote').each(function () {
				var $textArea = $(this);

				$textArea.summernote({
					onkeyup: function (e) {
						$textArea.val($(this).code());
						$textArea.change(); //To update any action binded on the control
					},
					onchange: function (e) {
						$textArea.val($(this).code());
						$textArea.change(); //To update any action binded on the control
					}
				});
			});
		}
	}

	// public functions

	return {
		//main function
		init: function()
		{
			handleSpinners();
			handleDatetimePicker();
			togglePortletCollapse();
			initHtmlEditor();

			if($('body').hasClass('admin-dashboard'))
			{
				//initCharts();
				//initJQVMAP();
				initFavoriteReportTable();
				initShowingReportTable();
				initContactReportTable();
			}

			if($('body').hasClass('admin-users'))
			{
				initUserListTable();
				initTeamListTable();
				initOfficeListTable();
				initCompanyListTable();
				initAclListTable();
				initUsersButtons();
				initTeamButtons();
				initOfficeButtons();
				initCompanyButtons();
				initAclButtons();
			}

			if($('body').hasClass('admin-vendors'))
			{
				initVendorListTable();
			}

			if($('body').hasClass('admin-integration'))
			{
				initIntegrationButtons();
			}
		}
	};

}();

