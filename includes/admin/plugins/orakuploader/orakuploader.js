(function($)
{
	$.fn.orakuploader = function(options)
	{
		var settings = $.extend({
			orakuploader: true,
			orakuploader_use_main: false,
			orakuploader_use_sortable: false,
			orakuploader_use_dragndrop: false,
			orakuploader_use_rotation: false,
			orakuploader_attach_images: [],
			orakuploader_path: 'orakuploader/',
			orakuploader_main_path: 'files',
			orakuploader_thumbnail_path: 'files/tn',
			orakuploader_resize_to: 0,
			orakuploader_thumbnail_size: 0,
			orakuploader_file_delete_label: "",
			orakuploader_file_rotation_label: "",
			orakuploader_field_name: $(this).attr('id'),
			orakuploader_add_image: '',
			orakuploader_add_label: 'Browser for images',
			orakuploader_main_changed: "",
			orakuploader_finished: "",
			orakuploader_picture_deleted: "",
			orakuploader_maximum_uploads: 100,
			orakuploader_max_exceeded: "",
			orakuploader_watermark: "",
			orakuploader_type: ""
		}, options);

		var holdername = this;

		if($(this).attr('orakuploader') == 'on')
		{
			var imageHolderId = '#' + $(this).attr('id');
			holdername =
				$(this).replaceWith(getHtml($(this).attr('id'), settings.orakuploader_add_image,
					settings.orakuploader_add_label, settings.orakuploader_path));
			holdername = $("body").find(imageHolderId);
		}

		jQuery.data(holdername, 'already_uploaded', 1);
		jQuery.data(holdername, 'count', 0);
		jQuery.data(holdername, 'counter', 0);

		if(settings.orakuploader_use_sortable)
		{
			$(holdername).sortable({
				update: function(event, ui)
				{
					if(settings.orakuploader_type == "communities")
					{
						var communityUrl = "/admin/communities/picture/sort/";
						var image_files = "";

						$('input[name^="' + (holdername).attr("id") + '"]').each(function()
						{
							image_files += $(this).val() + ",";
						});

						image_files = image_files.substr(0, image_files.length-1);

						$.ajax({
							url: communityUrl,
							type: "POST",
							data: {images: image_files}
						}).done(function()
						{
							console.log("Successfully sorted community images!");
						}).fail(function()
						{
							alert("Unable to sort community images at this time.");
						});
					}
					else
					{
						var ajaxUrl = "/admin/properties/picture/sort/";
						var image_files = "";

						$('input[name^="' + (holdername).attr("id") + '"]').each(function()
						{
							image_files += $(this).val() + ",";
						});

						image_files = image_files.substr(0, image_files.length-1);

						$.ajax({
							url: ajaxUrl,
							type: "POST",
							data: {images: image_files}
						}).done(function()
						{
							console.log("Successfully sorted property images!");
						}).fail(function()
						{
							alert("Unable to sort property images at this time.");
						});
					}

					if(typeof settings.orakuploader_rearranged == 'function')
					{
						settings.orakuploader_rearranged();
					}
					changeMain(holdername, settings);
				}
			});

			$("#" + holdername.attr("id") + "_to_clone").find(".file").css("cursor", "move");
			$("#" + holdername.attr("id") + "_to_clone").find(".multibox .file").css("cursor", "move");
		}
		else
		{
			$("#" + holdername.attr("id") + "_to_clone").find(".file").css("cursor", "auto");
			$("#" + holdername.attr("id") + "_to_clone").find(".multibox .file").css("cursor", "auto");
		}
		$(holdername).disableSelection();

		$(document).on("change", "." + $(holdername).attr("id") + "Input", function()
		{
			jQuery.data(holdername, 'count', 0);
			orakuploaderHandle(this.files, holdername, settings);
		});

		if(settings.orakuploader_type == "communities")
		{
			var communityID = $('#community_id').val();

			$.ajax({
				url: '/admin/communities/picture/list/' + communityID + '/'
			}).done(function(data)
			{
				var response = $.parseJSON(data);
				var status = response["status"];
				var imageStr = response["imageStr"];
				var current_images;

				if(status == "success")
				{
					console.log("Successfully found current community images.");

					current_images = imageStr.split(",");

					if(current_images.length > 0)
						settings.orakuploader_attach_images = current_images;

					for(i = 0; i < settings.orakuploader_attach_images.length; i++)
					{
						var image = settings.orakuploader_attach_images[i];

						var clone = $("#" + $(holdername).attr("id") + "_to_clone").find(".multibox").clone();
						$(holdername).append($(clone));
						$(clone).html("<div class='picture_delete'>" + settings.orakuploader_file_delete_label +
						"</div><img src='" + settings.orakuploader_thumbnail_path + "/" + image +
						"' alt='' onerror=this.src='" + settings.orakuploader_path +
						"/images/no-image.jpg' class='picture_uploaded'/> <input type='hidden' value='" + image +
						"' name='" + settings.orakuploader_field_name + "[]' />");
						$(clone).attr('id', image);
						$(clone).attr('filename', image);
					}

					if(settings.orakuploader_attach_images.length > 0)
					{
						changeMain(holdername, settings);
					}

				}
			}).fail(function()
			{
				alert("Unable to locate current community images at this time.");
			});

			$(holdername).on("click", ".picture_delete", function()
			{
				jQuery.data(holdername, "already_uploaded",
					jQuery.data(holdername, "already_uploaded") - 1);

				var formData = new FormData();
				var propID = $('#community_id').val();
				var ajaxUrl = '/admin/communities/picture/delete/' + communityID + '/';

				formData.append("image", encodeURIComponent($(this).parent().attr('filename')));
				formData.append("path", settings.orakuploader_path);
				formData.append("resize_to", settings.orakuploader_resize_to);
				formData.append("thumbnail_size", settings.orakuploader_thumbnail_size);
				formData.append("main_path", settings.orakuploader_main_path);
				formData.append("thumbnail_path", settings.orakuploader_thumbnail_path);
				formData.append("watermark", settings.orakuploader_watermark);

				$.ajax({
					url: ajaxUrl,
					type: "POST",
					data: formData,
					processData: false,
					contentType: false
				}).done(function(data)
				{
					var response = $.parseJSON(data);
					var status = response["status"];
					var image_id = response["id"];
					var responseText = response["filename"];

					if(status == "success")
					{
						console.log("Successfully deleted community image!");
					}
					else
					{
						alert("Unable to delete community image at this time.");
					}
				}).fail(function()
				{
					alert("Unable to delete community image at this time.");
				});

				$(this).parent().fadeOut("slow", function()
				{
					$(this).remove();

					if(jQuery.data(holdername, "already_uploaded") - 1 > 0)
					{
						changeMain(holdername, settings);
					}
				});

				if(typeof settings.orakuploader_picture_deleted == 'function')
				{
					settings.orakuploader_picture_deleted($(this).parent().attr('filename'));
				}
			});

			$(holdername).on("click", ".rotate_picture", function()
			{
				var context = this;

				$.ajax({
					url: settings.orakuploader_path + "orakuploader.php?rotate=" +
					encodeURIComponent($(this).parent().attr('filename')) + "&degree_lvl=" +
					$(this).closest('.rotate_picture').attr('degree-lvl') + "&path=" +
					settings.orakuploader_path + "&resize_to=" + settings.orakuploader_resize_to +
					"&thumbnail_size=" + settings.orakuploader_thumbnail_size + "&main_path=" +
					settings.orakuploader_main_path + "&thumbnail_path=" +
					settings.orakuploader_thumbnail_path
				}).done(function(file_name)
				{
					$img = $('html,body').find("input[value^='" + file_name + "']").prev('img');

					if(parseInt($(context).closest('.rotate_picture').attr('degree-lvl')) > 3)
					{
						$(context).closest('.rotate_picture').attr('degree-lvl', 1)
					}
					else
					{
						$(context).closest('.rotate_picture').attr('degree-lvl',
							parseInt($(context).closest('.rotate_picture').attr('degree-lvl')) + 1)
					}

					$img.attr('src', $img.attr('src') + "?" + new Date().getTime());
				});
			});
		}
		else
		{
			var propID = $('#property_id').val();

			$.ajax({
				url: '/admin/properties/picture/list/' + propID + '/'
			}).done(function(data)
			{
				var response = $.parseJSON(data);
				var status = response["status"];
				var imageStr = response["imageStr"];
				var current_images;

				if(status == "success")
				{
					console.log("Successfully found current property images.");

					current_images = imageStr.split(",");

					if(current_images.length > 0)
						settings.orakuploader_attach_images = current_images;

					for(i = 0; i < settings.orakuploader_attach_images.length; i++)
					{
						var image = settings.orakuploader_attach_images[i];

						var clone = $("#" + $(holdername).attr("id") + "_to_clone").find(".multibox").clone();
						$(holdername).append($(clone));
						$(clone).html("<div class='picture_delete'>" + settings.orakuploader_file_delete_label +
						"</div><img src='" + settings.orakuploader_thumbnail_path + "/" + image +
						"' alt='' onerror=this.src='" + settings.orakuploader_path +
						"/images/no-image.jpg' class='picture_uploaded'/> <input type='hidden' value='" + image +
						"' name='" + settings.orakuploader_field_name + "[]' />");
						$(clone).attr('id', image);
						$(clone).attr('filename', image);
					}

					if(settings.orakuploader_attach_images.length > 0)
					{
						changeMain(holdername, settings);
					}

				}
			}).fail(function()
			{
				alert("Unable to locate current property images at this time.");
			});

			$(holdername).on("click", ".picture_delete", function()
			{
				jQuery.data(holdername, "already_uploaded",
					jQuery.data(holdername, "already_uploaded") - 1);

				var formData = new FormData();
				var propID = $('#property_id').val();
				var ajaxUrl = '/admin/properties/picture/delete/' + propID + '/';

				formData.append("image", encodeURIComponent($(this).parent().attr('filename')));
				formData.append("path", settings.orakuploader_path);
				formData.append("resize_to", settings.orakuploader_resize_to);
				formData.append("thumbnail_size", settings.orakuploader_thumbnail_size);
				formData.append("main_path", settings.orakuploader_main_path);
				formData.append("thumbnail_path", settings.orakuploader_thumbnail_path);
				formData.append("watermark", settings.orakuploader_watermark);

				$.ajax({
					url: ajaxUrl,
					type: "POST",
					data: formData,
					processData: false,
					contentType: false
				}).done(function(data)
				{
					var response = $.parseJSON(data);
					var status = response["status"];
					var image_id = response["id"];
					var responseText = response["filename"];

					if(status == "success")
					{
						console.log("Successfully deleted property image!");
					}
					else
					{
						alert("Unable to delete property image at this time.");
					}
				}).fail(function()
				{
					alert("Unable to delete property image at this time.");
				});

				$(this).parent().fadeOut("slow", function()
				{
					$(this).remove();

					if(jQuery.data(holdername, "already_uploaded") - 1 > 0)
					{
						changeMain(holdername, settings);
					}
				});

				if(typeof settings.orakuploader_picture_deleted == 'function')
				{
					settings.orakuploader_picture_deleted($(this).parent().attr('filename'));
				}
			});

			$(holdername).on("click", ".rotate_picture", function()
			{
				var context = this;

				$.ajax({
					url: settings.orakuploader_path + "orakuploader.php?rotate=" +
					encodeURIComponent($(this).parent().attr('filename')) + "&degree_lvl=" +
					$(this).closest('.rotate_picture').attr('degree-lvl') + "&path=" +
					settings.orakuploader_path + "&resize_to=" + settings.orakuploader_resize_to +
					"&thumbnail_size=" + settings.orakuploader_thumbnail_size + "&main_path=" +
					settings.orakuploader_main_path + "&thumbnail_path=" +
					settings.orakuploader_thumbnail_path
				}).done(function(file_name)
				{
					$img = $('html,body').find("input[value^='" + file_name + "']").prev('img');

					if(parseInt($(context).closest('.rotate_picture').attr('degree-lvl')) > 3)
					{
						$(context).closest('.rotate_picture').attr('degree-lvl', 1)
					}
					else
					{
						$(context).closest('.rotate_picture').attr('degree-lvl',
							parseInt($(context).closest('.rotate_picture').attr('degree-lvl')) + 1)
					}

					$img.attr('src', $img.attr('src') + "?" + new Date().getTime());
				});
			});
		}

		if(settings.orakuploader_use_dragndrop)
		{
			var holder = document.getElementById($(holdername).attr("id") + "DDArea");
			holder.ondragover = function()
			{
				$(".uploadButton").addClass("DragAndDropHover");
				return false;
			};
			holder.ondragend = function()
			{
				$(".uploadButton").removeClass("DragAndDropHover");
				return false;
			};

			holder.ondrop = function(e)
			{
				$(".uploadButton").removeClass("DragAndDropHover");
				e.preventDefault();
				orakuploaderHandle(e.dataTransfer.files, holdername, settings);
			}
		}

	};

	function changeMain(holder, settings)
	{
		if(settings.orakuploader_use_main)
		{
			$(holder).find(".multibox").removeClass("main");
			$(holder).find(".multibox").first().addClass("main");

			if(typeof settings.orakuploader_main_changed == 'function')
			{
				settings.orakuploader_main_changed($(holder).find(".multibox").first().attr('filename'));
			}
		}
	}

	function orakuploaderHandle(files, holder, settings)
	{
		var i = 0;
		var msg_alert = false;
		for(i = 0; i < files.length; i++)
		{
			if(jQuery.data(holder, "already_uploaded") > settings.orakuploader_maximum_uploads &&
				(typeof settings.orakuploader_max_exceeded == 'function'))
			{
				if(msg_alert == false) settings.orakuploader_max_exceeded();
				msg_alert = true;
			}
			var re = /(?:\.([^.]+))?$/;
			var ext = re.exec(files[i].name)[1].toLowerCase();

			if((ext == 'jpg' || ext == 'jpeg' || ext == 'png') &&
				jQuery.data(holder, "already_uploaded") <= settings.orakuploader_maximum_uploads)
			{
				var clone = $("#" + $(holder).attr("id") + "_to_clone").find(".multibox").clone();

				$(holder).append($(clone));
				upload(files[i], clone, i, holder, settings);
				jQuery.data(holder, "already_uploaded", jQuery.data(holder, "already_uploaded") + 1);
				jQuery.data(holder, "count", jQuery.data(holder, "count") + 1);
			}
		}
	}

	window.counter = 0;
	function upload(file, clone, place, holder, settings)
	{
		var formData = new FormData();
		var propID = $('#property_id').val();
		var ajaxUrl = '';

		if(settings.orakuploader_type == "communities")
		{
			var communityID = $('#community_id').val();

			ajaxUrl = '/admin/communities/picture/upload/' + communityID + '/';
		}
		else
		{
			var propID = $('#property_id').val();

			ajaxUrl = '/admin/properties/picture/upload/' + propID + '/';
		}

		formData.append("image", file);
		formData.append("path", settings.orakuploader_path);
		formData.append("resize_to", settings.orakuploader_resize_to);
		formData.append("thumbnail_size", settings.orakuploader_thumbnail_size);
		formData.append("main_path", settings.orakuploader_main_path);
		formData.append("thumbnail_path", settings.orakuploader_thumbnail_path);
		formData.append("watermark", settings.orakuploader_watermark);

		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data: formData,
			processData: false,
			contentType: false
		}).done(function(data)
		{
			var response = $.parseJSON(data);
			var status = response["status"];
			var image_id = response["id"];
			var responseText = response["filename"];

			if(status == "success")
			{
				console.log("Successfully uploaded property image!");
			}
			else
			{
				alert("Unable to upload property image at this time.");
			}

			var rotation_html = '';
			if(settings.orakuploader_use_rotation == true)
			{
				rotation_html =
					"<div class='rotate_picture' degree-lvl='1'>" +
					settings.orakuploader_file_rotation_label + "</div>";
			}

			var imageHtml = "<div class='picture_delete'>" + settings.orakuploader_file_delete_label +
				"</div>" + rotation_html + "<img src='" + settings.orakuploader_thumbnail_path + "/" +
				responseText + "' alt='' onerror=this.src='" + settings.orakuploader_path +
				"/images/no-image.jpg' class='picture_uploaded'/> <input type='hidden' value='" +
				responseText + "' name='" + settings.orakuploader_field_name + "[]' />";

			$(clone).html(imageHtml);
			$(clone).attr('id', responseText);
			$(clone).attr('filename', responseText);
			jQuery.data(holder, "counter", jQuery.data(holder, "counter") + 1);

			if(jQuery.data(holder, "count") == jQuery.data(holder, "counter"))
			{
				if(typeof settings.orakuploader_finished == 'function')
				{
					settings.orakuploader_finished();
				}
				changeMain(holder, settings);
				jQuery.data(holder, "counter", 0);
			}
		}).fail(function()
		{
			alert("Unable to upload property image at this time.");
		});
	}

}(jQuery));

window.initialized = 0;

function orakuploaderLoad(name)
{
	$('.' + (name) + 'Input').click();
	window.initialized++;
}

function getHtml(name, add_image, add_label, path)
{
	return '<div id="' + name +
		'_to_clone" class="clone_item"><div class="multibox file"><div class="loading"><img src="' +
		path + '/images/loader.gif" alt="loader"/></div></div></div>\
		<div id="' + name + 'DDArea">\
			<div id="' + name + '">\
			</div>\
			<div class="multibox uploadButton" onclick="javascript:orakuploaderLoad(\'' + name + '\');">\
			<img src="' + add_image + '"/>\
			<br/><br/>' + add_label + '\
			</div>\
			<input type="file" class="' + name + 'Input orakuploaderFileInput" accept="image/*" multiple/>\
			<div class="clear"> </div>\
		</div>';

}
