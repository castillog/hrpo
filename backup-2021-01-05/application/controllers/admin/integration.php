<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Integration
 */
class Integration extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('Activity');
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_access('admin-integration');

		if(empty($site_id) && !$access_admin)
			header("Location: /admin/login/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		$data = array();

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		// If the user can view this, but only do that, we'll hide our add/edit/delete buttons
		// and set any visiable form fields to readonly
		$access_update       = $this->Accounts->can_user_update('admin-integration');
		$access_update_micro = $this->Accounts->can_user_update('admin-integration-micro');

		$access_create       = $this->Accounts->can_user_create('admin-integration');
		$access_create_micro = $this->Accounts->can_user_create('admin-integration-micro');

		$access_delete       = $this->Accounts->can_user_delete('admin-integration');
		$access_delete_micro = $this->Accounts->can_user_delete('admin-integration-micro');

		if(!empty($site_id))
		{
			$access_update = $access_update_micro;
			$access_create = $access_create_micro;
			$access_delete = $access_delete_micro;
		}

		$data['readonly']             = (!$access_update) ? 'disabled' : '';

		//------------------------
		//  INTEGRATION - WIDGET
		//------------------------
		$site_domain     = (!empty($site['domain'])) ? $site['domain'] : "";
		$site_subdomain  = (!empty($site['subdomain'])) ? $site['subdomain'] : "";

		$site_url = (!empty($site_domain)) ? "http://" . $site_domain : "";
		$site_url = (empty($site_url) && !empty($site_subdomain)) ? "http://" . $site_subdomain . ".comingsoonhomes.com" : "http://comingsoonhomes.com";

		$widget_embed_code         =
			"<iframe seamless name='comingsoonhomes.com' width='100%;' height='350' scrolling='no' marginwidth='0' marginheight='0' src='{$site_url}/widget' frameborder='0'></iframe>";
		$data['widget_embed_code'] = htmlentities($widget_embed_code);

		//------------------------
		//  INTEGRATION - BOMBBOMB
		//------------------------
		$data['bombbomb_save_button']       = ($access_update) ? '<button id="bombbomb_save" class="btn grey-gallery" type="submit">Save</button>' : '';
		$data['bombbomb_flash_text']        = '';
		$data['bombbomb_flash_class']       = '';
		$data['bombbomb_api_key']           = '';
		$data['bombbomb_api_verified']      = 'verify-warning';
		$data['bombbomb_api_verified_note'] = 'API Key Not Verified';
		$data['bombbomb_lists']             = '<option>Enter API Key</option>';

		$postback = $this->input->post("bombbomb_form_submit");

		if($postback)
		{
			if($user)
			{
				$bombbomb_api_key = $this->input->post("bombbomb_api_key");
				$bombbomb_api_key = (!empty($bombbomb_api_key)) ? $bombbomb_api_key : "";
				$bombbomb_list_id = $this->input->post("bombbomb_list_id");
				$bombbomb_list_id = (!empty($bombbomb_list_id)) ? $bombbomb_list_id : "";

				// Update integration - bombbomb
				$update_data['bombbomb_api_key'] = $bombbomb_api_key;
				$update_data['bombbomb_list_id'] = $bombbomb_list_id;
				$update_results = $this->Accounts->update_user($update_data, $user_id);

				if($update_results)
					log_message('debug', "Successfully updated integration - bombbomb.");
				else
					log_message('error', "Failed to update integration - bombbomb!");

				$data['bombbomb_flash_text']  = 'Successfully saved settings.';
				$data['bombbomb_flash_class'] = 'alert alert-success';

				$data['bombbomb_api_key'] = $bombbomb_api_key;

				// Grab the account's current contact lists
				$this->load->library("Bombbomb", array('api_key' => $bombbomb_api_key));
				$bombbomb_lists = $this->bombbomb->get_lists();

				if(!empty($bombbomb_lists) && is_array($bombbomb_lists))
				{
					$data['bombbomb_lists']             = "<option value=''>None</option>";
					$data['bombbomb_api_verified']      = 'verify-success';
					$data['bombbomb_api_verified_note'] = 'API Key Verified';

					foreach($bombbomb_lists as $list)
					{
						$list_id   = $list['id'];
						$list_name = $list['name'] . " ({$list['ContactCount']} Contacts)";

						if(!empty($bombbomb_list_id) && $bombbomb_list_id == $list_id)
							$data['bombbomb_lists'] .= "<option value='{$list_id}' selected>{$list_name}</option>";
						else
							$data['bombbomb_lists'] .= "<option value='{$list_id}'>{$list_name}</option>";
					}
				}
				else
					$data['bombbomb_api_verified_note'] = 'API Key Failed Verification!';
			}
			else
			{
				$data['bombbomb_flash_text']  = 'Internal Server Error!';
				$data['bombbomb_flash_class'] = 'alert alert-warning';
			}

		}
		else
		{
			if($user)
			{
				$bombbomb_api_key = (!empty($user['bombbomb_api_key'])) ? $user['bombbomb_api_key'] : "";
				$bombbomb_list_id = (!empty($user['bombbomb_list_id'])) ? $user['bombbomb_list_id'] : "";

				if(!empty($bombbomb_api_key))
				{
					$data['bombbomb_api_key'] = $bombbomb_api_key;

					// Verify the API Key is valid by trying to grab the account's current contact lists
					$this->load->library("Bombbomb", array('api_key' => $bombbomb_api_key));
					$bombbomb_lists = $this->bombbomb->get_lists();

					if(!empty($bombbomb_lists) && is_array($bombbomb_lists))
					{
						$data['bombbomb_lists']             = "<option value=''>None</option>";
						$data['bombbomb_api_verified']      = 'verify-success';
						$data['bombbomb_api_verified_note'] = 'API Key Verified';

						foreach($bombbomb_lists as $list)
						{
							$list_id   = $list['id'];
							$list_name = $list['name'] . " ({$list['ContactCount']} Contacts)";

							if(!empty($bombbomb_list_id) && $bombbomb_list_id == $list_id)
								$data['bombbomb_lists'] .= "<option value='{$list_id}' selected>{$list_name}</option>";
							else
								$data['bombbomb_lists'] .= "<option value='{$list_id}'>{$list_name}</option>";
						}
					}
					else
						$data['bombbomb_api_verified_note'] = 'API Key Failed Verification!';
				}
			}
		}

		//------------------------
		//  INTEGRATION - REW
		//------------------------
		$data['rew_save_button']       = ($access_update) ? '<button id="rew_save" class="btn green-lilahmedia" type="submit">Save</button>' : '';
		$data['rew_flash_text']        = '';
		$data['rew_flash_class']       = '';
		$data['rew_domain']            = '';
		$data['rew_api_key']           = '';
		$data['rew_api_verified']      = 'verify-warning';
		$data['rew_api_verified_note'] = 'API Key Not Verified';
		$data['rew_lists']             = '<option>Enter API Key</option>';

		$postback = $this->input->post("rew_form_submit");

		if($postback)
		{
			if($user)
			{
				$rew_domain  = $this->input->post("rew_domain");
				$rew_domain  = (!empty($rew_domain)) ? $rew_domain : "";
				$rew_api_key = $this->input->post("rew_api_key");
				$rew_api_key = (!empty($rew_api_key)) ? $rew_api_key : "";
				$rew_list_id = $this->input->post("rew_list_id");
				$rew_list_id = (!empty($rew_list_id)) ? $rew_list_id : "";

				// Update integration - rew
				$update_data['rew_domain']  = $rew_domain;
				$update_data['rew_api_key'] = $rew_api_key;
				$update_data['rew_list_id'] = $rew_list_id;
				$update_results = $this->Accounts->update_user($update_data, $user_id);

				if($update_results)
					log_message('debug', "Successfully updated integration - rew.");
				else
					log_message('error', "Failed to update integration - rew!");

				$data['rew_flash_text']  = 'Successfully saved settings.';
				$data['rew_flash_class'] = 'alert alert-success';
				$data['rew_domain']      = $rew_domain;
				$data['rew_api_key']     = $rew_api_key;

				// Grab the account's current contact lists
				$this->load->library("REW", array('domain' => $rew_domain, 'api_key' => $rew_api_key));
				$rew_lists = $this->rew->get_all_groups();

				if(!empty($rew_lists) && is_array($rew_lists))
				{
					$data['rew_lists']             = "<option value=''>None</option>";
					$data['rew_api_verified']      = 'verify-success';
					$data['rew_api_verified_note'] = 'API Key Verified';

					foreach($rew_lists as $list)
					{
						$list_id   = $list['id'];
						$list_name = $list['name'];

						if(!empty($rew_list_id) && $rew_list_id == $list_id)
							$data['rew_lists'] .= "<option value='{$list_id}' selected>{$list_name}</option>";
						else
							$data['rew_lists'] .= "<option value='{$list_id}'>{$list_name}</option>";
					}
				}
				else
					$data['rew_api_verified_note'] = 'API Key Failed Verification!';
			}
			else
			{
				$data['rew_flash_text']  = 'Internal Server Error!';
				$data['rew_flash_class'] = 'alert alert-warning';
			}

		}
		else
		{
			if($user)
			{
				$rew_domain  = (!empty($user['rew_domain'])) ? $user['rew_domain'] : "";
				$rew_api_key = (!empty($user['rew_api_key'])) ? $user['rew_api_key'] : "";
				$rew_list_id = (!empty($user['rew_list_id'])) ? $user['rew_list_id'] : "";

				if(!empty($rew_domain) && !empty($rew_api_key))
				{
					$data['rew_domain']  = $rew_domain;
					$data['rew_api_key'] = $rew_api_key;

					// Verify the API Key is valid by trying to grab the account's current contact lists
					$this->load->library("REW", array('domain' => $rew_domain, 'api_key' => $rew_api_key));
					$rew_lists = $this->rew->get_all_groups();

					if(!empty($rew_lists) && is_array($rew_lists))
					{
						$data['rew_lists']             = "<option value=''>None</option>";
						$data['rew_api_verified']      = 'verify-success';
						$data['rew_api_verified_note'] = 'API Key Verified';

						foreach($rew_lists as $list)
						{
							$list_id   = $list['id'];
							$list_name = $list['name'];

							if(!empty($rew_list_id) && $rew_list_id == $list_id)
								$data['rew_lists'] .= "<option value='{$list_id}' selected>{$list_name}</option>";
							else
								$data['rew_lists'] .= "<option value='{$list_id}'>{$list_name}</option>";
						}
					}
					else
						$data['rew_api_verified_note'] = 'API Key Failed Verification!';
				}
			}
		}

		//------------------------
		//  INTEGRATION - TP
		//------------------------
		$data['tp_save_button']       = ($access_update) ? '<button id="tp_save" class="btn grey-gallery" type="submit">Save</button>' : '';
		$data['tp_flash_text']        = '';
		$data['tp_flash_class']       = '';
		$data['tp_lead_email']        = '';

		$postback = $this->input->post("tp_form_submit");

		if($postback)
		{
			if($user)
			{
				$tp_lead_email  = $this->input->post("tp_lead_email");
				$tp_lead_email  = (!empty($tp_lead_email)) ? $tp_lead_email : "";

				// Update integration - tp
				$update_data['tp_lead_email']  = $tp_lead_email;
				$update_results = $this->Accounts->update_user($update_data, $user_id);

				if($update_results)
					log_message('debug', "Successfully updated integration - tp.");
				else
					log_message('error', "Failed to update integration - tp!");

				$data['tp_flash_text']  = 'Successfully saved settings.';
				$data['tp_flash_class'] = 'alert alert-success';
				$data['tp_lead_email']  = $tp_lead_email;
			}
			else
			{
				$data['tp_flash_text']  = 'Internal Server Error!';
				$data['tp_flash_class'] = 'alert alert-warning';
			}

		}
		else
		{
			if($user)
			{
				$data['tp_lead_email']  = (!empty($user['tp_lead_email'])) ? $user['tp_lead_email'] : "";
			}
		}

		//------------------------
		//  INTEGRATION - BOOMTOWN
		//------------------------
		$data['boomtown_save_button']       = ($access_update) ? '<button id="boomtown_save" class="btn green-lilahmedia" type="submit">Save</button>' : '';
		$data['boomtown_flash_text']        = '';
		$data['boomtown_flash_class']       = '';
		$data['boomtown_lead_email']        = '';

		$postback = $this->input->post("boomtown_form_submit");

		if($postback)
		{
			if($user)
			{
				$boomtown_lead_email  = $this->input->post("boomtown_lead_email");
				$boomtown_lead_email  = (!empty($boomtown_lead_email)) ? $boomtown_lead_email : "";

				// Update integration - boomtown
				$update_data['boomtown_lead_email']  = $boomtown_lead_email;
				$update_results = $this->Accounts->update_user($update_data, $user_id);

				if($update_results)
					log_message('debug', "Successfully updated integration - boomtown.");
				else
					log_message('error', "Failed to update integration - boomtown!");

				$data['boomtown_flash_text']  = 'Successfully saved settings.';
				$data['boomtown_flash_class'] = 'alert alert-success';
				$data['boomtown_lead_email']  = $boomtown_lead_email;
			}
			else
			{
				$data['boomtown_flash_text']  = 'Internal Server Error!';
				$data['boomtown_flash_class'] = 'alert alert-warning';
			}

		}
		else
		{
			if($user)
			{
				$data['boomtown_lead_email']  = (!empty($user['boomtown_lead_email'])) ? $user['boomtown_lead_email'] : "";
			}
		}
		                                                                                                                                                                                                                                              // Set leads as the active navbar link
		$nav_active['integration'] = 'active';

		$data_nav    = array(
			'active' => $nav_active
		);
		$data_header = array(
			'title'         => 'Lonnie Bush Property Management | Admin - Integration',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-integration',
			'user_name'     => $access_name
		);
		$data['role'] = $role;
		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/integration", $data);
		$this->load->view("admin/footer", $data_footer);
	}

	public function bombbomb()
	{
		if($this->input->is_ajax_request())
		{
			$bombbomb_api_key = $this->input->post("bombbomb_api_key");

			if(!empty($bombbomb_api_key))
			{
				// Verify the API Key is valid by trying to grab the account's current contact lists
				$this->load->library("Bombbomb", array('api_key' => $bombbomb_api_key));
				$bombbomb_lists = $this->bombbomb->get_lists();

				if(!empty($bombbomb_lists) && is_array($bombbomb_lists))
				{
					$bombbomb_lists_html = "<option value=''>None</option>";

					foreach($bombbomb_lists as $list)
					{
						$list_id   = $list['id'];
						$list_name = $list['name'] . " ({$list['ContactCount']} Contacts)";

						$bombbomb_lists_html .= "<option value='{$list_id}'>{$list_name}</option>";
					}

					$package = array(
						"status"  => "success",
						"message" => "Successfully retrieved contact lists!",
						"lists"   => $bombbomb_lists_html
					);
					echo json_encode($package);
					exit;
				}
				else
				{
					$package = array(
						"status"  => "error",
						"message" => "Failed! API key failed verification!"
					);
					echo json_encode($package);
					exit;
				}

			}
			else
			{
				$package = array(
					"status"  => "error",
					"message" => "Failed! No API key provided!"
				);
				echo json_encode($package);
				exit;
			}
		}
	}

	public function rew()
	{
		if($this->input->is_ajax_request())
		{
			$rew_domain  = $this->input->post("rew_domain");
			$rew_api_key = $this->input->post("rew_api_key");

			if(!empty($rew_domain) && !empty($rew_api_key))
			{
				// Verify the API Key is valid by trying to grab the account's current contact lists
				$this->load->library("REW", array('domain' => $rew_domain, 'api_key' => $rew_api_key));
				$rew_lists = $this->rew->get_all_groups();

				if(!empty($rew_lists) && is_array($rew_lists))
				{
					$rew_lists_html = "<option value=''>None</option>";

					foreach($rew_lists as $list)
					{
						$list_id   = $list['id'];
						$list_name = $list['name'];

						if(!empty($rew_list_id) && $rew_list_id == $list_id)
							$rew_lists_html .= "<option value='{$list_id}' selected>{$list_name}</option>";
						else
							$rew_lists_html .= "<option value='{$list_id}'>{$list_name}</option>";
					}

					$package = array(
						"status"  => "success",
						"message" => "Successfully retrieved contact lists!",
						"lists"   => $rew_lists_html
					);
					echo json_encode($package);
					exit;
				}
				else
				{
					$package = array(
						"status"  => "error",
						"message" => "Failed! API key failed verification!"
					);
					echo json_encode($package);
					exit;
				}

			}
			else
			{
				$package = array(
					"status"  => "error",
					"message" => "Failed! No API key provided!"
				);
				echo json_encode($package);
				exit;
			}
		}
	}

}

/* End of file integration.php */
/* Location: ./application/controllers/admin/integration.php */

//ob_start();
//var_dump($feed);
//$test = ob_get_clean();
//log_message('error', $test);
