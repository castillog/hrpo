<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Users
 */
class Users extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->helper('file');
		$this->load->library('Activity');
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_access('admin-users');

		if(empty($site_id) && !$access_admin)
			header("Location: /admin/login/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// If the user can view this, but only do that, we'll hide our add/edit/delete buttons
		// and set any visiable form fields to readonly
		$access_update       = $this->Accounts->can_user_update('admin-users');
		$access_create       = $this->Accounts->can_user_create('admin-users');
		$access_delete       = $this->Accounts->can_user_delete('admin-users');

		$data['readonly'] = (!$access_update) ? 'disabled' : '';

		$data = array();

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		$organization = array();

		if($role == 'agent')
		{
			$organization              = $this->Accounts->get_organization_by_user($user_id);
			$organization['teams']     = array();
			$organization['offices']   = array();
			$organization['companies'] = array();
		}
		else if($role == 'team leader')
		{
			$organization              = $this->Accounts->get_organization_by_team($team_id);
			$organization['teams']     = array();
			$organization['offices']   = array();
			$organization['companies'] = array();
		}
		else if($role == 'office manager')
		{
			$organization              = $this->Accounts->get_organization_by_office($office_id);
			$organization['offices']   = array();
			$organization['companies'] = array();
		}
		else if($role == 'company owner')
		{
			$organization = $this->Accounts->get_organization($company_id);
		}
		else if($role == 'admin')
		{
			$organization = $this->Accounts->get_organization($company_id);
		}
		else if($role == 'master')
		{
			$organization = $this->Accounts->get_organization_all();
		}

		//ob_start();
		//var_dump($organization);
		//$test = ob_get_clean();
		//log_message('error', $test);

		//-------------
		//  USERS LIST
		//-------------

		// Set all our view form variables to empty strings
		$data['user_enabled']             = '';
		$data['user_featured']            = '';
		$data['user_role_client']         = '';
		$data['user_role_agent']          = '';
		$data['user_role_team_leader']    = '';
		$data['user_role_office_manager'] = '';
		$data['user_role_company_owner']  = '';
		$data['user_role_admin']          = '';
		$data['user_role_master']         = '';
		$data['user_username']            = '';
		$data['user_password']            = '';
		$data['user_first_name']          = '';
		$data['user_last_name']           = '';
		$data['user_email']               = '';
		$data['user_phone']               = '';
		$data['user_mobile']              = '';
		$data['user_street']              = '';
		$data['user_city']                = '';
		$data['user_state']               = '';
		$data['user_zipcode']             = '';
		$data['user_license']             = '';
		$data['user_title']               = '';
		$data['user_image']               = '';
		$data['user_bio']                 = '';
		$data['user_companies']           = '';
		$data['user_offices']             = '';
		$data['user_teams']               = '';
		$data['user_lenders']             = '';

		$data['team_enabled']   = '';
		$data['team_name']      = '';
		$data['team_leader']    = '';
		$data['team_email']     = '';
		$data['team_phone']     = '';
		$data['team_mobile']    = '';
		$data['team_street']    = '';
		$data['team_city']      = '';
		$data['team_state']     = '';
		$data['team_zipcode']   = '';
		$data['team_companies'] = '';
		$data['team_offices']   = '';
		$data['team_lenders']   = '';

		$data['office_enabled']   = '';
		$data['office_name']      = '';
		$data['office_manager']   = '';
		$data['office_email']     = '';
		$data['office_phone']     = '';
		$data['office_mobile']    = '';
		$data['office_street']    = '';
		$data['office_city']      = '';
		$data['office_state']     = '';
		$data['office_zipcode']   = '';
		$data['office_companies'] = '';

		$data['company_enabled'] = '';
		$data['company_name']    = '';
		$data['company_owner']   = '';
		$data['company_email']   = '';
		$data['company_phone']   = '';
		$data['company_mobile']  = '';
		$data['company_street']  = '';
		$data['company_city']    = '';
		$data['company_state']   = '';
		$data['company_zipcode'] = '';

		$data['access_enabled']                = '';
		$data['access_type_group']             = '';
		$data['access_type_user']              = '';
		$data['access_role_all']               = '';
		$data['access_role_admin']             = '';
		$data['access_role_client']            = '';
		$data['access_role_agent']             = '';
		$data['access_role_team']              = '';
		$data['access_role_office']            = '';
		$data['access_role_company']           = '';
		$data['access_level_all']              = '';
		$data['access_level_agent']            = '';
		$data['access_level_team']             = '';
		$data['access_level_office']           = '';
		$data['access_level_company']          = '';
		$data['access_page_all']               = '';
		$data['access_page_public']            = '';
		$data['access_page_admin']             = '';
		$data['access_page_admin_dashboard']   = '';
		$data['access_page_admin_pages']       = '';
		$data['access_page_admin_properties']  = '';
		$data['access_page_admin_users']       = '';
		$data['access_page_admin_leads']       = '';
		$data['access_page_admin_communities'] = '';
		$data['access_page_admin_settings']    = '';
		$data['access_section_all']            = '';
		$data['access_read']                   = '';
		$data['access_create']                 = '';
		$data['access_update']                 = '';
		$data['access_delete']                 = '';

		$data['users']             = '';
		$data['user_list_hide']    = '';
		$data['team_list_hide']    = '';
		$data['office_list_hide']  = '';
		$data['company_list_hide'] = '';

		// Set our flash notice to default empty value
		$data['flash_text']  = '';
		$data['flash_class'] = '';

		$data['users_add_button'] = ($access_create) ? '<a id="user_add" class="btn btn-default btn-sm " href="/admin/users/add/user/" style="margin-right: 5px;"><i
					class="fa fa-plus"></i> Add User </a>' : '';

		if(!empty($organization['users']))
		{
			$users = $organization['users'];

			for($i = 0; $i < count($users); $i++)
			{
				$account             = $users[$i];
				$enabled             = ($account['enabled']) ? 'Yes' : 'No';
				$users_edit_button   = ($access_update) ? "<a class='btn default btn-xs green edit'
							href='/admin/users/update/user/{$account['user_id']}/'>
							<i class='fa fa-edit'></i> Edit </a>" : "";
				$users_delete_button = ($access_delete) ? "<a class='btn default btn-xs red delete'
							href='/admin/users/delete/user/{$account['user_id']}/'>
							<i class='fa fa-trash-o'></i> Delete </a>" : "";

				$data['users'] .= "<tr class='odd gradeX'>
					<td>{$account['first_name']}</td>
					<td>{$account['last_name']}</td>
					<td>{$account['username']}</td>
					<td>{$account['role']}</td>
					<td>{$enabled}</td>
					<td>
						{$users_edit_button}
						{$users_delete_button}
					</td>
				</tr>\n";
			}

			$data['user_list_hide'] = '';
		}
		else
		{
			if($role != "master" && $role != "admin" && $role != "company owner" && $role != "office manager" && $role != "team leader")
				$data['user_list_hide'] = 'display-hide';
		}

		//------------
		//  TEAM LIST
		//------------
		$data['teams'] = '';

		$data['teams_add_button'] = ($access_create) ? '<a id="team_add" class="btn btn-default btn-sm " href="/admin/users/add/team/" style="margin-right: 5px;"><i
					class="fa fa-plus"></i> Add Team </a>' : '';

		if(!empty($organization['teams']))
		{
			$teams = $organization['teams'];

			for($i = 0; $i < count($teams); $i++)
			{
				$team                = $teams[$i];
				$enabled             = ($team['enabled']) ? 'Yes' : 'No';
				$teams_edit_button   = ($access_update) ? "<a class='btn default btn-xs green edit'
							href='/admin/users/update/team/{$team['team_id']}/'>
							<i class='fa fa-edit'></i> Edit </a>" : "";
				$teams_delete_button = ($access_delete) ? "<a class='btn default btn-xs red delete'
							href='/admin/users/delete/team/{$team['team_id']}/'>
							<i class='fa fa-trash-o'></i> Delete </a>" : "";

				$data['teams'] .= "<tr class='odd gradeX'>
					<td>{$team['team_name']}</td>
					<td>{$team['team_leader']}</td>
					<td>{$team['email']}</td>
					<td>{$enabled}</td>
					<td>
						{$teams_edit_button}
						{$teams_delete_button}
					</td>
				</tr>\n";
			}

			$data['team_list_hide'] = '';
		}
		else
		{
			if($role != "master" && $role != "admin" && $role != "company owner" && $role != "office manager")
				$data['team_list_hide'] = 'display-hide';
		}

		//---------------
		//  OFFICES LIST
		//---------------
		$data['offices'] = '';

		$data['offices_add_button'] = ($access_create) ? '<a id="office_add" class="btn btn-default btn-sm " href="/admin/users/add/office/" style="margin-right: 5px;">
				<i class="fa fa-plus"></i> Add Office </a>' : '';

		if(!empty($organization['offices']))
		{
			$offices = $organization['offices'];

			for($i = 0; $i < count($offices); $i++)
			{
				$office                = $offices[$i];
				$enabled               = ($office['enabled']) ? 'Yes' : 'No';
				$offices_edit_button   = ($access_update) ? "<a class='btn default btn-xs green edit'
							href='/admin/users/update/office/{$office['office_id']}/'>
							<i class='fa fa-edit'></i> Edit </a>" : "";
				$offices_delete_button = ($access_delete) ? "<a class='btn default btn-xs red delete'
							href='/admin/users/delete/office/{$office['office_id']}/'>
							<i class='fa fa-trash-o'></i> Delete </a>" : "";

				$data['offices'] .= "<tr class='odd gradeX'>
					<td>{$office['office_name']}</td>
					<td>{$office['office_manager']}</td>
					<td>{$office['email']}</td>
					<td>{$enabled}</td>
					<td>
						{$offices_edit_button}
						{$offices_delete_button}
					</td>
				</tr>\n";
			}

			$data['office_list_hide'] = '';
		}
		else
		{
			if($role != "master" && $role != "admin" && $role != "company owner")
				$data['office_list_hide'] = 'display-hide';
		}

		//-----------------
		//  COMPANIES LIST
		//-----------------
		$data['companies'] = '';

		$data['companies_add_button'] = ($access_create) ? '<a id="company_add" class="btn btn-default btn-sm " href="/admin/users/add/company/" style="margin-right: 5px;">
				<i class="fa fa-plus"></i> Add Company </a>' : '';

		if(!empty($organization['companies']))
		{
			$companies = $organization['companies'];

			for($i = 0; $i < count($companies); $i++)
			{
				$company                 = $companies[$i];
				$enabled                 = ($company['enabled']) ? 'Yes' : 'No';
				$companies_edit_button   = ($access_update) ? "<a class='btn default btn-xs green edit'
							href='/admin/users/update/company/{$company['company_id']}/'>
							<i class='fa fa-edit'></i> Edit </a>" : "";
				$companies_delete_button = ($access_delete) ? "<a class='btn default btn-xs red delete'
							href='/admin/users/delete/company/{$company['company_id']}/'>
							<i class='fa fa-trash-o'></i> Delete </a>" : "";

				$data['companies'] .= "<tr class='odd gradeX'>
					<td>{$company['company_name']}</td>
					<td>{$company['company_owner']}</td>
					<td>{$company['email']}</td>
					<td>{$enabled}</td>
					<td>
						{$companies_edit_button}
						{$companies_delete_button}
					</td>
				</tr>\n";
			}

			$data['company_list_hide'] = '';
		}
		else
		{
			if($role != "master" && $role != "admin")
				$data['company_list_hide'] = 'display-hide';
		}

		//--------------------
		//  ACCESS RIGHT LIST
		//--------------------
		$data['access'] = '';
		$rights         = $this->Accounts->get_access_rights_all();

		$data['access_add_button'] = ($access_create) ? '<a id="acl_add" class="btn btn-default btn-sm " href="/admin/users/add/access/" style="margin-right: 5px;">
				<i class="fa fa-plus"></i> Add Access Right </a>' : '';

		for($i = 0; $i < count($rights); $i++)
		{
			$right                = $rights[$i];
			$access_edit_button   = ($access_update) ? "<a class='btn default btn-xs green edit'
							href='/admin/users/update/access/{$right['access_id']}/'>
							<i class='fa fa-edit'></i> Edit </a>" : "";
			$access_delete_button = ($access_delete) ? "<a class='btn default btn-xs red delete'
							href='/admin/users/delete/access/{$right['access_id']}/'>
							<i class='fa fa-trash-o'></i> Delete </a>" : "";

			$data['access'] .= "<tr class='odd gradeX'>
					<td>{$right['type']}</td>
					<td>{$right['role']}</td>
					<td>{$right['level']}</td>
					<td>{$right['page']}</td>
					<td>
						{$access_edit_button}
						{$access_delete_button}
					</td>
				</tr>\n";
		}

		// Disable the access portlet for now
		//$data['access'] = '';

		$data['user_info_hide']    = 'display-hide';
		$data['team_info_hide']    = 'display-hide';
		$data['office_info_hide']  = 'display-hide';
		$data['company_info_hide'] = 'display-hide';
		$data['access_list_hide']  = 'display-hide';
		$data['access_info_hide']  = 'display-hide';

		// Set users as the active navbar link
		$nav_active['users'] = 'active';

		$data_nav = array(
			'active' => $nav_active
		);

		$data_header = array(
			'title'         => 'Lonnie Bush | Admin - Users',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-users',
			'user_name'     => $access_name
		);

		$data['role']        = $role;

		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/users", $data);
		$this->load->view("admin/footer", $data_footer);
	}

	public function update($action = null, $id = null)
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_update('admin-users');

		if(empty($site_id) && !$access_admin)
			header("Location: /admin/users/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		// We need to be able to set the class and content of our flash banner for form submission errors
		$flash_class = "display-hide";
		$flash_text  = "";

		// Preset all our portlets to hide so we can just use one line to show the portlet
		// we need below
		$data['user_list_hide']    = 'display-hide';
		$data['user_info_hide']    = 'display-hide';
		$data['team_list_hide']    = 'display-hide';
		$data['team_info_hide']    = 'display-hide';
		$data['office_list_hide']  = 'display-hide';
		$data['office_info_hide']  = 'display-hide';
		$data['company_list_hide'] = 'display-hide';
		$data['company_info_hide'] = 'display-hide';
		$data['access_list_hide']  = 'display-hide';
		$data['access_info_hide']  = 'display-hide';

		// Set all our view form variables to their default empty values
		$data['users']     = '';
		$data['teams']     = '';
		$data['offices']   = '';
		$data['companies'] = '';
		$data['access']    = '';
		
		$data['user_enabled']             = '';
		$data['user_featured']            = '';
		$data['user_role_client']         = '';
		$data['user_role_agent']          = '';
		$data['user_role_team_leader']    = '';
		$data['user_role_office_manager'] = '';
		$data['user_role_company_owner']  = '';
		$data['user_role_admin']          = '';
		$data['user_role_master']         = '';
		$data['user_username']            = '';
		$data['user_password']            = '';
		$data['user_first_name']          = '';
		$data['user_last_name']           = '';
		$data['user_email']               = '';
		$data['user_phone']               = '';
		$data['user_mobile']              = '';
		$data['user_street']              = '';
		$data['user_city']                = '';
		$data['user_state']               = '';
		$data['user_zipcode']             = '';
		$data['user_license']             = '';
		$data['user_title']               = '';
		$data['user_image']               = 'http://via.placeholder.com/250x300?text=No+Image';
		$data['user_bio']                 = '';
		$data['user_companies']           = '';
		$data['user_offices']             = '';
		$data['user_teams']               = '';
		$data['user_lenders']             = '';
		$data['access']                   = '';
		
		$data['team_enabled']   = '';
		$data['team_name']      = '';
		$data['team_leader']    = '';
		$data['team_email']     = '';
		$data['team_phone']     = '';
		$data['team_mobile']    = '';
		$data['team_street']    = '';
		$data['team_city']      = '';
		$data['team_state']     = '';
		$data['team_zipcode']   = '';
		$data['team_image']     = '';
		$data['team_companies'] = '';
		$data['team_offices']   = '';

		$data['office_enabled']   = '';
		$data['office_name']      = '';
		$data['office_manager']   = '';
		$data['office_email']     = '';
		$data['office_phone']     = '';
		$data['office_mobile']    = '';
		$data['office_street']    = '';
		$data['office_city']      = '';
		$data['office_state']     = '';
		$data['office_zipcode']   = '';
		$data['office_companies'] = '';

		$data['company_enabled'] = '';
		$data['company_name']    = '';
		$data['company_owner']   = '';
		$data['company_email']   = '';
		$data['company_phone']   = '';
		$data['company_mobile']  = '';
		$data['company_street']  = '';
		$data['company_city']    = '';
		$data['company_state']   = '';
		$data['company_zipcode'] = '';

		$data['access_enabled']                = '';
		$data['access_type_group']             = '';
		$data['access_type_user']              = '';
		$data['access_role_all']               = '';
		$data['access_role_admin']             = '';
		$data['access_role_client']            = '';
		$data['access_role_agent']             = '';
		$data['access_role_team']              = '';
		$data['access_role_office']            = '';
		$data['access_role_company']           = '';
		$data['access_level_all']              = '';
		$data['access_level_agent']            = '';
		$data['access_level_team']             = '';
		$data['access_level_office']           = '';
		$data['access_level_company']          = '';
		$data['access_page_all']               = '';
		$data['access_page_public']            = '';
		$data['access_page_admin']             = '';
		$data['access_page_admin_dashboard']   = '';
		$data['access_page_admin_pages']       = '';
		$data['access_page_admin_properties']  = '';
		$data['access_page_admin_users']       = '';
		$data['access_page_admin_leads']       = '';
		$data['access_page_admin_communities'] = '';
		$data['access_page_admin_settings']    = '';
		$data['access_section_all']            = '';
		$data['access_read']                   = '';
		$data['access_create']                 = '';
		$data['access_update']                 = '';
		$data['access_delete']                 = '';

		$data['users_add_button']     = '';
		$data['teams_add_button']     = '';
		$data['offices_add_button']   = '';
		$data['companies_add_button'] = '';
		$data['access_add_button']    = '';

		if(!empty($id))
		{
			switch($action)
			{
				case "user":
					$update_user    = $this->Accounts->get_user_by_id($id);
					$update_allowed = false;

					if($update_user)
					{
						// We need to make sure that the current user has access to update this specific user
						// before we allow them to do so
						if($role == 'agent' && $update_user['user_id'] == $user_id)
							$update_allowed = true;
						else if($role == 'team leader' && $update_user['team_id'] == $team_id)
							$update_allowed = true;
						else if($role == 'office manager' && $update_user['office_id'] == $office_id)
							$update_allowed = true;
						else if($role == 'company owner' && $update_user['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'admin' && $update_user['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'master')
							$update_allowed = true;

						if($update_allowed)
						{
							$data['user_info_hide'] = '';

							$postback = $this->input->post('user_form_submit');

							if($postback)
							{
								// Before we process the update, we need to check and make sure the username is
								// not already being used in the system. If it is, we need to bail and inform
								// the user.
								$new_username = $this->input->post('user_info_username');
								$old_username = $update_user['username'];

								if($new_username)
								{
									// Our email/username must be unique in the user table
									$user_check   = $this->Accounts->get_user($new_username);

									if(($user_check) && $old_username != $new_username)
									{
										// Yep, username already exists, we need to bail and let the user know.
										$flash_class = "alert alert-danger";
										$flash_text  = "Failed! Username already exists!";
									}
									else
									{
										$update_data['enabled']  = ($this->input->post('user_info_enabled') == "on") ? 1 : 0;
										$update_data['featured'] = ($this->input->post('user_info_featured') == "on") ? 1 : 0;
										
										if($role == "master" || $role == "admin" || $role == "company owner")
										{
											if($update_user['role'] != 'master')
												$update_data['role'] = $this->input->post('user_info_role');
										}
										
										$update_data['lead_id']  = (!empty($update_data['role']) && $update_data['role'] == 'client') ? $update_user['lead_id'] : 0;
										$update_data['username'] = $this->input->post('user_info_username');
										$password                = $this->input->post('user_info_password');
										if(!empty($password))
											$update_data['password'] = md5($password);

										$update_data['first_name'] = $this->input->post('user_info_first_name');
										$update_data['last_name']  = $this->input->post('user_info_last_name');
										$update_data['email']      = $this->input->post('user_info_email');
										$update_data['phone']      = $this->input->post('user_info_phone');
										$update_data['mobile']     = $this->input->post('user_info_mobile');
										$update_data['street']     = $this->input->post('user_info_street');
										$update_data['city']       = $this->input->post('user_info_city');
										$update_data['state']      = $this->input->post('user_info_state');
										$update_data['zipcode']    = $this->input->post('user_info_zipcode');
										$update_data['license_no'] = $this->input->post('user_info_license');
										$update_data['title']      = $this->input->post('user_info_title');
										$update_data['bio']        = $this->input->post('user_info_bio');
										$update_data['address']    = $update_data['street'] . ", " . $update_data['city'] . ", " . $update_data['state'] . " " . $update_data['zipcode'];

										if($role != "agent")
										{
											$update_data['company_id'] = $this->input->post('user_info_company');
											$update_data['office_id']  = $this->input->post('user_info_office');
											$update_data['team_id']    = $this->input->post('user_info_team');
										}

										$upload_filename  = $_FILES['user_info_image']['name'];

										if(!empty($upload_filename))
										{
											$filename_prefix  = uniqid();
											$filename_replace = strtolower(str_replace(' ', '_', $upload_filename));
											$filename         = $filename_prefix . "-" . $filename_replace;
											$upload_path      = '/data/images/';

											$config['upload_path']   = '.' . $upload_path;
											$config['allowed_types'] = 'gif|jpg|jpeg|png';
											$config['file_name']     = $filename;
											$config['max_size']      = '2048';
											$config['overwrite']     = true;

											$this->load->library('upload', $config);

											if(!$this->upload->do_upload('user_info_image'))
												log_message('error', "Failed to upload file!");
											else
											{
												$upload_data          = $this->upload->data();
												$update_data['image'] = $upload_path . $filename;
											}
										}

										// Update the user record in the db
										$update_results = $this->Accounts->update_user($update_data, $id);

										if($update_results)
										{
											$this->activity->log('UUPF');

											log_message('debug', "Successfully updated user!");
											header("Location: /admin/users/");
										}
										else
										{
											log_message('error', "Failed to update user!");
											header("Location: /admin/users/");
										}
									}
								}
							}
							else
							{
								$data['user_enabled']             = ($update_user['enabled']) ? 'checked' : '';
								$data['user_featured']            = ($update_user['featured']) ? 'checked' : '';
								$data['user_role_client']         = ($update_user['role'] == 'client') ? 'selected' : '';
								$data['user_role_agent']          = ($update_user['role'] == 'agent') ? 'selected' : '';
								$data['user_role_team_leader']    = ($update_user['role'] == 'team leader') ? 'selected' : '';
								$data['user_role_office_manager'] = ($update_user['role'] == 'office manager') ? 'selected' : '';
								$data['user_role_company_owner']  = ($update_user['role'] == 'company owner') ? 'selected' : '';
								$data['user_role_admin']          = ($update_user['role'] == 'admin') ? 'selected' : '';
								$data['user_role_master']         = ($update_user['role'] == 'master') ? 'selected' : '';
								
								$data['user_username']   = $update_user['username'];
								$data['user_password']   = $update_user['password'];
								$data['user_first_name'] = $update_user['first_name'];
								$data['user_last_name']  = $update_user['last_name'];
								$data['user_email']      = $update_user['email'];
								$data['user_phone']      = $update_user['phone'];
								$data['user_mobile']     = $update_user['mobile'];
								$data['user_street']     = $update_user['street'];
								$data['user_city']       = $update_user['city'];
								$data['user_state']      = $update_user['state'];
								$data['user_zipcode']    = $update_user['zipcode'];
								$data['user_license']    = $update_user['license_no'];
								$data['user_title']      = $update_user['title'];
								$data['user_image']      = $update_user['image'];
								$data['user_bio']        = $update_user['bio'];

								$data['user_companies'] = '';
								$data['user_offices']   = '';
								$data['user_teams']     = '';

								$organization      = array();

								if($role == 'agent')
								{
									$organization = $this->Accounts->get_organization_by_user($user_id);
								}
								else if($role == 'team leader')
								{
									$organization = $this->Accounts->get_organization_by_team($team_id);
								}
								else if($role == 'office manager')
								{
									$organization = $this->Accounts->get_organization_by_office($office_id);
								}
								else if($role == 'company owner')
								{
									$organization = $this->Accounts->get_organization($company_id);
								}
								else if($role == 'admin')
								{
									$organization = $this->Accounts->get_organization($company_id);
								}
								else if($role == 'master')
								{
									$organization = $this->Accounts->get_organization_all();
								}

								if(!empty($organization['companies']))
								{
									foreach($organization['companies'] as $company)
									{
										if($company['company_id'] == $update_user['company_id'])
											$data['user_companies'] .= "<option value='{$company['company_id']}' selected>{$company['company_name']}</option>\n";
										else
											$data['user_companies'] .= "<option value='{$company['company_id']}'>{$company['company_name']}</option>\n";
									}
								}
								else
									$data['user_companies'] .= "<option value='0'>No Company</option>\n";

								$data['user_offices'] .= "<option value='0'>No Office</option>\n";

								if(!empty($organization['offices']))
								{
									if($role == "agent")
										$data['user_offices'] = "";

									foreach($organization['offices'] as $office)
									{
										if($office['office_id'] == $update_user['office_id'])
											$data['user_offices'] .= "<option value='{$office['office_id']}' selected>{$office['office_name']}</option>\n";
										else
											$data['user_offices'] .= "<option value='{$office['office_id']}'>{$office['office_name']}</option>\n";
									}
								}

								$data['user_teams'] .= "<option value='0'>No Team</option>\n";

								if(!empty($organization['teams']))
								{
									if($role == "agent")
										$data['user_teams'] = "";

									foreach($organization['teams'] as $team)
									{
										if($team['team_id'] == $update_user['team_id'])
											$data['user_teams'] .= "<option value='{$team['team_id']}' selected>{$team['team_name']}</option>\n";
										else
											$data['user_teams'] .= "<option value='{$team['team_id']}'>{$team['team_name']}</option>\n";
									}
								}
							}
						}
						else
							header("Location: /admin/users/");
					}
					break;
				case "team":
					$update_team    = $this->Accounts->get_team($id);
					$update_allowed = false;

					if($update_team)
					{
						// We need to make sure that the current user has access to update this specific team
						// before we allow them to do so
						if($role == 'team leader' && $update_team['team_id'] == $team_id)
							$update_allowed = true;
						else if($role == 'office manager' && $update_team['office_id'] == $office_id)
							$update_allowed = true;
						else if($role == 'company owner' && $update_team['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'admin' && $update_team['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'master')
							$update_allowed = true;

						if($update_allowed)
						{
							$data['team_info_hide'] = '';

							$postback = $this->input->post('team_form_submit');

							if($postback)
							{
								$update_data['enabled']     = ($this->input->post('team_info_enabled') == "on") ? 1 : 0;
								$update_data['team_name']   = $this->input->post('team_info_team_name');
								$update_data['team_leader'] = $this->input->post('team_info_team_leader');
								$update_data['email']       = $this->input->post('team_info_email');
								$update_data['phone']       = $this->input->post('team_info_phone');
								$update_data['mobile']      = $this->input->post('team_info_mobile');
								$update_data['street']      = $this->input->post('team_info_street');
								$update_data['city']        = $this->input->post('team_info_city');
								$update_data['state']       = $this->input->post('team_info_state');
								$update_data['zipcode']     = $this->input->post('team_info_zipcode');
								$update_data['address']     = $update_data['street'] . ", " . $update_data['city'] . ", " . $update_data['state'] . " " . $update_data['zipcode'];

								$update_data['company_id'] = $this->input->post('team_info_company');
								$update_data['office_id']  = $this->input->post('team_info_office');

								// Update the user record in the db
								$update_results = $this->Accounts->update_team($update_data, $id);

								if($update_results)
								{
									log_message('debug', "Successfully updated team!");
									header("Location: /admin/users/");
								}
								else
								{
									log_message('error', "Failed to update team!");
									header("Location: /admin/users/");
								}
							}
							else
							{
								$data['team_enabled'] = ($update_team['enabled']) ? 'checked' : '';
								$data['team_name']    = $update_team['team_name'];
								$data['team_leader']  = $update_team['team_leader'];
								$data['team_email']   = $update_team['email'];
								$data['team_phone']   = $update_team['phone'];
								$data['team_mobile']  = $update_team['mobile'];
								$data['team_street']  = $update_team['street'];
								$data['team_city']    = $update_team['city'];
								$data['team_state']   = $update_team['state'];
								$data['team_zipcode'] = $update_team['zipcode'];

								$data['team_companies'] = '';
								$data['team_offices']   = '';

								$organization      = array();
								$lenders           = array();
								$preferred_lenders = (!empty($update_user['lenders'])) ? explode(",", $update_user['lenders']) : array();

								if($role == 'agent')
								{
									$organization = $this->Accounts->get_organization_by_user($user_id);
								}
								else if($role == 'team leader')
								{
									$organization = $this->Accounts->get_organization_by_team($team_id);
								}
								else if($role == 'office manager')
								{
									$organization = $this->Accounts->get_organization_by_office($office_id);
								}
								else if($role == 'company owner')
								{
									$organization = $this->Accounts->get_organization($company_id);
								}
								else if($role == 'admin')
								{
									$organization = $this->Accounts->get_organization($company_id);
								}
								else if($role == 'master')
								{
									$organization = $this->Accounts->get_organization_all();
								}

								if(!empty($organization['companies']))
								{
									foreach($organization['companies'] as $company)
									{
										if($company['company_id'] == $update_team['company_id'])
											$data['team_companies'] .= "<option value='{$company['company_id']}' selected>{$company['company_name']}</option>\n";
										else
											$data['team_companies'] .= "<option value='{$company['company_id']}'>{$company['company_name']}</option>\n";
									}
								}
								else
									$data['team_companies'] .= "<option value='0'>No Company</option>\n";

								$data['team_offices'] .= "<option value='0'>No Team</option>\n";

								if(!empty($organization['offices']))
								{
									foreach($organization['offices'] as $office)
									{
										if($office['office_id'] == $update_team['office_id'])
											$data['team_offices'] .= "<option value='{$office['office_id']}' selected>{$office['office_name']}</option>\n";
										else
											$data['team_offices'] .= "<option value='{$office['office_id']}'>{$office['office_name']}</option>\n";
									}
								}
							}
						}
						else
							header("Location: /admin/users/");
					}
					break;
				case "office":
					$update_office  = $this->Accounts->get_office($id);
					$update_allowed = false;

					if($update_office)
					{
						// We need to make sure that the current user has access to update this specific office
						// before we allow them to do so
						if($role == 'office manager' && $update_office['office_id'] == $office_id)
							$update_allowed = true;
						else if($role == 'company owner' && $update_office['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'admin' && $update_office['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'master')
							$update_allowed = true;

						if($update_allowed)
						{
							$data['office_info_hide'] = '';

							$postback = $this->input->post('office_form_submit');

							if($postback)
							{
								$update_data['enabled']        = ($this->input->post('office_info_enabled') == "on") ? 1 : 0;
								$update_data['office_name']    = $this->input->post('office_info_office_name');
								$update_data['office_manager'] = $this->input->post('office_info_office_manager');
								$update_data['email']          = $this->input->post('office_info_email');
								$update_data['phone']          = $this->input->post('office_info_phone');
								$update_data['mobile']         = $this->input->post('office_info_mobile');
								$update_data['street']         = $this->input->post('office_info_street');
								$update_data['city']           = $this->input->post('office_info_city');
								$update_data['state']          = $this->input->post('office_info_state');
								$update_data['zipcode']        = $this->input->post('office_info_zipcode');
								$update_data['address']        = $update_data['street'] . ", " . $update_data['city'] . ", " . $update_data['state'] . " " . $update_data['zipcode'];

								$update_data['company_id'] = $this->input->post('office_info_company');

								// Update the user record in the db
								$update_results = $this->Accounts->update_office($update_data, $id);

								if($update_results)
								{
									log_message('debug', "Successfully updated office!");
									header("Location: /admin/users/");
								}
								else
								{
									log_message('error', "Failed to update office!");
									header("Location: /admin/users/");
								}
							}
							else
							{
								$data['office_enabled'] = ($update_office['enabled']) ? 'checked' : '';
								$data['office_name']    = $update_office['office_name'];
								$data['office_manager'] = $update_office['office_manager'];
								$data['office_email']   = $update_office['email'];
								$data['office_phone']   = $update_office['phone'];
								$data['office_mobile']  = $update_office['mobile'];
								$data['office_street']  = $update_office['street'];
								$data['office_city']    = $update_office['city'];
								$data['office_state']   = $update_office['state'];
								$data['office_zipcode'] = $update_office['zipcode'];

								$data['office_companies'] = '';

								if($role == 'agent')
									$organization = $this->Accounts->get_organization_by_user($user_id);
								else if($role == 'team leader')
									$organization = $this->Accounts->get_organization_by_team($team_id);
								else if($role == 'office manager')
									$organization = $this->Accounts->get_organization_by_office($office_id);
								else if($role == 'company owner')
									$organization = $this->Accounts->get_organization($company_id);
								else if($role == 'admin')
									$organization = $this->Accounts->get_organization($company_id);
								else if($role == 'master')
								{
									$organization = $this->Accounts->get_organization_all();
								}

								if(!empty($organization['companies']))
								{
									foreach($organization['companies'] as $company)
									{
										if($company['company_id'] == $update_office['company_id'])
											$data['office_companies'] .= "<option value='{$company['company_id']}'
												selected>{$company['company_name']}</option>\n";
										else
											$data['office_companies'] .= "<option
												value='{$company['company_id']}'>{$company['company_name']}</option>\n";
									}
								}
								else
									$data['office_companies'] .= "<option value='0'>No Company</option>\n";
							}
						}
						else
							header("Location: /admin/users/");
					}
					break;
				case "company":
					$update_company = $this->Accounts->get_company($id);
					$update_allowed = false;

					if($update_company)
					{
						// We need to make sure that the current user has access to update this specific company
						// before we allow them to do so
						if($role == 'company owner' && $update_company['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'admin' && $update_company['company_id'] == $company_id)
							$update_allowed = true;
						else if($role == 'master')
							$update_allowed = true;

						if($update_allowed)
						{
							$data['company_info_hide'] = '';

							$postback = $this->input->post('company_form_submit');

							if($postback)
							{
								$update_data['enabled']       = ($this->input->post('company_info_enabled') == "on") ? 1 : 0;
								$update_data['company_name']  = $this->input->post('company_info_company_name');
								$update_data['company_owner'] = $this->input->post('company_info_company_owner');
								$update_data['email']         = $this->input->post('company_info_email');
								$update_data['phone']         = $this->input->post('company_info_phone');
								$update_data['mobile']        = $this->input->post('company_info_mobile');
								$update_data['street']        = $this->input->post('company_info_street');
								$update_data['city']          = $this->input->post('company_info_city');
								$update_data['state']         = $this->input->post('company_info_state');
								$update_data['zipcode']       = $this->input->post('company_info_zipcode');
								$update_data['address']       = $update_data['street'] . ", " . $update_data['city'] . ", " . $update_data['state'] . " " . $update_data['zipcode'];

								// Update the user record in the db
								$update_results = $this->Accounts->update_company($update_data, $id);

								if($update_results)
								{
									log_message('debug', "Successfully updated company!");
									header("Location: /admin/users/");
								}
								else
								{
									log_message('error', "Failed to update company!");
									header("Location: /admin/users/");
								}
							}
							else
							{
								$data['company_enabled'] = ($update_company['enabled']) ? 'checked' : '';
								$data['company_name']    = $update_company['company_name'];
								$data['company_owner']   = $update_company['company_owner'];
								$data['company_email']   = $update_company['email'];
								$data['company_phone']   = $update_company['phone'];
								$data['company_mobile']  = $update_company['mobile'];
								$data['company_street']  = $update_company['street'];
								$data['company_city']    = $update_company['city'];
								$data['company_state']   = $update_company['state'];
								$data['company_zipcode'] = $update_company['zipcode'];
							}
						}
						else
							header("Location: /admin/users/");
					}
					break;
				case "access":
					$update_allowed = false;

					if($role == 'master')
						$update_allowed = true;

					if($update_allowed)
					{
						$data['access_info_hide'] = 'display-hide';

						$postback = $this->input->post('access_form_submit');

						if($postback)
						{
							$update_data['enabled'] = ($this->input->post('acl_info_enabled') == "on") ? 1 : 0;
							$update_data['type']    = $this->input->post('acl_info_type');
							$update_data['role']    = $this->input->post('acl_info_role');
							$update_data['level']   = $this->input->post('acl_info_level');
							$update_data['page']    = $this->input->post('acl_info_page');
							$update_data['section'] = $this->input->post('acl_info_section');
							$update_data['read']    = ($this->input->post('acl_info_read') == "on") ? 1 : 0;
							$update_data['create']  = ($this->input->post('acl_info_create') == "on") ? 1 : 0;
							$update_data['update']  = ($this->input->post('acl_info_update') == "on") ? 1 : 0;
							$update_data['delete']  = ($this->input->post('acl_info_delete') == "on") ? 1 : 0;

							// Update the user record in the db
							$update_results = $this->Accounts->update_access_right($update_data, $id);

							if($update_results)
							{
								log_message('debug', "Successfully updated access right!");
								header("Location: /admin/users/");
							}
							else
							{
								log_message('error', "Failed to update access right!");
								header("Location: /admin/users/");
							}
						}
						else
						{
							$access = $this->Accounts->get_access_rights_by_id($id);

							if($access)
							{
								$data['access_enabled'] = ($access['enabled']) ? 'checked' : '';

								$data['access_type_group'] = ($access['type'] == 'group') ? 'selected' : '';
								$data['access_type_user']  = ($access['type'] == 'user') ? 'selected' : '';

								$data['access_role_all']     = ($access['role'] == 'all') ? 'selected' : '';
								$data['access_role_admin']   = ($access['role'] == 'admin') ? 'selected' : '';
								$data['access_role_client']  = ($access['role'] == 'client') ? 'selected' : '';
								$data['access_role_agent']   = ($access['role'] == 'agent') ? 'selected' : '';
								$data['access_role_team']    = ($access['role'] == 'team leader') ? 'selected' : '';
								$data['access_role_office']  = ($access['role'] == 'office manager') ? 'selected' : '';
								$data['access_role_company'] = ($access['role'] == 'company owner') ? 'selected' : '';

								$data['access_level_all']     = ($access['level'] == 'all') ? 'selected' : '';
								$data['access_level_agent']   = ($access['level'] == 'agent') ? 'selected' : '';
								$data['access_level_team']    = ($access['level'] == 'team') ? 'selected' : '';
								$data['access_level_office']  = ($access['level'] == 'office') ? 'selected' : '';
								$data['access_level_company'] = ($access['level'] == 'company') ? 'selected' : '';

								$data['access_page_all']               = ($access['page'] == 'all') ? 'selected' : '';
								$data['access_page_public']            = ($access['page'] == 'public') ? 'selected' : '';
								$data['access_page_admin']             = ($access['page'] == 'admin') ? 'selected' : '';
								$data['access_page_admin_dashboard']   = ($access['page'] == 'admin_dashboard') ? 'selected' : '';
								$data['access_page_admin_pages']       = ($access['page'] == 'admin_pages') ? 'selected' : '';
								$data['access_page_admin_properties']  = ($access['page'] == 'admin_properties') ? 'selected' : '';
								$data['access_page_admin_users']       = ($access['page'] == 'admin_users') ? 'selected' : '';
								$data['access_page_admin_leads']       = ($access['page'] == 'admin_leads') ? 'selected' : '';
								$data['access_page_admin_communities'] = ($access['page'] == 'admin_communities') ? 'selected' : '';
								$data['access_page_admin_settings']    = ($access['page'] == 'admin_settings') ? 'selected' : '';

								$data['access_section_all'] = ($access['section'] == 'all') ? 'selected' : '';

								$data['access_read']   = ($access['read']) ? 'checked' : '';
								$data['access_create'] = ($access['create']) ? 'checked' : '';
								$data['access_update'] = ($access['update']) ? 'checked' : '';
								$data['access_delete'] = ($access['delete']) ? 'checked' : '';
							}
						}
					}
					else
						header("Location: /admin/users/");
					break;
				default:
					header("Location: /adming/users/");
					break;
			}

			// Set users as the active navbar link
			$nav_active = array(
				'home'        => '',
				'pages'       => '',
				'properties'  => '',
				'openhouses'  => '',
				'feeds'       => '',
				'sites'       => '',
				'users'       => 'active',
				'leads'       => '',
				'lenders'     => '',
				'communities' => '',
				'settings'    => '',
				'tutorials'   => ''
			);

			// Only show links the user has read access to its page
			$nav_hide['home']      = '';
			$nav_hide['tutorials'] = '';

			if(!empty($site_id))
			{
				$nav_hide['pages']       = (!$this->Accounts->can_user_access('admin-pages-micro')) ? 'display-hide' : '';
				$nav_hide['properties']  = (!$this->Accounts->can_user_access('admin-properties-micro')) ? 'display-hide' : '';
				$nav_hide['openhouses']  = (!$this->Accounts->can_user_access('admin-openhouses-micro')) ? 'display-hide' : '';
				$nav_hide['feeds']       = (!$this->Accounts->can_user_access('admin-feeds-micro')) ? 'display-hide' : '';
				$nav_hide['sites']       = (!$this->Accounts->can_user_access('admin-sites-micro')) ? 'display-hide' : '';
				$nav_hide['users']       = (!$this->Accounts->can_user_access('admin-users-micro')) ? 'display-hide' : '';
				$nav_hide['leads']       = (!$this->Accounts->can_user_access('admin-leads-micro')) ? 'display-hide' : '';
				$nav_hide['lenders']     = (!$this->Accounts->can_user_access('admin-lenders-micro')) ? 'display-hide' : '';
				$nav_hide['communities'] = (!$this->Accounts->can_user_access('admin-communities-micro')) ? 'display-hide' : '';
				$nav_hide['settings']    = (!$this->Accounts->can_user_access('admin-settings-micro')) ? 'display-hide' : '';
			}
			else
			{
				$nav_hide['pages']       = (!$this->Accounts->can_user_access('admin-pages')) ? 'display-hide' : '';
				$nav_hide['properties']  = (!$this->Accounts->can_user_access('admin-properties')) ? 'display-hide' : '';
				$nav_hide['openhouses']  = (!$this->Accounts->can_user_access('admin-openhouses')) ? 'display-hide' : '';
				$nav_hide['feeds']       = (!$this->Accounts->can_user_access('admin-feeds')) ? 'display-hide' : '';
				$nav_hide['sites']       = (!$this->Accounts->can_user_access('admin-sites')) ? 'display-hide' : '';
				$nav_hide['users']       = (!$this->Accounts->can_user_access('admin-users')) ? 'display-hide' : '';
				$nav_hide['leads']       = (!$this->Accounts->can_user_access('admin-leads')) ? 'display-hide' : '';
				$nav_hide['lenders']     = (!$this->Accounts->can_user_access('admin-lenders')) ? 'display-hide' : '';
				$nav_hide['communities'] = (!$this->Accounts->can_user_access('admin-communities')) ? 'display-hide' : '';
				$nav_hide['settings']    = (!$this->Accounts->can_user_access('admin-settings')) ? 'display-hide' : '';
			}

			// Hide the communities panel for microsites
			if(!empty($site_id))
				$nav_hide['communities'] = 'display-hide';

			$data_nav            = array(
				'hide'   => $nav_hide,
				'active' => $nav_active
			);
			$data_header         = array(
				'title'         => 'Lonnie Bush | Admin - Users',
				'meta_desc'     => '',
				'meta_keywords' => '',
				'body_class'    => 'admin-users',
				'user_name'     => $access_name
			);
			$data['flash_text']  = $flash_text;
			$data['flash_class'] = $flash_class;
			$data['role']        = $role;
			$data_footer         = array(
				'placeholder' => ''
			);

			$this->load->view("admin/header", $data_header);
			$this->load->view("admin/navbar", $data_nav);
			$this->load->view("admin/users", $data);
			$this->load->view("admin/footer", $data_footer);
		}
		else
			header("Location: /admin/users/");
	}

	public function add($action = null, $id = null)
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_create('admin-users');

		if(empty($site_id) && !$access_admin)
			header("Location: /admin/users/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		// We need to be able to set the class and content of our flash banner for form submission errors
		$flash_class = "display-hide";
		$flash_text  = "";

		// Preset all our portlets to hide so we can just use one line to show the portlet
		// we need below
		$data['user_list_hide']    = 'display-hide';
		$data['user_info_hide']    = 'display-hide';
		$data['team_list_hide']    = 'display-hide';
		$data['team_info_hide']    = 'display-hide';
		$data['office_list_hide']  = 'display-hide';
		$data['office_info_hide']  = 'display-hide';
		$data['company_list_hide'] = 'display-hide';
		$data['company_info_hide'] = 'display-hide';
		$data['access_list_hide']  = 'display-hide';
		$data['access_info_hide']  = 'display-hide';

		// Set all our view form variables to empty strings
		$data['users']     = '';
		$data['teams']     = '';
		$data['offices']   = '';
		$data['companies'] = '';
		$data['access']    = '';

		$data['user_enabled']             = 'checked';
		$data['user_featured']            = '';
		$data['user_role_client']         = '';
		$data['user_role_agent']          = '';
		$data['user_role_team_leader']    = '';
		$data['user_role_office_manager'] = '';
		$data['user_role_company_owner']  = '';
		$data['user_role_admin']          = '';
		$data['user_role_master']         = '';
		$data['user_username']            = '';
		$data['user_password']            = '';
		$data['user_first_name']          = '';
		$data['user_last_name']           = '';
		$data['user_email']               = '';
		$data['user_phone']               = '';
		$data['user_mobile']              = '';
		$data['user_street']              = '';
		$data['user_city']                = '';
		$data['user_state']               = '';
		$data['user_zipcode']             = '';
		$data['user_license']             = '';
		$data['user_title']               = '';
		$data['user_image']               = 'http://via.placeholder.com/250x300?text=No+Image';
		$data['user_bio']                 = '';
		$data['user_companies']           = '';
		$data['user_offices']             = '';
		$data['user_teams']               = '';
		$data['access']                   = '';

		$data['team_enabled']   = 'checked';
		$data['team_name']      = '';
		$data['team_leader']    = '';
		$data['team_email']     = '';
		$data['team_phone']     = '';
		$data['team_mobile']    = '';
		$data['team_street']    = '';
		$data['team_city']      = '';
		$data['team_state']     = '';
		$data['team_zipcode']   = '';
		$data['team_image']     = '';
		$data['team_companies'] = '';
		$data['team_offices']   = '';

		$data['office_enabled']   = 'checked';
		$data['office_name']      = '';
		$data['office_manager']   = '';
		$data['office_email']     = '';
		$data['office_phone']     = '';
		$data['office_mobile']    = '';
		$data['office_street']    = '';
		$data['office_city']      = '';
		$data['office_state']     = '';
		$data['office_zipcode']   = '';
		$data['office_companies'] = '';

		$data['company_enabled'] = 'checked';
		$data['company_name']    = '';
		$data['company_owner']   = '';
		$data['company_email']   = '';
		$data['company_phone']   = '';
		$data['company_mobile']  = '';
		$data['company_street']  = '';
		$data['company_city']    = '';
		$data['company_state']   = '';
		$data['company_zipcode'] = '';

		$data['access_enabled']                = '';
		$data['access_type_group']             = '';
		$data['access_type_user']              = '';
		$data['access_role_all']               = '';
		$data['access_role_admin']             = '';
		$data['access_role_client']            = '';
		$data['access_role_agent']             = '';
		$data['access_role_team']              = '';
		$data['access_role_office']            = '';
		$data['access_role_company']           = '';
		$data['access_level_all']              = '';
		$data['access_level_agent']            = '';
		$data['access_level_team']             = '';
		$data['access_level_office']           = '';
		$data['access_level_company']          = '';
		$data['access_page_all']               = '';
		$data['access_page_public']            = '';
		$data['access_page_admin']             = '';
		$data['access_page_admin_dashboard']   = '';
		$data['access_page_admin_pages']       = '';
		$data['access_page_admin_properties']  = '';
		$data['access_page_admin_users']       = '';
		$data['access_page_admin_leads']       = '';
		$data['access_page_admin_communities'] = '';
		$data['access_page_admin_settings']    = '';
		$data['access_section_all']            = '';
		$data['access_read']                   = '';
		$data['access_create']                 = '';
		$data['access_update']                 = '';
		$data['access_delete']                 = '';

		$data['users_add_button']     = '';
		$data['teams_add_button']     = '';
		$data['offices_add_button']   = '';
		$data['companies_add_button'] = '';
		$data['access_add_button']    = '';

		switch($action)
		{
			case "user":
				$insert_allowed = false;

				// We need to make sure that the current user has access to add a new user.
				// However since our rights check above is handling the heavy lifting for the moment
				// we'll just include the standard role check code below, but set everything to true
				if($role == 'team leader')
					$insert_allowed = true;
				else if($role == 'office manager')
					$insert_allowed = true;
				else if($role == 'company owner')
					$insert_allowed = true;
				else if($role == 'admin')
					$insert_allowed = true;
				else if($role == 'master')
					$insert_allowed = true;

				if($insert_allowed)
				{
					$data['user_info_hide'] = '';

					$postback = $this->input->post('user_form_submit');

					if($postback)
					{
						// Before we process the insert, we need to check and make sure the username is
						// not already being used in the system. If it is, we need to bail the insert and inform
						// the user.
						$username = $this->input->post('user_info_username');

						if(!empty($username))
						{
							// Our email/username must be unique in both the lead and user tables
							$user_check   = $this->Accounts->get_user($username);

							if($user_check)
							{
								// Yep, username already exists, we need to bail and let the user know.
								$flash_class = "alert alert-danger";
								$flash_text  = "Failed! Username already exists!";
							}
							else
							{
								$insert_data['enabled']  = ($this->input->post('user_info_enabled') == "on") ? 1 : 0;
								$insert_data['featured'] = ($this->input->post('user_info_featured') == "on") ? 1 : 0;
								
								if($role == "master" || $role == "admin" || $role == "company owner")
									$insert_data['role']    = $this->input->post('user_info_role');
								else
									$insert_data['role']    = "agent";
								
								$insert_data['username']   = $this->input->post('user_info_username');
								$insert_data['password']   = md5($this->input->post('user_info_password'));
								$insert_data['first_name'] = $this->input->post('user_info_first_name');
								$insert_data['last_name']  = $this->input->post('user_info_last_name');
								$insert_data['email']      = $this->input->post('user_info_email');
								$insert_data['phone']      = $this->input->post('user_info_phone');
								$insert_data['mobile']     = $this->input->post('user_info_mobile');
								$insert_data['street']     = $this->input->post('user_info_street');
								$insert_data['city']       = $this->input->post('user_info_city');
								$insert_data['state']      = $this->input->post('user_info_state');
								$insert_data['zipcode']    = $this->input->post('user_info_zipcode');
								$insert_data['license_no'] = $this->input->post('user_info_license');
								$insert_data['title']      = $this->input->post('user_info_title');
								$insert_data['bio']        = $this->input->post('user_info_bio');
								$insert_data['address']    = $insert_data['street'] . ", " . $insert_data['city'] . ", " . $insert_data['state'] . " " . $insert_data['zipcode'];

								if($role != "agent")
								{
									$insert_data['company_id'] = $this->input->post('user_info_company');
									$insert_data['office_id']  = $this->input->post('user_info_office');
									$insert_data['team_id']    = $this->input->post('user_info_team');
								}

								$preferred_lenders      = $this->input->post('user_info_lenders');
								$preferred_lenders      = (!empty($preferred_lenders)) ? trim(implode(",", $preferred_lenders), ",") : "";
								$insert_data['lenders'] = $preferred_lenders;

								$upload_filename  = $_FILES['user_info_image']['name'];
								$filename_prefix  = uniqid();
								$filename_replace = strtolower(str_replace(' ', '_', $upload_filename));
								$filename         = $filename_prefix . "-" . $filename_replace;
								$upload_path      = '/data/images/';

								$config['upload_path']   = '.' . $upload_path;
								$config['allowed_types'] = 'gif|jpg|jpeg|png';
								$config['file_name']     = $filename;
								$config['max_size']      = '2048';
								$config['overwrite']     = true;

								$this->load->library('upload', $config);

								if(!$this->upload->do_upload('user_info_image'))
									log_message('error', "Failed to upload file!");
								else
								{
									$upload_data          = $this->upload->data();
									$insert_data['image'] = $upload_path . $filename;
								}

								// Update the user record in the db
								$insert_results = $this->Accounts->create_user($insert_data);

								if($insert_results)
								{
									$this->activity->log('UCA');

									log_message('debug', "Successfully added user!");
									header("Location: /admin/users/");
								}
								else
								{
									log_message('error', "Failed to add user!");
									header("Location: /admin/users/");
								}
							}
						}
					}
					else
					{
						// The only thing we need to do here is setup our organization dropdowns for the view form
						// We'll only show options that the current user has access rights to
						$organization = array();
						$lenders      = array();

						if($role == 'agent')
						{
							$organization = $this->Accounts->get_organization_by_user($user_id);
						}
						else if($role == 'team leader')
						{
							$organization = $this->Accounts->get_organization_by_team($team_id);
						}
						else if($role == 'office manager')
						{
							$organization = $this->Accounts->get_organization_by_office($office_id);
						}
						else if($role == 'company owner')
						{
							$organization = $this->Accounts->get_organization($company_id);
						}
						else if($role == 'admin')
						{
							$organization = $this->Accounts->get_organization($company_id);
						}
						else if($role == 'master')
						{
							$organization = $this->Accounts->get_organization_all();
						}

						if(!empty($organization['companies']))
						{
							foreach($organization['companies'] as $company)
							{
								$data['user_companies'] .= "<option value='{$company['company_id']}'>{$company['company_name']}</option>\n";
							}
						}
						else
							$data['user_companies'] .= "<option value='0'>No Company</option>\n";

						$data['user_offices'] .= "<option value='0'>No Office</option>\n";

						if(!empty($organization['offices']))
						{
							foreach($organization['offices'] as $office)
							{
								$data['user_offices'] .= "<option value='{$office['office_id']}'>{$office['office_name']}</option>\n";
							}
						}

						$data['user_teams'] .= "<option value='0'>No Team</option>\n";

						if(!empty($organization['teams']))
						{
							foreach($organization['teams'] as $team)
							{
								$data['user_teams'] .= "<option value='{$team['team_id']}'>{$team['team_name']}</option>\n";
							}
						}
					}
				}
				else
					header("Location: /admin/users/");

				break;
			case "team":
				$insert_allowed = false;

				// We need to make sure that the current user has access to add a new team
				// before we allow them to do so
				if($role == 'office manager')
					$insert_allowed = true;
				else if($role == 'company owner')
					$insert_allowed = true;
				else if($role == 'admin')
					$insert_allowed = true;
				else if($role == 'master')
					$insert_allowed = true;

				if($insert_allowed)
				{
					$data['team_info_hide'] = '';

					$postback = $this->input->post('team_form_submit');

					if($postback)
					{
						$insert_data['enabled']     = ($this->input->post('team_info_enabled') == "on") ? 1 : 0;
						$insert_data['team_name']   = $this->input->post('team_info_team_name');
						$insert_data['team_leader'] = $this->input->post('team_info_team_leader');
						$insert_data['email']       = $this->input->post('team_info_email');
						$insert_data['phone']       = $this->input->post('team_info_phone');
						$insert_data['mobile']      = $this->input->post('team_info_mobile');
						$insert_data['street']      = $this->input->post('team_info_street');
						$insert_data['city']        = $this->input->post('team_info_city');
						$insert_data['state']       = $this->input->post('team_info_state');
						$insert_data['zipcode']     = $this->input->post('team_info_zipcode');
						$insert_data['address']     = $insert_data['street'] . ", " . $insert_data['city'] . ", " . $insert_data['state'] . " " . $insert_data['zipcode'];

						$insert_data['company_id'] = $this->input->post('team_info_company');
						$insert_data['office_id']  = $this->input->post('team_info_office');

						$preferred_lenders      = $this->input->post('team_info_lenders');
						$preferred_lenders      = (!empty($preferred_lenders)) ? trim(implode(",", $preferred_lenders), ",") : "";
						$update_data['lenders'] = $preferred_lenders;

						// Create the user record in the db
						$insert_results = $this->Accounts->create_team($insert_data);

						if($insert_results)
						{
							log_message('debug', "Successfully added team!");
							header("Location: /admin/users/");
						}
						else
						{
							log_message('error', "Failed to add team!");
							header("Location: /admin/users/");
						}
					}
					else
					{
						$data['team_companies'] = '';
						$data['team_offices']   = '';

						// The only thing we need to do here is setup our organization dropdowns for the view form
						// We'll only show options that the current user has access rights to
						$organization = array();
						$lenders      = array();

						if($role == 'agent')
						{
							$organization = $this->Accounts->get_organization_by_user($user_id);
						}
						else if($role == 'team leader')
						{
							$organization = $this->Accounts->get_organization_by_team($team_id);
						}
						else if($role == 'office manager')
						{
							$organization = $this->Accounts->get_organization_by_office($office_id);
						}
						else if($role == 'company owner')
						{
							$organization = $this->Accounts->get_organization($company_id);
						}
						else if($role == 'admin')
						{
							$organization = $this->Accounts->get_organization($company_id);
						}
						else if($role == 'master')
						{
							$organization = $this->Accounts->get_organization_all();
						}

						if(!empty($organization['companies']))
						{
							foreach($organization['companies'] as $company)
							{
								$data['team_companies'] .= "<option value='{$company['company_id']}'>{$company['company_name']}</option>\n";
							}
						}
						else
							$data['team_companies'] .= "<option value='0'>No Company</option>\n";

						$data['team_offices'] .= "<option value='0'>No Office</option>\n";

						if(!empty($organization['offices']))
						{
							foreach($organization['offices'] as $office)
							{
								$data['team_offices'] .= "<option value='{$office['office_id']}'>{$office['office_name']}</option>\n";
							}
						}
					}
				}
				else
					header("Location: /admin/users/");

				break;
			case "office":
				$insert_allowed = false;

				// We need to make sure that the current user has access to add a new office
				// before we allow them to do so
				if($role == 'company owner')
					$insert_allowed = true;
				else if($role == 'admin')
					$insert_allowed = true;
				else if($role == 'master')
					$insert_allowed = true;

				if($insert_allowed)
				{
					$data['office_info_hide'] = '';

					$postback = $this->input->post('office_form_submit');

					if($postback)
					{
						$insert_data['enabled']        = ($this->input->post('office_info_enabled') == "on") ? 1 : 0;
						$insert_data['office_name']    = $this->input->post('office_info_office_name');
						$insert_data['office_manager'] = $this->input->post('office_info_office_manager');
						$insert_data['email']          = $this->input->post('office_info_email');
						$insert_data['phone']          = $this->input->post('office_info_phone');
						$insert_data['mobile']         = $this->input->post('office_info_mobile');
						$insert_data['street']         = $this->input->post('office_info_street');
						$insert_data['city']           = $this->input->post('office_info_city');
						$insert_data['state']          = $this->input->post('office_info_state');
						$insert_data['zipcode']        = $this->input->post('office_info_zipcode');
						$insert_data['address']        = $insert_data['street'] . ", " . $insert_data['city'] . ", " . $insert_data['state'] . " " . $insert_data['zipcode'];

						$insert_data['company_id'] = $this->input->post('office_info_company');

						// insert the user record in the db
						$insert_results = $this->Accounts->create_office($insert_data);

						if($insert_results)
						{
							log_message('debug', "Successfully added office!");
							header("Location: /admin/users/");
						}
						else
						{
							log_message('error', "Failed to add office!");
							header("Location: /admin/users/");
						}
					}
					else
					{
						$data['office_companies'] = '';

						// The only thing we need to do here is setup our organization dropdowns for the view form
						// We'll only show options that the current user has access rights to
						$organization = array();

						if($role == 'agent')
							$organization = $this->Accounts->get_organization_by_user($user_id);
						else if($role == 'team leader')
							$organization = $this->Accounts->get_organization_by_team($team_id);
						else if($role == 'office manager')
							$organization = $this->Accounts->get_organization_by_office($office_id);
						else if($role == 'company owner')
							$organization = $this->Accounts->get_organization($company_id);
						else if($role == 'admin')
							$organization = $this->Accounts->get_organization($company_id);
						else if($role == 'master')
						{
							$organization = $this->Accounts->get_organization_all();
						}

						if(!empty($organization['companies']))
						{
							foreach($organization['companies'] as $company)
							{
								$data['office_companies'] .= "<option
									value='{$company['company_id']}'>{$company['company_name']}</option>\n";
							}
						}
						else
							$data['office_companies'] .= "<option value='0'>No Company</option>\n";
					}
				}
				else
					header("Location: /admin/users/");

				break;
			case "company":
				$insert_allowed = false;

				// We need to make sure that the current user has access to add a new company
				// before we allow them to do so
				if($role == 'master')
					$insert_allowed = true;

				if($insert_allowed)
				{
					$data['company_info_hide'] = '';

					$postback = $this->input->post('company_form_submit');

					if($postback)
					{
						$insert_data['enabled']       = ($this->input->post('company_info_enabled') == "on") ? 1 : 0;
						$insert_data['company_name']  = $this->input->post('company_info_company_name');
						$insert_data['company_owner'] = $this->input->post('company_info_company_owner');
						$insert_data['email']         = $this->input->post('company_info_email');
						$insert_data['phone']         = $this->input->post('company_info_phone');
						$insert_data['mobile']        = $this->input->post('company_info_mobile');
						$insert_data['street']        = $this->input->post('company_info_street');
						$insert_data['city']          = $this->input->post('company_info_city');
						$insert_data['state']         = $this->input->post('company_info_state');
						$insert_data['zipcode']       = $this->input->post('company_info_zipcode');
						$insert_data['address']       = $insert_data['street'] . ", " . $insert_data['city'] . ", " . $insert_data['state'] . " " . $insert_data['zipcode'];

						// insert the user record in the db
						$insert_results = $this->Accounts->create_company($insert_data);

						if($insert_results)
						{
							log_message('debug', "Successfully added company!");
							header("Location: /admin/users/");
						}
						else
						{
							log_message('error', "Failed to add company!");
							header("Location: /admin/users/");
						}
					}
					else
					{
						// Nothing to do here
					}
				}
				else
					header("Location: /admin/users/");

				break;
			case "access":
				$insert_allowed = false;

				// We need to make sure that the current user has access to add a new company
				// before we allow them to do so
				if($role == 'master')
					$insert_allowed = true;

				if($insert_allowed)
				{
					$data['access_info_hide'] = 'display-hide';

					$postback = $this->input->post('access_form_submit');

					if($postback)
					{
						$insert_data['enabled'] = ($this->input->post('acl_info_enabled') == "on") ? 1 : 0;
						$insert_data['type']    = $this->input->post('acl_info_type');
						$insert_data['role']    = $this->input->post('acl_info_role');
						$insert_data['level']   = $this->input->post('acl_info_level');
						$insert_data['page']    = $this->input->post('acl_info_page');
						$insert_data['section'] = $this->input->post('acl_info_section');
						$insert_data['read']    = ($this->input->post('acl_info_read') == "on") ? 1 : 0;
						$insert_data['create']  = ($this->input->post('acl_info_create') == "on") ? 1 : 0;
						$insert_data['update']  = ($this->input->post('acl_info_update') == "on") ? 1 : 0;
						$insert_data['delete']  = ($this->input->post('acl_info_delete') == "on") ? 1 : 0;

						// insert the user record in the db
						$insert_results = $this->Accounts->create_access_right($insert_data);

						if($insert_results)
						{
							log_message('debug', "Successfully added access right!");
							header("Location: /admin/users/");
						}
						else
						{
							log_message('error', "Failed to add access right!");
							header("Location: /admin/users/");
						}
					}
					else
					{
						$access = $this->Accounts->get_access_rights_by_id($id);

						if($access)
						{
							// Nothing to do here
						}
					}
				}
				else
					header("Location: /admin/users/");

				break;
			default:
				header("Location: /admin/users/");
				break;
		}

		// Set users as the active navbar link
		$nav_active = array(
			'home'        => '',
			'pages'       => '',
			'properties'  => '',
			'openhouses'  => '',
			'feeds'       => '',
			'sites'       => '',
			'users'       => 'active',
			'leads'       => '',
			'lenders'     => '',
			'communities' => '',
			'settings'    => '',
			'tutorials'   => ''
		);

		// Only show links the user has read access to its page
		$nav_hide['home']      = '';
		$nav_hide['tutorials'] = '';

		if(!empty($site_id))
		{
			$nav_hide['pages']       = (!$this->Accounts->can_user_access('admin-pages-micro')) ? 'display-hide' : '';
			$nav_hide['properties']  = (!$this->Accounts->can_user_access('admin-properties-micro')) ? 'display-hide' : '';
			$nav_hide['openhouses']  = (!$this->Accounts->can_user_access('admin-openhouses-micro')) ? 'display-hide' : '';
			$nav_hide['feeds']       = (!$this->Accounts->can_user_access('admin-feeds-micro')) ? 'display-hide' : '';
			$nav_hide['sites']       = (!$this->Accounts->can_user_access('admin-sites-micro')) ? 'display-hide' : '';
			$nav_hide['users']       = (!$this->Accounts->can_user_access('admin-users-micro')) ? 'display-hide' : '';
			$nav_hide['leads']       = (!$this->Accounts->can_user_access('admin-leads-micro')) ? 'display-hide' : '';
			$nav_hide['lenders']     = (!$this->Accounts->can_user_access('admin-lenders-micro')) ? 'display-hide' : '';
			$nav_hide['communities'] = (!$this->Accounts->can_user_access('admin-communities-micro')) ? 'display-hide' : '';
			$nav_hide['settings']    = (!$this->Accounts->can_user_access('admin-settings-micro')) ? 'display-hide' : '';
		}
		else
		{
			$nav_hide['pages']       = (!$this->Accounts->can_user_access('admin-pages')) ? 'display-hide' : '';
			$nav_hide['properties']  = (!$this->Accounts->can_user_access('admin-properties')) ? 'display-hide' : '';
			$nav_hide['openhouses']  = (!$this->Accounts->can_user_access('admin-openhouses')) ? 'display-hide' : '';
			$nav_hide['feeds']       = (!$this->Accounts->can_user_access('admin-feeds')) ? 'display-hide' : '';
			$nav_hide['sites']       = (!$this->Accounts->can_user_access('admin-sites')) ? 'display-hide' : '';
			$nav_hide['users']       = (!$this->Accounts->can_user_access('admin-users')) ? 'display-hide' : '';
			$nav_hide['leads']       = (!$this->Accounts->can_user_access('admin-leads')) ? 'display-hide' : '';
			$nav_hide['lenders']     = (!$this->Accounts->can_user_access('admin-lenders')) ? 'display-hide' : '';
			$nav_hide['communities'] = (!$this->Accounts->can_user_access('admin-communities')) ? 'display-hide' : '';
			$nav_hide['settings']    = (!$this->Accounts->can_user_access('admin-settings')) ? 'display-hide' : '';
		}

		// Hide the communities panel for microsites
		if(!empty($site_id))
			$nav_hide['communities'] = 'display-hide';

		$data_nav            = array(
			'hide'   => $nav_hide,
			'active' => $nav_active
		);
		$data_header         = array(
			'title'         => 'Lonnie Bush | Admin - Users',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-users',
			'user_name'     => $access_name
		);
		$data['flash_text']  = $flash_text;
		$data['flash_class'] = $flash_class;
		$data['role']        = $role;
		$data_footer         = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/users", $data);
		$this->load->view("admin/footer", $data_footer);
	}

	public function delete($action = null, $id = null)
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_delete('admin-users');

		if(empty($site_id) && !$access_admin)
			header("Location: /admin/users/");

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		// Preset all our portlets to hide so we can just use one line to show the portlet
		// we need below
		$data['user_list_hide']    = 'display-hide';
		$data['user_info_hide']    = 'display-hide';
		$data['team_list_hide']    = 'display-hide';
		$data['team_info_hide']    = 'display-hide';
		$data['office_list_hide']  = 'display-hide';
		$data['office_info_hide']  = 'display-hide';
		$data['company_list_hide'] = 'display-hide';
		$data['company_info_hide'] = 'display-hide';
		$data['access_list_hide']  = 'display-hide';
		$data['access_info_hide']  = 'display-hide';

		$data['users_add_button']     = '';
		$data['teams_add_button']     = '';
		$data['offices_add_button']   = '';
		$data['companies_add_button'] = '';
		$data['access_add_button']    = '';

		switch($action)
		{
			case "user":
				$delete_user    = $this->Accounts->get_user_by_id($id);
				$delete_allowed = false;

				if($delete_user)
				{
					// We need to make sure that the current user has access to delete this specific user
					// before we allow them to do so
					if($role == 'team leader' && $delete_user['team_id'] == $team_id)
						$delete_allowed = true;
					else if($role == 'office manager' && $delete_user['office_id'] == $office_id)
						$delete_allowed = true;
					else if($role == 'company owner' && $delete_user['company_id'] == $company_id)
						$delete_allowed = true;
					else if($role == 'admin' && $delete_user['company_id'] == $company_id)
						$delete_allowed = true;
					else if($role == 'master')
						$delete_allowed = true;

					if($delete_allowed)
					{
						$data['user_info_hide'] = '';

						// Delete the selected user from the db
						if(!empty($id))
						{
							$delete_results = $this->Accounts->delete_user($id);

							if($delete_results)
								log_message('debug', "Successfully deleted user.");
							else
								log_message('error', "Failed to delete user!");
						}
					}

					header("Location: /admin/users/");
				}

				break;
			case "team":
				$delete_team    = $this->Accounts->get_team($id);
				$delete_allowed = false;

				if($delete_team)
				{
					// We need to make sure that the current user has access to delete this specific team
					// before we allow them to do so
					if($role == 'office manager' && $delete_team['office_id'] == $office_id)
						$delete_allowed = true;
					else if($role == 'company owner' && $delete_team['company_id'] == $company_id)
						$delete_allowed = true;
					else if($role == 'admin' && $delete_team['company_id'] == $company_id)
						$delete_allowed = true;
					else if($role == 'master')
						$delete_allowed = true;

					if($delete_allowed)
					{
						$data['team_info_hide'] = '';

						// Delete the selected team from the db
						if(!empty($id))
						{
							$delete_results = $this->Accounts->delete_team($id);

							if($delete_results)
								log_message('debug', "Successfully deleted team.");
							else
								log_message('error', "Failed to delete team!");
						}
					}
				}

				header("Location: /admin/users/");
				break;
			case "office":
				$delete_office  = $this->Accounts->get_office($id);
				$delete_allowed = false;

				if($delete_office)
				{
					// We need to make sure that the current user has access to delete this specific team
					// before we allow them to do so
					if($role == 'company owner' && $delete_office['company_id'] == $company_id)
						$delete_allowed = true;
					else if($role == 'admin' && $delete_office['company_id'] == $company_id)
						$delete_allowed = true;
					else if($role == 'master')
						$delete_allowed = true;

					if($delete_allowed)
					{
						$data['office_info_hide'] = '';

						// Delete the selected office from the db
						if(!empty($id))
						{
							$delete_results = $this->Accounts->delete_office($id);

							if($delete_results)
								log_message('debug', "Successfully deleted office.");
							else
								log_message('error', "Failed to delete office!");
						}
					}
				}

				header("Location: /admin/users/");
				break;
			case "company":
				$delete_company = $this->Accounts->get_company($id);
				$delete_allowed = false;

				if($delete_company)
				{
					// We need to make sure that the current user has access to delete this specific company
					// before we allow them to do so
					if($role == 'master')
						$delete_allowed = true;

					if($delete_allowed)
					{
						$data['company_info_hide'] = '';

						// Delete the selected company from the db
						if(!empty($id))
						{
							$delete_results = $this->Accounts->delete_company($id);

							if($delete_results)
								log_message('debug', "Successfully deleted company.");
							else
								log_message('error', "Failed to delete company!");
						}
					}
				}

				header("Location: /admin/users/");
				break;
			case "access":
				$delete_allowed = false;

				// We need to make sure that the current user has access to delete this specific access right
				// before we allow them to do so
				if($role == 'master')
					$delete_allowed = true;

				if($delete_allowed)
				{
					$data['access_info_hide'] = '';

					// Delete the selected access right from the db
					if(!empty($id))
					{
						$delete_results = $this->Accounts->delete_access_right($id);

						if($delete_results)
							log_message('debug', "Successfully deleted access right.");
						else
							log_message('error', "Failed to delete access right!");
					}
				}

				header("Location: /admin/users/");
				break;
			default:
				header("Location: /adming/users/");
				break;
		}
	}

	public function getoffices()
	{
		if($this->input->is_ajax_request())
		{
			$company_id   = $this->input->post("id");
			$company_id   = (!empty($company_id)) ? $company_id : 0;
			$organization = $this->Accounts->get_organization($company_id);
			$offices      = (!empty($organization['offices'])) ? $organization['offices'] : array();
			$package      = array();

			foreach($offices as $office)
			{
				$package[] = array('id' => $office['office_id'], 'name' => $office['office_name']);
			}

			echo json_encode($package);
			exit;
		}
		else
			header("Location: /admin/users");
	}
}

/* End of file users.php */
/* Location: ./application/controllers/admin/users.php */
