<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Home
 */
class Home extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('Activity');
		//$this->load->library('Visits');
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_access('admin-dashboard');

		if(!$access_admin)
			header("Location: /admin/login/");

		// Grab the current user's id since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		$start_date = date("Y-m-d H:i:s", strtotime("-30 days"));
		$end_date   = date("Y-m-d H:i:s");

		$contactReport = "";
		$contactReportHtml  = "";

		// Grab dashboard repots
		switch($role)
		{
			case "agent":
			case "team leader":
			case "office manager":
			case "company owner":
			case "admin":
			case "master":
				$contactReport  = $this->Accounts->get_contacts_all($start_date, $end_date);
				break;
		}

		if($contactReport)
		{
			foreach($contactReport as $contact)
			{
				$name       = $contact['first_name'] . " " . $contact['last_name'];
				$email      = $contact['email'];
				$phone      = $contact['phone'];
				$message    = $contact['note'];;
				$date       = date("Y-m-d", strtotime($contact['_created']));


				$contactReportHtml .= "<tr class='odd gradeX'>
					<td>{$name}</td>
					<td>{$email}</td>
					<td>{$phone}</td>
					<td>{$message}</td>
					<td>{$date}</td>
				</tr>\n";
			}
		}

		// Set our home page as the active navbar link
		$nav_active['home'] = 'active';

		$data_nav    = array(
			'active' => $nav_active
		);

		$data_header = array(
			'title'         => 'Lonnie Bush | Admin - Dashboard',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-dashboard',
			'user_name'     => $access_name
		);

		$data        = array(
			'contactReportHtml' => $contactReportHtml
		);

		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/home", $data);
		$this->load->view("admin/footer", $data_footer);
	}
}

/* End of file home.php */
/* Location: ./application/controllers/admin/home.php */

//ob_start();
//var_dump($visits_by_locaton);
//$test = ob_get_clean();
//log_message('error', $test);
