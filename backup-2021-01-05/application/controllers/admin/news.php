<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class News extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper('file');
		$this->load->model('news_model');
	}

	/**
	 * Index Page for this controller.
	 *
	 * We will be assuming in this function that all the content we need to pull exists
	 * in the database so we can skip the empty() checks
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_access('admin-pages');
		
		if(!$access_admin)
			header("Location: /admin/login/");
		
		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "News";
		
		$data = array();
		
		// If the user can view this, but only do that, we'll hide our add/edit/delete buttons
		// and set any visiable form fields to readonly
		$access_update = $this->Accounts->can_user_update('admin-pages');
		$access_create = $this->Accounts->can_user_create('admin-pages');
		$access_delete = $this->Accounts->can_user_delete('admin-pages');
		
		$data['readonly'] = (!$access_update) ? 'disabled' : '';
		
		$data['news_add_button'] = '<a id="user_add" class="btn btn-default btn-sm " href="/admin/news/add/" style="margin-right: 5px;"><i
					class="fa fa-plus"></i> Add Blog Article </a>';
		

		// Set pages as the active navbar link
		$nav_active['news'] = 'active';
		
		$data_nav = array(
			'active' => $nav_active
		);
		
		$data_header = array(
			'title'         => 'Lonnie Bush Property Management | Admin - Blog',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-news',
			'user_name'     => $access_name
		);
		 
		$news = array();
		$news = $this->news_model->get_news();				
		
		foreach ($news as $post ){
			
			
			if ($post['is_blog'] == 1){
				$isblog = "Yes";
			}
			else{
				$isblog = "No";
			}
			
			$data['news'] .= "<tr class='odd gradeX'>
				<td>{$post['title']}</td>
				<td>{$post['slug']}</td>
				<td>{$isblog}</td>
				<td><a href='/admin/news/edit/{$post['slug']}'>Edit</a></td>
			</tr>\n";
			
		}

		
		$data_footer = array(
			'placeholder' => ''
		);
		
		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/news", $data);
		$this->load->view("admin/footer", $data_footer);
	}
	
	public function edit($slug)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$update_post = $this->news_model->get_news($slug);
		$data['update_post'] = $update_post;
		$data['currentslug'] = $slug;
		$data['title'] = 'Edit Article:'.$update_post['title'] ;
		
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'text', 'required');
		$this->form_validation->set_rules('slug', 'Slug', 'required');
	
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view("admin/header", $data_header);
			$this->load->view("admin/navbar", $data_nav);
			$this->load->view('news/edit', $data);
			$this->load->view("admin/footer", $data_footer);	
		}
		else
		{
			if ($this->input->post('is_blog') == "Yes"){
				$isblog = 1;	
			}
			else{
				$isblog = 0;
			}
			$datasubmit = array(
				'title' => $this->input->post('title'),
				'slug' => $this->input->post('slug'),
				'seo_title' => $this->input->post('seo_title'),
				'seo_desc' => $this->input->post('seo_desc'),			
				'text' => $this->input->post('text'),
				'is_blog' => $isblog
			);
			$this->news_model->update_news($datasubmit, $slug);
			$this->save_routes();
			header("Location: /admin/news");
		}
	}
	
	public function save_routes() {
        $routes = $this->news_model->get_all_routes();

        $data = array();

        if (!empty($routes )) {
			$data[] = '<?php';
			
            foreach ($routes as $route) {
                $data[] = '$route[\'' . $route['slug'] . '\'] = \'' . $route['controller'] . '/' . $route['action'] . '/' . $route['slug'] . '\';';
            }
            $output = implode("\n", $data);

            write_file(APPPATH . 'cache/routes.php', $output);
        }
    }
    
	public function add($section = null)
	{
		$this->save_routes();
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$data['title'] = 'Create a news item';
	
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('text', 'text', 'required');
		$this->form_validation->set_rules('slug', 'Slug', 'required');
	
		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view("admin/header", $data_header);
			$this->load->view("admin/navbar", $data_nav);
			$this->load->view('news/add');
			$this->load->view("admin/footer", $data_footer);	
		}
		else
		{
			
			if ($this->input->post('is_blog') == "Yes"){
				$isblog = 1;	
			}
			else{
				$isblog = 0;
			}

			$data = array(
				'title' => $this->input->post('title'),
				'slug' => $this->input->post('slug'),
				'seo_title' => $this->input->post('seo_title'),
				'seo_desc' => $this->input->post('seo_desc'),			
				'text' => $this->input->post('text'),
				'is_blog' => $isblog
			);
			
			$this->news_model->set_news($data);
			$this->save_routes();
			$this->load->view("admin/header", $data_header);
			$this->load->view("admin/navbar", $data_nav);
			$this->load->view('news/success');
			$this->load->view("admin/footer", $data_footer);	
	
		}
	}
	
	
	
	public function upload_image(){
		$upload_filename  = $_FILES['imagefile']['name'];

		if(!empty($upload_filename))
		{
			$filename_prefix  = uniqid();
			$filename_replace = strtolower(str_replace(' ', '_', $upload_filename));
			$filename         = $filename_prefix . "-" . $filename_replace;
			$upload_path      = '/data/images/';

			$config['upload_path']   = '.' . $upload_path;
			$config['allowed_types'] = 'gif|jpg|jpeg|png';
			$config['file_name']     = $filename;
			$config['max_size']      = '2048';
			$config['overwrite']     = true;

			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('imagefile'))
				log_message('error', "Failed to upload file!");
			else
			{
				$upload_data          = $this->upload->data();
				return true;
			}
		}
	}
	public function delete($section = null)
	{
		header("Location: /admin/news/");
	}
}

/* End of file pages.php */
/* Location: ./application/controllers/admin/pages.php */
