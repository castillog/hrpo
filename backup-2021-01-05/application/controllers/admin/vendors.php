<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Vendors
 */
class Vendors extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('Activity');
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_access('admin-vendors');

		if(!$access_admin)
			header("Location: /admin/login/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		$data = array();

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		// We don't actually need to use the acl breakdown here, but we include it for consistancy throughout
		// the site
		if($role == 'agent')
			$criteria = array('user_id' => $user_id);
		else if($role == 'team leader')
			$criteria = array('team_id' => $team_id);
		else if($role == 'office manager')
			$criteria = array('office_id' => $office_id);
		else if($role == 'company owner')
			$criteria = array('company_id' => $company_id);
		else if($role == 'site-agent')
			$criteria = array('company_id' => $company_id);
		else if($role == 'admin')
			$criteria = array('company_id' => $company_id);
		else if($role == 'master')
			$criteria = array('vendor_id >' => 0);
		else
			$criteria = array('vendor_id <' => 0);

		$data['vendor_id'] = '';
		$data['vendors']   = '';

		$vendors = $this->Content->get_vendors_all();

		// If the user can view this, but only do that, we'll hide our add/edit/delete buttons
		// and set any visiable form fields to readonly
		$access_update = $this->Accounts->can_user_update('admin-vendors');
		$access_create = $this->Accounts->can_user_create('admin-vendors');
		$access_delete = $this->Accounts->can_user_delete('admin-vendors');

		$data['readonly'] = (!$access_update) ? 'disabled' : '';

		// Set all our view form field variables to their default values
		$data['vendor_contact'] = '';
		$data['vendor_phone']   = '';
		$data['vendor_email']   = '';
		$data['vendor_logo']    = '';
		$data['vendor_name']    = '';
		$data['vendor_website'] = '';

		$data['vendors'] = "";

		$data['vendor_add_button'] = ($access_create) ? '<a id="vendor_add" class="btn btn-default btn-sm " href="/admin/vendors/add/"
				style="margin-right: 5px;"><i class="fa fa-plus"></i> Add Vendor </a>' : '';

		if(!empty($vendors))
		{
			for($i = 0; $i < count($vendors); $i++)
			{
				$vendor               = $vendors[$i];
				$vendor_edit_button   = ($access_update) ? "<a class='btn default btn-xs green edit'
							href='/admin/vendors/update/{$vendor['vendor_id']}/'><i class='fa fa-edit'></i> Edit </a>\n"
					: "";
				$vendor_delete_button = ($access_delete) ? "<a class='btn default btn-xs red delete'
							href='/admin/vendors/delete/{$vendor['vendor_id']}/'><i class='fa fa-trash-o'></i> Delete </a>\n"
					: "";

				$vendor_logo = (!empty($vendor['logo'])) ? $vendor['logo'] : "/data/images/no-image.png";

				$data['vendors'] .= "<tr class='odd gradeX'>
					<td>{$vendor['name']}</td>
					<td>{$vendor['website']}</td>
					<td><img src='{$vendor_logo}'></td>
					<td>
						{$vendor_edit_button}
						{$vendor_delete_button}
					</td>
				</tr>\n";
			}
		}

		$data['vendor_list_hide'] = '';
		$data['vendor_info_hide'] = 'display-hide';

		// Set vendors as the active navbar link
		$nav_active['vendors'] = 'active';

		$data_nav    = array(
			'active' => $nav_active
		);
		$data_header = array(
			'title'         => 'Lonnie Bush | Admin - Vendors',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-vendors',
			'user_name'     => $access_name
		);
		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/vendors", $data);
		$this->load->view("admin/footer", $data_footer);
	}

	public function update($vendor_id = null)
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_update('admin-vendors');

		if(!$access_admin)
			header("Location: /admin/vendors/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		$data['vendors'] = '';

		// Set all our view form field variables to their default values
		$data['vendor_logo']    = '';
		$data['vendor_contact'] = '';
		$data['vendor_phone']   = '';
		$data['vendor_email']   = '';
		$data['vendor_name']    = '';
		$data['vendor_website'] = '';

		$data['vendors_add_button'] = '';

		if(!empty($vendor_id))
		{
			$vendor = $this->Content->get_vendor($vendor_id);

			$update_allowed = false;

			if($vendor)
			{
				// We need to make sure that the current user has access to update this specific lead
				// before we allow them to do so
				if($role == 'agent' && $vendor['user_id'] == $user_id)
					$update_allowed = true;
				else if($role == 'team leader' && $vendor['team_id'] == $team_id)
					$update_allowed = true;
				else if($role == 'office manager' && $vendor['office_id'] == $office_id)
					$update_allowed = true;
				else if($role == 'company owner' && $vendor['company_id'] == $company_id)
					$update_allowed = true;
				else if($role == 'site-agent' && $vendor['company_id'] == $company_id)
					$update_allowed = true;
				else if($role == 'admin' && $vendor['company_id'] == $company_id)
					$update_allowed = true;
				else if($role == 'master')
					$update_allowed = true;

				if($update_allowed)
				{
					$postback = $this->input->post('vendor_form_submit');

					if($postback)
					{
						$update_data['name']    = $this->input->post('vendor_info_name');
						$update_data['contact'] = $this->input->post('vendor_info_contact');
						$update_data['phone']   = $this->input->post('vendor_info_phone');
						$update_data['email']   = $this->input->post('vendor_info_email');
						$update_data['website'] = $this->input->post('vendor_info_website');

						$upload_filename = $_FILES['vendor_info_logo']['name'];
						$upload_path     = '/data/images/';

						$config['upload_path']   = '.' . $upload_path;
						$config['allowed_types'] = 'gif|jpg|png';
						$config['file_name']     = strtolower($upload_filename);
						$config['max_size']      = '2048';
						$config['overwrite']     = true;

						$this->load->library('upload', $config);

						if(!$this->upload->do_upload('vendor_info_logo'))
							log_message('error', "Failed to upload file!");
						else
						{
							$upload_data          = $this->upload->data();
							$update_data['logo'] = $upload_path . strtolower($upload_data['file_name']);
						}

						// Update the user record in the db
						$update_results = $this->Content->update_vendor($update_data, $vendor_id);

						if($update_results)
						{
							$this->activity->log('UULD');

							log_message('error', "Successfully updated vendor!");
							header("Location: /admin/vendors/");
						}
						else
						{
							log_message('error', "Failed to update vendor!");
							header("Location: /admin/vendors/");
						}
					}
					else
					{
						$data['vendor_name']    = $vendor['name'];
						$data['vendor_contact'] = $vendor['contact'];
						$data['vendor_phone']   = $vendor['phone'];
						$data['vendor_email']   = $vendor['email'];
						$data['vendor_website'] = $vendor['website'];
						$data['vendor_logo']    = $vendor['logo'];
					}
				}
			}
		}

		$data['vendor_list_hide']           = 'display-hide';
		$data['vendor_info_hide']           = '';

		// Set vendors as the active navbar link
		$nav_active = array(
			'home'       => '',
			'pages'      => '',
			'properties' => '',
			'floorplans' => '',
			'lots'       => '',
			'vendors'   => 'active',
			'users'      => '',
			'leads'      => '',
			'settings'   => ''
		);

		// Only show links the user has read access to its page
		$nav_hide['home']       = ''; // No reason to hide the home/dashboard nav, user would be bounced regardless
		$nav_hide['pages']      = (!$this->Accounts->can_user_access('admin-pages')) ? 'display-hide' : '';
		$nav_hide['properties'] = (!$this->Accounts->can_user_access('admin-properties')) ? 'display-hide' : '';
		$nav_hide['floorplans'] = (!$this->Accounts->can_user_access('admin-floorplans')) ? 'display-hide' : '';
		$nav_hide['lots']       = (!$this->Accounts->can_user_access('admin-lots')) ? 'display-hide' : '';
		$nav_hide['vendors']   = (!$this->Accounts->can_user_access('admin-vendors')) ? 'display-hide' : '';
		$nav_hide['users']      = (!$this->Accounts->can_user_access('admin-users')) ? 'display-hide' : '';
		$nav_hide['leads']      = (!$this->Accounts->can_user_access('admin-leads')) ? 'display-hide' : '';
		$nav_hide['settings']   = (!$this->Accounts->can_user_access('admin-settings')) ? 'display-hide' : '';

		$data_nav    = array(
			'hide'   => $nav_hide,
			'active' => $nav_active
		);
		$data_header = array(
			'title'         => 'Lonnie Bush | Admin - Vendors',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-vendors',
			'user_name'     => $access_name
		);
		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/vendors", $data);
		$this->load->view("admin/footer", $data_footer);
	}

	public function add()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_create('admin-vendors');

		if(!$access_admin)
			header("Location: /admin/vendors/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		$data['vendors'] = '';

		// Set all our view form field variables to their default values
		$data['vendor_logo']    = '';
		$data['vendor_contact'] = '';
		$data['vendor_phone']   = '';
		$data['vendor_email']   = '';
		$data['vendor_name']    = '';
		$data['vendor_website'] = '';

		$data['vendors_add_button'] = '';

		$insert_allowed = false;

		// We need to make sure that the current user has access to add a new lead.
		// However since our rights check above is handling the heavy lifting for the moment
		// we'll just include the standard role check code below, but set everything to true
		if($role == 'agent')
			$insert_allowed = true;
		else if($role == 'team leader')
			$insert_allowed = true;
		else if($role == 'office manager')
			$insert_allowed = true;
		else if($role == 'company owner')
			$insert_allowed = true;
		else if($role == 'site-agent')
			$insert_allowed = true;
		else if($role == 'admin')
			$insert_allowed = true;
		else if($role == 'master')
			$insert_allowed = true;

		if($insert_allowed)
		{
			$postback = $this->input->post('vendor_form_submit');

			if($postback)
			{
				$insert_data['name']    = $this->input->post('vendor_info_name');
				$insert_data['contact'] = $this->input->post('vendor_info_contact');
				$insert_data['phone']   = $this->input->post('vendor_info_phone');
				$insert_data['email']   = $this->input->post('vendor_info_email');
				$insert_data['website'] = $this->input->post('vendor_info_website');

				$upload_filename = $_FILES['vendor_info_logo']['name'];
				$upload_path     = '/data/images/';

				$config['upload_path']   = '.' . $upload_path;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name']     = strtolower($upload_filename);
				$config['max_size']      = '2048';
				$config['overwrite']     = true;

				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('vendor_info_logo'))
					log_message('error', "Failed to upload file!");
				else
				{
					$upload_data          = $this->upload->data();
					$insert_data['logo'] = $upload_path . strtolower($upload_data['file_name']);
				}

				// insert the user record in the db
				$insert_results = $this->Content->create_vendor($insert_data);

				if($insert_results)
				{
					$this->activity->log('SAL');

					log_message('error', "Successfully added vendor!");
					header("Location: /admin/vendors/");
				}
				else
				{
					log_message('error', "Failed to add vendor!");
					header("Location: /admin/vendors/");
				}
			}
			else
			{
				$data['vendor_name']    = '';
				$data['vendor_website'] = '';
				$data['vendor_logo']    = '';
			}
		}
		else
			header("Location: /admin/vendors/");

		$data['vendor_list_hide']           = 'display-hide';
		$data['vendor_info_hide']           = '';

		// Set vendors as the active navbar link
		$nav_active['vendors'] = 'active';

		$data_nav    = array(
			'active' => $nav_active
		);
		$data_header = array(
			'title'         => 'Lonnie Bush | Admin - Vendors',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-vendors',
			'user_name'     => $access_name
		);
		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/vendors", $data);
		$this->load->view("admin/footer", $data_footer);
	}

	public function delete($vendor_id = null)
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_delete('admin-vendors');

		if(!$access_admin)
			header("Location: /admin/vendors/");

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		if(!empty($vendor_id))
		{
			$delete_vendor    = $this->Content->get_vendor($vendor_id);
			$delete_allowed = false;

			if($delete_vendor)
			{
				// We need to make sure that the current user has access to delete this specific user
				// before we allow them to do so
				if($role == 'team leader' && $delete_vendor['team_id'] == $team_id)
					$delete_allowed = true;
				else if($role == 'office manager' && $delete_vendor['office_id'] == $office_id)
					$delete_allowed = true;
				else if($role == 'company owner' && $delete_vendor['company_id'] == $company_id)
					$delete_allowed = true;
				else if($role == 'site-agent' && $delete_vendor['company_id'] == $company_id)
					$delete_allowed = true;
				else if($role == 'admin' && $delete_vendor['company_id'] == $company_id)
					$delete_allowed = true;
				else if($role == 'master')
					$delete_allowed = true;

				if($delete_allowed)
				{
					$delete_results = $this->Content->delete_vendor($vendor_id);

					if($delete_results)
					{
						log_message('error', "Successfully deleted vendor!");
						header("Location: /admin/vendors/");
					}
					else
					{
						log_message('error', "Failed to delete vendor!");
						header("Location: /admin/vendors/");
					}
				}
				else
					header("Location: /admin/vendors/");
			}
		}
		else
			header("Location: /admin/vendors/");
	}
}

/* End of file Vendors.php */
/* Location: ./application/controllers/admin/Vendors.php */
