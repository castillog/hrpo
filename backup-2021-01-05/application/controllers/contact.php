<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Contact
 */
class Contact extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab our company settings content data
		$content = $this->Content->get_content("settings");

		$company_name          = $content['company']['name'];
		$company_street        = $content['company']['street'];
		$company_city          = $content['company']['city'];
		$company_state         = $content['company']['state'];
		$company_zipcode       = $content['company']['zipcode'];
		$company_phone         = $content['company']['phone'];
		$company_email         = $content['company']['email'];
		$company_lead_email    = $content['company']['lead_email'];
		$company_facebook_url  = $content['company']['facebook_url'];
		$company_twitter_url   = $content['company']['twitter_url'];
		$company_google_url    = $content['company']['google_url'];
		$company_linkedin_url  = $content['company']['linkedin_url'];
		$company_youtube_url   = $content['company']['youtube_url'];
		$company_pinterest_url = $content['company']['pinterest_url'];

		$company_address = $company_street . ", " . $company_city . ", " . $company_state . " " . $company_zipcode;
		$latitude        = 36.817318;
		$longitude       = -76.065741;
		//$coords          = $this->_get_coords($company_address);

		if(!empty($coords))
		{
			$latitude  = $coords['latitude'];
			$longitude = $coords['longitude'];
		}

		$flash_class   = "";
		$flash_msg     = "";
		$footer_script = "";

		// Set contact as the active navbar link
		$nav_active['about'] = 'active';

		$footer_script =
			"<script src='https://www.google.com/recaptcha/api.js'></script>
			<script>
				_latitude = {$latitude};
				_longitude = {$longitude};
				google.maps.event.addDomListener(window, 'load', init_map(_latitude, _longitude));
			</script>";

		$data_header = array(
			'title'       => 'Contact for Property Management in Hampton Roads | Lonnie Bush Property Management',
			'description' => 'Hampton Roads Property Management with Lonnie Bush Property Management. Area leads in professional property management services',
			'keywords'    => 'Hampton Roads Property Management, Hampton Roads Real Estate Rental, Renting Residential Property, Hampton Roads Homes for Rent',
			'bodyClass'   => 'page-contact',
			'nav_active'  => $nav_active
		);
		$data        = array(
			'flash_class'       => $flash_class,
			'flash_msg'         => $flash_msg,
			'company_name'      => $company_name,
			'company_street'    => $company_street,
			'company_city'      => $company_city,
			'company_state'     => $company_state,
			'company_zipcode'   => $company_zipcode,
			'company_phone'     => $company_phone,
			'company_email'     => $company_email,
			'company_facebook'  => $company_facebook_url,
			'company_twitter'   => $company_twitter_url,
			'company_linkedin'  => $company_linkedin_url,
			'company_google'    => $company_google_url,
			'company_youtube'   => $company_youtube_url,
			'company_pinterest' => $company_pinterest_url,
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('contact', $data);
		$this->load->view('footer', $data_footer);
	}

	public function send($type = null)
	{
		if($this->input->is_ajax_request())
		{

			// Grab our company settings content data
			$content    = $this->Content->get_content("settings");
			//$lead_email = (!empty($content['company']['lead_email'])) ? $content['company']['lead_email'] : $this->default->default_email_address;
			$lead_email = $this->config->item('default_email_address');

			$email_body = "";
			$email_to   = $lead_email;
			$email_from    = $this->config->item('default_from_email_address');
			$email_subject = "Contact Form from Lonnie Bush Property Management";

			// Grab all of our posted fields
			$name      = "";
			$firstname = "";
			$lastname  = "";
			$phone     = "";
			$email     = "";
			$city      = "";
			$message   = "";

			switch($type)
			{
				case "manage":
					$name = $this->input->post('owner_name');
					$name = (!empty($name)) ? $name : "invalid";

					$phone = $this->input->post('owner_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('owner_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "invalid";

					$city = $this->input->post('owner_city');
					$city = (!empty($city)) ? $city : "invalid";

					if($name == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid first name."
						);
						echo json_encode($package);
						exit;
					}

					/*
					if($phone == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid phone number."
						);
						echo json_encode($package);
						exit;
					}
					*/

					if($email == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid email."
						);
						echo json_encode($package);
						exit;
					}

					if($city == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a city of the property."
						);
						echo json_encode($package);
						exit;
					}

					$nameSplit = (!empty($name)) ? explode(' ', $name) : "";

					if(!empty($name) && empty($firstname))
					{
						if(count($nameSplit) > 1)
						{
							$firstname = $nameSplit[0];
							$lastname  = $nameSplit[1];
						}
						else if(!empty($name))
						{
							$firstname = $name;
						}
					}

					// Format our phone number to the standard (###) ###-#### if possible
					$phone_formatted = "";
					if(strlen($phone) == 7)
					{
						$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
					}
					else if(strlen($phone) == 10)
					{
						$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
					}
					else
						$phone_formatted = $phone;

					$phone = $phone_formatted;

					$email_body .= "A visitor to Lonnie Bush Property Management has submitted a message via the Manage My Property form::<br><br>
						Name: {$name}<br>
						Phone: {$phone}<br>
						Email: {$email}<br>
						City of Property: {$city}<br><br>
						- Lonnie Bush Property Management";

					break;
				case "ask":
					$firstname = $this->input->post('ask_question_firstname');
					$firstname = (!empty($firstname)) ? $firstname : "invalid";

					$lastname = $this->input->post('ask_question_lastname');
					$lastname = (!empty($lastname)) ? $lastname : "invalid";

					$phone = $this->input->post('ask_question_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('ask_question_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "invalid";

					$message = $this->input->post('ask_question_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "invalid";

					if($firstname == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid first name."
						);
						echo json_encode($package);
						exit;
					}

					if($lastname == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid last name."
						);
						echo json_encode($package);
						exit;
					}

					/*
					if($phone == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid phone number."
						);
						echo json_encode($package);
						exit;
					}
					*/

					if($email == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid email."
						);
						echo json_encode($package);
						exit;
					}

					if($message == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid message."
						);
						echo json_encode($package);
						exit;
					}

					// Format our phone number to the standard (###) ###-#### if possible
					$phone_formatted = "";
					if(strlen($phone) == 7)
					{
						$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
					}
					else if(strlen($phone) == 10)
					{
						$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
					}
					else
						$phone_formatted = $phone;

					$phone = $phone_formatted;

					$email_body .= "A visitor to Lonnie Bush Property Management has submitted a message via the Ask A Question form::<br><br>
						Name: {$firstname} {$lastname}<br>
						Phone: {$phone}<br>
						Email: {$email}<br>
						Message: {$message}<br><br>
						- Lonnie Bush Property Management";

					break;
				case "contact":
					$name = $this->input->post('contact_us_name');
					$name = (!empty($name) && preg_match('/\w+/', $name) !== false) ? $name : "invalid";

					$email = $this->input->post('contact_us_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "invalid";

					$message = $this->input->post('contact_us_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "invalid";

					// Grab the recaptcha response code and client ip address
					$recaptcha_response = $this->input->get_post('g-recaptcha-response');
					$recaptcha_response = (!empty($recaptcha_response)) ? $recaptcha_response : "invalid";

					$contact_ip = $this->input->get_post('contact_ip');
					$contact_ip = (!empty($contact_ip)) ? $contact_ip : "invalid";

					//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					if($recaptcha_response == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must be a real human!.",
							"id" => 0
						);
						echo json_encode($package);
						exit;
					}
					else
					{
						// Check with Google to verify the recaptcha response
						$google_check = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LfCVI4UAAAAABnP-lOp7TCDTxjsf5Ak1pPLUV-S&response={$recaptcha_response}&remoteip={$contact_ip}");
						$google_check = (!empty($google_check)) ? json_decode($google_check, true) : array();

						if(empty($google_check['success']))
						{
							$package = array(
								"status"  => "error",
								"message" => "Failed! You must be a real human!.",
								"id" => 0
							);
							echo json_encode($package);
							exit;
						}
					}

					if($name == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid name."
						);
						echo json_encode($package);
						exit;
					}

					if($email == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid email."
						);
						echo json_encode($package);
						exit;
					}

					if($message == "invalid")
					{
						$package = array(
							"status"  => "error",
							"message" => "Failed! You must enter a valid message."
						);
						echo json_encode($package);
						exit;
					}

					$nameSplit = (!empty($name)) ? explode(' ', $name) : "";

					if(!empty($name) && empty($firstname))
					{
						if(count($nameSplit) > 1)
						{
							$firstname = $nameSplit[0];
							$lastname  = $nameSplit[1];
						}
						else if(!empty($name))
						{
							$firstname = $name;
						}
					}

					$email_body .= "A visitor to Lonnie Bush Property Management has submitted a message via the Contact form::<br><br>
						Name: {$name}<br>
						Email: {$email}<br>
						Message: {$message}<br><br>
						- Lonnie Bush Property Management";

					break;
				default:
					break;
			}

			$this->email->from($email_from, "Lonnie Bush Property Management");
			$this->email->to($email_to);

			$this->email->subject($email_subject);
			$this->email->message($email_body);

			$email_results = $this->email->send();

			if($email_results)
			{
				// Store this contact request in the db
				$insert_data    = array(
					'first_name' => $firstname,
					'last_name'  => $lastname,
					'email'      => $email,
					'phone'      => $phone,
					'city'       => $city,
					'note'       => $message
				);
				$insert_results = $this->Accounts->create_contact($insert_data);

				if($insert_results)
				{
					log_message('debug', "Successfully inserted contact request to db.");
				}
				else
				{
					log_message('error', "Failed to insert contact request to db.");
				}

				$package = array(
					"status"  => "success",
					"message" => "Your contact request has been successfully submitted."
				);
				echo json_encode($package);
				exit;
			}
			else
			{
				$package = array(
					"status"  => "error",
					"message" => "Failed! Unable to submit your contact request at this time."
				);
				echo json_encode($package);
				exit;
			}
		}
	}

	public function bombbomb($type = null)
	{
		if($this->input->is_ajax_request())
		{
			// Grab all of our posted fields
			$name      = "";
			$firstname = "";
			$lastname  = "";
			$phone     = "";
			$email     = "";
			$city      = "";
			$message   = "";

			switch($type)
			{
				case "manage":
					$name = $this->input->post('owner_name');
					$name = (!empty($name)) ? $name : "N/A";

					$phone = $this->input->post('owner_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('owner_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$city = $this->input->post('owner_city');
					$city = (!empty($city)) ? $city : "N/A";

					//log_message('error', "Name: " . $name . ", Email: " . $email . ", Phone: " . $phone . ", City: " . $city);

					break;
				case "ask":
					$firstname = $this->input->post('ask_question_firstname');
					$firstname = (!empty($firstname)) ? $firstname : "N/A";

					$lastname = $this->input->post('ask_question_lastname');
					$lastname = (!empty($lastname)) ? $lastname : "N/A";

					$phone = $this->input->post('ask_question_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('ask_question_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('ask_question_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " . $firstname . " " . $lastname . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
				default:
					$name = $this->input->post('contact_us_name');
					$name = (!empty($name) && preg_match('/\w+/', $name) !== false) ? $name : "N/A";

					$email = $this->input->post('contact_us_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('contact_us_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
			}

			$nameSplit = (!empty($name)) ? explode(' ', $name) : "";

			if(!empty($name) && empty($firstname))
			{
				if(count($nameSplit) > 1)
				{
					$firstname = $nameSplit[0];
					$lastname  = $nameSplit[1];
				}
				else if(!empty($name))
				{
					$firstname = $name;
					$lastname  = "N/A";
				}
			}

			// Format our phone number to the standard (###) ###-#### if possible
			$phone_formatted = "";
			if(strlen($phone) == 7)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			}
			else if(strlen($phone) == 10)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
			}
			else
				$phone_formatted = $phone;

			$phone = $phone_formatted;

			// Grab our api integration content in case this person has BombBomb's API setup
			// For this site, we need to hardcode our agent_id
			$agent_id         = 4;
			$agent            = $this->Accounts->get_user_by_id($agent_id);
			$bombbomb_api_key = (!empty($agent['bombbomb_api_key'])) ? $agent['bombbomb_api_key'] : "";
			$bombbomb_list_id = (!empty($agent['bombbomb_list_id'])) ? $agent['bombbomb_list_id'] : "";

			// If this lead was unassigned, we need to check the company's settings to make sure there isn't
			// an override set to route leads to
			if(empty($agent))
			{
				$content_settings = $this->Content->get_content("settings");
				$lead_default_id  = (!empty($content_settings['company']['lead_default'])) ? $content_settings['company']['lead_default'] : 0;
				$lead_default     = $this->Accounts->get_user_by_id($lead_default_id);

				if(!empty($lead_default))
				{
					$bombbomb_api_key = (!empty($lead_default['bombbomb_api_key'])) ? $lead_default['bombbomb_api_key'] : "";
					$bombbomb_list_id = (!empty($lead_default['bombbomb_list_id'])) ? $lead_default['bombbomb_list_id'] : "";
				}

			}

			if(!empty($bombbomb_api_key))
			{
				$this->load->library("Bombbomb", array('api_key' => $bombbomb_api_key));

				$contact_data = array(
					'firstname'      => $firstname,
					'lastname'       => $lastname,
					'eml'            => $email,
					'phone_number'   => $phone,
					'address_line_1' => '',
					'city'           => $city,
					'state'          => '',
					'postal_code'    => '',
					'comments'       => 'http://hamptonroadspropertyowners.com',
					'listlist'       => $bombbomb_list_id
				);

				$bombbomb_results = $this->bombbomb->add_contact($contact_data);

				if($bombbomb_results)
				{
					log_message('debug', "Successfully added contact to BombBomb contact list.");
					$package = array(
						"status"  => "success",
						"message" => "Successfully added contact to BombBomb contact list."
					);
					echo json_encode($package);
					exit;
				}
				else
				{
					log_message('error', "Failed to add contact to BombBomb contact list.");
					$package = array(
						"status"  => "error",
						"message" => "Failed to add contact to BombBomb contact list."
					);
					echo json_encode($package);
					exit;
				}
			}
			else
			{
				log_message('debug', "Skipped! BombBomb API Not Setup!");
				$package = array(
					"status"  => "skipped",
					"message" => "Skipped! BombBomb API Not Setup!"
				);
				echo json_encode($package);
				exit;
			}
		}
	}

	public function rew($type = null)
	{
		if($this->input->is_ajax_request())
		{
			// Grab all of our posted fields
			$name      = "";
			$firstname = "";
			$lastname  = "";
			$phone     = "";
			$email     = "";
			$city      = "";
			$message   = "";

			switch($type)
			{
				case "manage":
					$name = $this->input->post('owner_name');
					$name = (!empty($name)) ? $name : "N/A";

					$phone = $this->input->post('owner_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('owner_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$city = $this->input->post('owner_city');
					$city = (!empty($city)) ? $city : "N/A";

					//log_message('error', "Name: " . $name . ", Email: " . $email . ", Phone: " . $phone . ", City: " . $city);

					break;
				case "ask":
					$firstname = $this->input->post('ask_question_firstname');
					$firstname = (!empty($firstname)) ? $firstname : "N/A";

					$lastname = $this->input->post('ask_question_lastname');
					$lastname = (!empty($lastname)) ? $lastname : "N/A";

					$phone = $this->input->post('ask_question_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('ask_question_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('ask_question_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " . $firstname . " " . $lastname . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
				default:
					$name = $this->input->post('contact_us_name');
					$name = (!empty($name) && preg_match('/\w+/', $name) !== false) ? $name : "N/A";

					$email = $this->input->post('contact_us_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('contact_us_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
			}

			$nameSplit = (!empty($name)) ? explode(' ', $name) : "";

			if(!empty($name) && empty($firstname))
			{
				if(count($nameSplit) > 1)
				{
					$firstname = $nameSplit[0];
					$lastname  = $nameSplit[1];
				}
				else if(!empty($name))
				{
					$firstname = $name;
					$lastname  = "N/A";
				}
			}

			// Format our phone number to the standard (###) ###-#### if possible
			$phone_formatted = "";
			if(strlen($phone) == 7)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			}
			else if(strlen($phone) == 10)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
			}
			else
				$phone_formatted = $phone;

			$phone = $phone_formatted;

			// Grab our api integration content in case this person has BombBomb's API setup
			// For this site, we need to hardcode our agent_id
			$agent_id    = 4;
			$agent       = $this->Accounts->get_user_by_id($agent_id);
			$rew_domain  = (!empty($agent['rew_domain'])) ? $agent['rew_domain'] : "";
			$rew_api_key = (!empty($agent['rew_api_key'])) ? $agent['rew_api_key'] : "";
			$rew_list_id = (!empty($agent['rew_list_id'])) ? $agent['rew_list_id'] : "";

			// If this lead was unassigned, we need to check the company's settings to make sure there isn't
			// an override set to route leads to
			if(empty($agent))
			{
				$content_settings = $this->Content->get_content("settings");
				$lead_default_id  = (!empty($content_settings['company']['lead_default'])) ? $content_settings['company']['lead_default'] : 0;
				$lead_default     = $this->Accounts->get_user_by_id($lead_default_id);

				$rew_domain  = (!empty($lead_default['rew_domain'])) ? $lead_default['rew_domain'] : "";
				$rew_api_key = (!empty($lead_default['rew_api_key'])) ? $lead_default['rew_api_key'] : "";
				$rew_list_id = (!empty($lead_default['rew_list_id'])) ? $lead_default['rew_list_id'] : "";
			}

			if(!empty($rew_domain) && !empty($rew_api_key))
			{
				$this->load->library("REW", array('domain' => $rew_domain, 'api_key' => $rew_api_key));

				$contact_data = array(
					'first_name'    => $firstname,
					'last_name'     => $lastname,
					'email'         => $email,
					'phone'         => $phone,
					'address'       => '',
					'city'          => $city,
					'state'         => '',
					'zip'           => '',
					'origin'        => 'HamptonRoadsPropertyOwners.com',
					"opt_marketing" => "in",
					"opt_searches"  => "in",
					'groups'        => array($rew_list_id)
				);

				$rew_results = $this->rew->add_lead($contact_data);

				if($rew_results)
				{
					log_message('debug', "Successfully added contact to REW account.");
					$package = array(
						"status"  => "success",
						"message" => "Successfully added contact to REW account."
					);
					echo json_encode($package);
					exit;
				}
				else
				{
					log_message('error', "Failed to add contact to REW account.");
					$package = array(
						"status"  => "error",
						"message" => "Failed to add contact to REW account."
					);
					echo json_encode($package);
					exit;
				}
			}
			else
			{
				log_message('debug', "Skipped! REW API Not Setup!");
				$package = array(
					"status"  => "skipped",
					"message" => "Skipped! REW API Not Setup!"
				);
				echo json_encode($package);
				exit;
			}
		}
	}

	public function tp($type = null)
	{
		if($this->input->is_ajax_request())
		{
			// Grab all of our posted fields
			$name      = "";
			$firstname = "";
			$lastname  = "";
			$phone     = "";
			$email     = "";
			$city      = "";
			$message   = "";

			switch($type)
			{
				case "manage":
					$name = $this->input->post('owner_name');
					$name = (!empty($name)) ? $name : "N/A";

					$phone = $this->input->post('owner_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('owner_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$city = $this->input->post('owner_city');
					$city = (!empty($city)) ? $city : "N/A";

					//log_message('error', "Name: " . $name . ", Email: " . $email . ", Phone: " . $phone . ", City: " . $city);

					break;
				case "ask":
					$firstname = $this->input->post('ask_question_firstname');
					$firstname = (!empty($firstname)) ? $firstname : "N/A";

					$lastname = $this->input->post('ask_question_lastname');
					$lastname = (!empty($lastname)) ? $lastname : "N/A";

					$phone = $this->input->post('ask_question_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('ask_question_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('ask_question_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " . $firstname . " " . $lastname . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
				default:
					$name = $this->input->post('contact_us_name');
					$name = (!empty($name) && preg_match('/\w+/', $name) !== false) ? $name : "N/A";

					$email = $this->input->post('contact_us_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('contact_us_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
			}

			$nameSplit = (!empty($name)) ? explode(' ', $name) : "";

			if(!empty($name) && empty($firstname))
			{
				if(count($nameSplit) > 1)
				{
					$firstname = $nameSplit[0];
					$lastname  = $nameSplit[1];
				}
				else if(!empty($name))
				{
					$firstname = $name;
					$lastname  = "N/A";
				}
			}

			// Format our phone number to the standard (###) ###-#### if possible
			$phone_formatted = "";
			if(strlen($phone) == 7)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			}
			else if(strlen($phone) == 10)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
			}
			else
				$phone_formatted = $phone;

			$phone = $phone_formatted;

			// Grab our integration content in case this person has TP's API setup
			// For this site, we need to hardcode our agent_id
			$agent_id      = 4;
			$agent         = $this->Accounts->get_user_by_id($agent_id);
			$tp_lead_email = (!empty($agent['tp_lead_email'])) ? $agent['tp_lead_email'] : "";

			// If this lead was unassigned, we need to check the company's settings to make sure there isn't
			// an override set to route leads to
			if(empty($agent))
			{
				$content_settings = $this->Content->get_content("settings");
				$lead_default_id  = (!empty($content_settings['company']['lead_default'])) ? $content_settings['company']['lead_default'] : 0;
				$lead_default     = $this->Accounts->get_user_by_id($lead_default_id);

				$tp_lead_email = (!empty($lead_default['tp_lead_email'])) ? $lead_default['tp_lead_email'] : "";
			}

			if(!empty($tp_lead_email))
			{
				// Send lead xml email to user's TP lead email address
				$email_to = $tp_lead_email;
				$email_from    = "no-reply@hamptonroadspropertyowners.com";
				$email_subject = (!empty($name)) ? "New Lead ($name) registered at HamptonRoadsPropertyOwners.com" : "New Lead registered at HamptonRoadsPropertyOwners.com";

				$email_body = "Source: HamptonRoadsPropertyOwners.com \n";
				$email_body .= "Name: $name \n";
				$email_body .= "Email: $email \n";
				$email_body .= "Phone: $phone \n";
				$email_body .= "Address: \n";
				$email_body .= "MLS Number: \n";
				$email_body .= "Notes: $message \n";

				$this->email->from($email_from, "Hampton Roads Property Owners");
				$this->email->to($email_to);

				$this->email->subject($email_subject);
				$this->email->message($email_body);

				$email_results = $this->email->send();

				if($email_results)
				{
					log_message('debug', "Successfully sent contact to TP account.");
					$package = array(
						"status"  => "success",
						"message" => "Successfully sent contact to TP account."
					);
					echo json_encode($package);
					exit;
				}
				else
				{
					log_message('error', "Failed to send contact to TP account.");
					$package = array(
						"status"  => "error",
						"message" => "Failed to send contact to TP account."
					);
					echo json_encode($package);
					exit;
				}
			}
			else
			{
				log_message('debug', "Skipped! TP API Not Setup!");
				$package = array(
					"status"  => "skipped",
					"message" => "Skipped! TP API Not Setup!"
				);
				echo json_encode($package);
				exit;
			}
		}
	}

	public function boomtown($type = null)
	{
		if($this->input->is_ajax_request())
		{
			// Grab all of our posted fields
			$name      = "";
			$firstname = "";
			$lastname  = "";
			$phone     = "";
			$email     = "";
			$city      = "";
			$message   = "";

			switch($type)
			{
				case "manage":
					$name = $this->input->post('owner_name');
					$name = (!empty($name)) ? $name : "N/A";

					$phone = $this->input->post('owner_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('owner_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$city = $this->input->post('owner_city');
					$city = (!empty($city)) ? $city : "N/A";

					//log_message('error', "Name: " . $name . ", Email: " . $email . ", Phone: " . $phone . ", City: " . $city);

					break;
				case "ask":
					$firstname = $this->input->post('ask_question_firstname');
					$firstname = (!empty($firstname)) ? $firstname : "N/A";

					$lastname = $this->input->post('ask_question_lastname');
					$lastname = (!empty($lastname)) ? $lastname : "N/A";

					$phone = $this->input->post('ask_question_phone');
					$phone = (!empty($phone)) ? preg_replace('/\D/', '', $phone) : "";
					$phone = (!empty($phone)) ? $phone : "N/A";

					$email = $this->input->post('ask_question_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('ask_question_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " . $firstname . " " . $lastname . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
				default:
					$name = $this->input->post('contact_us_name');
					$name = (!empty($name) && preg_match('/\w+/', $name) !== false) ? $name : "N/A";

					$email = $this->input->post('contact_us_email');
					$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "N/A";

					$message = $this->input->post('contact_us_message');
					$message = (!empty($message) && preg_match('/\w+/', $message) !== false) ? $message : "N/A";

					//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

					break;
			}

			$nameSplit = (!empty($name)) ? explode(' ', $name) : "";

			if(!empty($name) && empty($firstname))
			{
				if(count($nameSplit) > 1)
				{
					$firstname = $nameSplit[0];
					$lastname  = $nameSplit[1];
				}
				else if(!empty($name))
				{
					$firstname = $name;
					$lastname  = "N/A";
				}
			}

			// Format our phone number to the standard (###) ###-#### if possible
			$phone_formatted = "";
			if(strlen($phone) == 7)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
			}
			else if(strlen($phone) == 10)
			{
				$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
			}
			else
				$phone_formatted = $phone;

			$phone = $phone_formatted;

			// Grab our integration content in case this person has BoomTown's API setup
			// For this site, we need to hardcode our agent_id
			$agent_id            = 4;
			$agent               = $this->Accounts->get_user_by_id($agent_id);
			$boomtown_lead_email = (!empty($agent['boomtown_lead_email'])) ? $agent['boomtown_lead_email'] : "";

			$agent_first = (!empty($agent['first_name'])) ? $agent['first_name'] : "n/a";
			$agent_last  = (!empty($agent['last_name'])) ? $agent['last_name'] : "n/a";
			$agent_name  = $agent_first . " " . $agent_last;

			// If this lead was unassigned, we need to check the company's settings to make sure there isn't
			// an override set to route leads to
			if(empty($agent))
			{
				$content_settings = $this->Content->get_content("settings");
				$lead_default_id  = (!empty($content_settings['company']['lead_default'])) ? $content_settings['company']['lead_default'] : 0;
				$lead_default     = $this->Accounts->get_user_by_id($lead_default_id);

				$boomtown_lead_email = (!empty($lead_default['boomtown_lead_email'])) ? $lead_default['boomtown_lead_email'] : "";

				$agent_first = (!empty($lead_default['first_name'])) ? $lead_default['first_name'] : "n/a";
				$agent_last  = (!empty($lead_default['last_name'])) ? $lead_default['last_name'] : "n/a";
				$agent_name  = $agent_first . " " . $agent_last;
			}

			if(!empty($boomtown_lead_email))
			{
				// Send lead xml email to user's TP lead email address
				$email_to      = $boomtown_lead_email;
				$email_to      = $this->config->item('default_email_address');
				$email_from    = $this->config->item('default_from_email_address');
				$email_subject = (!empty($name)) ? "New Lead ($name) registered at HamptonRoadsPropertyOwners.com" : "New Lead registered at HamptonRoadsPropertyOwners.com";

				$email_body = "[New Account Registered - Hampton Roads Property Owners]:<br><br>
				Name: {$firstname} {$lastname}<br>
				Email: {$email}<br>
				Phone: {$phone_formatted}<br><br>
				Agent: {$agent_name}<br><br>
				Message: {$message}<br><br>
				- Hampton Roads Property Owners<br><br>

				=== BoomTown Lead Parser Block ===
				<span hidden>
					<xml id = 'contactDetails'>
						<contact>
						<firstName>{$firstname}</firstName>
						<lastName>{$lastname}</lastName>
						<email>{$email}</email>
						<phone>{$phone_formatted}</phone>
						<address></address>
						<city>{$city}</city>
						<state></state>
						<zip></zip>
						<isBuyer>True</isBuyer>
						<isSeller>False</isSeller>
						<source>Hampton Roads Property Owners</source>
						<note>{$message}</note>
						</contact>
					</xml>
				</span>";

				$this->email->from($email_from, "Hampton Roads Property Owners");
				$this->email->to($email_to);

				$this->email->subject($email_subject);
				$this->email->message($email_body);

				$email_results = $this->email->send();

				if($email_results)
				{
					log_message('debug', "Successfully sent contact to BoomTown account.");
					$package = array(
						"status"  => "success",
						"message" => "Successfully sent contact to BoomTown account."
					);
					echo json_encode($package);
					exit;
				}
				else
				{
					log_message('error', "Failed to send contact to BoomTown account.");
					$package = array(
						"status"  => "error",
						"message" => "Failed to send contact to BoomTown account."
					);
					echo json_encode($package);
					exit;
				}
			}
			else
			{
				log_message('debug', "Skipped! BoomTown API Not Setup!");
				$package = array(
					"status"  => "skipped",
					"message" => "Skipped! BoomTown API Not Setup!"
				);
				echo json_encode($package);
				exit;
			}
		}
	}

	private function _get_coords($address)
	{
		if(!empty($address))
		{
			$address = urlencode($address);
			$region  = 'USA';
			$url     = "http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region&key=AIzaSyDwMOguzqjGF9fvJNyIwHNCa6wIWK7Py6s";
			$ch      = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
			$response = curl_exec($ch);
			curl_close($ch);
			$results = json_decode($response);

			if(!empty($results) && $results->status == "OK")
			{
				$latitude  = $results->results[0]->geometry->location->lat;
				$longitude = $results->results[0]->geometry->location->lng;

				$coord = array('latitude' => $latitude, 'longitude' => $longitude);

				return $coord;
			}
			else
				return false;
		}
	}
}

/* End of file contact.php */
/* Location: ./application/controllers/contact.php */
