<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Team extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab our about page content data
		$content = $this->Content->get_content("team");
		
		$title = $content['content']['title'];
		$byline = $content['content']['byline'];
		$text = $content['content']['text'];
		$text = '';
		
		// Grab all of our current active agents from the db
		$criteria = array(
			'enabled'    => true,
			'featured'  => true,
			'company_id' => 1
		);
		$agents     = $this->Accounts->get_users($criteria, "first_name");
		$agentsHtml = "";
		
		if($agents)
		{
			$agentsHtml .= "<div class='row' style='margin-bottom: 25px;'>\n";
			
			for($i = 0; $i < count($agents); $i++)
			{
				$agent = $agents[$i];
				
				$firstname   = (!empty($agent['first_name'])) ? $agent['first_name'] : "";
				$lastname    = (!empty($agent['last_name'])) ? $agent['last_name'] : "";
				$name        = (!empty($firstname) && !empty($lastname)) ? $firstname . " " . $lastname : "N/A";
				$image       = (!empty($agent['image'])) ? $agent['image'] : "/data/images/user.png";
				$agent_title = (!empty($agent['title'])) ? $agent['title'] : "Agent";
				$email       = (!empty($agent['email'])) ? $agent['email'] : "N/A";
				$phone       = (!empty($agent['phone'])) ? $agent['phone'] : "";
				$phone       = preg_replace('/\W/', '', $phone);
				
				// Format our phone number to the standard (###) ###-#### if possible
				$phone_formatted = "";
				if(strlen($phone) == 7)
				{
					$phone_formatted = preg_replace("/([0-9A-Z]{3})([0-9A-Z]{4})/", "$1-$2", $phone);
				}
				else if(strlen($phone) == 10)
				{
					$phone_formatted = preg_replace("/([0-9]{3})([0-9A-Z]{3})([0-9A-Z]{4})/", "($1) $2-$3", $phone);
				}
				else
					$phone_formatted = $phone;
				
				$phone = $phone_formatted;
				
				$phoneHtml = "";
				
				if(!empty($phone))
				{
					$phoneHtml .=
						"<div class='team-phone'><i class='fa fa-phone'></i> {$phone}</div>\n";
				}
				
				$slug = preg_replace('/\s/', '-', preg_replace('/[^\w\s]/', '', $name));
				
				$agentsHtml .=
					"<div class='team-item col-xs-12 col-sm-4 col-md-3 col-lg-2'>
						<div class='team-image'>
							<a href='/team/agent/{$agent['user_id']}/{$slug}/'>
								<img class='' src='/data/images/user_empty.png' style='width:100%;height:auto;background:url({$image}) no-repeat center/cover;'>
							</a>
						</div>
						<div class='team-info'>
							<h4 class='team-name'>{$name}</h4>
							<div class='team-title'>{$agent_title}</div>
							{$phoneHtml}
						</div>
					</div>\n";
				
				//if(($i + 1) % 3 == 0)
				//	$agentsHtml .=
				//		"</div>
				//			<div class='row' style='margin-bottom: 25px;'>\n";
			}
			
			$agentsHtml .= "</div>\n";
		}
		else
		{
			$agentsHtml .=
				"<li class='team-item col-md-12'>
					<header class='team-head'>
						<h4 class='team-name text-center'>There are no agents to display at this time.</h4>
					</header>
				</li>\n";
		}
		
		// Set about as the active navbar link
		$nav_active['about'] = 'active';
		
		$footer_script = "";
		
		$data_header = array(
			'title'         => 'Meet The Team | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-team',
			'nav_active'    => $nav_active
		);
		
		$data = array(
			'title'      => $title,
			'byline'     => $byline,
			'text'       => $text,
			'agentsHtml' => $agentsHtml
		);
		
		$data_footer = array(
			'footer_script' => $footer_script
		);
		
		$this->load->view('header', $data_header);
		$this->load->view('team', $data);
		$this->load->view('footer', $data_footer);
	}
	
	public function agent($id = null, $slug = null)
	{
		// Grab our about page content data
		$content = $this->Content->get_content("team");
		
		$title  = $content['content']['title'];
		$byline = $content['content']['byline'];
		$text   = $content['content']['text'];
		
		$firstname      = "";
		$lastname       = "";
		$name           = "";
		$team_name      = "";
		$image          = "";
		$agent_title    = "";
		$email          = "";
		$phone          = "";
		$mobile         = "";
		$website        = "";
		$location       = "";
		$bio            = "";
		$facebook_link  = "";
		$twitter_link   = "";
		$linkedin_link  = "";
		$youtube_link   = "";
		$instagram_link = "";
		$zillow_ratings = "";
		$trulia_ratings = "";
		
		if(!empty($id))
		{
			// Grab our current active agent from the db
			$agent = $this->Accounts->get_user_by_id($id);
			
			if(!empty($agent))
			{
				$firstname = (!empty($agent['first_name'])) ? $agent['first_name'] : "";
				$lastname  = (!empty($agent['last_name'])) ? $agent['last_name'] : "";
				$name      = (!empty($firstname) && !empty($lastname)) ? $firstname . " " . $lastname : "Agent";
				
				$image = (!empty($agent['image'])) ? $agent['image'] : "/data/images/user.png";
				
				$agent_title = (!empty($agent['title'])) ? $agent['title'] : "Agent";
				$email       = (!empty($agent['email'])) ? $agent['email'] : "";
				$phone       = (!empty($agent['phone'])) ? $agent['phone'] : "";
				$phone       = preg_replace('/\W/', '', $phone);
				
				// Format our phone number to the standard (###) ###-#### if possible
				$phone_formatted = "";
				if(strlen($phone) == 7)
				{
					$phone_formatted = preg_replace("/([0-9A-Z]{3})([0-9A-Z]{4})/", "$1-$2", $phone);
				}
				else if(strlen($phone) == 10)
				{
					$phone_formatted = preg_replace("/([0-9]{3})([0-9A-Z]{3})([0-9A-Z]{4})/", "($1) $2-$3", $phone);
				}
				else
					$phone_formatted = $phone;
				
				$phone = $phone_formatted;
				
				$mobile      = (!empty($agent['mobile'])) ? $agent['mobile'] : "";
				$mobile       = preg_replace('/\D/', '', $mobile);
				
				// Format our mobile number to the standard (###) ###-#### if possible
				$mobile_formatted = "";
				if(strlen($mobile) == 7)
				{
					$mobile_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $mobile);
				}
				else if(strlen($mobile) == 10)
				{
					$mobile_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $mobile);
				}
				else
					$mobile_formatted = $mobile;
				
				$mobile = $mobile_formatted;
				
				$website  = (!empty($agent['website'])) ? $agent['website'] : "";
				$website  = (!empty($website) && !preg_match('/^https?/',$website)) ? 'http://' . $website : $website;
				
				$location = (!empty($agent['address'])) ? $agent['address'] : "";
				$bio      = (!empty($agent['bio'])) ? $agent['bio'] : "";
				
				$facebook_link  = (!empty($agent['facebook_url'])) ? "<li><a href='" . $agent['facebook_url'] . "'><i class='fa fa-facebook'></i></a></li>" : "";
				$twitter_link   = (!empty($agent['twitter_url'])) ? "<li><a href='" . $agent['twitter_url'] . "'><i class='fa fa-twitter'></i></a></li>" : "";
				$linkedin_link  = (!empty($agent['linkedin_url'])) ? "<li><a href='" . $agent['linkedin_url'] . "'><i class='fa fa-linkedin'></i></a></li>" : "";
				$youtube_link   = (!empty($agent['youtube_url'])) ? "<li><a href='" . $agent['youtube_url'] . "'><i class='fa fa-youtube'></i></a></li>" : "";
				$instagram_link = (!empty($agent['instagram_url'])) ? "<li><a href='" . $agent['instagram_url'] . "'><i class='fa fa-instagram'></i></a></li>" : "";
			}
			//else
			//	header("Location: /404/");
		}
		else
			header("Location: /team/test/");
		
		// Set about as the active navbar link
		$nav_active['about'] = 'active';
		
		$footer_script = "";
		
		$data_header = array(
			'title'         => 'Meet The Team | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-team',
			'nav_active'    => $nav_active
		);
		
		$data = array(
			'title'          => $title,
			'text'           => $text,
			'name'           => $name,
			'image'          => $image,
			'agent_title'    => $agent_title,
			'email'          => $email,
			'phone'          => $phone,
			'mobile'         => $mobile,
			'website'        => $website,
			'location'       => $location,
			'bio'            => $bio,
			'facebook_link'  => $facebook_link,
			'twitter_link'   => $twitter_link,
			'linkedin_link'  => $linkedin_link,
			'youtube_link'   => $youtube_link,
			'instagram_link' => $instagram_link,
			'zillow_ratings' => $zillow_ratings,
			'trulia_ratings' => $trulia_ratings,
		);
		
		$data_footer = array(
			'footer_script' => $footer_script
		);
		
		$this->load->view('header', $data_header);
		$this->load->view('team-agent', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file team.php */
/* Location: ./application/controllers/team.php */
