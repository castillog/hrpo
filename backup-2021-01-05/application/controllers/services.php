<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Services extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab our company settings content data
		$content = $this->Content->get_content("settings");
		$company_lead_email    = $content['company']['lead_email'];

		$flash_class = "display-hide";
		$flash_msg = "";
		$footer_script = "";

		// Set services as the active navbar link
		$nav_active['services'] = 'active';

		$data_header = array(
			'title'         => 'Services | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-services',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('services', $data);
		$this->load->view('footer', $data_footer);
	}

	public function management()
	{
		// Set services as the active navbar link
		$nav_active['services'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Full Service Management | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-services-management',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('services-management', $data);
		$this->load->view('footer', $data_footer);
	}

	public function marketing()
	{
		// Set services as the active navbar link
		$nav_active['services'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Marketing Plan | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-services-marketing',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('services-marketing', $data);
		$this->load->view('footer', $data_footer);
	}

	public function selection()
	{
		// Set services as the active navbar link
		$nav_active['services'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Tenant Selection | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-services-selection',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('services-selection', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file services.php */
/* Location: ./application/controllers/services.php */
