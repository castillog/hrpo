<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Error404 extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Don't set an active navbar link for this one
		$nav_active = array();

		$footer_script = "";

		$data_header = array(
			'title'         => '404 - Page Not Found | Lonnie Bush',
			'bodyClass'     => 'page-error-404',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('error404', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file contact.php */
/* Location: ./application/controllers/contact.php */
