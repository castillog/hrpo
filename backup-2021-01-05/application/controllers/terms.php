<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Terms extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Set about as the active navbar link
		$nav_active['home'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Terms of Use | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-terms',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('terms', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file terms.php */
/* Location: ./application/controllers/terms.php */
