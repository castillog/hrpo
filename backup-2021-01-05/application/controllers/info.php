<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Info
 */
class Info extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		$flash_class = "display-hide";
		$flash_msg   = "";
		$hasErrors = false;
		$footer_script = "";

		$postback = $this->input->post("info-form-submit");

		if($postback)
		{
			// Grab our company settings content data
			$content            = $this->Content->get_content("settings");
			$company_lead_email = $content['company']['lead_email'];

			$lead_email = (!empty($company_lead_email)) ? $company_lead_email : $this->config->item('default_email_address');

			// Grab all of our posted fields
			$firstname = $this->input->post('first-name');
			$firstname = (!empty($firstname)) ? $firstname : "invalid";

			$lastname = $this->input->post('last-name');
			$lastname = (!empty($lastname)) ? $lastname : "invalid";

			$email = $this->input->post('email');
			$email = (!empty($email)) ? $email : "invalid";

			$phone = $this->input->post('phone');
			$phone = preg_replace('/\D/', '', $phone);
			$phone = (!empty($phone)) ? $phone : "invalid";

			//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

			if($firstname == "invalid")
			{
				$flash_class = "alert alert-danger alert-dismissible";
				$flash_msg = "Failed! You must enter a valid first name.";
				$hasErrors = true;
			}

			if($lastname == "invalid")
			{
				$flash_class = "alert alert-danger alert-dismissible";
				$flash_msg = "Failed! You must enter a valid last name.";
				$hasErrors = true;
			}

			if($email == "invalid")
			{
				$flash_class = "alert alert-danger alert-dismissible";
				$flash_msg = "Failed! You must provide a valid email address.";
				$hasErrors = true;
			}

			if($phone == "invalid")
			{
				$flash_class = "alert alert-danger alert-dismissible";
				$flash_msg = "Failed! You must provide a valid phone number.";
				$hasErrors = true;
			}

			if(!$hasErrors)
			{
				// Format our phone number to the standard (###) ###-#### if possible
				$phone_formatted = "";
				if(strlen($phone) == 7)
				{
					$phone_formatted = preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
				}
				else if(strlen($phone) == 10)
				{
					$phone_formatted = preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
				}
				else
					$phone_formatted = $phone;

				$email_body    = "";
				$email_to      = $lead_email;
				$email_from    = $this->config->item('default_email_address');
				$email_subject = "Free Guide Request from Lonnie Bush Property Management";

				$email_body .= "An owner has made a request for a free Property Management Guide on Lonnie Bush Property Management:<br><br>
				Name: {$firstname} {$lastname}<br>
				Email: {$email}<br>
				Phone: {$phone_formatted}<br><br>
				- Lonnie Bush Property Management";

				$this->email->from($email_from, "Lonnie Bush");
				$this->email->to($email_to);

				$this->email->subject($email_subject);
				$this->email->message($email_body);

				$email_results = $this->email->send();

				if($email_results)
				{
					$flash_class = "alert alert-success alert-dismissible";
					$flash_msg = "Success! Your request has been submitted.";

					$footer_script .=
						"<!-- Google Code for lead Conversion Page -->
					<script type='text/javascript'>
					/* <![CDATA[ */
					var google_conversion_id = 976918088;
					var google_conversion_language = 'en';
					var google_conversion_format = '3';
					var google_conversion_color = 'ffffff';
					var google_conversion_label = 'pZkOCIiyhwgQyKzq0QM';
					var google_conversion_value = 0;
					var google_remarketing_only = false;
					/* ]]> */
					</script>
					<script type='text/javascript' src='//www.googleadservices.com/pagead/conversion.js'>
					</script>
					<noscript>
					<div style='display:inline;'>
					<img height='1' width='1' style='border-style:none;' alt='' src='//www.googleadservices.com/pagead/conversion/976918088/?value=0&amp;label=pZkOCIiyhwgQyKzq0QM&amp;guid=ON&amp;script=0'/>
					</div>
					</noscript>
					\n";
				}
				else
				{
					$flash_class = "alert alert-danger alert-dismissible";
					$flash_msg = "Failed! Unable to submit your request at this time.";
				}
			}
		}

		// Set our home page as the active navbar link
		$nav_active['home'] = 'active';

		$footer_script .=
			"<script>
				if($('.alert-success').length > 0)
				{
					setTimeout(function()
					{
						window.location.href = '/';
					}, 2000);
				}
			</script>\n";

		$data_header = array(
			'title'         => 'Info for Property Management in Hampton Roads, VA | Lonnie Bush Property Management',
			'description'   => 'Hampton Roads Property Management with Lonnie Bush Property Management. Area leads in professional property management services',
			'keywords'      => 'Hampton Roads Property Management, Hampton Roads Real Estate Rental, Renting Residential Property, Hampton Roads Homes for Rent',
			'bodyClass'     => 'page-info',
			'nav_active'    => $nav_active
		);

		$data = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header-info', $data_header);
		$this->load->view('info', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file info.php */
/* Location: ./application/controllers/info.php */
