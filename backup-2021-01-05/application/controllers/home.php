<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Home
 */
class Home extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab our home page content data
		$content       = $this->Content->get_content("home");

		$content_title  = $content['body']['content_title'];
		$content_top = $content['body']['content_top'];
		$callout1       = $content['services']['callout1'];
		$callout2       = $content['services']['callout2'];
		$callout3       = $content['services']['callout3'];
		$callout4       = $content['services']['callout4'];

		$flash_class = "display-hide";
		$flash_msg   = "";
		$footer_script = "";

		// Set our home page as the active navbar link
		$nav_active['home'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Property Management Virginia Beach - Serving Hampton Roads VA',
			'description'   => 'Hampton Roads Property Management with Lonnie Bush Property Management. Area leads in professional property management services',
			'keywords'      => 'Hampton Roads Property Management, Hampton Roads Real Estate Rental,   Renting Residential Property, Hampton Roads Homes for Rent',
			'bodyClass'     => 'page-home',
			'nav_active'    => $nav_active
		);

		$data = array(
			'content_title' => $content_title,
			'content_top' => $content_top,
			'callout1' => $callout1,
			'callout2' => $callout2,
			'callout3' => $callout3,
			'callout4' => $callout4,
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('home', $data);
		$this->load->view('footer', $data_footer);
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
