<?php
class News extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('news_model');
	}

	public function index()
	{
		
		$data_header = array(
			'title'         => 'Property Management Blog',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'blog',
			'nav_active'    => ''
		);
		
		$data['news'] = array_filter( $this->news_model->get_news(), array($this, 'is_blog') );
		$data['title'] = 'News archive';
		$this->load->view('header', $data_header);
		$this->load->view('news/index', $data);
		$this->load->view('footer', '');
		
	}
	
	private function is_blog($post){
		return $post['is_blog'] == 1 ? true : false;
	}
	
	public function view($slug)
	{

	$data['title'] = $data['news_item']['title'];
	$data['news_item'] = $this->news_model->get_news($slug);
	
	if (empty($data['news_item']))
	{
		show_404();
	}



	$data_header = array(
		'title'         => $data['news_item']['seo_title'],
		'description'   => $data['news_item']['seo_desc'],
		'keywords'      => '',
		'bodyClass'     => 'blog-article',
		'nav_active'    => ''
	);

	$this->load->view('header', $data_header);
	$this->load->view('news/view', $data);
	$this->load->view('footer', '');

	}
}

