<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Class Content
 *
 * Manages user accounts, access rights, organizations, leads, and activity logs
 */
class Content extends CI_Model {

	/**
	 * Constructor
	 */
	function __construct()
	{
		// Load parent's constructor
		parent::__construct();
	}

	//---------------------
	//  GET CONTENT
	//---------------------
	/**
	 * Get page content
	 *
	 * @param string $page
	 * @param int|string $site_id
	 *
	 * @return mixed|bool Content data or false on search failure
	 */
	public function get_content($page, $site_id = 0)
	{
		// Make sure we have a page and it only contains valid characters
		if(!empty($page) && preg_match('/\w+/', $page))
		{
			$criteria = array(
				'page' => $page,
			   'site_id' => $site_id
			);

			$select = 'section, element, content, type';

			$result = $this->db_fetch('content', $criteria, $select);

			if(!empty($result))
			{
				$content = array();

				foreach($result as $c)
				{
					$content[$c['section']][$c['element']] = $c['content'];
				}

				return $content;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get page content with all columns
	 *
	 * @param string $page
	 * @param int|string $site_id
	 *
	 * @return mixed|bool Content data or false on search failure
	 */
	public function get_content_full($page, $site_id = 0)
	{
		// Make sure we have a page and it only contains valid characters
		if(!empty($page) && preg_match('/\w+/', $page))
		{
			$criteria = array(
				'page' => $page,
				'site_id' => $site_id
			);

			$result = $this->db_fetch('content', $criteria);

			if(!empty($result))
			{
				$content = array();

				foreach($result as $c)
				{
					$content[$c['section']][$c['element']] = array('id' => $c['content_id'], 'content' => $c['content']);
				}

				return $content;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get page content by id
	 *
	 * @param int|string $content_id
	 *
	 * @return mixed|bool Content data or false on search failure
	 */
	public function get_content_by_id($content_id)
	{
		// Make sure we have a content_id and it only contains valid characters
		if(!empty($content_id) && preg_match('/\d+/', $content_id))
		{
			$criteria = array(
				'content_id' => $content_id
			);

			$result = $this->db_fetch('content', $criteria);

			if(!empty($result))
			{
				$content = array();

				foreach($result as $c)
				{
					$content[$c['section']][$c['element']] = $c['content'];
				}

				return $content;
			}
			else
				return false;
		}
		else
			return false;
	}

	//---------------------
	//  GET PICTURES
	//---------------------
	/**
	 * Get picture data
	 *
	 * @param int|string $picture_id
	 *
	 * @return mixed|bool Picture data or false on search failure
	 */
	public function get_picture($picture_id)
	{
		// Make sure we have a picture_id and it only contains valid characters
		if(!empty($picture_id) && preg_match('/\d+/', $picture_id))
		{
			$criteria = array(
				'picture_id' => $picture_id
			);
			$result = $this->db_fetch('pictures', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get pictures data by MLS
	 *
	 * @param int|string $mlsnumber
	 *
	 * @return mixed|bool Picture data or false on search failure
	 */
	public function get_pictures($mlsnumber)
	{
		// Make sure we have an mlsnumber and it only contains valid characters
		if(!empty($mlsnumber) && preg_match('/\d+/', $mlsnumber))
		{
			$resultProperty = current($this->get_property($mlsnumber));

			if(!empty($resultProperty))
			{
				$property_id = $resultProperty['property_id'];

				if(!empty($property_id))
				{
					$result = $this->get_pictures_by_property($property_id);

					if($result)
						return $result;
					else
						return false;
				}
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get pictures data by property
	 *
	 * @param int|string $property_id
	 * @param string $type
	 *
	 * @return mixed|bool Picture data or false on search failure
	 */
	public function get_pictures_by_property($property_id, $type = null)
	{
		// Make sure we have a property_id and it only contains valid characters
		if(!empty($property_id) && preg_match('/\d+/', $property_id))
		{
			$criteria['property_id'] = $property_id;
			if(!empty($type))
				$criteria['type'] = $type;

			$result = $this->db_fetch('pictures', $criteria, "index ASC");

			if($result)
				return $result;
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get picture id
	 *
	 * @param int|string $filename
	 *
	 * @return mixed|bool Picture id or false on search failure
	 */
	public function get_picture_id($filename)
	{
		// Make sure we have a picture_id and it only contains valid characters
		if(!empty($filename))
		{
			$criteria = array(
				'image' => $filename
			);
			$result = $this->db_fetch('pictures', $criteria);

			if($result)
			{
				$result = current($result);
				return $result['picture_id'];
			}
			else
				return false;
		}
		else
			return false;
	}

	//---------------------
	//  GET VENDORS
	//---------------------
	/**
	 * Get a vendor
	 *
	 * @param int|string $vendor_id
	 *
	 * @return mixed|bool Builder data or false on search failure
	 */
	public function get_vendor($vendor_id)
	{
		// Make sure we have a vendor_id and it only contains valid characters
		if(!empty($vendor_id) && preg_match('/\d+/', $vendor_id))
		{
			$criteria = array(
				'vendor_id' => $vendor_id
			);
			$result = $this->db_fetch('vendors', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get all vendors
	 *
	 * @return mixed|bool Builders data or false on search failure
	 */
	public function get_vendors_all()
	{
		$criteria = array();
		$result = $this->db_fetch('vendors', $criteria);

		return $result;
	}

	//---------------------
	//  CREATE RECORDS
	//---------------------
	/**
	 * Create new content
	 *
	 * @param mixed $data New content data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_content($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('content', $data);
		else
			return false;
	}
	/**
	 * Create a new picture
	 *
	 * @param mixed $data New picture data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_picture($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('pictures', $data);
		else
			return false;
	}
	/**
	 * Create a new vendor
	 *
	 * @param mixed $data New vendor data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_vendor($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('vendors', $data);
		else
			return false;
	}

	//---------------------
	//  UPDATE RECORDS
	//---------------------
	/**
	 * Update content
	 *
	 * @param mixed $data Content data pre-formatted into an Active Record criteria array
	 * @param mixed $content_id Content id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_content($data, $content_id)
	{
		// Make sure we actually have the criteria to update
		if(!empty($data) && !empty($content_id))
		{
			$id = array('content_id' => $content_id);
			return $this->db_update('content', $data, $id);
		}

		else
			return false;
	}
	/**
	 * Update a picture
	 *
	 * @param mixed $data Picture data pre-formatted into an Active Record criteria array
	 * @param mixed $picture_id Picture id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_picture($data, $picture_id)
	{
		// Make sure we actually have  the criteria to update
		if(!empty($data) && !empty($picture_id))
		{
			$id = array('picture_id' => $picture_id);
			return $this->db_update('pictures', $data, $id);
		}

		else
			return false;
	}
	/**
	 * Update a vendor
	 *
	 * @param mixed $data Builder data pre-formatted into an Active Record criteria array
	 * @param mixed $vendor_id Builder id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_vendor($data, $vendor_id)
	{
		// Make sure we actually have the criteria to update
		if(!empty($vendor_id) && !empty($vendor_id))
		{
			$id = array('vendor_id' => $vendor_id);
			return $this->db_update('vendors', $data, $id);
		}
		else
			return false;
	}

	//---------------------
	//  DELETE RECORDS
	//---------------------
	/**
	 * Delete content
	 *
	 * @param int|string $content_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_content($content_id)
	{
		// Make sure we have a content_id and it only contains valid characters
		if(!empty($content_id) && preg_match('/\d+/', $content_id))
		{
			$criteria = array(
				'content_id' => $content_id
			);
			$result = $this->db_delete('content', $criteria);

			return $result;
		}
		else
			return false;
	}
	/**
	 * Delete content by site
	 *
	 * @param int|string $site_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_content_by_site($site_id)
	{
		// Make sure we have a site_id and it only contains valid characters
		if(!empty($site_id) && preg_match('/\d+/', $site_id))
		{
			$criteria = array(
				'site_id' => $site_id
			);
			$result = $this->db_delete('content', $criteria);

			return $result;
		}
		else
			return false;
	}
	/**
	 * Delete a picture
	 *
	 * @param int|string $picture_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_picture($picture_id)
	{
		// Make sure we have a picture_id and it only contains valid characters
		if(!empty($picture_id) && preg_match('/\d+/', $picture_id))
		{
			$criteria = array(
				'picture_id' => $picture_id
			);
			$result = $this->db_delete('pictures', $criteria);

			return $result;
		}
		else
			return false;
	}
	/**
	 * Delete a vendor
	 *
	 * @param int|string $vendor_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_vendor($vendor_id)
	{
		// Make sure we have a vendor_id and it only contains valid characters
		if(!empty($vendor_id) && preg_match('/\d+/', $vendor_id))
		{
			$criteria = array(
				'vendor_id' => $vendor_id
			);
			$result = $this->db_delete('vendors', $criteria);

			return $result;
		}
		else
			return false;
	}

	//-------------------------
	//  DB FUNCTIONS (PRVIATE)
	//-------------------------
	/**
	 * Fetch records from the database
	 *
	 * @param string $table
	 * @param mixed $criteria Search criteria pre-formatted into an Active Record array
	 * @param string $sort Sort field and direction
	 * @param int|string $limit Limit of results returned
	 * @param int|string offset Record offset for results returned
	 *
	 * @return mixed|bool Selected data or false on failure
	 */
	private function db_fetch($table, $criteria, $select = '*', $sort = null, $limit = null, $offset = null)
	{
		if(!empty($criteria))
		{
			$where_in = array();

			foreach($criteria as $key => $value)
			{
				if(is_array($value))
				{
					$where_in[$key] = $value;
					unset($criteria[$key]);
				}
			}

			$this->db->select('*')->from($table)->where($criteria);

			if(!empty($where_in))
			{
				foreach($where_in as $field => $selection)
				{
					$this->db->where_in($field, $selection);
				}
			}
		}
		else
			$this->db->select('*')->from($table);

		if(!empty($sort))
			$this->db->order_by($sort);

		if(!empty($limit) && !(empty($offset)))
			$this->db->limit($limit, $offset);
		else if(!empty($limit))
			$this->db->limit($limit);

		/** @var CI_DB_mysqli_result $result */
		$result = $this->db->get();

		if($result->num_rows() > 0)
		{
			return $result->result_array();
		}
		else
			return false;
	}

	/**
	 * Insert records in the database
	 *
	 * @param string $table
	 * @param mixed $data Data pre-formatted into an Active Record array
	 *
	 * @return bool True on success and false on failure
	 */
	private function db_insert($table, $data)
	{
		$data['_created'] = date('Y-m-d H:i:s');
		$data['_modified'] = date('Y-m-d H:i:s');

		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		if($this->db->affected_rows() < 1)
			return false;
		else
			return $insert_id;
	}

	/**
	 * Update records in the database
	 *
	 * @param string $table
	 * @param mixed $data Data pre-formatted into an Active Record array
	 * @param mixed $id Record id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	private function db_update($table, $data, $id)
	{
		$data['_modified'] = date('Y-m-d H:i:s');

		$this->db->update($table, $data, $id);

		if($this->db->affected_rows() < 1)
			return false;
		else
			return true;
	}

	/**
	 * Delete records from the database
	 *
	 * @param string $table
	 * @param mixed $id Record id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	private function db_delete($table, $id)
	{
		$this->db->delete($table, $id);

		if($this->db->affected_rows() < 1)
			return false;
		else
			return true;
	}
}

/* End of file content.php */
/* Location: ./application/models/content.php */
