<?php
class News_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_news($slug = FALSE)
	{
		if ($slug === FALSE)
		{
			$query = $this->db->get('news');
			return $query->result_array();
		}
	
		$query = $this->db->get_where('news', array('slug' => $slug));
		return $query->row_array();
	}

	public function update_news($data, $slug)
	{
	
		
		if(!empty($data) && !empty($slug))
		{

			$updateQuery = $this->db->update_string('news', $data, 'slug = "'.$slug.'"');
			return $this->db->query($updateQuery);

		}
		else
			{
			return false;
		}
	}	

	public function set_news($data)
	{
		return $this->db->insert('news', $data);
	}	
	
	public function get_all_routes()
	{
		$query = $this->db->get('news');
		$routes = [];
		
		foreach ($query->result() as $row)
		{
		    $route = [
			    'slug' => $row->slug,
			    'controller' => 'news',
			    'action' => 'view'
		    ];
		    array_push($routes, $route);
		}

		return $routes;
	
	}
	
	
}