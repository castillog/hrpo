<?php
$route['newport-news-property-management'] = 'news/view/newport-news-property-management';
$route['suffolk-property-management'] = 'news/view/suffolk-property-management';
$route['security-deposit-laws-va'] = 'news/view/security-deposit-laws-va';
$route['norfolk-property-management'] = 'news/view/norfolk-property-management';
$route['virginia-eviction-laws'] = 'news/view/virginia-eviction-laws';
$route['virginia-squatters-rights'] = 'news/view/virginia-squatters-rights';