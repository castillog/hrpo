<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Refer Owners</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/agents/refer/">Refer Owners</a>
		</nav>
	</div>
</section><!--/ page title -->

<main class="page-content">
	<div class="grid-row">
		<div class="col-md-12 col-sm-12 block">
			<section id="team">
				<section>
					
					<div class="widget-title"><?php echo $title ?></div>
					
					<?php echo $text ?>
				
				</section>
			</section>
			<!-- /#about-us -->
		</div>
		<!-- /.col-md-12 -->
	</div>
</main>
