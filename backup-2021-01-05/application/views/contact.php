<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Contact</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;
			<a href="/contact/">Contact</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content">
	<div class="grid-row">

			<div class="col-md-12 col-sm-12">
				<section id="agent-detail">
					<section id="contact-information">
						<div class="row">
							<div class="col-md-4 col-sm-5">
								<section id="address">
									<header><h3>Address</h3></header>
									<address>
										<a href="//hamptonroadspropertyowners.com"><strong><?php echo $company_name ?></strong></a><br>
										<?php echo $company_street ?><br>
										<?php echo $company_city . ", " . $company_state . " " . $company_zipcode ?>
									</address>
									<?php echo $company_phone ?><br>
									<a href="#"><?php echo $company_email ?></a><br>
								</section>
								<!-- /#address -->
								<section id="social">
									<header><h3>Social Profiles</h3></header>
									<div class="agent-social">
										<ul id="social-icons" class="list-unstyled list-links list-inline">
											<?php if(!empty($company_facebook)) : ?>
												<li><a href="<?php echo $company_facebook ?>"><div class=""><i class="fa fa-facebook"></i></div></a></li>
											<?php endif; ?>
											<?php if(!empty($company_twitter)) : ?>
												<li><a href="<?php echo $company_twitter ?>"><div class=""><i class="fa fa-twitter"></i></div></a></li>
											<?php endif; ?>
											<?php if(!empty($company_google)) : ?>
												<li><a href="<?php echo $company_google ?>"><div class=""><i class="fa fa-google-plus"></i></div></a></li>
											<?php endif; ?>
											<?php if(!empty($company_linkedin)) : ?>
												<li><a href="<?php echo $company_linkedin ?>"><div class=""><i class="fa fa-linkedin"></i></div></a></li>
											<?php endif; ?>
											<?php if(!empty($company_youtube)) : ?>
												<li><a href="<?php echo $company_youtube ?>"><div class=""><i class="fa fa-youtube"></i></div></a></li>
											<?php endif; ?>
											<?php if(!empty($company_pinterest)) : ?>
												<li><a href="<?php echo $company_pinterest ?>"><div class=""><i class="fa fa-pinterest"></i></div></a></li>
											<?php endif; ?>
										</ul>
									</div>
								</section>
								<!-- /#social -->
							</div>
							<!-- /.col-md-4 -->
							<div class="col-md-8 col-sm-7">
								<header><h3>Where We Are</h3></header>
								<div id="map"></div>
							</div>
							<!-- /.col-md-8 -->
						</div>
						<!-- /.row -->
					</section>
					<!-- /#agent-info -->
					<hr class="thick">
					<section id="form">
						<header><h3>Send Us a Message</h3></header>
						<form role="form" id="contact_us_form" method="post" action="" class="clearfix">
							<div id="contact_us_flash"></div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="contact_us_name">Your Name<em>*</em></label>
										<input type="text" class="form-control" id="contact_us_name" name="contact_us_name"
											required>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-md-6 -->
								<div class="col-md-6">
									<div class="form-group">
										<label for="contact_us_email">Your Email<em>*</em></label>
										<input type="email" class="form-control" id="contact_us_email" name="contact_us_email"
											required>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-md-6 -->
							</div>
							<!-- /.row -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<label for="contact_us_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="contact_us_message" rows="8" name="contact_us_message"
											required></textarea>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-md-12 -->
							</div>
							<!-- /.row -->
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div class="g-recaptcha" data-sitekey="6LfCVI4UAAAAAIk8lGxyzB3Z9baFFjznvyFkwnsp" style="float:right;margin-bottom:15px;"></div>
									</div>
									<!-- /.form-group -->
								</div>
								<!-- /.col-md-12 -->
							</div>
							<!-- /.row -->
							<div class="form-group clearfix">
								<input type="hidden" id="contact_form_submit" name="contact_form_submit" value="1">
								<input type="hidden" id="contact_ip" name="contact_ip" value="<?php echo $_SERVER['REMOTE_ADDR']?>">
								<button type="submit" class="button pull-right" id="contact_us_submit">Send a Message</button>
							</div>
							<!-- /.form-group -->
							<div id="form-status"></div>
						</form>
						<!-- /#form-contact -->
					</section>
				</section>
				<!-- /#agent-detail -->
			</div>
			<!-- /.col-md-12 -->

	</div>
</main>
<!--/ page content -->
