<?php

// Set up our default navbar button classes
$active['home']        = (!empty($active['home'])) ? $active['home'] : '';
$active['pages']       = (!empty($active['pages'])) ? $active['pages'] : '';
$active['content']     = (!empty($active['content'])) ? $active['content'] : '';
$active['news']        = (!empty($active['news'])) ? $active['news'] : '';
$active['users']       = (!empty($active['users'])) ? $active['users'] : '';
$active['vendors']     = (!empty($active['vendors'])) ? $active['vendors'] : '';
$active['integration'] = (!empty($active['integration'])) ? $active['integration'] : '';
$active['settings']    = (!empty($active['settings'])) ? $active['settings'] : '';

// Only show links the user has read access to its page
$hide['home']      = '';
$hide['tutorials'] = '';

$hide['pages']       = (!$this->Accounts->can_user_access('admin-pages')) ? 'display-hide' : '';
$hide['content']       = (!$this->Accounts->can_user_access('admin-pages')) ? 'display-hide' : '';
$hide['news']       = (!$this->Accounts->can_user_access('admin-pages')) ? 'display-hide' : '';
$hide['users']       = (!$this->Accounts->can_user_access('admin-users')) ? 'display-hide' : '';
$hide['vendors']     = (!$this->Accounts->can_user_access('admin-vendors')) ? 'display-hide' : '';
$hide['integration'] = (!$this->Accounts->can_user_access('admin-integration')) ? 'display-hide' : '';
$hide['settings']    = (!$this->Accounts->can_user_access('admin-settings')) ? 'display-hide' : '';

?>
<!-- BEGIN CONTAINER -->
<div class="page-container">

	<!-- BEGIN SIDEBAR -->
	<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">

			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">

					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler"></div>
					<!-- END SIDEBAR TOGGLER BUTTON -->

				</li>
				<li class="start <?php echo $active['home'] . " " . $hide['home'] ?>">
					<a href="/admin/"><i class="icon-home"></i><span class="title">Dashboard</span><span class="selected"></span></a>
				</li>
				<li class="start <?php echo $active['pages'] . " " . $hide['pages'] ?>">
					<a href="/admin/pages/"><i class="icon-question"></i><span class="title">Misc</span><span class="arrow "></span></a>
				</li>
				<li class="start <?php echo $active['content'] . " " . $hide['content'] ?>">
					<a href="/admin/content/"><i class="icon-docs"></i><span class="title">Content</span><span class="arrow "></span></a>
				</li>
				
				<li class="start <?php echo $active['news'] . " " . $hide['news'] ?>">
					<a href="/admin/news/"><i class="icon-pencil"></i><span class="title">Blog</span><span class="arrow "></span></a>
				</li>
				<li class="start <?php echo $active['users'] . " " . $hide['users'] ?>">
					<a href="/admin/users/"><i class="icon-user"></i><span class="title">Users</span><span class="arrow "></span></a>
				</li>
				<li class="start <?php echo $active['vendors'] . " " . $hide['vendors'] ?>">
					<a href="/admin/vendors/"><i class="icon-like"></i><span class="title">Vendors</span><span class="arrow "></span></a>
				</li>
				<li class="start <?php echo $active['integration'] . " " . $hide['integration'] ?>">
					<a href="/admin/integration/"> <i class="icon-layers"></i> <span class="title">Integration</span> <span class="arrow "></span> </a>
				</li>
				<li class="start <?php echo $active['settings'] . " " . $hide['settings'] ?>">
					<a href="/admin/settings/"> <i class="icon-settings"></i> <span class="title">Settings</span> <span class="arrow "></span> </a>
				</li>

			</ul>
			<!-- END SIDEBAR MENU -->

		</div>
	</div>
	<!-- END SIDEBAR -->
