<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">

		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">

				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Blog
					<small>Lonnie Bush Blog Articles</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i> <a href="/admin/">Dashboard</a> <i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/admin/news/">Blog</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->

			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN USER LIST PORTLET-->
		<div id="user_list_portlet" class="portlet box green-lilahmedia">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Blog Article List
				</div>
				<div class="tools">
					<a id="user_list_collapse" class="collapse" href="javascript:;"></a>
				</div>
				<div class="actions">
					<?php echo $news_add_button ?>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="blog_list">
								<thead>
								<tr>
									<th>Title</th>
									<th>Slug</th>
									<th>In Blog</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $news ?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END USER LIST PORTLET-->
		
	</div>
</div><!-- END CONTENT -->

</div><!-- END CONTAINER -->
