<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title><?php echo $title ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta content="<?php echo $meta_desc ?>" name="description"/>
	<meta content="<?php echo $meta_keywords ?>" name="keywords"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/simple-line-icons/simple-line-icons.min.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/uniform/css/uniform.default.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/login.css"/>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/components.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/plugins.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/themes/comingsoon.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/custom.css"/>
	<!-- END THEME STYLES -->
	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- BEGIN BODY -->
<body class="<?php echo $body_class?>">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="/admin/">
		<img src="/includes/img/logo-csc-1.png" alt="" style="width: 400px; height: auto;"/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="" method="post">
	<h3 class="form-title">Login to your account</h3>
	<div class="alert alert-success">
		<button class="close" data-close="alert"></button>
			<span>An email with instructions to reset your password has been sent to your registered email address.</span>
	</div>

</form>
<!-- END LOGIN FORM -->

</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	Copyright © 2014. All Rights Reserved | <a href="http://www.lilahmedia.com" target="_blank">
	Website Design | SEO &amp; Internet Marketing by Lilah Media.</a>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script type="text/javascript" src="/includes/admin/plugins/respond.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/includes/admin/plugins/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery-migrate-1.2.1.min.js"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script type="text/javascript" src="/includes/admin/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.blockui.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.cokie.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/includes/admin/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/includes/admin/scripts/metronic.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/layout.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/quick-sidebar.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/login.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	jQuery(document).ready(function() {
		Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		QuickSidebar.init() // init quick sidebar
		Login.init();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
