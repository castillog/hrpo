<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8"/>
	<title><?php echo $title ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
	<meta content="<?php echo $meta_desc ?>" name="description"/>
	<meta content="<?php echo $meta_keywords ?>" name="keywords"/>
	<meta content="" name="author"/>
	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/font-awesome/css/font-awesome.min.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/simple-line-icons/simple-line-icons.min.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap/css/bootstrap.min.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/uniform/css/uniform.default.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-switch/css/bootstrap-switch.min.css"/>
	<!-- END GLOBAL MANDATORY STYLES -->
	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/login.css"/>
	<!-- END PAGE LEVEL SCRIPTS -->
	<!-- BEGIN THEME STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/components.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/plugins.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/layout.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/themes/comingsoon.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/custom.css"/>
	<!-- END THEME STYLES -->
	<link rel="shortcut icon" href="favicon.ico"/>
</head>
<!-- BEGIN BODY -->
<body class="<?php echo $body_class?>">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="/">
		<img src="/data/images/logo-bl.png" alt="Lonnie Bush" style="width: 400px; height: auto;"/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="" method="post">
	<h3 class="form-title">Login to your account</h3>
	<div class="alert <?php echo $flash_class ?>">
		<button class="close" data-close="alert"></button>
			<span><?php echo $flash_text ?></span>
	</div>
	<div class="form-group">
		<!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
		<label class="control-label visible-ie8 visible-ie9">Username</label>
		<div class="input-icon">
			<i class="fa fa-user"></i>
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="username"/>
		</div>
	</div>
	<div class="form-group">
		<label class="control-label visible-ie8 visible-ie9">Password</label>
		<div class="input-icon">
			<i class="fa fa-lock"></i>
			<input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
		</div>
	</div>
	<div class="form-actions">
		<input type="hidden" name="form_submit" value="login">
		<button type="submit" class="btn green pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
		</button>
	</div>
	<div class="forget-password">
		<h4>Forgot your password ?</h4>
		<p>
			no worries, click <a href="javascript:;" id="forget-password">
				here </a>
			to reset your password.
		</p>
	</div>

</form>
<!-- END LOGIN FORM -->
<!-- BEGIN FORGOT PASSWORD FORM -->
<form class="forget-form" action="/admin/login/reset/" method="post">
	<h3>Forget Password ?</h3>
	<p>
		Enter your e-mail address below to reset your password.
	</p>
	<div class="form-group">
		<div class="input-icon">
			<i class="fa fa-envelope"></i>
			<input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
		</div>
	</div>
	<div class="form-actions">
		<button type="button" id="back-btn" class="btn">
			<i class="m-icon-swapleft"></i> Back </button>
		<input type="hidden" name="form_submit" value="forgot">
		<button type="submit" class="btn green pull-right">
			Submit <i class="m-icon-swapright m-icon-white"></i>
		</button>
	</div>
</form>
<!-- END FORGOT PASSWORD FORM -->

</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	Copyright © 2015. All Rights Reserved | <a href="http://www.lilahmedia.com" target="_blank">
	Website Design | SEO &amp; Internet Marketing by Lilah Media.</a>
</div>
<!-- END COPYRIGHT -->
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script type="text/javascript" src="/includes/admin/plugins/respond.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/includes/admin/plugins/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery-migrate-1.2.1.min.js"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script type="text/javascript" src="/includes/admin/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.blockui.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.cokie.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="/includes/admin/plugins/jquery-validation/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/select2/select2.min.js"></script>
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/includes/admin/scripts/metronic.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/layout.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/quick-sidebar.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/login.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
	jQuery(document).ready(function() {
		Metronic.init(); // init metronic core components
		Layout.init(); // init current layout
		QuickSidebar.init() // init quick sidebar
		Login.init();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>
