<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">

		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			Settings
			<small>global content and options</small>
		</h3>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="/admin/">Dashboard</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="/admin/settings/">Settings</a>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->

	</div>
</div>
<!-- END PAGE HEADER-->

<!-- BEGIN SETTINGS PORTLET-->
<div id="settings_portlet" class="portlet box green-lilahmedia">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gear"></i>Settings
		</div>
		<div class="tools">
			<a class="collapse" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body form light-grey">
		<form id="settings_form" class="form-horizontal" action="" method="post" role="form" enctype="multipart/form-data">
			<div class="form-body">

				<h4 class="form-section">Information</h4>
				<div class="form-group">
					<label for="setting_company_name" class="col-md-3 control-label">Company Name:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_company_name" id="setting_company_name"
							value="<?php echo $company_name ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<?php /*
				<div class="form-group">
					<label for="setting_site_name" class="col-md-3 control-label">Site Name:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_site_name" id="setting_site_name"
							value="<?php echo $site_name ?>">
					</div>
				</div>
				*/ ?>
				<div class="form-group">
					<label for="setting_company_street" class="col-md-3 control-label">Company Street:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_company_street" id="setting_company_street"
							value="<?php echo $company_street ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_company_city" class="col-md-3 control-label">Company City:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_company_city" id="setting_company_city"
							value="<?php echo $company_city ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_company_state" class="col-md-3 control-label">Company State:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_company_state" id="setting_company_state"
							value="<?php echo $company_state ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_company_zipcode" class="col-md-3 control-label">Company Zipcode:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_company_zipcode" id="setting_company_zipcode"
							value="<?php echo $company_zipcode ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_company_phone" class="col-md-3 control-label">Company Phone:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="setting_company_phone" id="setting_company_phone"
							value="<?php echo $company_phone ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_company_email" class="col-md-3 control-label">Company Email:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_company_email" id="setting_company_email"
							value="<?php echo $company_email ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_company_lead_email" class="col-md-3 control-label">Company Lead Email:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_company_lead_email" id="setting_company_lead_email"
							value="<?php echo $company_lead_email ?>" <?php echo $readonly ?>>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-3 control-label">Company Logo:</label>
					<div class="col-md-9">
						<div style="font-style: italic; margin-bottom: 5px;"></div>
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: auto; height: 150px;">
								<img src="<?php echo $company_logo ?>" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="width: auto; height: 150px;">
							</div>
							<div>
								<span class="btn default btn-file">
									<span class="fileinput-new"> Select image </span>
									<span class="fileinput-exists"> Change </span>
									<input type="file" name="setting_company_logo">
								</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
							</div>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Company Logo Footer:</label>
					<div class="col-md-9">
						<div style="font-style: italic; margin-bottom: 5px;"></div>
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: auto; height: 150px;">
								<img src="<?php echo $company_logo_footer ?>" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="width: auto; height: 150px;">
							</div>
							<div>
							<span class="btn default btn-file">
								<span class="fileinput-new"> Select image </span>
								<span class="fileinput-exists"> Change </span>
								<input type="file" name="setting_company_logo_footer">
							</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
							</div>
						</div>
					</div>
				</div>

				<h4 class="form-section">Social Media</h4>
				<div class="form-group">
					<label for="setting_facebook" class="col-md-3 control-label">Facebook URL:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_facebook" id="setting_facebook"
							value="<?php echo $company_facebook ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_twitter" class="col-md-3 control-label">Twitter URL:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_twitter" id="setting_twitter"
							value="<?php echo $company_twitter ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_google" class="col-md-3 control-label">Google+ URL:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_google" id="setting_google"
							value="<?php echo $company_google ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_linkedin" class="col-md-3 control-label">LinkedIn URL:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_linkedin" id="setting_linkedin"
							value="<?php echo $company_linkedin ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_youtube" class="col-md-3 control-label">YouTube URL:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_youtube" id="setting_youtube"
							value="<?php echo $company_youtube ?>" <?php echo $readonly ?>>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_rss" class="col-md-3 control-label">Pinterest URL:</label>
					<div class="col-md-6">
						<input type="text" class="form-control" name="setting_pinterest" id="setting_pinterest"
							value="<?php echo $company_pinterest ?>" <?php echo $readonly ?>>
					</div>
				</div>

				<h4 class="form-section">Embed Scripts</h4>
				<div class="form-group">
					<label for="setting_adwords_embed" class="col-md-3 control-label">Adwords Embed Code:</label>
					<div class="col-md-8">
						<textarea class="form-control" rows="6" name="setting_adwords_embed"
							id="setting_adwords_embed"><?php echo $site_adwords ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_analytics_embed" class="col-md-3 control-label">Analytics Embed Code:</label>
					<div class="col-md-8">
						<textarea class="form-control" rows="6" name="setting_analytics_embed"
							id="setting_analytics_embed"><?php echo $site_analytics ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_facebook_embed" class="col-md-3 control-label">Facebook Embed Code:</label>
					<div class="col-md-8">
						<textarea class="form-control" rows="6" name="setting_facebook_embed"
							id="setting_facebook_embed"><?php echo $site_facebook ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_bing_embed" class="col-md-3 control-label">Bing Embed Code:</label>
					<div class="col-md-8">
						<textarea class="form-control" rows="6" name="setting_bing_embed"
							id="setting_bing_embed"><?php echo $site_bing ?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="setting_misc_embed" class="col-md-3 control-label">Misc. Embed Code:</label>
					<div class="col-md-8">
						<textarea class="form-control" rows="6" name="setting_misc_embed"
							id="setting_misc_embed"><?php echo $site_misc ?></textarea>
					</div>
				</div>
			</div>
			<div class="form-actions fluid">
				<div class="text-center">
					<input type="hidden" name="settings_form_submit" value="1">
					<?php echo $settings_save_button ?>
					<button id="settings_cancel" class="btn default" type="button">Cancel</button>
				</div>
			</div>

		</form>
	</div>
</div>
<!-- END SETTINGS PORTLET-->

</div>
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
