<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">

		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			Vendors
			<small>information and activity</small>
		</h3>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="/admin/">Dashboard</a>
				<i class="fa fa-angle-right"></i>
			</li>
			<li>
				<a href="/admin/vendors/">Vendors</a>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->

	</div>
</div>
<!-- END PAGE HEADER-->

<!-- BEGIN VENDOR LIST PORTLET-->
<div id="vendor_list_portlet" class="portlet box green-lilahmedia <?php echo $vendor_list_hide ?>">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gear"></i>Vendor List
		</div>
		<div class="tools">
			<a id="vendorlist_collapse" class="collapse" href="javascript:;"></a>
		</div>
		<div class="actions">
			<?php echo $vendor_add_button ?>
		</div>
	</div>
	<div class="portlet-body form light-grey">
		<div class="form-body">
			<div class="row">
				<div class="col-md-12">

					<table class="table table-striped table-bordered table-hover" id="vendor_list">
						<thead>
						<tr>
							<th>Name</th>
							<th>Website</th>
							<th>Logo</th>
							<th>Actions</th>
						</tr>
						</thead>
						<tbody>

							<?php echo $vendors ?>

						</tbody>
					</table>

				</div>
			</div>
		</div>
	</div>
</div>
<!-- END VENDOR LIST PORTLET-->

<!-- BEGIN VENDOR INFO PORTLET-->
<div id="vendor_info_portlet" class="portlet box green-lilahmedia <?php echo $vendor_info_hide ?>">
	<div class="portlet-title">
		<div class="caption">
			<i class="fa fa-gear"></i>Builder Info
		</div>
		<div class="tools">
			<a id="vendor_info_collapse" class="collapse" href="javascript:;"></a>
		</div>
	</div>
	<div class="portlet-body form light-grey">
		<form enctype="multipart/form-data" id="vendor_info_form" class="form-horizontal" action="" method="post" role="form">
			<div class="form-body">
				<div class="form-group">
					<label for="vendor_info_name" class="col-md-3 control-label">Name:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="vendor_info_name" id="vendor_info_name"
							value="<?php echo $vendor_name ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="vendor_info_contact" class="col-md-3 control-label">Contact:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="vendor_info_contact" id="vendor_info_contact"
							value="<?php echo $vendor_contact ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="vendor_info_phone" class="col-md-3 control-label">Phone:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="vendor_info_phone" id="vendor_info_phone"
							value="<?php echo $vendor_phone ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="vendor_info_email" class="col-md-3 control-label">Email:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="vendor_info_email" id="vendor_info_email"
							value="<?php echo $vendor_email ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="vendor_info_website" class="col-md-3 control-label">Website:</label>
					<div class="col-md-5">
						<input type="text" class="form-control" name="vendor_info_website" id="vendor_info_website"
							value="<?php echo $vendor_website ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-3 control-label">Builder Logo:</label>
					<div class="col-md-9">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
								<img src="<?php echo $vendor_logo ?>" alt=""/>
							</div>
							<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
							</div>
							<div>
							<span class="btn default btn-file">
								<span class="fileinput-new"> Select image </span>
								<span class="fileinput-exists"> Change </span>
								<input type="file" name="vendor_info_logo">
							</span>
								<a href="#" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="form-actions fluid">
				<div class="text-center">
					<input type="hidden" name="vendor_form_submit" value="1">
					<button id="vendor_info_save" class="btn green-lilahmedia" type="submit">Save</button>
					<button id="vendor_info_cancel" class="btn default" type="button">Cancel</button>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- END VENDOR INFO PORTLET-->

</div>
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
