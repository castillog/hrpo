<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">

		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">

				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Integration
					<small>api management</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i> <a href="/admin/">Dashboard</a> <i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/admin/integration/">Integration</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->

			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN WIDGET PORTLET-->
      <?php /*
		<div id="widget_portlet" class="portlet box green-lilahmedia">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Widget
				</div>
				<div class="tools">
					<a id="widget_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<div class="row">
					<div class="col-md-12">
						<div id="widget_embed_box" class="panel panel-default">
							<div class="panel-heading">Copy and paste the following code to your preferred destination.</div>
							<div class="panel-body">
								<div id="widget_embed_code">

									<?php echo $widget_embed_code ?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		*/ ?>
		<!-- END WIDGET PORTLET-->

		<!-- BEGIN BOMBBOMB INTEGRATION PORTLET-->
		<?php /*
		<div id="bombbomb_portlet" class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>BombBomb
				</div>
				<div class="tools">
					<a id="bombbomb_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="bombbomb_form" class="form-horizontal" action="" method="post" role="form">
					<div id="bombbomb_flash" class="<?php echo $bombbomb_flash_class ?>" role="alert"><?php echo $bombbomb_flash_text ?></div>
					<div class="form-body">
						<h4 class="form-section">Lead Integration</h4>
						<div class="form-group">
							<label for="bombbomb_api_key" class="col-md-3 control-label">API Key:</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="bombbomb_api_key" name="bombbomb_api_key" placeholder="Enter API Key"
									value="<?php echo $bombbomb_api_key ?>" <?php echo $readonly ?>>
								<div id="bombbomb_verify_note" class="verify-note <?php echo $bombbomb_api_verified ?>"><?php echo $bombbomb_api_verified_note ?></div>
							</div>
							<div class="col-md-2">
								<button id="bombbomb_verify" class="btn green-lilahmedia" type="button">Verify</button>
							</div>
						</div>

						<div class="form-group">
							<label for="bombbomb_list_id" class="col-md-3 control-label">Contact List:</label>
							<div class="col-md-5">
								<select class="form-control" id="bombbomb_list_id" name="bombbomb_list_id" <?php echo $readonly ?>>

									<?php echo $bombbomb_lists ?>

								</select>
							</div>
						</div>
					</div>

					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="bombbomb_form_submit" value="1">
							<?php echo $bombbomb_save_button ?>
							<button id="bombbomb_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		*/ ?>
		<!-- END BOMBBOMB INTEGRATION PORTLET-->

		<!-- BEGIN REW INTEGRATION PORTLET-->
		<div id="rew_portlet" class="portlet box green-lilahmedia">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Real Estate Webmasters
				</div>
				<div class="tools">
					<a id="rew_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="rew_form" class="form-horizontal" action="" method="post" role="form">
					<div id="rew_flash" class="<?php echo $rew_flash_class ?>" role="alert"><?php echo $rew_flash_text ?></div>
					<div class="form-body">
						<h4 class="form-section">Lead Integration</h4>
						<div class="form-group">
							<label for="rew_domain" class="col-md-3 control-label">Domain Name:</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="rew_domain" name="rew_domain" placeholder="www.yourdomain.com"
									value="<?php echo $rew_domain ?>" <?php echo $readonly ?>>
							</div>
						</div>
						<div class="form-group">
							<label for="rew_api_key" class="col-md-3 control-label">API Key:</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="rew_api_key" name="rew_api_key" placeholder="Enter API Key"
									value="<?php echo $rew_api_key ?>" <?php echo $readonly ?>>
								<div id="rew_verify_note" class="verify-note <?php echo $rew_api_verified ?>"><?php echo $rew_api_verified_note ?></div>
							</div>
							<div class="col-md-2">
								<button id="rew_verify" class="btn green-lilahmedia" type="button">Verify</button>
							</div>
						</div>

						<div class="form-group">
							<label for="rew_list_id" class="col-md-3 control-label">Contact Group:</label>
							<div class="col-md-5">
								<select class="form-control" id="rew_list_id" name="rew_list_id" <?php echo $readonly ?>>

									<?php echo $rew_lists ?>

								</select>
							</div>
						</div>
					</div>

					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="rew_form_submit" value="1">
							<?php echo $rew_save_button ?>
							<button id="rew_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END REW INTEGRATION PORTLET-->

		<!-- BEGIN TOP PRODUCER INTEGRATION PORTLET-->
		<?php /*
		<div id="tp_portlet" class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Top Producer
				</div>
				<div class="tools">
					<a id="tp_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="tp_form" class="form-horizontal" action="" method="post" role="form">
					<div id="tp_flash" class="<?php echo $tp_flash_class ?>" role="alert"><?php echo $tp_flash_text ?></div>
					<div class="form-body">
						<h4 class="form-section">Lead Integration</h4>
						<div class="form-group">
							<label for="tp_api_key" class="col-md-3 control-label">Lead Email:</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="tp_lead_email" name="tp_lead_email" placeholder="Enter TP Lead Email Address"
									value="<?php echo $tp_lead_email ?>" <?php echo $readonly ?>>
							</div>
						</div>
					</div>

					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="tp_form_submit" value="1">
							<?php echo $tp_save_button ?>
							<button id="tp_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		*/ ?>
		<!-- END TOP PRODUCER INTEGRATION PORTLET-->

		<!-- BEGIN BOOMTOWN INTEGRATION PORTLET-->
		<?php /*
		<div id="boomtown_portlet" class="portlet box green-lilahmedia">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>BoomTown
				</div>
				<div class="tools">
					<a id="boomtown_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="boomtown_form" class="form-horizontal" action="" method="post" role="form">
					<div id="boomtown_flash" class="<?php echo $boomtown_flash_class ?>" role="alert"><?php echo $boomtown_flash_text ?></div>
					<div class="form-body">
						<h4 class="form-section">Lead Integration</h4>
						<div class="form-group">
							<label for="boomtown_api_key" class="col-md-3 control-label">Lead Email:</label>
							<div class="col-md-5">
								<input type="text" class="form-control" id="boomtown_lead_email" name="boomtown_lead_email" placeholder="Enter BoomTown Lead Email Address"
									value="<?php echo $boomtown_lead_email ?>" <?php echo $readonly ?>>
							</div>
						</div>
					</div>

					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="boomtown_form_submit" value="1">
							<?php echo $boomtown_save_button ?>
							<button id="boomtown_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		*/ ?>
		<!-- END BOOMTOWN INTEGRATION PORTLET-->

	</div>
</div><!-- END CONTENT -->

</div><!-- END CONTAINER -->
