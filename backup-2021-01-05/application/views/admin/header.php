<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class="no-js">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title><?php echo $title ?></title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="description" content="<?php echo $meta_desc ?>" />
	<meta name="keywords" content="<?php echo $meta_keywords ?>" />
	<meta name="author" content="" />

	<!-- BEGIN GLOBAL MANDATORY STYLES -->
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|Montserrat:400,700|Lora:400,700|Open+Sans:400,300,600,700&subset=all" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/simple-line-icons/simple-line-icons.min.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/uniform/css/uniform.default.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-switch/css/bootstrap-switch.min.css" />
	<!-- END GLOBAL MANDATORY STYLES -->

	<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/gritter/css/jquery.gritter.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-datepicker/css/datepicker3.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-datetimepicker/css/datetimepicker.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/fullcalendar/fullcalendar/fullcalendar.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/jqvmap/jqvmap/jqvmap.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/select2/select2.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/fancybox/source/jquery.fancybox.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/youtube-video-gallery/youtube-video-gallery.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/orakuploader/orakuploader.css"/>
	<link rel="stylesheet" type="text/css" href="/includes/admin/plugins/bootstrap-summernote/summernote.css"/>
	<!-- END PAGE LEVEL PLUGIN STYLES -->

	<!-- BEGIN PAGE LEVEL STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/tasks.css" />
	<!-- END PAGE LEVEL STYLES -->

	<!-- BEGIN THEME STYLES -->
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/components.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/plugins.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/layout.css" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/themes/lilahmedia.css" id="style_color" />
	<link rel="stylesheet" type="text/css" href="/includes/admin/css/custom.css" />
	<!-- END THEME STYLES -->

	<link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body class="page-header-fixed page-quick-sidebar-over-content <?php echo $body_class?>">
<!-- BEGIN HEADER -->

<div class="page-header navbar navbar-fixed-top">
	<!-- BEGIN HEADER INNER -->
	<div class="page-header-inner">
		<!-- BEGIN LOGO -->
		<div class="page-logo">
			<a href="/">
				<div class="logo-text-company">
					<span class="logo-text-raised">S</span>ite
					<span class="logo-text-raised">A</span>dmin -
					<span class="logo-text-raised">L</span>onnie
					<span class="logo-text-raised">B</span>ush

				</div>
			</a>

			<div class="menu-toggler sidebar-toggler hide">
				<!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
			</div>
		</div>
		<!-- END LOGO -->
		<!-- BEGIN RESPONSIVE MENU TOGGLER -->
		<a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
			data-target=".navbar-collapse">
		</a>
		<!-- END RESPONSIVE MENU TOGGLER -->
		<!-- BEGIN TOP NAVIGATION MENU -->
		<div class="top-menu">
			<ul class="nav navbar-nav pull-right">
				<!-- BEGIN USER LOGIN DROPDOWN -->
				<li class="dropdown dropdown-user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
						data-close-others="true">
						<!--<img alt="" class="/includes/admin/img-circle" src="/includes/admin/img/avatar_small.png" />-->
						<span class="username"><?php echo $user_name ?> </span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="/admin/logout/">
								<i class="icon-key"></i> Log Out </a>
						</li>
					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->
				<!-- BEGIN QUICK SIDEBAR TOGGLER -->
				<!--<li class="dropdown dropdown-quick-sidebar-toggler">
					<a href="javascript:;" class="dropdown-toggle">
						<i class="icon-logout"></i>
					</a>
				</li>-->
				<!-- END QUICK SIDEBAR TOGGLER -->
			</ul>
		</div>
		<!-- END TOP NAVIGATION MENU -->
	</div>
	<!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix"></div>
