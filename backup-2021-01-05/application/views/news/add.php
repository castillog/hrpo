<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">

		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">

				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Blog
					<small>Lonnie Bush Blog Articles</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i> <a href="/admin/">Dashboard</a> <i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/admin/news/">Blog</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->

			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN USER LIST PORTLET-->
				<!-- BEGIN VENDORS FAQ PORTLET-->
				<div id="pages_vendors_faq_portlet" class="portlet box grey-gallery <?php echo $pages_vendors_faq_hide ?>">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gear"></i>New Blog Article
						</div>
					</div>
					<div class="portlet-body form light-grey">
						<?php echo validation_errors(); ?>
						<form id="blog_form" class="form" action="/admin/news/add" method="post" role="form" enctype="multipart/form-data">
							<div class="form-body">
								<div class="row">
									<div class="col-md-12">
										
										<h4 class="form-section">Content</h4>
										
										<div class="form-group col-md-12">
											<label for="vendors_faq_title">Title</label>
											<input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
										</div>

										<div class="form-group col-md-12">
											<label for="vendors_faq_title">Slug</label>
											<input type="text" class="form-control" id="slug" name="slug" placeholder="Enter Slug" value="<?php echo $vendors_faq_title ?>" <?php echo $readonly ?>>
										</div>
										
										<div class="form-group col-md-12">
											<label for="is_blog">Display in Blog?</label>
											<div>
											<input type="radio" class="form-control" id="Yes" name="is_blog" value="Yes" placeholder="Enter Slug" checked>
											<label for="Yes">Yes</label>
											</div>
											<div>
											<input type="radio" class="form-control" id="No" name="is_blog" value="No" placeholder="Enter Slug">
											<label for="No">No</label>
											</div>
										</div>										
										
										<div class="form-group col-md-12">
											<label for="vendors_faq_text">Text</label>
											<textarea class="summernote form-control" rows="25" name="text" id="text"></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										
										<h4 class="form-section">SEO Details</h4>
										
										<div class="form-group col-md-12">
											<label for="seo_title">SEO Title</label>
											<input type="text" class="form-control" id="seo_title" name="seo_title" placeholder="Enter Title" >
										</div>
										
										<div class="form-group col-md-12">
											<label for="seo_title">Meta Description</label>
											<input type="text" class="form-control" id="seo_desc" name="seo_desc" placeholder="Enter Title">
										</div>
																				

									</div>
								</div>								
							</div>
							<div class="form-actions fluid">
								<div class="text-center">
									<input type="submit" name="submit" value="Save Blog Post" class="btn btn-sucess"/>
									<button id="pages_vendors_faq_cancel" class="btn default" type="button">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END VENDORS FAQ PORTLET-->
		
	</div>
</div><!-- END CONTENT -->

</div><!-- END CONTAINER -->