<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">

		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">

				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Blog
					<small>Lonnie Bush Blog Articles</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i> <a href="/admin/">Dashboard</a> <i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/admin/news/">Blog</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->

			</div>
			<div class="col-12">
				<h1 class="text-success">Your new blog post has been created!</h1>
			</div>
		</div>
		
		<!-- END PAGE HEADER-->					
	</div>
</div><!-- END CONTENT -->

</div><!-- END CONTAINER -->