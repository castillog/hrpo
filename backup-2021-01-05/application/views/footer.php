<?php

// Grab our company/social media content data
$content_about    = $this->Content->get_content("about");
$content_settings = $this->Content->get_content("settings");

$about_title = (!empty($content_about['content']['title'])) ? $content_about['content']['title'] : "";
$about_text  = (!empty($content_about['content']['text'])) ? $content_about['content']['text'] : "";

$company_name            = (!empty($content_settings['company']['name'])) ? $content_settings['company']['name'] : "";
$company_street          =	(!empty($content_settings['company']['street'])) ? $content_settings['company']['street'] : "";
$company_city            = (!empty($content_settings['company']['city'])) ? $content_settings['company']['city'] : "";
$company_state           = (!empty($content_settings['company']['state'])) ? $content_settings['company']['state'] : "";
$company_zipcode         =	(!empty($content_settings['company']['zipcode'])) ? $content_settings['company']['zipcode'] : "";
$company_phone           = (!empty($content_settings['company']['phone'])) ? $content_settings['company']['phone'] : "";
$company_email           = (!empty($content_settings['company']['email'])) ? $content_settings['company']['email'] : "";
$company_facebook_url    =	(!empty($content_settings['company']['facebook_url'])) ? $content_settings['company']['facebook_url'] : "";
$company_twitter_url     =	(!empty($content_settings['company']['twitter_url'])) ? $content_settings['company']['twitter_url'] : "";
$company_google_url      =	(!empty($content_settings['company']['google_url'])) ? $content_settings['company']['google_url'] : "";
$company_linkedin_url    =	(!empty($content_settings['company']['linkedin_url'])) ? $content_settings['company']['linkedin_url'] : "";
$company_youtube_url     =	(!empty($content_settings['company']['youtube_url'])) ? $content_settings['company']['youtube_url'] : "";
$company_pinterest_url   =	(!empty($content_settings['company']['pinterest_url'])) ? $content_settings['company']['pinterest_url'] : "";
$company_adwords_embed   = (!empty($content_settings['site']['adwords_embed'])) ? $content_settings['site']['adwords_embed'] : "";
$company_analytics_embed = (!empty($content_settings['site']['analytics_embed'])) ? $content_settings['site']['analytics_embed'] : "";
$company_facebook_embed  = (!empty($content_settings['site']['facebook_embed'])) ? $content_settings['site']['facebook_embed'] : "";
$company_bing_embed      = (!empty($content_settings['site']['bing_embed'])) ? $content_settings['site']['bing_embed'] : "";
$company_misc_embed      = (!empty($content_settings['site']['misc_embed'])) ? $content_settings['site']['misc_embed'] : "";

$company_logo_footer_id     = (!empty($content_settings['company']['logo_footer'])) ?
	$content_settings['company']['logo_footer'] :	"";
$company_logo_footer = $this->Content->get_picture($company_logo_footer_id);
$company_logo_footer = ($company_logo_footer) ? $company_logo_footer['image'] : "";

?>

<!-- page footer -->
<footer class="page-footer">
	<a href="#" id="top-link" class="top-link"><i class="fa fa-angle-double-up"></i></a>

		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<h3>Stay Connected With Us</h3>
					<div id="social-icons">
						<?php if(!empty($company_facebook_url)) : ?>
							<a href="<?php echo $company_facebook_url ?>"><i class="fa fa-facebook"></i></a>
						<?php endif; ?>
						<?php if(!empty($company_twitter_url)) : ?>
							<a href="<?php echo $company_twitter_url ?>"><i class="fa fa-twitter"></i></a>
						<?php endif; ?>
						<?php if(!empty($company_google_url)) : ?>
							<a href="<?php echo $company_google_url; ?>"><i class="fa fa-google-plus"></i></a>
						<?php endif; ?>
						<?php if(!empty($company_linkedin_url)) : ?>
							<a href="<?php echo $company_linkedin_url ?>"><i class="fa fa-linkedin"></i></a>
						<?php endif; ?>
						<?php if(!empty($company_youtube_url)) : ?>
							<a href="<?php echo $company_youtube_url ?>"><i class="fa fa-youtube"></i></a>
						<?php endif; ?>
						<?php if(!empty($company_pinterest_url)) : ?>
							<a href="<?php echo $company_pinterest_url ?>"><i class="fa fa-pinterest"></i></a>
						<?php endif; ?>
					</div>
					<div style="height: 50px;width:auto;margin-top:15px;">
						<p>
							Copyright &copy; <?php echo date('Y'); ?>. All Rights Reserved | <a href="http://www.lilahmedia.com"
								target="_blank"> Website Design | SEO &amp; Internet Marketing by Lilah Media. </a>
						</p>
					</div>
				</div>
				<div class="col-md-2" style="text-align: center;">
				
				</div>
				<div class="col-md-5" style="text-align: right;">
					<p>
						<h3>Our Office</h3>
						<?php echo $company_name; ?><br>
						<?php echo $company_street . ", " . $company_city . ", " . $company_state . " " . $company_zipcode ?>
					</p>
					<p>
						Ph: <?php echo $company_phone ?>
					</p>
					<img src="/data/images/handicap_icon.png" alt="" style="height: 50px;width:auto;margin-right:15px;">
					<img src="/data/images/logo-white.png" alt="Lonnie Bush Property Management" style="height: 50px;width:auto;">
				</div>
			</div>
		</div>
	<!-- end container -->

</footer>
<!--/ page footer -->

<div class="page-footer-base">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<a href='/sitemap.xml'>Sitemap</a>
			</div>
		</div>
	</div>
</div>

<!-- scripts -->
<script type="text/javascript" src="/includes/js/jquery.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery.migrate.min.js"></script>
<script type="text/javascript" src="/includes/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCJNXhmSMuLa3Voh2xy_4qE7B9KUUWRzCY"></script>
<script type="text/javascript" src="/includes/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery.isotope.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/includes/js/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="/includes/js/jquery.flot.js"></script>
<script type="text/javascript" src="/includes/js/jquery.flot.pie.js"></script>
<script type="text/javascript" src="/includes/js/jquery.flot.categories.js"></script>
<script type="text/javascript" src="/includes/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="/includes/js/greensock.js"></script>
<script type="text/javascript" src="/includes/js/layerslider.transitions.js"></script>
<script type="text/javascript" src="/includes/js/layerslider.kreaturamedia.jquery.js"></script>

<!-- Superscrollorama -->
<script type="text/javascript" src="/includes/js/jquery.superscrollorama.js"></script>
<script type="text/javascript" src="/includes/js/TweenMax.min.js"></script>
<script type="text/javascript" src="/includes/js/TimelineMax.min.js"></script>
<!--/ Superscrollorama -->

<script type="text/javascript" src="/includes/js/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery.ui.tabs.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery-ui-tabs-rotate.js"></script>
<script type="text/javascript" src="/includes/js/jquery.ui.accordion.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery.tweet.js"></script>
<!-- EASYPIECHART -->
<script type="text/javascript" src="/includes/js/jquery.easypiechart.js"></script>
<!--/ EASYPIECHART -->
<script type="text/javascript" src="/includes/js/jquery.autocomplete.min.js"></script>
<!--<script type="text/javascript" src="/includes/js/scripts.js"></script>-->
<script type="text/javascript" src="/includes/js/custom.js"></script>
<!--/ scripts -->

<?php echo $footer_script; ?>

<?php echo $company_adwords_embed; ?>
<?php echo $company_analytics_embed; ?>
<?php echo $company_facebook_embed; ?>
<?php echo $company_bing_embed; ?>
<?php echo $company_misc_embed; ?>

</body>
</html>
