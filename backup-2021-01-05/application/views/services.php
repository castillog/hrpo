<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Services</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="#">Services</a>
		</nav>
	</div>
</section><!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Management</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

					<p>
						At <a href="//hamptonroadspropertyowners.com">Lonnie Bush Property Management</a>, we offer a wide range of
						property management services to fit your needs as a property owner. We ensure that we over
						deliver when it comes to the management of your home. </p>

					<ul>
						<li>Perform a market analysis to determine fair market value</li>
						<li>Market your property across the web for maximum exposure to buyers</li>
						<li>List your property on the MLS so thousands of Realtors in Hampton Roads can show it</li>
						<li>Complete a tenant application</li>
						<li>Process income verification for tenant</li>
						<li>Thoroughly check references including credit reports</li>
						<li>Contact former landlord and employers</li>
						<li>Perform criminal background check</li>
						<li>Run National eviction search</li>
						<li>Twice a year Property Inspections</li>
						<li>Drafting of lease documents and proper landlord disclosure statements</li>
						<li>Collection of rent and automatic deposits into your account</li>
						<li>Move-In and Move-Out walk-throughs</li>
						<li>We handle all landlord/tenant communication, contractor services and various professionals available at all times</li>
					</ul>

				</div>
			</div>
		</section>
	</div>

	<div class="grid-row">
		<div class="col-md-12 col-sm-12 block">
			<section>
				<div class="row">
					<div class="col-md-12">
						<div class="widget-title">Marketing</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-8">
						<p>
							Lonnie Bush Property Management has a nearly unlimited budget to market your home. We use the latest technology, creative and innovative marketing strategies
							to expose your home to prospective tenants. This allows us to get your home rented in the shortest possible time and for the best possible price. We generate
							hundreds of tenant leads each month and match them up to our available listings before searching outside the company. </p>
					</div>
					<div class="col-md-4">
						<img src="/includes/pic/websites_400.jpg" style="max-width: 300px;margin: 0px auto;">
					</div>
				</div>

			</section>
		</div>
		<!-- /.col-md-12 -->
	</div>

	<div class="grid-row">
		<div class="col-md-12 col-sm-12 block">
			<section>
				<div class="row">
					<div class="col-md-12">
						<div class="widget-title">Screening and Selecting Tenants</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<p>All property management companies say they screen their tenants and that they run credit checks, but do you think a credit check provides enough information
							about a tenant to make a decision to allowing them into your property? We do not. That's why we take this process seriously and perform a through background
							check of all applicants. That's the reason why our eviction rate is so low, and we have less problems collecting rent and keeping tenants in our
							properties.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h3>Screening Process</h3>
						<ul>
							<li> Complete credit evaluation</li>
							<li> Employment verification</li>
							<li> Income verification</li>
							<li> Criminal background check</li>
							<li> Eviction history</li>
							<li> Sex offender verification</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h3>Move-In Process</h3>
						<ul>
							<li> Write up leasing agreement</li>
							<li> Confirm move-in date</li>
							<li> Review all lease guidelines, terms and conditions</li>
							<li> Collect first months rent and security deposit</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h3>Rent Collection</h3>
						<ul>
							<li> Receiving rent</li>
							<li> Obtaining late payments</li>
							<li> Sending out Pay-or-quite notices</li>
							<li> Collecting late fees</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h3>Evictions</h3>
						<ul>
							<li> Filing all paperwork to initiate unlawful detainer</li>
							<li> Work with attorney for legal representation in court</li>
							<li> Coordinate with law enforcement for eviction and possession of property</li>
							<li> Advise in the event of a legal dispute</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h3>Inspections</h3>
						<ul>
							<li> Fully inspect property twice per year both inside and out</li>
							<li> Send owner reports and photos on the condition of the property</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h3>Financial</h3>
						<ul>
							<li> Provide full accounting and management services</li>
							<li> Detailed reports on expenses via receipts and invoices</li>
							<li> Maintain historical records of invoices, leases, warranties, inspections, etc.</li>
							<li> Provide all required tax forms including 1099</li>
						</ul>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<h3>Maintenance and Repairs</h3>
						<ul>
							<li> Work with trustworthy contracts for repairs and maintenance issues</li>
							<li> Establish preventative maintenance policy to uncover issues before they arise</li>
							<li> Maintain 24-hour emergency contact for tenants</li>
							<li> Provide recommendations for larger repairs, remodeling, and rehab projects</li>
						</ul>
					</div>
					<div class="col-md-4">
						<h3>Tenant Move-out</h3>
						<ul>
							<li> Thoroughly inspect property and report on condition</li>
							<li> Provide tenant with copies of all reports and estimated damages</li>
							<li> Return the security deposit to tenant</li>
							<li> Clean property and advise on any repairs</li>
							<li> Re-list property for rent</li>
						</ul>
					</div>
				</div>
			</section>

			<section id="question-form">

				<div class="widget-title">Have a question? Let us know!</div>

				<div id="ask_question_flash" class="<?php echo $flash_class ?>" role="alert">
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<?php echo $flash_msg ?>
				</div>

				<form id="ask_question_form" method="post" action="" role="form">
					<div class="row">

						<div class="col-md-6">
							<div class="form-group">
								<label for="ask_question_name">First Name<em>*</em></label>
								<input type="text" class="form-control" id="ask_question_firstname" name="ask_question_firstname"
									required>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col-md-6 -->

						<div class="col-md-6">
							<div class="form-group">
								<label for="ask_question_name">Last Name<em>*</em></label>
								<input type="text" class="form-control" id="ask_question_lastname" name="ask_question_lastname"
									required>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col-md-6 -->

						<div class="col-md-6">
							<div class="form-group">
								<label for="ask_question_email">Email<em>*</em></label>
								<input type="email" class="form-control" id="ask_question_email" name="ask_question_email"
									required>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col-md-6 -->

						<div class="col-md-6">
							<div class="form-group">
								<label for="ask_question_phone">Phone</label>
								<input type="text" class="form-control" id="ask_question_phone" name="ask_question_phone"
									required>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col-md-6 -->
					</div>
					<!-- /.row -->

					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								<label for="ask_question_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="ask_question_message" rows="8" name="ask_question_message"
											required></textarea>
							</div>
							<!-- /.form-group -->
						</div>
						<!-- /.col-md-12 -->
					</div>
					<!-- /.row -->

					<div class="form-group clearfix">
						<input type="hidden" id="ask_question_form_submit" name="ask_question_form_submit" value="1">
						<button type="submit" class="button pull-right" id="ask_question_submit">Send a Message</button>
					</div>
					<!-- /.form-group -->
					<div id="form-status"></div>
				</form>
				<!-- /#form-contact -->
			</section>
		</div>
	</div>
</main><!--/ page content -->
