<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Find a Rental</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="#">Find A Rental</a>
		</nav>
	</div>
</section><!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">
					
					<?php /*
					<div style="font-size: 1.2em; color: #1e1e1e;margin-bottom: 15px;">To narrow down your Rental Search, click on the “FILTER" option on the very right of the search bar.</div>
					<script type="text/javascript" charset="utf-8">
						document.write(unescape("%3Cscript src='" + (('https:' == document.location.protocol) ? 'https:' : 'http:') +
							"//remax.appfolio.com/javascripts/listing.js' type='text/javascript'%3E%3C/script%3E"));
					</script>
					<script type="text/javascript" charset="utf-8">
						Appfolio.Listing({
							hostUrl: 'remax.appfolio.com',
							//propertyGroup: 'My Group Name',
							themeColor: '#D2000A',
							width: '100%'
						});
					</script>
					*/ ?>
					
					<p id="ttWidget"><!-- Tenant Turner widget -->
						<script type="text/javascript">// <![CDATA[
							try {
								window.tenantTurnerSettings = {
									// Your Tenant Turner customer ID (pre-filled)
									customer_id: 1967,

									// Enable the map view, true or false
									map: true,

									// Show the map view on page load, true or false
									default_to_map: false,

									// Show the map and listing views together
									show_map_and_listings: false,

									// Map height and width
									map_height: "600px",
									map_width: "100%",

									// Default sort and search
									default_sort: "Newest", // Options: "Newest", "Most Bedrooms", "Lowest Rent", "Highest Rent"
									default_search: "", // Any search keyword (can be overidden with "search" URL parameter)

									// Theme color (do not include the #)
									theme_color: "0d99dd",

									// Auto-detect http/https (do not change)
									protocol: window.location.toString().split('://')[0]
								};
							} catch (err) { if (console) { console.log('TT widget error: ' + err); } }
							// ]]></script>
						<script type="text/javascript">// <![CDATA[
							try {(function () {var w = window;var tt = w.TenantTurner;if (typeof tt !== "function") {var d = document;function l() {var s = d.createElement('script');s.type = 'text/javascript';s.async = true;s.src = (window.tenantTurnerSettings.secure == 'true' ? 'https:' : window.tenantTurnerSettings.protocol == 'https' ? 'https:' : 'http:') + (window.tenantTurnerSettings.env_scripts ? window.tenantTurnerSettings.env_scripts + '/widget/widget.js' : '//app.tenantturner.com/widget/widget.js');var x = d.getElementById('ttWidget');x.parentNode.insertBefore(s, x);}if (w.attachEvent) {w.attachEvent('onload', l);} else {w.addEventListener('load', l, false);}}})()} catch (err) {if(console){console.log('TT widget error: ' + err)}}
							// ]]></script>
						<!-- end Tenant Turner widget --></p>
						<a href="//hamptonroadspropertyowners.com">Back to the Homepage</a>
				</div>
			</div>
		</section>
	</div>
</main><!--/ page content -->
