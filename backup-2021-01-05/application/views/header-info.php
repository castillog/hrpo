<?php

$title       = (!empty($title)) ? $title : '';
$description = (!empty($description)) ? $description : '';
$keywords    = (!empty($keywords)) ? $keywords : '';
$bodyClass   = (!empty($bodyClass)) ? $bodyClass : '';

// Set up our default navbar button classes
$nav_active['home']     = (!empty($nav_active['home'])) ? $nav_active['home'] : '';
$nav_active['services'] = (!empty($nav_active['services'])) ? $nav_active['services'] : '';
$nav_active['search']   = (!empty($nav_active['search'])) ? $nav_active['search'] : '';
$nav_active['owners']   = (!empty($nav_active['owners'])) ? $nav_active['owners'] : '';
$nav_active['tenants']  = (!empty($nav_active['tenants'])) ? $nav_active['tenants'] : '';
$nav_active['about']    = (!empty($nav_active['about'])) ? $nav_active['about'] : '';
$nav_active['contact']  = (!empty($nav_active['contact'])) ? $nav_active['contact'] : '';

?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>

	<!-- metas -->
	<meta charset="utf-8">
	<meta name="author" content="Lonnie Bush Property Management">
	<meta name="keywords" content="<?php echo $keywords ?>">
	<meta name="description" content="<?php echo $description ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<!--/ metas -->

	<!-- styles -->
	<link rel="stylesheet" type="text/css" href="/includes/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/layerslider.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/fullwidth/skin.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/styles.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/color-red.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/custom.css">

	<!--/ styles -->

	<!--[if lt IE 9]>
	<script src="/includes/js/html5.js"></script><![endif]-->
</head>

<body class="<?php echo $bodyClass ?>">
<div class="page">

	<!-- page header -->
	<header class="page-header main-page sticky">

		<div class="sticky-wrapp">
			<div class="sticky-container">
				<!-- logo -->
				<section id="logo" class="logo">
					<div>
						<a href="/"><img src="/data/images/LBpropmgmt.jpg" alt="Lonnie Bush Property Management"></a>
					</div>
				</section>
				<!--/ logo -->

			</div>
		</div>
	</header>
	<!--/ page header -->

	<!-- quick search -->
	<div id="quick-search" class="quick-search">
		<fieldset></fieldset>
	</div>
	<!--/ quick search -->
