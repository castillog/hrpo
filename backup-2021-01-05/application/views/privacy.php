<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Privacy Policy</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;
			<a href="#">Privacy Policy</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

					<p>By accessing the LonnieBush.com, you consent to the collection and use of information as described herein. We reserve the right to change this privacy statement at any time. If we make any material changes to our privacy statement, we will post a new statement on the Website and update the revision date.</p>

					<h4>Information we collect</h4>

					<p>Personal Information. When you register for certain services on the Website we may ask you to provide personal information. When you make inquiries through our services or contact us for support we may also collect personal information. For some services we collect personal information, such as your e-mail address, name, home or work address or telephone number. We may collect information about your visit to the Website, including the pages you view, the links you click and other actions taken in connection with the Website and services. We may also collect certain standard information that your browser sends to every web site you visit, such as your IP address, browser type and language, access times and referring Web site addresses. Our Website may also provide you with various services related to home selling and home buying (e.g., estimates of current market value of homes, rental values, moving costs, mortgage or finance costs, current home listings, neighborhood information, etc.) in these instances we may collect additional information related to the services. In order for us to provide these services, we also may collect supplementary personally identifying information about you from third party sources to correct or supplement the information we have requested from you. By filling out any forms on the Website, providing information to us or making any inquiry, you acknowledge that we have an established business relationship and you expressly consent to being contacted by us or our service providers, whether by phone, mobile phone, email, mail, texting or otherwise.</p>

					<h4>Use of personal information</h4>

					<p>We use personal information internally to serve our users and to enhance and extend our relationship with our users. We may, from time to time, contact you on behalf of external business partners about a particular offering that may be of interest to you. In those cases, your personal information (e-mail, name, address, telephone number) may be transferred to the third party. By registering for our Website or otherwise submitting an inquiry to us you agree to receive the materials described herein. You may unsubscribe at any time from receiving non-service related communications.</p>

					<h4>Disclosure of personal information</h4>

					<p>Except as set forth in this privacy statement, we do not disclose, sell, rent, loan, trade or lease any personally identifying information collected at our Website to any third party.</p>

					<p>Transaction Processing. We will disclose personal information to one or more third parties, as necessary, to provide you with the services. As stated above, your personal information is forwarded to one or more third party service providers (such as real estate agents, brokers, lenders and other real estate professionals) in order to provide you with the services. If a particular service provider is unable to fulfill your request within a reasonable time, we may forward your information to an alternate service provider to ensure that your request is fulfilled. Service providers may contact you to complete the transactions you have requested by any methods available to him or her including via e-mail, telephone, regular mail, in person visits or other means. We do not regulate how these third party service providers use or disclose your personal information and are not responsible for removing your information from any third party lists. Please contact the third party service providers directly to be removed from their lists.</p>

					<h4>Cookies and web beacons</h4>

					<p>We may use cookies to identify users and customize their experience on our website based on information they give us. Cookies are small pieces of information or files that are stored on a userÕs computer by the Web server through a userÕs browser for record keeping purposes. A cookie itself is not designed or intended to read any information from your computer (other than the contents of the cookie); rather, it is an identifier used by the Web site that originally placed it on your hard drive. The actual contents of the cookie information can be retrieved by the same siteÕs Web server in order to personalize, monitor, or regulate the use of the site. We do not use Web beacons, or similar technology on our Website.</p>

					<h4>Security</h4>

					<p>We use reasonable security measures to store and maintain personally identifying information to protect it from loss, misuse, alternation or destruction by any unauthorized party while it is under our control.</p>

					<h4>Use of third party ad networks</h4>

					<p>We may allow other companies, called third-party ad servers or ad networks, to display advertisements on the Website. Some of these ad networks may place a persistent cookie on your computer in order to recognize your computer each time they send you an online advertisement. In this way, ad networks may compile information about where you, or others who are using your computer, saw their advertisements and determine which ads are clicked on. This information allows an ad network to deliver targeted advertisements that they believe will be of most interest to you. We do not have access to the cookies that may be placed by the third-party ad servers or ad networks.</p>

					<h4>Children / Minors</h4>

					<p>We do not intend, and this Website is not designed, to collect personal information from children or minors under the age of 18. If you are under 18 years old, you should not register for our services or otherwise provide information on our Website.</p>

					<h4>Choice and access</h4>

					<p>LonnieBush.com gives users the following options for accessing, changing and deleting personal information previously provided, or opting out of receiving communications from us email us at support@lilahmedia.com.</p>

					<h4>Questions and concerns</h4>

					<p>If you have any questions about this privacy statement, the practices of our Website, or your dealings with our Website, please contact us at the following address or email us at support@lilahmedia.com</p>

					<p>LonnieBush.com, 770 Lynnhaven Pkwy, Ste 120, Virginia Beach, VA 23452</p>

					<p>Last revised: July 2, 2015</p>

				</div>
			</div>
		</section>
	</div>
</main>
<!--/ page content -->
