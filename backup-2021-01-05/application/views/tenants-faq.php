<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Tenants</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;
			<a href="/tenants/faq/">Tenant FAQ</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Tenant FAQ</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

                    <p>Below is a list of the most frequent questions we get asked about Lonnie Bush Property
                        Management. If you have any other questions that are not listed here, please take a moment to
                        fill out the contact form at the bottom of the page or call us at 757-610-9800. We'll get back
                        to you promptly with an answer!</p>

					<!-- accordion -->
					<div class="wpb_accordion wpb_accordion_alt wpb_content_element" data-active-tab="1">
						<div class="wpb_accordion_wrapper" role="tablist">



							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header ui-state-focus"><a>How do I pay rent?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>You can pay your rent via the Tenant Portal or in person at our office.</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header ui-state-focus"><a>When is my rent due?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>Rent is due on the 1st late after the 5th.</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header ui-state-focus"><a>Is my deposit refundable?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>Your deposit is refundable as long as the property is left in move in
											condition, rent is paid in full, all of the items on the check list are
											done and there are no damages during your tenancy.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header ui-state-focus"><a>I want a pet what do I do?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>If you would like to get a pet you must first have the pet approved by
											the landlord.  Contact our office in writing for approval.  If you get a
											pet without prior approval you are in violation of your lease and there is
											a $500 charge due immediately.  You also may have to get rid of the pet
											depending on the landlords decision.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>If I have a pet(s), will my rent be more?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>There are fees for pets.  This will be discussed with you when you
											request pet approval.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>Can I terminate my lease with 30 days notice?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>You can not terminate your lease prior to lease expiration unless you have
											received military orders to leave the area.  In that case you would use
											your Military Clause and give 30 days written notice.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>Can I refuse to pay rent if I feel repairs are not being done to my rental?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>
											You can not refuse to pay rent.  If there is a discrepancy or repairs not
											being made you will have to make arrangements with the court in your city to
											pay the rent to them until the violations are taken care of.  If you do not
											pay your rent to us you will need to pay it to the courts or you could
											face eviction.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>Can my landlord inspect the rental at anytime?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>We do a minimum of 2 inspections per year.  The owner may ask us to come out
											and inspect a property or if we have had complaints regarding a property we
											may set up an inspection but we will always give you notice that we are
											coming out.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>In case of natural disaster (fire, storm, flood, etc) and belongings are damaged.  Is the landlord responsible?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>We require all tenants to have their own renters insurance policy.  This
											covers your belongings in case of any type of disaster etc.  The owner is
											not responsible for a tenants personal property.
										</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>What must happen for my landlord to evict me?</a></h3>
								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>In order to be evicted you must be in violation of your lease.  Whether it
											be for non payment of rent or other circumstances such as not taking care
											of or damaging the property or behavior such as illegal use of the property
											or illegal behaviors at the property.
										</p>
									</div>
								</div>
							</div>

						</div>
					</div>
					<!--/ accordion-->


				</div>
			</div>
		</section>

		<section id="question-form">

			<div class="widget-title">Have a question? Let us know!</div>

			<div id="ask_question_flash" class="<?php echo $flash_class ?>" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php echo $flash_msg ?>
			</div>

			<form id="ask_question_form" method="post" action="" role="form">
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">First Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_firstname" name="ask_question_firstname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">Last Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_lastname" name="ask_question_lastname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_email">Email<em>*</em></label>
							<input type="email" class="form-control" id="ask_question_email" name="ask_question_email"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_phone">Phone</label>
							<input type="text" class="form-control" id="ask_question_phone" name="ask_question_phone"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="ask_question_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="ask_question_message" rows="8" name="ask_question_message"
											required></textarea>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-12 -->
				</div>
				<!-- /.row -->

				<div class="form-group clearfix">
					<input type="hidden" id="ask_question_form_submit" name="ask_question_form_submit" value="1">
					<button type="submit" class="button pull-right" id="ask_question_submit">Send a Message</button>
				</div>
				<!-- /.form-group -->
				<div id="form-status"></div>
			</form>
			<!-- /#form-contact -->
		</section>

	</div>
</main>
<!--/ page content -->
