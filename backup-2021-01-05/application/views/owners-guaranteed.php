<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Owners</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/owners/guaranteeed/">Guaranteed Rent Program</a>
		</nav>
	</div>
</section><!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Guaranteed Rent Program</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

					<div style="font-size: 1.5em; color: #1e1e1e;margin-bottom: 15px;">OUR RESULTS ARE GUARANTEED....If We Don't Lease Your Home in 60 Days, We'll Pay The Rent!</div>

					<!-- accordion -->
					<div class="wpb_accordion wpb_accordion_alt wpb_content_element" data-active-tab="1">
						<div class="wpb_accordion_wrapper" role="tablist">

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header ui-state-focus"><a>How can we offer this?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>At <a href="//hamptonroadspropertyowners.com">Lonnie Bush Property Management</a>, we maybe the only Property Management company in the Hampton Roads area, willing to put our money where our mouth is. We
											created this program because we have been doing this long enough that we know which marketing strategies work and which ones don't. We have an
											enormous database of prospective renters in the area who are always looking for homes to rent. Between our client database and our extensive
											marketing plan, we have no problem making a written guarantee that we will have a signed lease on your home or we will pay the rent!</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>How Does This Benefit You?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>If you’re out shopping for a new home, and you see one that you like, you can place an offer on it immediately! Also, if you are not completely
											satisfied with our level of customer service or marketing efforts, then you can cancel the listing at any time. No matter what happens, you know
											the absolute bottom line you will get for rent.</p></div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>Do I Have To Do The Guaranteed Rent Program?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>Absolutely not. This program is not for everyone. If you elect not to get involved in the "Guaranteed Rent" contract, we will still implement our
											entire marketing plan to rent your home to the right tenant in the shortest amount of time.</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>What Is My “Guaranteed Rent” Price?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>After we complete an in-depth examination of your home we will perform a detailed market analysis and present the entire pricing structure to you
											BEFORE we ever start marketing your home. You will then have an opportunity to hire us to manage your home.. </p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>How Much Is My Home Worth In This Market?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>If you would like a Free Price Evaluation OR to know what your Guaranteed Rent Price would be, please fill out the contact form below and we will
											get back to you shortly. </p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>What If That Tenant Does Not Fulfill The Lease Agreement?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p> If both you and Lonnie Bush Property Management agree to enter into a Guaranteed Rent Contract, we will also guarantee that the tenant we place in the
											property will pay the agreed upon rent for a minimum of 6 months or we will lease the property again at no additional fee.. It is not often that a
											tenant stops paying rent because of our in depth screening process, however anything is possible. It's always in our best interest to place a
											tenant that pays their rent because our management company is set up that if we don't collect rent, we don't get paid. So you can see it's always
											in our best interest to place the most qualified person in your property.</p>
									</div>
								</div>
							</div>

						</div>
					</div>
					<!--/ accordion-->

				</div>
			</div>
		</section>

		<section id="question-form">

			<div class="widget-title">Have a question? Let us know!</div>

			<div id="ask_question_flash" class="<?php echo $flash_class ?>" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php echo $flash_msg ?>
			</div>

			<form id="ask_question_form" method="post" action="" role="form">
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">First Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_firstname" name="ask_question_firstname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">Last Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_lastname" name="ask_question_lastname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_email">Email<em>*</em></label>
							<input type="email" class="form-control" id="ask_question_email" name="ask_question_email"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_phone">Phone</label>
							<input type="text" class="form-control" id="ask_question_phone" name="ask_question_phone"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="ask_question_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="ask_question_message" rows="8" name="ask_question_message"
											required></textarea>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-12 -->
				</div>
				<!-- /.row -->

				<div class="form-group clearfix">
					<input type="hidden" id="ask_question_form_submit" name="ask_question_form_submit" value="1">
					<button type="submit" class="button pull-right" id="ask_question_submit">Send a Message</button>
				</div>
				<!-- /.form-group -->
				<div id="form-status"></div>
			</form>
			<!-- /#form-contact -->
		</section>

	</div>
</main><!--/ page content -->
