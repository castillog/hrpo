<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Meet The Team</h1>
		
		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/team/">Meet The Team</a>
		</nav>
	</div>
</section><!--/ page title -->

<main class="page-content">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<section id="team">
					<section>
						
						<div class="widget-title"><?php echo $title ?></div>
						
						<?php echo $text ?>
						
						<?php echo $agentsHtml ?>
					
					</section>
				</section>
				<!-- /#about-us -->
			</div>
			<!-- /.col-md-12 -->
		</div>
	</div>
</main>
