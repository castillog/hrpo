<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Owners</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/owners/rentready/">Rent Ready</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Ensure Your Property is "Rent Ready"</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">
					<p style="margin-bottom: 30px;">Being “Rent-Ready” is critical for landlords and property managers. Presenting a home that is not ready for a renter can cost you valuable time and money. Showings are extremely time consuming for a landlord and showing a home that is not “Rent-Ready” wastes time. The showing sets the tone for the entire relationship and that first impression is critical! Make it count by presenting a well maintained, clean home that a renter can imagine themselves in the moment they walk in. Clean, Fresh, and Bright! Here is a check list to make it easy.</p>
					
					<ul>
						<li>Re-Key the Property. Re-keying the property is always a good idea to do after each tenant. We will set this all up with one of our vendors to take care of once you have vacated the property. </li>
						<li>Make Sure Your Utilities Are On. This may sound obvious but many landlords try to keep costs down by not turning on the power and water or having it turned off the day they leave. This is not recommended. First of all, you don’t want to be in a dark home with a person you don’t know and the tenants feel the same way. Second, a dark home is just not appealing to anyone, day or night, a dark home will stay on the market longer than a light and bright home. We also may need to make some repairs etc prior to the tenants moving in and we will need to make sure the utilities are on for that.  Make sure all your light bulbs are working and on during the showing.   </li>
						<li>Inspect Your Appliances. Make sure your Fridge, Range, Microwave, Water Heater, and any other included appliances are working properly. It’s much easier to fix things before tenants move in.</li>
						<li>Repairs. Walk the house and create a punch list of repairs. Look for light bulbs out, loose cabinet doors, leaky sink valves, check smoke detector batteries, etc. If anything needs to be done just let us know and we can set up for a contractor to come out and take care of all of those issues for you.  After all you are busy moving and getting ready for your new home…don’t bog yourself down with these minor details.</li>
						<li>Repairs. Walk the house and create a punch list of repairs. Look for light bulbs out, loose cabinet doors, leaky sink valves, check smoke detector batteries, etc. If anything needs to be done just let us know and we can set up for a contractor to come out and take care of all of those issues for you.  After all you are busy moving and getting ready for your new home…don’t bog yourself down with these minor details.</li>
						<li>Make Sure Your Air Conditioner and Heater are Working Well. Make sure it’s working and that the coils and filters are clean, you don’t want surprises when you go to show the home. The temperature in the home should be kept to a comfortable setting during all showings; its ok to turn it up (or down) when you leave but make sure it’s not turned off. We recommend setting it to 78 degrees. Air circulation is important to prevent mold growth and stagnant air. You want to be able to get to the home 10min early and turn the AC down so it’s cool for the showing.   This is really something that you should do 2 times per year even when a tenant is living in the home.  As us and we can get this set up for you.</li>
						<li>Inspect Your Property for Safety and Security Issues. You want to make sure that exterior motion lights are working, there are no broken windows, exposed electrical wiring, the doors lock properly, and there is no water leaks or mold. Safety should not ever be overlooked. </li>
						<li>Clean Up Any Personal Belongings and Repair Leftovers. This means “left over paint”, yard equipment, cleaning supplies. First anything you leave behind, assume your not getting it back. Second, moving usually involves getting rid of things and purging a bit, the last thing a renter wants is someone else’s leftovers. Clean and Empty is the key!</li>
						<li>Paint any Non-Neutral Colored Walls a Neutral Color. DO NOT TOUCH UP. Touching up never looks the way you think its going to look. Remember “Clean, Fresh, and Bright”! Color can be a deal breaker. Think neutral and “boring”. You want ANY potential renter to walk into the house and be able to envision their own furniture and décor in the home.</li>
						<li>Assess the “Curb Appeal”. Curb appeal is often overlooked by property managers and landlords. Its part of that “Fresh” look that draws people in. Mulch is an easy and inexpensive way to freshen up a yard. Many prospective tenants will see a home online then drive the neighborhood well before calling. You want the house looking good from the road. The first impression often is the difference between getting your home rented and getting your home rented today!</li>
						<li>Finally, CLEAN THE HOUSE. Showing a home that is not clean is the best way to lose a prospective tenant. No one wants to live in a dirty home and if their first thought is “Oh, I am going to have to clean the house before I move in”, your home will likely sit on the market until you do clean it. We require the tenants to have the property professionally cleaned and carpets professionally cleaned at move out so this is how it should be when they move in.  We will set this all up for you.</li>
					</ul>
					
					<p style="font-size: 1.2em;font-weight: 700;margin-top: 10px;">THE GOAL: Create a desirable home that appeals to as many people as possible. The prospective tenant MUST be able to happily imagine themselves in your home.</p>
				</div>
			</div>
		</section>

		<section id="question-form">

			<div class="widget-title">Have a question? Let us know!</div>

			<div class="<?php echo $flash_class ?>" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php echo $flash_msg ?>
			</div>

			<form id="ask_question_form" method="post" action="" role="form">
				<div id="ask_question_flash"></div>
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">First Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_firstname" name="ask_question_firstname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">Last Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_lastname" name="ask_question_lastname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_email">Email<em>*</em></label>
							<input type="email" class="form-control" id="ask_question_email" name="ask_question_email"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_email">Phone</label>
							<input type="text" class="form-control" id="ask_question_email" name="ask_question_phone"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="ask_question_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="ask_question_message" rows="8" name="ask_question_message"
											required></textarea>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-12 -->
				</div>
				<!-- /.row -->

				<div class="form-group clearfix">
					<input type="hidden" id="ask_question_form_submit" name="ask_question_form_submit" value="1">
					<button type="submit" class="button pull-right" id="ask_question_submit">Send a Message</button>
				</div>
				<!-- /.form-group -->
				<div id="form-status"></div>
			</form>
			<!-- /#form-contact -->
		</section>
	</div>
</main>
<!--/ page content -->
