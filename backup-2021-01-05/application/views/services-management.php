<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Services</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;
			<a href="/services/management/">Full Service Management</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Full Service Management</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

					<ul>
						<li>Perform a market analysis to determine fair market value</li>
						<li>Market your property across the web for maximum exposure to buyers</li>
						<li>List your property on the MLS so thousands of Realtors in Hampton Roads can show it</li>
						<li>Complete a tenant application</li>
						<li>Process income verification for tenant</li>
						<li>Thoroughly check references including credit reports</li>
						<li>Contact former landlord and employers</li>
						<li>Perform criminal background check</li>
						<li>Run National eviction search</li>
						<li>Twice a year Property Inspections</li>
						<li>Drafting of lease documents and proper landlord disclosure statements</li>
						<li>Collection of rent and automatic deposits into your account</li>
						<li>Move-In and Move-Out walk-throughs</li>
						<li>We handle all landlord/tenant communication, contractor services and various professionals available at all times</li>
					</ul>


				</div>
			</div>
		</section>
	</div>
</main>
<!--/ page content -->
