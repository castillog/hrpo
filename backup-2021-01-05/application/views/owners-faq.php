<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Owners</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/owners/faq/">Owners FAQ</a>
		</nav>
	</div>
</section><!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Owners FAQ</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

					<p>A little about us:</p>

					<p>In order to meet the ever demanding needs of Property Management we try to continually improve our efficiency and the profitability of your real estate
						investment. We are now using Appfolio for over a year and we are having great results. It has really streamlined the process of work orders and rental
						payments as well as paying the owners.</p>

					<p>As always owners will still always be paid on the 5th via direct deposit as long as their tenant has paid rent. If the 5th falls on a weekend or holiday it
						will be the next business day. We post statements on the 15th of the month and at that time you will be able to go into appfolio and find your statement
						along with any receipts or invoices for that month.</p>

					<p>** All maintenance requests will be run past the owner for approval unless the owner gives permission to have repairs done without approval. In the case of
						an emergency if an owner can not be reached immediately attempts to stop any further damage will be made until repairs can be approved.</p>

					<p>We consist of 4 team members:</p>
					<ul>
						<li>RuthAnn Fields/Property Manager has been with our company for over 12 years doing property management and oversees the department.</li>
						<li>Nicole Kropewnicki/Property Manager has been with our company for over 8 years. Nicole started out on our sales side but moved over to
							the property management department over 2 years ago.
						</li>
						<li>We have an administrative assistant does all of our back end tasks. Filing, organizing, scanning paperwork into appfolio and projects
							such as preparing files for collections when needed.
						</li>
						<li>Nancy Scala is our Rental Services Manager. Nancy does all of our walk thrus (remember we do 2 per year for you
							including photos and a detailed report), takes all ad calls, schedules all appointments to show listings and takes care of the lock boxes and signs for
							our listings. This helps Nicole and RuthAnn to be able to focus more on the management needs like maintenance calls and signing leases etc.
						</li>
					</ul>

					<!-- accordion -->
					<div class="wpb_accordion wpb_accordion_alt wpb_content_element" data-active-tab="1">
						<div class="wpb_accordion_wrapper" role="tablist">

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header ui-state-focus"><a>Where do you advertise?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>We advertise your property on the MLS where over 7,000 Real Estate Agents in Hampton Roads can view your property.  The MLS also shoots that listing out to other sites like Zillow, realtor.com and Trulia among others.  We also list on AHRN which is a military by owner site.  We have a contact at NATO that we send our listings to and they post it for NATO families coming to the area.  We have several in-house sites that we use as well the fact that we have a network of other property managers in the area and a client base also looking for rentals all the time.</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>How do you decide who to accept?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>We schedule all showings for the property.  Once we have an application we run a credit check, eviction and tenant history and criminal background check.  When we have compiled all of the info we come to you and go over it with you so that you can make an informed decision.  We like the owner to be a part of that decision as we do understand that this is the most important investment you have.  Once you have accepted an applicant we go to work and immediately sign a lease and get a security deposit.  We don’t take the house off the market or stop showing it until we have that in hand so that we know we have secured a definite tenant. </p></div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>What do you do once the tenant moves in?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>We do 2 walkthroughs per year with a report and photos emailed to you.  We collect the monthly rent and take all maintenance calls.  We provide monthly statements online as well as invoices, etc., so that you can pull the info whenever you need to.  We provide an end of year statement for you along with a 1099 for your taxes.</p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>What if the tenant does not pay their rent?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>Rent is due on the 1st late after the 5th.  If we have not received rent by the 5th of the month we immediately sent out a 5 day notice of default and send their info to our attorney on retainer.  If the tenant pays before we go to court and you would like them to stay that is fine if they do not pay then the attorney will go forward with eviction and the sheriff will call us to do a lock out. </p>
									</div>
								</div>
							</div>

							<div class="wpb_accordion_section">
								<h3 class="wpb_accordion_header"><a>What happens at the end of the lease?</a></h3>

								<div class="wpb_accordion_content clearfix">
									<div class="wpb_text_column wpb_content_element ">
										<p>About 75 days prior to the lease end we will get in touch with you to make sure you are ok with renewing the tenant.  If so then we will get with the tenant and let them know that.  If they are renewing we will send them a new lease and do a walk thru and they are set for another year.  If they are moving out then we will put the property on the market and start the process all over again.  We make sure we market it at least 60 days out so that gives us plenty of time to get a new tenant set for move in when the current tenants move out.  We do a walk thru with the tenants and disburse their deposit and get the new ones moved in.</p>
									</div>
								</div>
							</div>

						</div>
					</div>
					<!--/ accordion-->

					<div class="clr"></div>
				</div>
			</div>
		</section>

		<section id="question-form">

			<div class="widget-title">Have a question? Let us know!</div>

			<div id="ask_question_flash" class="<?php echo $flash_class ?>" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php echo $flash_msg ?>
			</div>

			<form id="ask_question_form" method="post" action="" role="form">
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">First Name<em>*</em></label> <input type="text" class="form-control" id="ask_question_firstname"
								name="ask_question_firstname" required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">Last Name<em>*</em></label> <input type="text" class="form-control" id="ask_question_lastname"
								name="ask_question_lastname" required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_email">Email<em>*</em></label> <input type="email" class="form-control" id="ask_question_email" name="ask_question_email"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_phone">Phone</label> <input type="text" class="form-control" id="ask_question_phone" name="ask_question_phone" required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="ask_question_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="ask_question_message" rows="8" name="ask_question_message" required></textarea>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-12 -->
				</div>
				<!-- /.row -->

				<div class="form-group clearfix">
					<input type="hidden" id="ask_question_form_submit" name="ask_question_form_submit" value="1">
					<button type="submit" class="button pull-right" id="ask_question_submit">Send a Message</button>
				</div>
				<!-- /.form-group -->
				<div id="form-status"></div>
			</form>
			<!-- /#form-contact -->
		</section>

	</div>
</main><!--/ page content -->
