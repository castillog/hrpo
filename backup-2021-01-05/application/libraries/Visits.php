<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

set_include_path(APPPATH . 'third_party/' . PATH_SEPARATOR . get_include_path());
require_once APPPATH . 'third_party/Google/Client.php';
require_once APPPATH . 'third_party/Google/Service/Analytics.php';
require_once APPPATH . 'third_party/Google/Auth/AssertionCredentials.php';

/**
 * Class Visits
 *
 * Grabs our page views and visitor info from the sites Analytic's account
 */
class Visits
{
	private $CI;
	private $client;
	private $service;
	private $profile_id = "ga:95855164";
	private $states = array(
		'Alabama'        => 'al',
		'Alaska'         => 'ak',
		'Arizona'        => 'az',
		'Arkansas'       => 'ar',
		'California'     => 'ca',
		'Colorado'       => 'co',
		'Connecticut'    => 'ct',
		'Delaware'       => 'de',
		'Florida'        => 'fl',
		'Georgia'        => 'ga',
		'Hawaii'         => 'hi',
		'Idaho'          => 'id',
		'Illinois'       => 'il',
		'Indiana'        => 'in',
		'Iowa'           => 'ia',
		'Kansas'         => 'ks',
		'Kentucky'       => 'ky',
		'Louisiana'      => 'la',
		'Maine'          => 'me',
		'Maryland'       => 'md',
		'Massachusetts'  => 'ma',
		'Michigan'       => 'mi',
		'Minnesota'      => 'mn',
		'Mississippi'    => 'ms',
		'Missouri'       => 'mo',
		'Montana'        => 'mt',
		'Nebraska'       => 'ne',
		'Nevada'         => 'nv',
		'New Hampshire'  => 'nh',
		'New Jersey'     => 'nj',
		'New Mexico'     => 'nm',
		'New York'       => 'ny',
		'North Carolina' => 'nc',
		'North Dakota'   => 'nd',
		'Ohio'           => 'oh',
		'Oklahoma'       => 'ok',
		'Oregon'         => 'or',
		'Pennsylvania'   => 'pa',
		'Rhode Island'   => 'ri',
		'South Carolina' => 'sc',
		'South Dakota'   => 'sd',
		'Tennessee'      => 'tn',
		'Texas'          => 'tx',
		'Utah'           => 'ut',
		'Vermont'        => 'vt',
		'Virginia'       => 'va',
		'Washington'     => 'wa',
		'West Virginia'  => 'wv',
		'Wisconsin'      => 'wi',
		'Wyoming'        => 'wy'
	);

	function __construct()
	{
		// Grab our Codeigniter instance
		$this->CI =& get_instance();

		$service_account_name = "700474886592-7gfjq0tqh4jo09fatjq4o8entv0a1fgb@developer.gserviceaccount.com";
		$key_file_location = APPPATH . "third_party/Google/SearchComingSoon-c4774eed2000.p12";

		$this->client = new Google_Client();
		$this->client->setApplicationName("ComingSoon");
		$this->service = new Google_Service_Analytics($this->client);

		if(isset($_SESSION['service_token']))
		{
			$this->client->setAccessToken($_SESSION['service_token']);
		}

		$key = file_get_contents($key_file_location);
		$cred = new Google_Auth_AssertionCredentials(
			$service_account_name,
			array('https://www.googleapis.com/auth/analytics.readonly'),
			$key
		);

		$this->client->setAssertionCredentials($cred);

		if($this->client->getAuth()->isAccessTokenExpired())
		{
			$this->client->getAuth()->refreshTokenWithAssertion($cred);
		}

		$_SESSION['service_token'] = $this->client->getAccessToken();
	}

	function get_visits($type = "monthly", $start = null, $end = null)
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", strtotime("-1 days"));
		$last_week = date("Y-m-d", strtotime("-7 days"));
		$last_month = date("Y-m-d", strtotime("-30 days"));

		if($type == "daily")
			$start = (!empty($start)) ? $start : $yesterday;
		else if($type == "weekly")
			$start = (!empty($start)) ? $start : $last_week;
		else
			$start = (!empty($start)) ? $start : $last_month;

		$end = (!empty($end)) ? $end : $today;

		$results = $this->service->data_ga->get($this->profile_id, $start, $end, "ga:sessions");
		$results_total = $results->getTotalsForAllResults();

		$visits = $results_total['ga:sessions'];

		return $visits;
	}

	function get_visits_per_day($type = "weekly", $start = null, $end = null)
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", strtotime("-1 days"));
		$last_week = date("Y-m-d", strtotime("-7 days"));
		$last_month = date("Y-m-d", strtotime("-30 days"));

		if($type == "daily")
			$start = (!empty($start)) ? $start : $yesterday;
		else if($type == "weekly")
			$start = (!empty($start)) ? $start : $last_week;
		else
			$start = (!empty($start)) ? $start : $last_month;

		$end = (!empty($end)) ? $end : $today;

		$time_start = strtotime($start);
		$time_end = strtotime($end);
		$time_diff = $time_end - $time_start;

		$secsPerDays  = 24 * 60 * 60;
		$total_days  = floor($time_diff / $secsPerDays);

		$day_visits = array();
		$date = array();
		$visits = array();

		for($day = 0; $day < $total_days; $day++)
		{
			$day_before = $day;
			$current_day = $day + 1;

			$range_start = date("Y-m-d", strtotime($start . " +" . $day_before . " days"));
			$range_end = date("Y-m-d", strtotime($start . " +" . $current_day . " days"));

			$results = $this->service->data_ga->get($this->profile_id, $range_start, $range_end, "ga:sessions");
			$results_total = $results->getTotalsForAllResults();

			$day_visits[] = array('date' => $range_end, 'visits' => $results_total['ga:sessions']);
		}

		// We want to sort our results by date in ascending order
		foreach($day_visits as $key => $row)
		{
			$date[$key] = $row['date'];
			$visits[$key] = $row['visits'];
		}

		array_multisort($date, SORT_ASC, $visits, SORT_ASC, $day_visits);

		return $day_visits;
	}

	function get_visits_locations($type = "monthly", $start = null, $end = null)
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", strtotime("-1 days"));
		$last_week = date("Y-m-d", strtotime("-7 days"));
		$last_month = date("Y-m-d", strtotime("-30 days"));

		if($type == "daily")
			$start = (!empty($start)) ? $start : $yesterday;
		else if($type == "weekly")
			$start = (!empty($start)) ? $start : $last_week;
		else
			$start = (!empty($start)) ? $start : $last_month;

		$end = (!empty($end)) ? $end : $today;

		$optParams = array(
			'dimensions' => 'ga:region',
			'filters' => 'ga:country==United States',
		   'sort' => 'ga:region'
		);

		$visits_by_location = array();

		// Zero out all of our visits per state array
		foreach($this->states as $state)
		{
			$visits_by_location[$state] = 0;
		}

		$results = $this->service->data_ga->get($this->profile_id, $start, $end, "ga:sessions", $optParams);
		$rows = $results->getRows();

		if(count($rows) > 0)
		{
			foreach($rows as $row)
			{
				if(!empty($this->states[$row[0]]))
				{
					$state_abbr = $this->states[$row[0]];
					$visits_by_location[$state_abbr] = $row[1];
				}

			}
		}

		return $visits_by_location;
	}

	function get_visits_page($type = "daily", $start = null, $end = null)
	{
		$today = date("Y-m-d");
		$yesterday = date("Y-m-d", strtotime("-1 days"));
		$last_week = date("Y-m-d", strtotime("-7 days"));
		$last_month = date("Y-m-d", strtotime("-30 days"));

		if($type == "daily")
			$start = (!empty($start)) ? $start : $yesterday;
		else if($type == "weekly")
			$start = (!empty($start)) ? $start : $last_week;
		else
			$start = (!empty($start)) ? $start : $last_month;

		$end = (!empty($end)) ? $end : $today;

		$optParams = array(
			'dimensions' => 'ga:pagePath,ga:pageTitle',
		   'sort' => '-ga:uniquePageviews'
		);

		$vists_by_page = array();

		$results = $this->service->data_ga->get($this->profile_id, $start, $end, "ga:uniquePageviews", $optParams);

		$headers = $results->getColumnHeaders();
		$rows = $results->getRows();

		if(count($rows) > 0)
		{
			for($i = 0; $i < count($rows); $i++)
			{
				for($j = 0; $j < count($headers); $j++)
				{
					$field = $headers[$j]->getName();

					$vists_by_page[$field][] = $rows[$i][$j];
				}
			}
		}

		return $vists_by_page;
	}
}

/* End of file activity.php */
/* Location: ./application/libraries/activity.php */
