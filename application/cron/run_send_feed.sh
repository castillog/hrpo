#!/bin/bash

# Call the CLI action at /feed/send to send an email to all subscribers
# for the new coming soon listings added to the site in the past 7 days

#cd /home/comingverysoon/www
cd /home/lilahmed/www/comingsoon
php -q index.php feed send >> log_send_feed
