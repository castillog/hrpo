<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

$config['debug'] = true;

// Server Information
$config['protocol'] = 'https';
$config['host']     = 'localhost';
$config['port']     = 2087;

// Output type
$config['output'] = 'array';

// Auth type (hash/pass)
$config['auth_type'] = 'pass';

// Username
$config['user'] = 'root';

// Password or hash string
$config['auth'] = '!L1la4Med1@';

// curl or fopen
$config['http_client'] = 'curl';


/* End of file */
