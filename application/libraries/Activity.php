<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Activity
 *
 * Logs client and user activity to the database for later reporting
 */
class Activity
{
	function log($note_id = null, $target_id = null)
	{
		$CI =& get_instance();

		$CI->load->model('Accounts');

		$user_id = $CI->Accounts->get_user_online();
		$user_type = $CI->Accounts->get_user_type_online();

		$user = array();

		$user    = $CI->Accounts->get_user_by_id($user_id);
		$user_id = ($user) ? $user['user_id'] : 0;
		$lead_id = 0;

		$note_id = (!empty($note_id)) ? $note_id : "";

		$activities = array(
			// System Adds Lead
			'SAL' => array(
				'type' => 'lead',
				'category' => 'new',
				'note' => 'YYYY has gotten a new lead (XXXX).',
			),
			// User Creates Account
			'UCA' => array(
				'type' => 'user',
				'category' => 'new',
				'note' => 'New user (XXXX) has been added to the site.',
			),
			// User Logs In
			'ULI' => array(
				'type' => 'user',
				'category' => 'login',
				'note' => 'XXXX has logged in.',
			),
			// User Logs Off
			'ULO' => array(
				'type' => 'user',
				'category' => 'logoff',
				'note' => 'XXXX has logged off.',
			),
			// User Updates Profile
			'UUPF' => array(
				'type' => 'user',
				'category' => 'update',
				'note' => 'XXXX has update their profile.',
			),
			// User Reset Password
			'URPW' => array(
				'type' => 'user',
				'category' => 'update',
				'note' => 'XXXX has reset their password.',
			),
			// User Updates Lead
			'UULD' => array(
				'type' => 'user',
				'category' => 'lead',
				'note' => 'XXXX has updated a lead (YYYY).',
			),
			// User Submits Contact Form
			'USCF' => array(
				'type' => 'user',
				'category' => 'contact',
				'note' => 'XXXX has submitted a contact form YYYY.',
			),
		);

		$activity = (!empty($activities[$note_id])) ? $activities[$note_id] : array();

		$name = "A visitor ";

		if(!empty($activity))
		{
			$agent_id = $user_id;
			$agent = $CI->Accounts->get_user_by_id($agent_id);

			if($user)
			{
				$name = $user['first_name'] . ' ' . $user['last_name'];
			}
			else if($activity['category'] == 'logoff')
			{
				$last_user_id = $CI->Accounts->get_last_user_online();
				$last_user_type = $CI->Accounts->get_last_user_type_online();

				if($last_user_id)
				{
					if ($last_user_type == 'lead')
					{
						$user = $CI->Accounts->get_user_by_id($last_user_id);
						$name = $user['first_name'] . ' ' . $user['last_name'];
					}
					else
					{
						$user = $CI->Accounts->get_user_by_id($last_user_id);
						$name = $user['first_name'] . ' ' . $user['last_name'];
					}
				}

			}

			$activity['note'] = str_replace('XXXX', $name, $activity['note']);

			// Based on our note_id, we need to string replace our second placeholder in the note string
			// to an agent, property, or the site as a fallback.
			$target = "NorthStar Search Group";

			if(!empty($target_id))
			{
				if(in_array($note_id, array('SAL')))
				{
					// We need an agent name here
					$agent = $CI->Accounts->get_user_by_id($target_id);

					if($agent)
						$target = $agent['first_name'] . ' ' . $agent['last_name'];
					else
						$target = "N/A";
				}
				else if(in_array($note_id, array('USTF')))
				{
					$target = $target_id;
				}
			}

			$activity['note'] = str_replace('YYYY', $target, $activity['note']);

			$activity['user_id'] = (!empty($agent['user_id'])) ? $agent['user_id'] : 0;
			$activity['team_id'] = (!empty($agent['team_id'])) ? $agent['team_id'] : 0;
			$activity['office_id'] = (!empty($agent['office_id'])) ? $agent['office_id'] : 0;
			$activity['company_id'] = (!empty($agent['company_id'])) ? $agent['company_id'] : 0;

			if(!empty($name))
			{
				$insert_result = $CI->Accounts->create_activity($activity);

				if($insert_result)
				{
					$package = array("status" => "success",  "message" => "Success! Logged activity: " . $activity['note']);
					return $package;
				}
				else
				{
					$package = array("status" => "error",  "message" => "Failed! Failed to log activity: " . $activity['note']);
					return $package;
				}
			}
		}
	}

	function get_icon($category = "activity")
	{
		switch($category)
		{
			case "new":
				$label = "label-success";
				$icon = "fa-user";
				break;
			case "login":
			case "logoff":
				$label = "label-info";
				$icon = "fa-user";
				break;
			case "update":
				$label = "label-warning";
				$icon = "fa-user";
				break;
			case "lead":
				$label = "label-warning";
				$icon = "fa-comments";
				break;
			case "property":
				$label = "label-success";
				$icon = "fa-home";
				break;
			case "showing":
				$label = "label-success";
				$icon = "fa-home";
				break;
			case "favorite":
				$label = "label-success";
				$icon = "fa-star";
				break;
			case "search":
				$label = "label-success";
				$icon = "fa-search";
				break;
			case "contact":
				$label = "label-info";
				$icon = "fa-envelope";
				break;
			case "activity":
				$label = "label-info";
				$icon = "fa-info-circle";
				break;
			case "feed":
				$label = "label-info";
				$icon = "fa-rss";
				break;
			default:
				$label = "label-success";
				$icon = "fa-info";
				break;
		}

		return array('label' => $label, 'icon' => $icon);
	}
}

/* End of file activity.php */
/* Location: ./application/libraries/activity.php */
