<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Bombbomb
 *
 * Post lead data to BombBomb account
 */
class Bombbomb
{
	private $account = array();

	function __construct($config = array())
	{
		// We require a passed in config array as well as the api key to continue
		if(!empty($config))
		{
			if(!empty($config['api_key']))
				$this->account['api_key'] = $config['api_key'];
			else
			{
				error_log("Bombbomb class constructor requires a valid BombBomb api key to continue!");

				return false;
			}

			if(!empty($config['email']))
				$this->account['email'] = $config['email'];

			if(!empty($config['pw']))
				$this->account['pw'] = $config['pw'];
		}
		else
		{
			error_log("Bombbomb class constructor requires a valid config array to continue!");

			return false;
		}

	}

	// Check to see if provided login is valid
	function is_valid_login()
	{
		// We must have a valid login email address and password to continue
		if(empty($this->account['email']))
		{
			error_log("BombBomb Error! Must have a valid email address to verify login!");

			return false;
		}

		if(empty($this->account['pw']))
		{
			error_log("BombBomb Error! Must have a valid password to verify login!");

			return false;
		}

		$params = array(
			'method' => 'IsValidLogin',
			'email'  => $this->account['email'],
			'pw'     => $this->account['pw']
		);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "IsValidLogin")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	//--------------------------------------------------
	// CONTACTS
	//--------------------------------------------------

	// Get lead on Bombbomb account
	function get_contact($params = array())
	{
		// We must have an api key and an email address to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get a contact!");

			return false;
		}

		if(empty($params['eml']))
		{
			error_log("BombBomb Error! Must have a valid email address to get a contact!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetContact',
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetContact")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Add lead to Bombbomb account
	function add_contact($params = array())
	{
		// We must have an api key and an email address to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to add a contact!");

			return false;
		}

		if(empty($params['eml']))
		{
			error_log("BombBomb Error! Must have a valid email address to add a contact!");

			return false;
		}

		$defaults = array(
			'api_key'        => $this->account['api_key'],
			'method'         => 'AddContact',
			'firstname'      => 'N/a',
			'lastname'       => 'N/a',
			'phone_number'   => 'N/a',
			'address_line_1' => 'N/a',
			'address_line_2' => 'N/a',
			'city'           => 'N/a',
			'state'          => 'N/a',
			'postal_code'    => 'N/a',
			'company'        => 'N/a',
			'comments'       => 'ComingSoonHomes.com'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		//https://app.bombbomb.com/app/api/api.php?method=AddContact&api_key=5d109bac-d1b8-4ca8-b290-1a32698fd31e&firstname=John&lastname=Smith&email=johnsmith@test.com&phone=910-555-5555&listlist=a0c18d78-7b7e-1f3f-4113-8a92ba55dbd4

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "AddContact")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Update lead to Bombomb account
	function update_contact($params = array())
	{
		// We must have an api key and an email address to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to update a contact!");

			return false;
		}

		if(empty($params['eml']))
		{
			error_log("BombBomb Error! Must have a valid email address to update a contact!");

			return false;
		}

		$defaults = array(
			'api_key'        => $this->account['api_key'],
			'method'         => 'UpdateContact'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "UpdateContact")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Get contact fields for a BombBomb account
	function get_contact_fields()
	{
		// We must have an api key to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get contact fields!");

			return false;
		}

		$params = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetContactFields',
		);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetContactFields")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Get a list of all emails in BombBomb account
	function get_emails()
	{
		// We must have an api key to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get all account emails!");

			return false;
		}

		$params = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetEmails',
		);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetEmails")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Get a list of email tag names and ids in BombBomb account
	function get_email_tags()
	{
		// We must have an api key to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get all account email tags!");

			return false;
		}

		$params = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetEmailTags',
		);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetEmailTags")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Get all contacts on a list on a Bombbomb account
	function get_all_emails_by_tag($params = array())
	{
		// We must have an api key and tag id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get all emails by tag!");

			return false;
		}

		if(empty($params['tag_id']))
		{
			error_log("BombBomb Error! Must have a valid tag id to get all emails by tag!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetAllEmailsByTag'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetAllEmailsByTag")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	//--------------------------------------------------
	// LISTS
	//--------------------------------------------------

	// Get a list of all Lists in BombBomb account
	function get_lists()
	{
		// We must have an api key to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get lists!");

			return false;
		}

		$params = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetLists',
		);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		// https://app.bombbomb.com/app/api/api.php?method=GetLists&api_key=5d109bac-d1b8-4ca8-b290-1a32698fd31e

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetLists")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Get all contacts on a list on a Bombbomb account
	function get_list_contacts($params = array())
	{
		// We must have an api key and list id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get list contacts!");

			return false;
		}

		if(empty($params['list_id']))
		{
			error_log("BombBomb Error! Must have a valid list id to get list contacts!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetListContacts'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetListContacts")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Get list processing status on a Bombbomb account
	function get_list_processing_status($params = array())
	{
		// We must have an api key and list id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get list processing status!");

			return false;
		}

		if(empty($params['list_id']))
		{
			error_log("BombBomb Error! Must have a valid list id to get list processing status!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetListProcessingStatus'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetListProcessingStatus")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Create a list on a Bombbomb account
	function create_list($params = array())
	{
		// We must have an api key and list name to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to create a list!");

			return false;
		}

		if(empty($params['name']))
		{
			error_log("BombBomb Error! Must have a valid list name to create a list!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'CreateList'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "CreateList")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Delete a list on a Bombbomb account
	function delete_list($params = array())
	{
		// We must have an api key and list id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to delete a list!");

			return false;
		}

		if(empty($params['list_id']))
		{
			error_log("BombBomb Error! Must have a valid list id to delete a list!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'DeleteList'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "DeleteList")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Add email to list on a Bombbomb account
	function add_email_to_list($params = array())
	{
		// We must have an api key, new email address, and list id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to add email to a list!");

			return false;
		}

		if(empty($params['new_email_address']))
		{
			error_log("BombBomb Error! Must have a valid new email address to add email to a list!");

			return false;
		}

		if(empty($params['list_id']))
		{
			error_log("BombBomb Error! Must have a valid list id to add email to a list!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'AddEmailToList'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "AddEmailToList")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Remove email from list on a Bombbomb account
	function remove_email_from_list($params = array())
	{
		// We must have an api key, new email address, and list id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to remove email from a list!");

			return false;
		}

		if(empty($params['new_email_address']))
		{
			error_log("BombBomb Error! Must have a valid new email address to remove email from a list!");

			return false;
		}

		if(empty($params['list_id']))
		{
			error_log("BombBomb Error! Must have a valid list id to remove email from a list!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'RemoveEmailFromList'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "RemoveEmailFromList")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	//--------------------------------------------------
	// DRIPS
	//--------------------------------------------------

	// Get a list of all drips in BombBomb account
	function get_drips()
	{
		// We must have an api key to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to get drips!");

			return false;
		}

		$params = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'GetDrips',
		);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "GetDrips")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Add email to drip on a Bombbomb account
	function add_to_drip($params = array())
	{
		// We must have an api key, new email address, and drip id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to add email to a drip!");

			return false;
		}

		if(empty($params['e']))
		{
			error_log("BombBomb Error! Must have a valid new email address to add email to a drip!");

			return false;
		}

		if(empty($params['d']))
		{
			error_log("BombBomb Error! Must have a valid drip id to add email to a drip!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'addToDrip'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "addToDrip")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}

	// Remove email from drip on a Bombbomb account
	function remove_from_drip($params = array())
	{
		// We must have an api key, email address, and drip id to continue
		if(empty($this->account['api_key']))
		{
			error_log("BombBomb Error! Must have a valid BombBomb API Key to remove email from a drip!");

			return false;
		}

		if(empty($params['e']))
		{
			error_log("BombBomb Error! Must have a valid new email address to remove email from a drip!");

			return false;
		}

		if(empty($params['d']))
		{
			error_log("BombBomb Error! Must have a valid drip id to remove email from a drip!");

			return false;
		}

		$defaults = array(
			'api_key' => $this->account['api_key'],
			'method'  => 'removeFromDrip'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$request_url = 'https://app.bombbomb.com/app/api/api.php';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if($response['status'] != "success")
				return false;
			if($response['methodName'] != "removeFromDrip")
				return false;
			if(empty($response['info']))
				return false;
			else
			{
				return $response['info'];
			}
		}
		else
			return false;
	}
}

/* End of file Bombbomb.php */
/* Location: ./application/libraries/Bombbomb.php */
