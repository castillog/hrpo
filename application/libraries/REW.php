<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class REW
 *
 * Post lead data to Real Estate Webmaster account
 */
class REW
{
	private $account = array();

	function __construct($config = array())
	{
		// We require a passed in config array as well as the api key to continue
		if(!empty($config))
		{
			if(!empty($config['domain']))
				$this->account['domain'] = $config['domain'];
			else
			{
				error_log("REW class constructor requires a valid REW domain name to continue!");

				return false;
			}

			if(!empty($config['api_key']))
				$this->account['api_key'] = $config['api_key'];
			else
			{
				error_log("REW class constructor requires a valid REW api key to continue!");

				return false;
			}
		}
		else
		{
			error_log("REW class constructor requires a valid config array to continue!");

			return false;
		}

	}

	//--------------------------------------------------
	// LEADS
	//--------------------------------------------------

	// Get lead on REW account
	function get_lead($email)
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to get a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to get a lead!");

			return false;
		}

		if(empty($email))
		{
			error_log("REW Error! Must provide a valid email address to get a lead!");

			return false;
		}

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/leads/' . $email;

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		// For whatever reason REW kicks back a double error string on this function that json can't decode.
		// The first error is an SQL code error which we don't care about. It's the second
		// one we want so we'll strip out the first one by splitting the string by the closing
		// braces on the first error. Hopefully this is a bug on their end that will be corrected and this
		// block can eventually be removed
		if(empty($response))
		{
			$output_split = explode('}}}', $output);
			$output       = $output_split[1];
			$response     = json_decode($output, true);
		}

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	// Add lead on REW account
	function add_lead($params = array())
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to add a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to add a lead!");

			return false;
		}

		if(empty($params['first_name']))
		{
			error_log("REW Error! Must have a valid first name to add a lead!");

			return false;
		}

		if(empty($params['last_name']))
		{
			error_log("REW Error! Must have a valid last name to add a lead!");

			return false;
		}

		if(empty($params['email']))
		{
			error_log("REW Error! Must have a valid email address to add a lead!");

			return false;
		}

		$defaults = array(
			'agent_id'   => '1',
			'address'    => 'N/a',
			'city'       => 'N/a',
			'zip'        => 'N/a',
			'phone'      => 'N/a',
			'origin'     => 'Hampton Roads Property Management',
			"opt_marketing" => "in",
			"opt_searches"  => "in",
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/leads';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	// Update lead on REW account
	function update_lead($params = array())
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to update a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to update a lead!");

			return false;
		}

		if(empty($params['email']))
		{
			error_log("REW Error! Must have a valid email address to update a lead!");

			return false;
		}

		$email = $params['email'];
		unset($params['email']);

		$defaults = array(
			'origin'   => 'Hampton Roads Property Management'
		);

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/leads/' . $email;

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	//--------------------------------------------------
	// AGENTS
	//--------------------------------------------------

	// Get a list of all agents on REW account
	function get_all_agents()
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to get a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to get a lead!");

			return false;
		}

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/agents';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	//--------------------------------------------------
	// GROUPS
	//--------------------------------------------------

	// Get a list of all groups on REW account
	function get_group($id)
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to get a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to get a lead!");

			return false;
		}

		if(empty($params['id']))
		{
			error_log("REW Error! Must have a valid group id to get a group!");

			return false;
		}

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/groups/' . $id;

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	// Get a list of all groups on REW account
	function get_all_groups()
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to get a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to get a lead!");

			return false;
		}

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/groups';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	// Add group on REW account
	function add_group($params = array())
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to add a group!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to add a group!");

			return false;
		}

		if(empty($params['name']))
		{
			error_log("REW Error! Must have a valid name to add a group!");

			return false;
		}

		$defaults = array();

		$params = array_merge($defaults, $params);

		$params = http_build_query($params);

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/groups';

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $params,
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}

	// Delete a group of all groups on REW account
	function delete_group($id)
	{
		// We must have a domain name and api key to continue
		if(empty($this->account['domain']))
		{
			error_log("REW Error! Must have a valid REW domain name to delete a lead!");

			return false;
		}

		if(empty($this->account['api_key']))
		{
			error_log("REW Error! Must have a valid REW API Key to delete a lead!");

			return false;
		}

		if(empty($params['id']))
		{
			error_log("REW Error! Must have a valid group id to delete a group!");

			return false;
		}

		$headers = array(
			'X-REW-API-Key: ' . $this->account['api_key'],
		);

		$request_url = 'http://' . $this->account['domain'] . '/api/crm/v1/groups/' . $id;

		$ch = curl_init();

		$options = array(
			CURLOPT_HEADER         => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_USERAGENT      => 'apiClient/1.0',
			CURLOPT_TIMEOUT        => 60,
			CURLINFO_HEADER_OUT    => true,
			CURLOPT_CUSTOMREQUEST  => "DELETE",
			CURLOPT_HTTPHEADER     => $headers,
			CURLOPT_URL            => $request_url
		);

		curl_setopt_array($ch, $options);
		$output = curl_exec($ch);
		curl_close($ch);

		$response = json_decode($output, true);

		if(!empty($response))
		{
			if(!empty($response['error']))
			{
				log_message('error', "REW API Error! Type: " . $response['error']['type'] . ", Message: " . $response['error']['message']);

				return false;
			}
			else
			{
				return $response['data'];
			}
		}
		else
			return false;
	}
}

/* End of file REW.php */
/* Location: ./application/libraries/REW.php */
