<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Services</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;
			<a href="/services/marketing/">Marketing Plan</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Marketing Plan</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">

					<div class="grid-col grid-col-9">
						<div class="content-container">

							<p>Lonnie Bush Property Management has a nearly unlimited budget to market your home.
								We use the latest technology, creative and innovative marketing strategies to expose
								your home to prospective tenants. This allows us to get your home rented in the shortest
								possible time and for the best possible price. We generate hundreds of tenant leads each
								month and match them up to our available listings before searching outside the company.
							</p>

						</div>
					</div>
					<div class="grid-col grid-col-3">
						<img src="/includes/pic/websites_400.jpg">
					</div

				</div>
			</div>
		</section>
	</div>
</main>
<!--/ page content -->
