<!-- slider -->
<div class="slider-wrapper">
	<section class="slider" id="slider">
		<div class="ls-slide" data-ls="transition2d:9;slidedelay:7000;">
			<img src="/data/images/slider-1.jpg" alt="" class="ls-bg">
		</div>
		<div class="ls-slide" data-ls="transition2d:40;slidedelay:7000;">
			<img src="/data/images/slider-2.jpg" alt="" class="ls-bg">
		</div>
		<div class="ls-slide" data-ls="transition2d:40;slidedelay:7000;">
			<img src="/data/images/slider-3.jpg" alt="" class="ls-bg">
		</div>
	</section>

</div><!--/ slider -->

<div class="slider-callout">
	<div class="intro">
		<span class="icon fa fa-home"></span>

		<h2><span>MANAGEMENT MADE</span> EASY!</h2>

		<p>Yes, I want my FREE Property Management Guide</p>

		<div class="<?php echo $flash_class ?>" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?php echo $flash_msg ?>
		</div>

		<form id="info-form" class="" method="post" action="" role="form">
			<div class="form-group">
				<label for="first-name" class="sr-only">First Name</label>
				<input class="form-control" id="first-name" name="first-name" type="text" value="" placeholder="First Name*" required>
			</div>
			<div class="form-group">
				<label for="last-name" class="sr-only">Last Name</label>
				<input class="form-control" id="last-name" name="last-name" type="text" value="" placeholder="Last Name*" required>
			</div>
			<div class="form-group">
				<label for="email" class="sr-only">Email</label>
				<input class="form-control" id="email" name="email" type="text" value="" placeholder="Email*" required>
			</div>
			<div class="form-group">
				<label for="phone" class="sr-only">Phone</label>
				<input class="form-control" id="phone" name="phone" type="text" value="" placeholder="Phone*" required>
			</div>
			<input type="hidden" id="info-form-submit" name="info-form-submit" value="1">
			<button id="info-submit-button" class="button" type="submit">Get Your FREE Guide Now</button>
		</form>

	</div>
</div>

<!-- page content -->
<main class="page-content">



</main><!--/ page content -->
