<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Tenants</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/tenants/portal/">Tenant Portal</a>
		</nav>
	</div>
</section><!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Tenant Portal</div>
				<div class="row">
					<div class="col-md-12">
						<section>
							<h2>Pay Rent Online</h2>

							<p>
								Did you know that you can now pay your rent online? It's fast, easy, and secure, so why wait? Below, you'll find some information
								on how to get started and a few reasons why so many others have already made the switch! </p>
						</section>

					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<section style="height: 75px;">
							<h3>Don't Have an Account?</h3>

							<p>It’s easy! Just let us know you want to pay your rent online and we'll email you an account activation link.</p>

						</section>
						<section>
							<a href="#get-started" class="button" id="button-get-started">Get Started!</a>
						</section>

					</div>
					<div class="col-md-6">
						<section style="height: 75px;">
							<h3>Already have an account?</h3>

							<p>Click the button below to be taken to the Tenant Portal login page.</p>
						</section>
						<section>
							<a href="https://remax.appfolio.com/connect/users/sign_in" class="button" id="button-portal-login">Log Into Portal</a>
						</section>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<section>
							<h3>Benefits</h3>

							Once you’ve signed up, you can:
							<ul>
								<li>View and pay your bills anytime (24/7)</li>
								<li>View and pay your bills from anywhere (any computer with an internet connection)</li>
								<li>Set up an automatic payment</li>
								<li>Sign up for automatic reminder emails</li>
								<li>Review your payment history</li>
							</ul>
						</section>

						<section>
							<h3>Convenience</h3>

							<p>Have you ever looked at a calendar and suddenly realized that your rent was due that day? Or worse yet, that it was due a few days ago and that it was now late?
								With online rent payments, these concerns are a thing of the past. Simply hop on your computer and in just a few minutes, your rent is paid! Or remove all doubt
								and schedule a payment in advance so your rent is paid automatically. And this is in addition to not having to write checks, address envelopes, or find/buy
								stamps...</p>
						</section>
						<section>
							<h3>Security</h3>

							<p>In a world where online financial predators seem more and more common, we understand if you have reservations about entering your bank account information online.
								But fear not! Your information is password protected and all transactions are both encrypted and securely transmitted.</p>
						</section>
						<section>
							<h3>Account Setup</h3>

							<p>It only takes a few minutes to get started! Here’s what you need to do: Contact us and let us know you’re interested in paying your rent online. Give us your
								current email address so we can send you an invitation email. When you get the invitation email (it will come from donotreply@appfolio.com), click on the link in
								the email to set up your account. Create a secure password and activate your account. That’s it! Now you can log in anytime, from anywhere and make payments,
								schedule payments, view your payment history, and more!</p>
						</section>
						<section></section>
							<h3>Request an Activation Link</h3>

							<p>Complete the form below and we'll email you the activation link you’ll need to set up your account</p>
						</section>

						<div class="<?php echo $flash_class ?>"><?php echo $flash_msg ?></div>
						<form id="get-started" method="post" action="" role="form">
							<div class="form-group">
								<input type="text" class="form-control" id="get-started-first" name="first-name" placeholder="First Name*" value="">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="get-started-last" name="last-name" placeholder="Last Name*" value="">
							</div>
							<div class="form-group">
								<input type="text" class="form-control" id="get-started-email" name="email" placeholder="Email*" value="">
							</div>
							<input type="hidden" id="form-submit" name="form-submit" value="1">
							<button type="submit" class="button" id="get-started-submit">Submit</button>
						</form>

					</div>
				</div>
			</div>
		</section>
	</div>
</main><!--/ page content -->
