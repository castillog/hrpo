<!-- slider -->
<div class="slider-wrapper">
	<section class="slider" id="slider">
		<div class="ls-slide" data-ls="transition2d:9;slidedelay:7000;">
			<img src="/data/images/slider-1.jpg" alt="" class="ls-bg">
		</div>
		<div class="ls-slide" data-ls="transition2d:40;slidedelay:7000;">
			<img src="/data/images/slider-2.jpg" alt="" class="ls-bg">
		</div>
		<div class="ls-slide" data-ls="transition2d:40;slidedelay:7000;">
			<img src="/data/images/slider-3.jpg" alt="" class="ls-bg">
		</div>
	</section>

</div><!--/ slider -->

<div class="slider-callout">
	<div class="intro">
		<span class="icon fa fa-home"></span>

		<h2><span>MANAGE MY</span> PROPERTY</h2>

		<p>Owners, Inquire about our Property Management Services</p>

		<div id="manage_flash" class="<?php echo $flash_class ?>" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<?php echo $flash_msg ?>
		</div>

		<form id="manage_form" class="form-inline" method="post" action="" role="form">
			<div class="form-group">
				<label for="owner_name" class="sr-only">Owner's First and Last Name</label>
				<input class="form-control" id="owner_name" name="owner_name" type="text" value="" placeholder="Owner's First and Last Name*" required>
			</div>
			<div class="form-group">
				<label for="owner_email" class="sr-only">Email</label>
				<input class="form-control" id="owner_email" name="owner_email" type="text" value="" placeholder="Email*" required>
			</div>
			<div class="form-group">
				<label for="owner_phone" class="sr-only">Phone</label>
				<input class="form-control" id="owner_phone" name="owner_phone" type="text" value="" placeholder="Phone*" required>
			</div>
			<div class="form-group">
				<label for="owner_city" class="sr-only">City of Property</label>
				<input class="form-control" id="owner_city" name="owner_city" type="text" value="" placeholder="City of Property*" required>
			</div>
			<input type="hidden" id="manage-form-submit" name="manage-form-submit" value="1">
			<button id="manage-submit-button" class="button" type="submit">Owners Inquire Now</button>
		</form>

	</div>
</div>

<!-- page content -->
<main class="page-content">
	<div class="grid-row">
		<!-- benefits -->
		<section class="benefits">
			<ul>
				<li>
					<a href="/services/" class="box-link">
						<div class="pic"><i class="fa fa-puzzle-piece"></i></div>
						<div class="text">
							<h2>Services</h2>

							<p>Property Management</p>
							<a href="/services/" class="more"></a>
						</div>
					</a>
				</li>
				<li>
					<a href="/owners/guaranteed/" class="box-link">
						<div class="pic"><i class="fa fa-money"></i></div>
						<div class="text">
							<h2>Guaranteed Rent</h2>

							<p>We'll Pay The Rent</p>
							<a href="/owners/guaranteed/" class="more"></a>
						</div>
					</a>
				</li>
				<li>
					<a href="https://remax.appfolio.com/oportal/users/log_in" class="box-link">
						<div class="pic"><i class="fa fa-users"></i></div>
						<div class="text">
							<h2>Owners</h2>

							<p>Login & Management</p>
							<a href="https://remax.appfolio.com/oportal/users/log_in" class="more"></a>
						</div>
					</a>
				</li>
				<li>
					<a href="/search/" class="box-link">
						<div class="pic"><i class="fa fa-laptop"></i></div>
						<div class="text">
							<h2>Rental Search</h2>

							<p>Find a Rental</p>
							<a href="/search/" class="more"></a>
						</div>
					</a>
				</li>
			</ul>
		</section>
		<!--/ benefits -->
	</div>

	<div class="grid-row">
		<section class="content-top">
			<div class="grid-col grid-col-9">
				<div class="content-container">
					<h2><a href="#">Lonnie Bush Property Management</a></h2>

					<p>Whether you own a large apartment complex with multiple units, a condo or a single family home
						that you are interested in renting out, having a property manager is in your best interest.
						Managing tenants is no easy task, but with Lonnie Bush Property Management, you can leave your property in
						our hands and sit back knowing that all is running smoothly.
					<p>
					<br>
					<p>
						We manage property in Virginia
						Beach, Chesapeake, Norfolk, Portsmouth, Hampton, Newport News, and Suffolk.  Call us today for
						more information (757) 610-9800.</p>

					<a href="#" class="more fa fa-long-arrow-right"></a>
				</div>
			</div>
			<div class="grid-col grid-col-3">
				<img src="/includes/pic/management.jpg">
			</div>
		</section>
	</div>


	<div class="grid-row">
		<!-- callout -->
		<div class="callout wpb_content_element">
			<div class="icon"><i class="fa fa-question"></i></div>
			<div class="callout-wrapper">
				<div class="callout-content">
					<div class="title">Why Lonnie Bush Property Management?</div>
					<div class="subtitle">Our team are the experts when it comes to property management in Hampton Roads.</div>
				</div>
				<div class="callout-button">
					<a href="/contact/" class="button">Hire Us Today!</a>
				</div>
			</div>
		</div>
		<!--/ callout -->
    </div>

	<div class="grid-row">
		<section id="benefits-management" class="content-bottom">
			<div class="grid-col grid-col-3">
				<img src="/includes/pic/property_manager.jpg">
			</div>
			<div class="grid-col grid-col-9">
				<div class="content-container">
					<h2><a href="#">Benefits of a Property Manager</a></h2>

					<ul>
						<li>Rental collection and separate trust account banking (professionally trained collection department)</li>
						<li>Payment of bills incurred on behalf of the property; including taxes, loan payment, insurance and repairs.</li>
						<li>Prepare necessary forms at no additional cost; notice to quit, change of terms, 5-day pay or quit.</li>
						<li>Complete eviction services (legal staff available from start to finish)</li>
						<li>Comprehensive, computerized monthly statements, including a complete record of all collections and disbursements.</li>
						<li>Provide year-end statements of all income and expense for your tax record.</li>
						<li>Conduct on-site exterior inspections 2 times per year.</li>
					</ul>

					<a href="#" class="more fa fa-long-arrow-right"></a>
				</div>
			</div>
		</section>
	</div>

	<div class="grid-row">
		<!-- services -->
		<section class="services">
			<div class="row">
				<div class="col-md-2">
					<h2>Areas We Serve</h2>
				</div>
				<div class="col-md-2">
					Virgina Beach, VA
				</div>
				<div class="col-md-2">
					Chesapeake, VA
				</div>
				<div class="col-md-2">
					Norfolk, VA
				</div>
				<div class="col-md-2">
					Portsmouth, VA
				</div>
				<div class="col-md-2">
					Hampton, VA
				</div>
				<div class="col-md-2">
					Newport News, VA
				</div>
				<div class="col-md-2">
					Suffolk, VA
				</div>
			</div>
		</section>
		<!--/ services -->
	</div>


</main><!--/ page content -->
