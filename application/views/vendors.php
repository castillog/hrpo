<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Preferred Vendors</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/vendors/">Preferred Vendors</a>
		</nav>
	</div>
</section><!--/ page title -->

<main class="page-content">
	<div class="grid-row">
		<section id="vendors">
			<div class="widget-title"></div>

			<?php echo $vendorHtml; ?>

		</section>
		<!-- /#about-us -->
	</div>
</main>
