<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>About Us</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/about/">About Us</a>
		</nav>
	</div>
</section><!--/ page title -->

<main class="page-content">
	<div class="grid-row">
		<div class="col-md-12 col-sm-12 block">
			<section id="about-us">
				<section>

					<div class="widget-title">Our Team at Lonnie Bush Property Management</div>

					<p>Lonnie Bush Property Management, founded by Lonnie Bush in 1999, is one of the fastest-growing
						and highest-reviewed property management companies in Hampton Roads. Managing properties in the
						Hampton Roads, Coastal Virginia and surrounding areas, our team of Property Managers are
						dedicated to helping homeowners lease and manage their residential real estate with confidence
						and always putting our Owners and their properties as our number 1 priority.
					</p>

					<p>Lonnie Bush Property Management is recognized as leaders in real estate and property management. Being
						recognized by The Wall Street Journal and Real Trends as one of the top 200 teams in the United
						States.  From evictions to tenant issues, maintenance calls and finding the right tenant for
						your property.  Our understanding of the market, what rents and how to get the most out of your
						investment property, and our extensive tenant screening are what sets us aside from the rest.
						This is why more home owners are putting their investments in our hands.
					</p>

				</section>
			</section>
			<!-- /#about-us -->
		</div>
		<!-- /.col-md-12 -->
	</div>
</main>
