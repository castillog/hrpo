<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Meet The Team</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/team/test/">Meet The Team</a>
		</nav>
	</div>
</section><!--/ page title -->

<main class="page-content">
	<div class="container">
		<section id="team">
			<div class="row">
				<div class='col-xs-12 col-md-4 col-lg-4'>
					<div class='team-image text-center'>
						<?php if(!empty($website)) : ?>
							<a href="<?php echo $website ?>">
								<img class='' src='/data/images/user_empty.png' style='width:100%;height:auto;background:url(<?php echo $image ?>) no-repeat center/cover;'>
							</a>
						<? else : ?>
							<img class='' src='/data/images/user_empty.png' style='width:100%;height:auto;background:url(<?php echo $image ?>) no-repeat center/cover;'>
						<? endif; ?>
					</div>
					<ul class='social-links social-links__primary text-center'>
						<?php echo $facebook_link ?>
						<?php echo $twitter_link ?>
						<?php echo $linkedin_link ?>
						<?php echo $youtube_link ?>
						<?php echo $instagram_link ?>
					</ul>
				</div>
				<div class='col-xs-12 col-md-8 col-lg-7'>
					
					<h2><?php echo $name ?></h2>
					<h3><?php echo $agent_title ?></h3>
					
					<?php if(!empty($bio)) : ?>
						<p><?php echo $bio ?></p>
					<?php endif; ?>
					
					<div class='contact-details'>
						
						<?php if(!empty($email)) : ?>
							<div class='contact-row'>
								<div class='contact-icon'><i class='fa fa-envelope'></i></div>
								<div class='contact-label'>Email:</div>
								<div class='contact-text'><a href="<?php echo $email ?>"><?php echo $email ?></a></div>
							</div>
						<?php endif; ?>
						
						<?php if(!empty($phone)) : ?>
							<div class='contact-row'>
								<div class='contact-icon'><i class='fa fa-phone'></i></div>
								<div class='contact-label'>Phone:</div>
								<div class='contact-text'><a href="tel:<?php echo $phone ?>"><?php echo $phone ?></a></div>
							</div>
						<?php endif; ?>
						
						<?php if(!empty($mobile)) : ?>
							<div class='contact-row'>
								<div class='contact-icon'><i class='fa fa-mobile'></i></div>
								<div class='contact-label'>Mobile:</div>
								<div class='contact-text'><a href="tel:<?php echo $mobile ?>"><?php echo $mobile ?></a></div>
							</div>
						<?php endif; ?>
						
						<?php if(!empty($website)) : ?>
							<div class='contact-row'>
								<div class='contact-icon'><i class='fa fa-desktop'></i></div>
								<div class='contact-label'>Website:</div>
								<div class='contact-text'><a href="<?php echo $website ?>"><?php echo $website ?></a></div>
							</div>
						<? endif; ?>
						
						<?php if(!empty($location)) : ?>
							<div class='contact-row'>
								<div class='contact-icon'><i class='fa fa-road'></i></div>
								<div class='contact-label'>Location:</div>
								<div class='contact-text'><?php echo $location ?></div>
							</div>
						<?php endif; ?>
					
					</div>
				</div>
			</div>
		</section>
	</div>
</main>
