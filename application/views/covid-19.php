<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>COVID-19 Resources</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp; <a href="/about/covid19/">COVID-19 Resources</a>
		</nav>
	</div>
</section><!--/ page title -->

<main class="page-content">
	<div class="grid-row">
		<div class="col-md-12 col-sm-12 block">
			<section id="about-covid">
				<section>

					<div class="widget-title">Lonnie Bush COVID-19 Resources and Information for Owners and Residents</div>
					
					<p>In response to the current COVID-19 crisis, the team at Lonnie Bush Property Management has put
						together this resource page to keep our owners and residents informed during this time. We understand
						that this might be an unprecedented time of confusion and concern for our owners and residents. By
						compiling a go-to page for your reference, we hope to reassure you that we are here for you through
						this time.</p>
					<p>To stay compliant with new (and changing!) protocols while still providing the best level of service
						to all owners and residents, we are working hard to stay up to date with current information from our
						government resources. Our team is doing everything we can to make sure residents have everything they
						need and can stay safe while staying home for work and life.</p>
					<p>We will update this page as needed for the duration of the COVID-19 situation. If you don't see the
						information you need here on the page, or you need immediate assistance, please contact us
						directly.</p>
					
					<ul>
						<li><strong>Phone: 757-217-0924</strong></li>
						<li><strong>Tenants, please email:</strong> <a href="mailto:residentservices@lonniebush.com">residentservices@lonniebush.com</a>
							and reference your <a href="https://remax.appfolio.com/connect/users/sign_in">online Tenant
								Portal</a></li>
						<li><strong>Owners, please email:</strong> <a href="mailto:ownerservices@lonniebush.com">ownerservices@lonniebush.com</a> and reference your
							<a href="https://remax.appfolio.com/oportal/users/log_in">online Owner Portal</a></li>
					</ul>
					
					<h2>For Current Federal and State COVID-19 Information</h2>
					
					<p>Please use the resources below to stay informed with the latest information about how to protect
						yourself and loved ones from COVID-19. Through these links, you'll find updates and recommendations
						from the Centers for Disease Control and the federal government.</p>
					
					<ul>
						<li><a href="https://www.cdc.gov/coronavirus/2019-ncov/index.html">Centers for Disease Control and Prevention (CDC) COVID-19 Updates</a></li>
						<li><a href="https://www.whitehouse.gov/">The White House</a></li>
					</ul>
					
					<p>For local updates, check with city government websites and your local news reporting agencies.</p>
					
					<h2>For Emergency Resources and Help</h2>
					
					<p>We understand that a change in your working conditions and limited local resources can make it
						challenging to find everyday essentials to get through work-at-home and social distancing conditions.
						We've compiled a few links to local organizations helping Hampton Roads residents find food and other
						assistance to help you get through this time.</p>
					<p>Please do not hesitate to reach out to these organizations for help!</p>
					
					<p><a href="https://www.norfolk.gov/CivicAlerts.aspx?AID=5168" title="Norfolk (GIVES HOUSING MONEY)">Norfolk (GIVES HOUSING MONEY)</a></p>

					<p><a href="https://www.npsk12.com/site/default.aspx?PageType=3&DomainID=4&ModuleInstanceID=62&ViewID=6446EE88-D30C-497E-9316-3F8874B3E108&RenderLoc=0&FlexDataID=19172&PageID=1" title="FOOD FOR KIDS">FOOD FOR KIDS</a></p>

					<p><a href="https://hamptonroadscf.org/leadership-initiatives/covid19" title="https://hamptonroadscf.org/leadership-initiatives/covid19">Hampton Roads (GIVES RELIEF LIKE FOOD AND ESSENTIALS)</a></p>

					<p>Chesapeake (UTILITIES ASSISTANCE)<br>
					<a href="https://cityofchesapeake.jotform.com/203415595844057" title="Chesapeake (UTILITIES ASSISTANCE)">https://cityofchesapeake.jotform.com/203415595844057</a></p>

					<p>Virginia Beach<br>

					<a href="https://www.vbschools.com/parents/menus___meal_payments" title="vbrelief.org (PROVIDES JOB TRAINING, CHILD CARE ASST, RENT ASST, FOOD RELIEF)">vbrelief.org (PROVIDES JOB TRAINING, CHILD CARE ASST, RENT ASST, FOOD RELIEF)</a></p>

					<p>Dominion Power will not cut you off for non payment you have to call them but they will help.</p>

					<h3>Food and Essentials</h3>
					
					<ul>
						<li><a
								href="https://foodbankonline.org/directory/listing/city-of-refuge-christian-church-of-greenbrier-2">City
								of Refuge Christian Church of Greenbriar</a></li>
						<li><a href="https://foodbankonline.org/directory/listing/star-of-the-sea">Star of the Sea</a></li>
					</ul>
					
					<p>Be sure to contact these organizations directly to confirm their current hours of operation and
						distribution locations.</p>
					
					<h3>Hospitals and Medical Assistance</h3>
					
					<p>For medical assistance, use these links to find your nearest local hospital. If you are experiencing a
						medical emergency, please call 911 first.</p>
					<p>If you suspect that you might have symptoms of COVID-19, please call your local hospital or doctor
						before arriving to receive instructions for testing and treatment.</p>
					
					<ul>
						<li><a
								href="https://www.sentara.com/hampton-roads-virginia/hospitalslocations/locations/sentara-virginia-beach-general-hospital/directions-parking.aspx">Sentara
								Virginia Beach General Hospital</a></li>
						<li><a
								href="https://www.sentara.com/hampton-roads-virginia/hospitalslocations/locations/sentara-princess-anne-hospital/directions-parking.aspx">Sentara
								Princess Anne Hospital</a></li>
						<li><a
								href="https://www.sentara.com/hampton-roads-virginia/hospitalslocations/locations/sentara-norfolk-general-hospital/directions-parking.aspx">Sentara
								Norfolk General Hospital</a></li>
						<li><a
								href="https://www.sentara.com/hampton-roads-virginia/hospitalslocations/locations/sentara-leigh-hospital/directions-parking.aspx">Sentara
								Leigh Hospital</a></li>
					</ul>
					
					<p>Be sure to confirm the location of the hospital nearest to your home before arriving. For non-medical
						emergencies, contact your doctor before visiting their office for treatment.</p>
					
					<h2>For Rent Assistance</h2>
					
					<p>If you need help with your monthly rent payments, there is help available! The following resource(s)
						can help you with short-term assistance.</p>
					
					<ul>
						<li><a href="https://www.needhelppayingbills.com/html/virginia_beach_rent_assistance.html">Virginia Beach Rent Assistance</a></li>
					</ul>
					
					<p>For questions about how the Lonnie Bush Property Management team is handling rent payments or
						evictions for residents during the COVID-19 situation, please contact us directly at 757-217-0924.</p>
					
					<h2>Lonnie Bush Property Management Office Hours</h2>
					
					<p>Our office is currently closed to the public, but we are here and available during the following
						office hours to continue serving owners and residents!</p>
					
					<ul>
						<li><strong>Monday thru Friday: 8:30 am to 5:00 pm</strong></li>
						<li><strong>Saturday and Sunday: Closed</strong></li>
					</ul>
					
					<p>What are your anticipated work order response times:</p>
					
					<h2>Our Current Response Times</h2>
					
					<p>Lonnie Bush Property Management continues to prioritize all requests based on the type of request to
						make sure we can respond promptly and with the best solution. While working under current COVID-19
						conditions might affect some of our response times, we are committed to:</p>
					<ul>
						<li><strong>Emergency requests: immediate</strong></li>
						<li><strong>Non-emergency: within 24 business hours</strong></li>
					</ul>
					
					<p>Please make requests for all work orders through your online <a href="https://www.propertymeld.com/">work
							order portal</a>.</p>
					<p>Check back to this page for any updates to these response estimates. We appreciate your patience as we
						work to honor social distancing and safe working guidelines to answer every request as effectively as
						we can.</p>
					
					<p><em>Please note: in the case of a lockdown, we will deal with non-emergency work orders first. Work
							orders that can wait will be put on hold until the ban is lifted.</em></p>
					
					<h2>For Our Owners</h2>
					
					<p>The team at Lonnie Bush Property Management will continue to monitor the COVID-19 situation and alert
						you to any changes necessary to maintain our high level of care for your properties. Because we
						already utilize property management technology, we are equipped to keep your investment properties
						running with little disruption. Our expert staff is going above and beyond to balance COVID-19
						protocols while keeping your tenants safe and your properties in business-as-usual conditions.</p>
					<p>If you have any questions about your properties or how we are adapting the way we work to comply with
						current quarantine and social distancing protocols, please contact us at <a
							href="mailto:ownerservices@lonniebush.com">ownerservices@lonniebush.com</a>
						or 757-217-0924.</p>
				
				
				</section>
			</section>
			<!-- /#about-us -->
		</div>
		<!-- /.col-md-12 -->
	</div>
</main>
