<?php

$title       = (!empty($title)) ? $title : '';
$description = (!empty($description)) ? $description : '';
$keywords    = (!empty($keywords)) ? $keywords : '';
$bodyClass   = (!empty($bodyClass)) ? $bodyClass : '';

// Set up our default navbar button classes
$nav_active['home']     = (!empty($nav_active['home'])) ? $nav_active['home'] : '';
$nav_active['services'] = (!empty($nav_active['services'])) ? $nav_active['services'] : '';
$nav_active['search']   = (!empty($nav_active['search'])) ? $nav_active['search'] : '';
$nav_active['owners']   = (!empty($nav_active['owners'])) ? $nav_active['owners'] : '';
$nav_active['tenants']  = (!empty($nav_active['tenants'])) ? $nav_active['tenants'] : '';
$nav_active['agents']  = (!empty($nav_active['agents'])) ? $nav_active['agents'] : '';
$nav_active['about']    = (!empty($nav_active['about'])) ? $nav_active['about'] : '';


?>

<!DOCTYPE html>
<html>
<head>
	<title><?php echo $title ?></title>

	<!-- metas -->
	<meta charset="utf-8">
	<meta name="author" content="Lonnie Bush Property Management">
	<meta name="keywords" content="<?php echo $keywords ?>">
	<meta name="description" content="<?php echo $description ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<meta name="google-site-verification" content="YLN9o_j4DwFSwPcqhu4mc-pdvhEG7pW6HyCOtegPAsY" />
	<link rel="shortcut icon" href="/favicon.ico">
	<!--/ metas -->

	<!-- styles -->
	<link rel="stylesheet" type="text/css" href="/includes/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/layerslider.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/fullwidth/skin.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/font-awesome.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/jquery.fancybox.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/styles.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/color-red.css">
	<link rel="stylesheet" type="text/css" href="/includes/css/custom.css">

	<!--/ styles -->

	<!--[if lt IE 9]>
	<script src="/includes/js/html5.js"></script><![endif]-->
</head>

<body class="<?php echo $bodyClass ?>">
<div class="page">

	<!-- page header -->
	<header class="page-header main-page sticky">

		<div class="sticky-wrapp">
			<div class="sticky-container">
				<!-- logo -->
				<section id="logo" class="logo">
					<div>
						<a href="/"><img src="/data/images/logo.jpg" alt="Lonnie Bush Property Management"></a>
					</div>
				</section>
				<!--/ logo -->

				<!-- main nav -->
				<nav class="main-nav">
					<ul>
						<li>
							<a class="<?php echo $nav_active['home'] ?>" href="/"><i class="fa fa-plus"></i>Home</a>
						</li>
						<li>
							<a class="<?php echo $nav_active['services'] ?>" href="/services/"><i class="fa fa-plus"></i>Services</a>

							<?php /*
							<ul>
								<li><a href="/services/management/">Full Service Management</a></li>
								<li><a href="/services/marketing/">Marketing Plan</a></li>
								<li><a href="/services/selection/">Tenant Selection</a></li>
							</ul>
                       */ ?>

						</li>
						<li>
							<a class="<?php echo $nav_active['search'] ?>" href="/search/"><i class="fa fa-plus"></i>Find a Rental</a>
						</li>
						<li>
							<a class="no-link <?php echo $nav_active['owners'] ?>" href="javascript: void(0);"><i class="fa fa-plus"></i>Owners</a>
							<ul>
								<li><a href="/owners/guaranteed/">Guaranteed Rent Program</a></li>
								<li><a href="/owners/faq/">Owner FAQ</a></li>
								<li><a href="https://remax.appfolio.com/oportal/users/log_in">Owner Login</a></li>
								<li><a href="https://www.lonniebush.com/buyers/">Help Me Buy A Home</a></li>
								<li><a href="https://www.lonniebush.com/sellers/">Help Me Sell A Home</a></li>
								<li><a href="/owners/rentready/">Rent Ready</a></li>
							</ul>
						</li>
						<li>
							<a class="no-link <?php echo $nav_active['tenants'] ?>" href="javascript:void(0);"><i class="fa fa-plus"></i>Tenants</a>
							<ul>
								<li><a href="/tenants/faq/">Tenant FAQ</a></li>
								<li><a href="https://app.propertymeld.com/tenant/lonnie-bush-property-management">Maintenance</a></li>
								<li><a href="/tenants/portal/">Pay Rent Online</a></li>
								<li><a href="/tenants/portal/">Tenant Portal</a></li>
								<li><a href="https://remax.appfolio.com/connect/users/sign_in">Tenant Login</a></li>
								<li><a href="https://myfreeconnection.com/lonniebushpm/">Utility Setup</a></li>
								<li><a href="https://www.lonniebush.com/buyers/">Help Me Buy A Home</a></li>

							</ul>
						</li>
						<li>
							<a class="no-link <?php echo $nav_active['about'] ?>" href="javascript:void(0);"><i class="fa fa-plus"></i>About</a>
							<ul>
								<li><a href="/about/">About Us</a></li>
								<li><a href="/team/">Meet The Team</a></li>
								<li><a href="/vendors/">Preferred Vendors</a></li>
								<li><a href="/vendors/faq/">Vendors FAQ</a></li>
								<li><a href="https://www.gatherkudos.com/lonniebushpm/" target="_blank">Reviews</a></li>
								<li><a href="/about/covid_19/">COVID-19 Resources</a></li>
								<li><a href="/contact/">Contact Us</a></li>
							</ul>
						</li>
						<li>
							<a class="no-link <?php echo $nav_active['agents'] ?>" href="javascript:void(0);"><i class="fa fa-plus"></i>Agents</a>
							<ul>
								<li><a href="/agents/refer/">Refer Owner</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!--/ main nav -->

				<!-- mobile nav -->
				<nav id="mobile-main-nav" class="mobile-main-nav">
					<i class="fa fa-bars"></i><a href="#" class="opener">Navigation</a>
					<ul>
						<li>
							<a href="/" class="active">Home</a>
						</li>
						<li>
							<a href="/services/">Services</a>
						</li>
						<li>
							<a href="/search/">Find A Rental</a>
						</li>
						<li>
							<i></i><a href="javascript:void(0);">Owners</a>
							<ul>
								<li><a href="/owners/guaranteed/">Guaranteed Rent Program</a></li>
								<li><a href="/owners/faq/">Owner FAQ</a></li>
								<li><a href="https://remax.appfolio.com/oportal/users/log_in">Owner Login</a></li>
								<li><a href="https://www.lonniebush.com/buyers/">Help Me Buy A Home</a></li>
								<li><a href="https://www.lonniebush.com/sellers/">Help Me Sell A Home</a></li>
							</ul>
						</li>
						<li>
							<i></i><a href="javascript:void(0);">Tenants</a>
							<ul>
								<li><a href="/tenants/faq/">Tenant FAQ</a></li>
								<li><a href="https://app.propertymeld.com/tenant/lonnie-bush-property-management">Maintenance</a></li>
								<li><a href="/tenants/portal/">Pay Rent Online</a></li>
								<li><a href="/tenants/portal/">Tenant Portal</a></li>
								<li><a href="https://remax.appfolio.com/connect/users/sign_in">Tenant Login</a></li>
								<li><a href="https://myfreeconnection.com/lonniebushpm/">Utility Setup</a></li>
								<li><a href="https://www.lonniebush.com/buyers/">Help Me Buy A Home</a></li>
							</ul>
						</li>
						<li>
							<i></i><a href="javascript:void(0);">About</a>
							<ul>
								<li><a href="/about/">About Us</a></li>
								<li><a href="/team/">Meet The Team</a></li>
								<li><a href="/vendors/">Preferred Vendors</a></li>
								<li><a href="/vendors/faq/">Vendors FAQ</a></li>
								<li><a href="/about/covid_19/">COVID-19 Resources</a></li>
								<li><a href="/contact/">>Contact Us</a></li>
							</ul>
						</li>
						<li>
							<i></i><a href="javascript:void(0);">Agents</a>
							<ul>
								<li><a href="/agents/refer/">Refer Owner</a></li>
							</ul>
						</li>
					</ul>
				</nav>
				<!--/ mobile nav -->
			</div>
		</div>
	</header>
	<!--/ page header -->

	<!-- quick search -->
	<div id="quick-search" class="quick-search">
		<fieldset>
			<span class="header-login"><a href="https://remax.appfolio.com/connect/users/sign_in">Tenant Login</a></span> <span class="header-divider"></span>
			<span class="header-login"><a href="https://remax.appfolio.com/oportal/users/log_in">Owner Login</a></span> <span class="header-divider"></span>
			<span class="header-phone">(757) 217-0924</span>
		</fieldset>
	</div>
	<!--/ quick search -->
