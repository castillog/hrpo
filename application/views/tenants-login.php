<!-- page title -->
<section class="page-title">
	<div class="grid-row clearfix">
		<h1>Tenants</h1>

		<nav class="bread-crumbs">
			<a href="/">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;
			<a href="/tenants/login/">Tenant Login</a>
		</nav>
	</div>
</section>
<!--/ page title -->

<!-- page content -->
<main class="page-content vc_responsive">
	<div class="grid-row">
		<!-- philosophy -->
		<section>
			<div class="widget-title">Tenant Login</div>
			<div class="wpb_text_column">
				<div class="wpb_wrapper">



				</div>
			</div>
		</section>

		<section id="question-form">

			<div class="widget-title">Have a question? Let us know!</div>

			<div id="ask_question_flash" class="<?php echo $flash_class ?>" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<?php echo $flash_msg ?>
			</div>

			<form id="ask_question_form" method="post" action="" role="form">
				<div class="row">

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">First Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_firstname" name="ask_question_firstname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_name">Last Name<em>*</em></label>
							<input type="text" class="form-control" id="ask_question_lastname" name="ask_question_lastname"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_email">Email<em>*</em></label>
							<input type="email" class="form-control" id="ask_question_email" name="ask_question_email"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->

					<div class="col-md-6">
						<div class="form-group">
							<label for="ask_question_phone">Phone</label>
							<input type="text" class="form-control" id="ask_question_phone" name="ask_question_phone"
								required>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-6 -->
				</div>
				<!-- /.row -->

				<div class="row">
					<div class="col-md-12">
						<div class="form-group">
							<label for="ask_question_message">Your Message<em>*</em></label>
										<textarea class="form-control" id="ask_question_message" rows="8" name="ask_question_message"
											required></textarea>
						</div>
						<!-- /.form-group -->
					</div>
					<!-- /.col-md-12 -->
				</div>
				<!-- /.row -->

				<div class="form-group clearfix">
					<input type="hidden" id="ask_question_form_submit" name="ask_question_form_submit" value="1">
					<button type="submit" class="button pull-right" id="ask_question_submit">Send a Message</button>
				</div>
				<!-- /.form-group -->
				<div id="form-status"></div>
			</form>
			<!-- /#form-contact -->
		</section>

	</div>
</main>
<!--/ page content -->
