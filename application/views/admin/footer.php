<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		Copyright &copy; 2015 Lilah Media. All Rights Reserved.
	</div>
	<div class="page-footer-tools">
		<span class="go-top">
		<i class="fa fa-angle-up"></i>
		</span>
	</div>
</div>
<!-- END FOOTER -->

<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->

<!-- BEGIN CORE PLUGINS /includes/admin/plugins -->
<!--[if lt IE 9]>
<script type="text/javascript" src="/includes/admin/plugins/respond.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript" src="/includes/admin/plugins/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery-migrate-1.2.1.min.js"></script>
<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
<script type="text/javascript" src="/includes/admin/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.blockui.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.cokie.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/uniform/jquery.uniform.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- END CORE PLUGINS /includes/admin/plugins -->

<!-- BEGIN PAGE LEVEL PLUGINS  /includes/admin/plugins -->
<script type="text/javascript" src="/includes/admin/plugins/jqvmap/jqvmap/jquery.vmap.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/flot/jquery.flot.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/flot/jquery.flot.categories.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/jquery.pulsate.min.js"></script>
<script type='text/javascript' src='/includes/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'></script>
<script type='text/javascript' src='/includes/admin/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js'></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-daterangepicker/moment.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->
<!--<script type="text/javascript" src="/includes/admin/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script> -->
<!--<script type="text/javascript" src="/includes/admin/plugins/jquery-easypiechart/jquery.easypiechart.min.js"></script> -->
<script type="text/javascript" src="/includes/admin/plugins/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/fuelux/js/spinner.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/gritter/js/jquery.gritter.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/select2/select2.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-fileinput/bootstrap-fileinput.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/fancybox/source/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/fancybox/source/helpers/jquery.fancybox-media.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/youtube-video-gallery/jquery.youtubevideogallery.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/orakuploader/orakuploader.js"></script>
<script type="text/javascript" src="/includes/admin/plugins/bootstrap-summernote/summernote.js"></script>
<!-- END PAGE LEVEL PLUGINS /includes/admin/plugins -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script type="text/javascript" src="/includes/admin/scripts/metronic.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/layout.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/quick-sidebar.js"></script>
<script type="text/javascript" src="/includes/admin/scripts/custom.js"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
	jQuery(document).ready(function()
	{
		Metronic.init();
		Layout.init();
		QuickSidebar.init();
		Custom.init();
	});
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>

