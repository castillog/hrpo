<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

<!-- BEGIN PAGE HEADER-->
<div class="row">
	<div class="col-md-12">

		<!-- BEGIN PAGE TITLE & BREADCRUMB-->
		<h3 class="page-title">
			Dashboard
			<small>statistics and more</small>
		</h3>
		<ul class="page-breadcrumb breadcrumb">
			<li>
				<i class="fa fa-home"></i>
				<a href="/admin/">Dashboard</a>
			</li>
		</ul>
		<!-- END PAGE TITLE & BREADCRUMB-->

	</div>
</div>
<!-- END PAGE HEADER-->

<!-- BEGIN DASHBOARD CONTENT -->

<!-- BEGIN DASHBOARD STATS -->
<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="dashboard-stat red-intense">
			<div class="visual">
				<i class="fa fa-question-circle"></i>
			</div>
			<div class="details">
				<div class="number">
					Site
				</div>
				<div class="desc">
					Support
				</div>
			</div>
			<a class="more" href="mailto:support@lilahmedia.com">
				Submit Request <i class="m-icon-swapright m-icon-white"></i>
			</a>
		</div>
	</div>
</div>
<!-- END DASHBOARD STATS -->

<div class="clearfix"></div>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<!-- BEGIN CONTACT REPORT PORTLET-->
		<div id="contact_report_portlet" class="portlet box grey-gallery">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-home" style="font-size:16px;"></i>Contact Request Report (Last 30 Days)
				</div>
				<div class="tools">
					<a id="contact_report_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="contact_report">
								<thead>
								<tr>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Message</th>
									<th>Date</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $contactReportHtml ?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END SHOWING REPORT PORTLET-->
	</div>
</div>
<!-- END DASHBOARD CONTENT -->

</div>
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->

