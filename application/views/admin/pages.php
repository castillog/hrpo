<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">
		
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Pages
					<small>content and options</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i> <a href="/admin/">Dashboard</a> <i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/admin/pages/home-page/">Pages</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			
			</div>
		</div>
		<!-- END PAGE HEADER-->
		
		<!-- BEGIN EDIT CONTENT FORM -->
		<div class="row">
			<div class="col-md-12 col-sm-12">
				
				<!-- BEGIN REFER OWNERS PORTLET-->
				<div id="pages_refer_owners_portlet" class="portlet box green-lilahmedia <?php echo $pages_refer_owners_hide ?>">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gear"></i>Refer Owners Page
						</div>
						<div class="tools">
							<a id="pages_refer_owners_collapse" class="expand" href="javascript:;"></a>
						</div>
					</div>
					<div class="portlet-body form light-grey display-hide">
						<form id="pages_refer_owners_form" action="/admin/pages/update/settings/" method="post" role="form">
							<div class="form-body">
								<div class="row">
									<div class="col-md-12">

										<h4 class="form-section">Content</h4>

										<div class="form-group col-md-12">
											<label for="refer_owners_title">Title</label>
											<input type="text" class="form-control" id="refer_owners_title" name="refer_owners_title" placeholder="Enter Title" value="<?php echo $refer_owners_title ?>" <?php echo $readonly ?>>
										</div>
										<div class="form-group col-md-12">
											<label for="refer_owners_text">Text</label>
											<textarea class="summernote form-control" rows="6" name="refer_owners_text" id="refer_owners_text" <?php echo $readonly ?>><?php echo $refer_owners_text ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-actions fluid">
								<div class="text-center">
									<input type="hidden" name="pages_form_submit" value="pages_refer_owners">
									<?php echo $refer_owners_save_button ?>
									<button id="pages_refer_owners_cancel" class="btn default" type="button">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END REFER OWNERS PORTLET-->
				
				<!-- BEGIN VENDORS FAQ PORTLET-->
				<div id="pages_vendors_faq_portlet" class="portlet box grey-gallery <?php echo $pages_vendors_faq_hide ?>">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gear"></i>Vendors FAQ Page
						</div>
						<div class="tools">
							<a id="pages_vendors_faq_collapse" class="expand" href="javascript:;"></a>
						</div>
					</div>
					<div class="portlet-body form light-grey display-hide">
						<form id="pages_vendors_faq_form" action="/admin/pages/update/settings/" method="post" role="form">
							<div class="form-body">
								<div class="row">
									<div class="col-md-12">
										
										<h4 class="form-section">Content</h4>
										
										<div class="form-group col-md-12">
											<label for="vendors_faq_title">Title</label>
											<input type="text" class="form-control" id="vendors_faq_title" name="vendors_faq_title" placeholder="Enter Title" value="<?php echo $vendors_faq_title ?>" <?php echo $readonly ?>>
										</div>
										<div class="form-group col-md-12">
											<label for="vendors_faq_text">Text</label>
											<textarea class="summernote form-control" rows="6" name="vendors_faq_text" id="vendors_faq_text" <?php echo $readonly ?>><?php echo $vendors_faq_text ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-actions fluid">
								<div class="text-center">
									<input type="hidden" name="pages_form_submit" value="pages_vendors_faq">
									<?php echo $vendors_faq_save_button ?>
									<button id="pages_vendors_faq_cancel" class="btn default" type="button">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END VENDORS FAQ PORTLET-->
				
				<!-- BEGIN MEET THE TEAM PORTLET-->
				<div id="pages_team_portlet" class="portlet box green-lilahmedia <?php echo $pages_team_hide ?>">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gear"></i>Meet The Team Page
						</div>
						<div class="tools">
							<a id="pages_team_collapse" class="expand" href="javascript:;"></a>
						</div>
					</div>
					<div class="portlet-body form light-grey display-hide">
						<form id="pages_team_form" action="/admin/pages/update/settings/" method="post" role="form">
							<div class="form-body">
								<div class="row">
									<div class="col-md-12">
										
										<h4 class="form-section">Content</h4>
										
										<div class="form-group col-md-12">
											<label for="team_title">Title</label>
											<input type="text" class="form-control" id="team_title" name="team_title" placeholder="Enter Introduction Title" value="<?php echo $team_title ?>" <?php echo $readonly ?>>
										</div>
										<div class="form-group col-md-12">
											<label for="team_text">Text</label>
											<textarea class="summernote form-control" rows="6" name="team_text" id="team_text" <?php echo $readonly ?>><?php echo $team_text ?></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-actions fluid">
								<div class="text-center">
									<input type="hidden" name="pages_form_submit" value="pages_team">
									<?php echo $team_save_button ?>
									<button id="pages_team_cancel" class="btn default" type="button">Cancel</button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<!-- END MEET THE TEAM PORTLET-->
				
			</div>
		</div>
		<!-- END ROW -->
		
		<!-- END EDIT CONTENT FORM -->
	
	</div>
</div><!-- END CONTENT -->

</div><!-- END CONTAINER -->

