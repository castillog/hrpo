<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
	<div class="page-content">

		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">

				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					Users
					<small>accounts and permissions</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i> <a href="/admin/">Dashboard</a> <i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="/admin/users/">Users</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->

			</div>
		</div>
		<!-- END PAGE HEADER-->

		<!-- BEGIN USER LIST PORTLET-->
		<div id="user_list_portlet" class="portlet box green-lilahmedia <?php echo $user_list_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>User List
				</div>
				<div class="tools">
					<a id="user_list_collapse" class="collapse" href="javascript:;"></a>
				</div>
				<div class="actions">
					<?php echo $users_add_button ?>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="user_list">
								<thead>
								<tr>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Username</th>
									<th>Role</th>
									<th>Enabled</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $users ?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END USER LIST PORTLET-->

		<!-- BEGIN USER INFO PORTLET-->
		<div id="user_info_portlet" class="portlet box green-lilahmedia <?php echo $user_info_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>User Info
				</div>
				<div class="tools">
					<a id="user_info_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form enctype="multipart/form-data" id="user_info_form" class="form-horizontal" action="" method="post" role="form">
					<div class="form-body">
						<div class="<?php echo $flash_class ?>"><?php echo $flash_text ?></div>
						<h4 class="form-section">Account Settings</h4>
						
						<div class="form-group">
							<div class="col-md-offset-3 col-md-3">
								<div class="checkbox">
									<label><input type="checkbox" name="user_info_enabled" <?php echo $user_enabled ?>> Enabled? </label>
								</div>
							</div>
							<div class="col-md-6">
								<div class="checkbox">
									<label><input type="checkbox" name="user_info_featured" <?php echo $user_featured ?>> Featured? </label>
								</div>
							</div>
						</div>

						<?php if(!empty($role) && ($role == "master" || $role == "admin" || $role == "company owner")) : ?>
							<div class="form-group">
								<label for="user_info_role" class="col-md-3 control-label">Role:</label>

								<div class="col-md-3">
									<select class="form-control select2me" id="user_info_role" name="user_info_role" data-placeholder="Select...">
										<option value="">Select...</option>
										<option value="agent" <?php echo $user_role_agent ?>>Agent</option>
										<option value="team leader" <?php echo $user_role_team_leader ?>>Team Leader</option>
										<option value="office manager" <?php echo $user_role_office_manager ?>>Office Manager</option>
										<option value="company owner" <?php echo $user_role_company_owner ?>>Company Owner</option>

										<?php if(!empty($role) && ($role == "master" || $role == "admin")) : ?>
											<option value="admin" <?php echo $user_role_admin ?>>Admin</option>
										<?php endif; ?>
										
										<?php if(!empty($role) && ($role == "master")) : ?>
											<option value="master" <?php echo $user_role_master ?>>Master</option>
										<?php endif; ?>
										
									</select>
								</div>
							</div>
						<?php endif; ?>

						<div class="form-group">
							<label for="user_info_username" class="col-md-3 control-label">Username:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="user_info_username" id="user_info_username" value="<?php echo $user_username ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_password" class="col-md-3 control-label">Password:</label>

							<div class="col-md-5">
								<input type="password" class="form-control" name="user_info_password" id="user_info_password" value="">
							</div>
						</div>
						<h4 class="form-section">User Information</h4>

						<div class="form-group">
							<label for="user_info_first_name" class="col-md-3 control-label">First Name:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="user_info_first_name" id="user_info_first_name" value="<?php echo $user_first_name ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_last_name" class="col-md-3 control-label">Last Name:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="user_info_last_name" id="user_info_last_name" value="<?php echo $user_last_name ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_email" class="col-md-3 control-label">Email:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="user_info_email" id="user_info_email" value="<?php echo $user_email ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_phone" class="col-md-3 control-label">Phone:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="user_info_phone" id="user_info_phone" value="<?php echo $user_phone ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_mobile" class="col-md-3 control-label">Mobile:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="user_info_mobile" id="user_info_mobile" value="<?php echo $user_mobile ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_street" class="col-md-3 control-label">Street:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="user_info_street" id="user_info_street" value="<?php echo $user_street ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_city" class="col-md-3 control-label">City:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="user_info_city" id="user_info_city" value="<?php echo $user_city ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_state" class="col-md-3 control-label">State:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="user_info_state" id="user_info_state" value="<?php echo $user_state ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_zipcode" class="col-md-3 control-label">Zipcode:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="user_info_zipcode" id="user_info_zipcode" value="<?php echo $user_zipcode ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_license" class="col-md-3 control-label">License #:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="user_info_license" id="user_info_license" value="<?php echo $user_license ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_title" class="col-md-3 control-label">Title:</label>
							
							<div class="col-md-8">
								<input type="text" class="form-control" name="user_info_title" id="user_info_title" value="<?php echo $user_title ?>">
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Image:</label>

							<div class="col-md-9">
								<div style="font-style: italic;margin: 10px 0px 5px;">Note: User images need to be sized to 250 x 300px.</div>
								<div class="fileinput <?php if(!empty($user_image)) echo 'fileinput-exists'; else echo 'fileinput-new'; ?>" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 250px; height: 300px;">
										<img src='http://via.placeholder.com/250x300?text=No+Image' alt='' style='width:100%;height:auto;'/>
									</div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="width: 250px; height: 300px;">
										<?php if(!empty($user_image)) echo "<img src='{$user_image}' alt='' style='width:100%;height:auto;'/>"; ?>
									</div>
									<div>
									<span class="btn default btn-file">
										<span class="fileinput-new"> Select image </span>
										<span class="fileinput-exists"> Change </span>
										<input type="file" name="user_info_image">
									</span>
										<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_bio" class="col-md-3 control-label">Bio:</label>
							
							<div class="col-md-8">
								<textarea class="summernote form-control" rows="6" name="user_info_bio" id="user_info_bio"><?php echo $user_bio ?></textarea>
							</div>
						</div>
						
						<h4 class="form-section">Group Settings</h4>

						<div class="form-group">
							<label for="user_info_company" class="col-md-3 control-label">Company:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="user_info_company" name="user_info_company" data-placeholder="Select...">

									<?php echo $user_companies ?>

								</select>
							</div>
						</div>
						
						<?php /*
						<div class="form-group">
							<label for="user_info_office" class="col-md-3 control-label">Office:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="user_info_office" name="user_info_office" data-placeholder="Select...">

									<?php echo $user_offices ?>

								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="user_info_team" class="col-md-3 control-label">Team:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="user_info_team" name="user_info_team" data-placeholder="Select...">

									<?php echo $user_teams ?>

								</select>
							</div>
						</div>
						*/ ?>
						
					</div>
					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="user_form_submit" value="1">
							<button id="user_info_save" class="btn green-lilahmedia" type="submit">Save</button>
							<button id="user_info_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END USER INFO PORTLET-->

		<?php /*
		<!-- BEGIN TEAM LIST PORTLET-->
		<div id="team_list_portlet" class="portlet box grey-gallery <?php echo $team_list_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Team List
				</div>
				<div class="tools">
					<a id="team_list_collapse" class="expand" href="javascript:;"></a>
				</div>
				<div class="actions">
					<?php echo $teams_add_button ?>
				</div>
			</div>
			<div class="portlet-body form light-grey display-hide">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="team_list">
								<thead>
								<tr>
									<th>Team Name</th>
									<th>Team Leader</th>
									<th>Email</th>
									<th>Enabled</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $teams ?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END TEAM LIST PORTLET-->

		<!-- BEGIN TEAM INFO PORTLET-->
		<div id="team_info_portlet" class="portlet box grey-gallery <?php echo $team_info_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Team Info
				</div>
				<div class="tools">
					<a id="team_info_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="team_info_form" class="form-horizontal" action="" method="post" role="form">
					<div class="form-body">
						<h4 class="form-section">Team Settings</h4>

						<div class="form-group">
							<div class="col-md-offset-3 col-md-3">
								<div class="checkbox">
									<label><input type="checkbox" name="team_info_enabled" <?php echo $team_enabled ?>> Enabled? </label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_team_name" class="col-md-3 control-label">Team Name:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="team_info_team_name" id="team_info_team_name" value="<?php echo $team_name ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_team_leader" class="col-md-3 control-label">Team Leader:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="team_info_team_leader" id="team_info_team_leader" value="<?php echo $team_leader ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_email" class="col-md-3 control-label">Email:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="team_info_email" id="team_info_email" value="<?php echo $team_email ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_phone" class="col-md-3 control-label">Phone:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="team_info_phone" id="team_info_phone" value="<?php echo $team_phone ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_mobile" class="col-md-3 control-label">Mobile:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="team_info_mobile" id="team_info_mobile" value="<?php echo $team_mobile ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_street" class="col-md-3 control-label">Street:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="team_info_street" id="team_info_street" value="<?php echo $team_street ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_city" class="col-md-3 control-label">City:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="team_info_city" is="team_info_city" value="<?php echo $team_city ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_state" class="col-md-3 control-label">State:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="team_info_state" id="team_info_state" value="<?php echo $team_state ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_zipcode" class="col-md-3 control-label">Zipcode:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="team_info_zipcode" id="team_info_zipcode" value="<?php echo $team_zipcode ?>">
							</div>
						</div>
						<h4 class="form-section">Group Settings</h4>

						<div class="form-group">
							<label for="team_info_company" class="col-md-3 control-label">Company:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="team_info_company" name="team_info_company" data-placeholder="Select...">

									<?php echo $team_companies ?>

								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="team_info_office" class="col-md-3 control-label">Office:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="team_info_office" name="team_info_office" data-placeholder="Select...">

									<?php echo $team_offices ?>

								</select>
							</div>
						</div>
					</div>
					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="team_form_submit" value="1">
							<button id="team_info_save" class="btn grey-gallery" type="submit">Save</button>
							<button id="team_info_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END TEAM INFO PORTLET-->

		<!-- BEGIN OFFICE LIST PORTLET-->
		<div id="office_list_portlet" class="portlet box green-lilahmedia <?php echo $office_list_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Office List
				</div>
				<div class="tools">
					<a id="office_list_collapse" class="expand" href="javascript:;"></a>
				</div>
				<div class="actions">
					<?php echo $offices_add_button ?>
				</div>
			</div>
			<div class="portlet-body form light-grey display-hide">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="office_list">
								<thead>
								<tr>
									<th>Office Name</th>
									<th>Office Manager</th>
									<th>Email</th>
									<th>Enabled</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $offices ?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END OFFICE LIST PORTLET-->

		<!-- BEGIN OFFICE INFO PORTLET-->
		<div id="office_info_portlet" class="portlet box green-lilahmedia <?php echo $office_info_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Office Info
				</div>
				<div class="tools">
					<a id="office_info_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="office_info_form" class="form-horizontal" action="" method="post" role="form">
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-offset-3 col-md-3">
								<div class="checkbox">
									<label><input type="checkbox" name="office_info_enabled" <?php echo $office_enabled ?>> Enabled? </label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_office_name" class="col-md-3 control-label">Office Name:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="office_info_office_name" id="office_info_office_name" value="<?php echo $office_name ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_office_manager" class="col-md-3 control-label">Office Manager:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="office_info_office_manager" id="office_info_office_manager" value="<?php echo $office_manager ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_email" class="col-md-3 control-label">Email:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="office_info_email" id="office_info_email" value="<?php echo $office_email ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_phone" class="col-md-3 control-label">Phone:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="office_info_phone" id="office_info_phone" value="<?php echo $office_phone ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_mobile" class="col-md-3 control-label">Mobile:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="office_info_mobile" id="office_info_mobile" value="<?php echo $office_mobile ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_street" class="col-md-3 control-label">Street:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="office_info_street" id="office_info_street" value="<?php echo $office_street ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_city" class="col-md-3 control-label">City:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="office_info_city" id="office_info_city" value="<?php echo $office_city ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_state" class="col-md-3 control-label">State:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="office_info_state" id="office_info_state" value="<?php echo $office_state ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_zipcode" class="col-md-3 control-label">Zipcode:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="office_info_zipcode" id="office_info_zipcode" value="<?php echo $office_zipcode ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="office_info_company" class="col-md-3 control-label">Company:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="office_info_company" name="office_info_company" data-placeholder="Select...">

									<?php echo $office_companies ?>

								</select>
							</div>
						</div>
					</div>
					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="office_form_submit" value="1">
							<button id="office_info_save" class="btn green-lilahmedia" type="submit">Save</button>
							<button id="office_info_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END OFFICE INFO PORTLET-->
		*/ ?>
		
		<!-- BEGIN COMPANY LIST PORTLET-->
		<div id="company_list_portlet" class="portlet box grey-gallery <?php echo $company_list_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Company List
				</div>
				<div class="tools">
					<a id="company_list_collapse" class="expand" href="javascript:;"></a>
				</div>
				<div class="actions">
					<?php echo $companies_add_button ?>
				</div>
			</div>
			<div class="portlet-body form light-grey display-hide">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="company_list">
								<thead>
								<tr>
									<th>Company Name</th>
									<th>Company Owner</th>
									<th>Email</th>
									<th>Enabled</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $companies ?>

								</tbody>
							</table>

						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END COMPANY LIST PORTLET-->

		<!-- BEGIN COMPANY INFO PORTLET-->
		<div id="company_info_portlet" class="portlet box grey-gallery <?php echo $company_info_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Company Info
				</div>
				<div class="tools">
					<a id="company_info_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="company_info_form" class="form-horizontal" action="" method="post" role="form">
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-offset-3 col-md-3">
								<div class="checkbox">
									<label><input type="checkbox" name="company_info_enabled" <?php echo $company_enabled ?>> Enabled? </label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_company_name" class="col-md-3 control-label">Company Name:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="company_info_company_name" id="company_info_company_name" value="<?php echo $company_name ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_company_owner" class="col-md-3 control-label">Company Owner:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="company_info_company_owner" id="company_info_company_owner" value="<?php echo $company_owner ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_email" class="col-md-3 control-label">Email:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="company_info_email" id="company_info_email" value="<?php echo $company_email ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_phone" class="col-md-3 control-label">Phone:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="company_info_phone" id="company_info_phone" value="<?php echo $company_phone ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_mobile" class="col-md-3 control-label">Mobile:</label>

							<div class="col-md-3">
								<input type="text" class="form-control" name="company_info_mobile" id="company_info_mobile" value="<?php echo $company_mobile ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_street" class="col-md-3 control-label">Street:</label>

							<div class="col-md-8">
								<input type="text" class="form-control" name="company_info_street" id="company_info_street" value="<?php echo $company_street ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_city" class="col-md-3 control-label">City:</label>

							<div class="col-md-5">
								<input type="text" class="form-control" name="company_info_city" id="company_info_city" value="<?php echo $company_city ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_state" class="col-md-3 control-label">State:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="company_info_state" id="company_info_state" value="<?php echo $company_state ?>">
							</div>
						</div>
						<div class="form-group">
							<label for="company_info_zipcode" class="col-md-3 control-label">Zipcode:</label>

							<div class="col-md-2">
								<input type="text" class="form-control" name="company_info_zipcode" id="company_info_zipcode" value="<?php echo $company_zipcode ?>">
							</div>
						</div>
					</div>
					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="company_form_submit" value="1">
							<button id="company_info_save" class="btn grey-gallery" type="submit">Save</button>
							<button id="company_info_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END COMPANY INFO PORTLET-->
		
		<?php /*
		<!-- BEGIN ACL LIST PORTLET-->
		<div id="acl_list_portlet" class="portlet box green-lilahmedia <?php echo $access_list_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Access Right List
				</div>
				<div class="tools">
					<a id="acl_list_collapse" class="expand" href="javascript:;"></a>
				</div>
				<div class="actions">
					<?php echo $access_add_button ?>
				</div>
			</div>
			<div class="portlet-body form light-grey display-hide">
				<div class="form-body">
					<div class="row">
						<div class="col-md-12">

							<table class="table table-striped table-bordered table-hover" id="acl_list">
								<thead>
								<tr>
									<th>Type</th>
									<th>Role</th>
									<th>Level</th>
									<th>Page</th>
									<th>Actions</th>
								</tr>
								</thead>
								<tbody>

								<?php echo $access ?>

								</tbody>
							</table>


						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END ACL LIST PORTLET-->

		<!-- BEGIN ACL INFO PORTLET-->
		<div id="acl_info_portlet" class="portlet box green-lilahmedia <?php echo $access_info_hide ?>">
			<div class="portlet-title">
				<div class="caption">
					<i class="fa fa-gear"></i>Access Right Info
				</div>
				<div class="tools">
					<a id="acl_info_collapse" class="collapse" href="javascript:;"></a>
				</div>
			</div>
			<div class="portlet-body form light-grey">
				<form id="acl_info_form" class="form-horizontal" action="" method="post" role="form">
					<div class="form-body">
						<div class="form-group">
							<div class="col-md-offset-3 col-md-3">
								<div class="checkbox">
									<label><input type="checkbox" name="acl_info_enabled"
											<?php echo $access_enabled ?>> Enabled? </label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="acl_info_type" class="col-md-3 control-label">Type:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="acl_info_type" name="acl_info_type" data-placeholder="Select...">
									<option value="group" <?php echo $access_type_group ?>>Group</option>
									<option value="user" <?php echo $access_type_user ?>>User</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="acl_info_role" class="col-md-3 control-label">Role:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="acl_info_role" name="acl_info_role" data-placeholder="Select...">
									<option value="all" <?php echo $access_role_all ?>>All</option>
									<option value="admin" <?php echo $access_role_admin ?>>Admin</option>
									<option value="client" <?php echo $access_role_client ?>>Client</option>
									<option value="agent" <?php echo $access_role_agent ?>>Agent</option>
									<option value="team leader" <?php echo $access_role_team ?>>Team Leader</option>
									<option value="office manager" <?php echo $access_role_office ?>>Office Manager</option>
									<option value="company owner" <?php echo $access_role_company ?>>Company Owner</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="acl_info_level" class="col-md-3 control-label">Level:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="acl_info_level" name="acl_info_level" data-placeholder="Select...">
									<option value="all" <?php echo $access_level_all ?>>All</option>
									<option value="agent" <?php echo $access_level_agent ?>>Agent</option>
									<option value="team" <?php echo $access_level_team ?>>Team</option>
									<option value="office" <?php echo $access_level_office ?>>Office</option>
									<option value="company" <?php echo $access_level_company ?>>Company</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="acl_info_page" class="col-md-3 control-label">Page:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="acl_info_page" name="acl_info_page" data-placeholder="Select...">
									<option value="all" <?php echo $access_page_all ?>>All</option>
									<option value="public" <?php echo $access_page_public ?>>Public</option>
									<option value="admin" <?php echo $access_page_admin ?>>Admin</option>
									<option value="admin_dashboard" <?php echo $access_page_admin_dashboard ?>>
										Admin - Dashboard
									</option>
									<option value="admin_pages" <?php echo $access_page_admin_pages ?>>
										Admin - Pages
									</option>
									<option value="admin_properties" <?php echo $access_page_admin_properties ?>>
										Admin - Properties
									</option>
									<option value="admin_users" <?php echo $access_page_admin_users ?>>
										Admin - Users
									</option>
									<option value="admin_leads" <?php echo $access_page_admin_leads ?>>
										Admin - Leads
									</option>
									<option value="admin_communities" <?php echo $access_page_admin_communities ?>>
										Admin - Communities
									</option>
									<option value="admin_settings" <?php echo $access_page_admin_settings ?>>
										Admin - Settings
									</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="acl_info_section" class="col-md-3 control-label">Section:</label>

							<div class="col-md-5">
								<select class="form-control select2me" id="acl_info_section" name="acl_info_section" data-placeholder="Select...">
									<option value="all" <?php echo $access_section_all ?>>All</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-md-3 control-label">Rights:</label>

							<div class="col-md-2">
								<div class="checkbox">
									<label><input type="checkbox" name="acl_info_read" id="acl_info_read"
											<?php echo $access_read ?>> Read </label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="checkbox">
									<label><input type="checkbox" name="acl_info_create" id="acl_info_create"
											<?php echo $access_create ?>> Create </label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="checkbox">
									<label><input type="checkbox" name="acl_info_update" id="acl_info_update"
											<?php echo $access_update ?>> Update </label>
								</div>
							</div>
							<div class="col-md-2">
								<div class="checkbox">
									<label><input type="checkbox" name="acl_info_delete" id="acl_info_delete"
											<?php echo $access_delete ?>> Delete </label>
								</div>
							</div>
						</div>
					</div>
					<div class="form-actions fluid">
						<div class="text-center">
							<input type="hidden" name="access_form_submit" value="1">
							<button id="acl_info_save" class="btn green-lilahmedia" type="submit">Save</button>
							<button id="acl_info_cancel" class="btn default" type="button">Cancel</button>
						</div>
					</div>
				</form>
			</div>
		</div>
		<!-- END ACL INFO PORTLET-->
		*/ ?>
		
	</div>
</div><!-- END CONTENT -->

</div><!-- END CONTAINER -->
