<!DOCTYPE html>

<html lang="en-US">
<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="author" content="ThemeStarz">

	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,700|Lora:400,700' type='text/css'>
	<link rel="stylesheet" href="/includes/css/font-awesome.css" type="text/css">
	<link rel="stylesheet" href="/includes/css/bootstrap.css" type="text/css">
	<link rel="stylesheet" href="/includes/css/style.css" type="text/css">

	<title>Coming Soon Homess | 404 Page Not Found</title>

</head>

<body class="" id="page-top" data-spy="scroll" data-target=".navigation" data-offset="90">
<!-- Wrapper -->
<div class="wrapper">

	<!-- Navigation -->
	<div class="navigation">
		<div class="container-fluid">
			<header class="navbar" id="top" role="banner">
				<div class="navbar-header">
					<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<div class="navbar-brand nav" style="">
						<div>
							<div class="logo">
								<a href="/">
									<img id="logo" src="/includes/img/logo-csc-1.png" alt="logo">
								</a>
							</div>
						</div>
					</div>
				</div>
				<nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
					<div class="nav-border-top"><img src="/includes/img/nav-border-1.png"></div>
					<ul class="nav navbar-nav">
						<li class="">
							<div class=""><a href="/">HOME</a></div>
						</li>
						<li class="has-child">
							<div class=""><a href="#">PROPERTIES</a></div>
							<ul class="child-navigation">
								<li><div class=""><a href="/coming/">Coming Soon</a></div></li>
								<li><div class=""><a href="/available/">Now Accepting Offers</a></div></li>
								<li><div class=""><a href="/listings/">Property Search</a></div></li>
								<li><div class=""><a href="/map/">Map Search</a></div></li>
							</ul>
						</li>
						<li class="">
							<div class=""><a href="/community/">COMMUNITY RESOURCE</a></div>
						</li>
						<li class="has-child">
							<div><a href="#">ABOUT</a></div>
							<ul class="child-navigation">
								<li class=""><div><a href="/about/">What is coming soon?</a></div></li>
								<li class=""><div><a href="/contact/">Contact Us</a></div></li>
							</ul>
						</li>
					</ul>
					<div class="nav-border-bottom"><img src="/includes/img/nav-border-1.png"></div>
				</nav>
				<!-- /.navbar collapse-->
			</header>
			<!-- /.navbar -->
		</div>
		<!-- /.container -->
	</div>
	<!-- /.navigation -->

	<!-- Page Content -->
	<div id="page-content">

		<div class="container">
			<section id="404">
				<div class="error-page">
					<div class="title">
						<img alt="" src="/includes/img/error-page-background.png" class="top">
						<header>404</header>
						<img alt="" src="/includes/img/error-page-background.png" class="bottom">
					</div>
					<h2 class="no-border">Page not found</h2>
					<a href="" class="link-arrow back" onclick="history.back(-1)">Go Back</a>
				</div>
			</section>
		</div>
		<!-- /.container -->
	</div>
	<!-- end Page Content -->

	<footer id="page-footer">
		<section id="footer-content">
			<div class="container-fluid">
				<div class="">
					<div id="links" class="col-md-8 col-sm-8 col-xs-12 footer-text">
						<ul class="list-unstyled list-links list-inline">
							<li><a href="#">Privacy Policy</a></li>
							<li><a href="#">FAQ</a></li>
							<li><a href="#">Terms and Conditions</a></li>
							<li><a href="/contact/">Contact Us</a></li>
							<li><a href="#">About Us</a></li>
						</ul>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 footer-text">
						<ul id="social-icons" class="list-unstyled list-links list-inline">

						</ul>
					</div>

					<div class="col-md-10 col-sm-10 col-xs-12">
					<span id="copyright" class="footer-text-smaller">Copyright © 2014. All Rights Reserved |
						<a href="http://www.lilahmedia.com" target="_blank">
							Website Design | SEO &amp; Internet Marketing by Lilah Media.</a></span>
					</div>
					<div class="col-md-2 col-sm-2 col-xs-12">
					<span id="top-link" class="footer-text-smaller">
						<a href="#page-top" class="roll">Go to top</a></span>
					</div>
				</div>
			</div>
		</section>
	</footer>
	<!-- end #page-footer -->
</div>

<div id="overlay"></div>

<script type="text/javascript" src="/includes/js/jquery-2.1.0.min.js"></script>
<script type="text/javascript" src="/includes/js/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="/includes/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/includes/js/smoothscroll.js"></script>
<script type="text/javascript" src="/includes/js/custom.js"></script>
<!--[if gt IE 8]>
<script type="text/javascript" src="/includes/js/ie.js"></script>
<![endif]-->

</body>
</html>
