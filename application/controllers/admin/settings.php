<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Settings extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->helper('file');
		$this->load->library('upload');
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin       = $this->Accounts->can_user_access('admin-settings');

		if(!$access_admin)
			header("Location: /admin/login/");

		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";

		// We need to know the current user's role, as well as their user_id, team_id, office_id, and company_id
		// respecitively based on that role. We'll get that from their user record
		$user_id    = $this->Accounts->get_user_online();
		$user       = $this->Accounts->get_user_by_id($user_id);
		$role       = $user['role'];
		$team_id    = $user['team_id'];
		$office_id  = $user['office_id'];
		$company_id = $user['company_id'];

		// If the user can view this, but only do that, we'll set any visiable form fields to readonly
		$access_update       = $this->Accounts->can_user_update('admin-settings');

		$readonly = (!$access_update) ? 'disabled' : '';

		$settings_save_button = ($access_update) ? '<button id="settings_save" class="btn green-lilahmedia" type="submit">Save</button>' : '';

		$postback = $this->input->post('settings_form_submit');

		if($postback)
		{
			if(!$access_update)
				header("Location: /admin/login/");

			// Grab our form variables
			$company_name         = $this->input->post('setting_company_name');
			$site_name            = $this->input->post('setting_site_name');
			$company_street       = $this->input->post('setting_company_street');
			$company_city         = $this->input->post('setting_company_city');
			$company_state        = $this->input->post('setting_company_state');
			$company_zipcode      = $this->input->post('setting_company_zipcode');
			$company_phone        = $this->input->post('setting_company_phone');
			$company_email        = $this->input->post('setting_company_email');
			$company_lead_email   = $this->input->post('setting_company_lead_email');
			$company_lead_default = $this->input->post('setting_company_lead_default');
			$company_logo         = "";
			$company_logo_footer  = "";

			$company_facebook  = $this->input->post('setting_facebook');
			$company_twitter   = $this->input->post('setting_twitter');
			$company_google    = $this->input->post('setting_google');
			$company_linkedin  = $this->input->post('setting_linkedin');
			$company_youtube   = $this->input->post('setting_youtube');
			$company_pinterest = $this->input->post('setting_pinterest');
			$gated_views       = $this->input->post('setting_gated_views');

			$site_adwords   = $this->input->post('setting_adwords_embed');
			$site_adwords   = (!empty($site_adwords)) ? $site_adwords : "";
			$site_analytics = $this->input->post('setting_analytics_embed');
			$site_analytics = (!empty($site_analytics)) ? $site_analytics : "";
			$site_facebook  = $this->input->post('setting_facebook_embed');
			$site_facebook  = (!empty($site_facebook)) ? $site_facebook : "";
			$site_bing      = $this->input->post('setting_bing_embed');
			$site_bing      = (!empty($site_bing)) ? $site_bing : "";
			$site_misc      = $this->input->post('setting_misc_embed');
			$site_misc      = (!empty($site_misc)) ? $site_misc : "";

			$settings = $this->Content->get_content_full('settings');

			// We need content ids to update our info, we can grab them straight from our home content array
			$company_name_id         = $settings['company']['name']['id'];
			$site_name_id            = $settings['site']['name']['id'];
			$company_street_id       = $settings['company']['street']['id'];
			$company_city_id         = $settings['company']['city']['id'];
			$company_state_id        = $settings['company']['state']['id'];
			$company_zipcode_id      = $settings['company']['zipcode']['id'];
			$company_phone_id        = $settings['company']['phone']['id'];
			$company_email_id        = $settings['company']['email']['id'];
			$company_lead_email_id   = $settings['company']['lead_email']['id'];
			$company_lead_default_id = $settings['company']['lead_default']['id'];
			$company_logo_id         = $settings['company']['logo']['id'];
			$company_logo_footer_id  = $settings['company']['logo_footer']['id'];

			$company_facebook_id  = $settings['company']['facebook_url']['id'];
			$company_twitter_id   = $settings['company']['twitter_url']['id'];
			$company_google_id    = $settings['company']['google_url']['id'];
			$company_linkedin_id  = $settings['company']['linkedin_url']['id'];
			$company_youtube_id   = $settings['company']['youtube_url']['id'];
			$company_pinterest_id = $settings['company']['pinterest_url']['id'];
			$gated_views_id       = $settings['site']['gated_views']['id'];

			$site_adwords_id   = $settings['site']['adwords_embed']['id'];
			$site_analytics_id = $settings['site']['analytics_embed']['id'];
			$site_facebook_id  = $settings['site']['facebook_embed']['id'];
			$site_bing_id      = $settings['site']['bing_embed']['id'];
			$site_misc_id      = $settings['site']['misc_embed']['id'];

			if(!empty($_FILES['setting_company_logo']))
			{
				$upload_filename = $_FILES['setting_company_logo']['name'];
				$upload_path     = '/data/images/';
				$image_prefix    = 's0' . "-" . uniqid() . '-';

				$config['upload_path']   = '.' . $upload_path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']     = $image_prefix . strtolower($upload_filename);
				$config['max_size']      = '2048';
				$config['overwrite']     = true;

				$this->upload->initialize($config);

				if(!$this->upload->do_upload('setting_company_logo'))
					log_message('error', "Failed to upload file!");
				else
				{
					$upload_data = $this->upload->data();
					$image_name  = $upload_data['file_name'];
					$image_path  = "/data/images/" . $image_name;

					// We have successfully uploaded our image, now we need to insert a new picture record in the db
					$data = array();

					if(!empty($image_path))
					{
						$data['full']  = $image_path;
						$data['image'] = $image_path;
						$data['thumb'] = $image_path;
					}

					$company_logo = $this->Content->create_picture($data);
				}
			}

			if(!empty($_FILES['setting_company_logo_footer']))
			{
				$upload_filename = $_FILES['setting_company_logo_footer']['name'];
				$upload_path     = '/data/images/';
				$image_prefix    = 's0' . "-" . uniqid() . '-';

				$config['upload_path']   = '.' . $upload_path;
				$config['allowed_types'] = 'gif|jpg|jpeg|png';
				$config['file_name']     = $image_prefix . strtolower($upload_filename);
				$config['max_size']      = '2048';
				$config['overwrite']     = true;

				$this->upload->initialize($config);

				if(!$this->upload->do_upload('setting_company_logo_footer'))
					log_message('error', "Failed to upload file!");
				else
				{
					$upload_data = $this->upload->data();
					$image_name  = $upload_data['file_name'];
					$image_path  = "/data/images/" . $image_name;

					// We have successfully uploaded our image, now we need to insert a new picture record in the db
					$data = array();

					if(!empty($image_path))
					{
						$data['full']  = $image_path;
						$data['image'] = $image_path;
						$data['thumb'] = $image_path;
					}

					$company_logo_footer = $this->Content->create_picture($data);
				}
			}

			// Update settings - company name
			$data           = array('content' => $company_name);
			$update_results = $this->Content->update_content($data, $company_name_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company name.");
			else
				log_message('error', "Failed to update settings - company name!");

			// Update settings - site name
			$data           = array('content' => $site_name);
			$update_results = $this->Content->update_content($data, $site_name_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - site name.");
			else
				log_message('error', "Failed to update settings - site name!");

			// Update settings - company street
			$data           = array('content' => $company_street);
			$update_results = $this->Content->update_content($data, $company_street_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company street.");
			else
				log_message('error', "Failed to update settings - company street!");

			// Update settings - company city
			$data           = array('content' => $company_city);
			$update_results = $this->Content->update_content($data, $company_city_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company city.");
			else
				log_message('error', "Failed to update settings - company city!");

			// Update settings - company state
			$data           = array('content' => $company_state);
			$update_results = $this->Content->update_content($data, $company_state_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company state.");
			else
				log_message('error', "Failed to update settings - company state!");

			// Update settings - company zipcode
			$data           = array('content' => $company_zipcode);
			$update_results = $this->Content->update_content($data, $company_zipcode_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company zipcode.");
			else
				log_message('error', "Failed to update settings - company zipcode!");

			// Update settings - company phone
			$data           = array('content' => $company_phone);
			$update_results = $this->Content->update_content($data, $company_phone_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company phone.");
			else
				log_message('error', "Failed to update settings - company phone!");

			// Update settings - company email
			$data           = array('content' => $company_email);
			$update_results = $this->Content->update_content($data, $company_email_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company email.");
			else
				log_message('error', "Failed to update settings - company email!");

			// Update settings - company lead email
			$data           = array('content' => $company_lead_email);
			$update_results = $this->Content->update_content($data, $company_lead_email_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company lead email.");
			else
				log_message('error', "Failed to update settings - company lead email!");

			// Update settings - company lead default
			$data           = array('content' => $company_lead_default);
			$update_results = $this->Content->update_content($data, $company_lead_default_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company lead default.");
			else
				log_message('error', "Failed to update settings - company lead default!");

			// Update settings - company logo
			if(!empty($company_logo))
			{
				$data           = array('content' => $company_logo);
				$update_results = $this->Content->update_content($data, $company_logo_id);

				if($update_results)
					log_message('debug', "Successfully updated settings - company logo.");
				else
					log_message('error', "Failed to update settings - company logo!");
			}

			// Update settings - company logo footer
			if(!empty($company_logo_footer))
			{
				$data           = array('content' => $company_logo_footer);
				$update_results = $this->Content->update_content($data, $company_logo_footer_id);

				if($update_results)
					log_message('debug', "Successfully updated settings - company logo footer.");
				else
					log_message('error', "Failed to update settings - company logo footer!");
			}

			// Update settings - facebook url
			$data           = array('content' => $company_facebook);
			$update_results = $this->Content->update_content($data, $company_facebook_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company facebook url.");
			else
				log_message('error', "Failed to update settings - company facebook url!");

			// Update settings - twitter url
			$data           = array('content' => $company_twitter);
			$update_results = $this->Content->update_content($data, $company_twitter_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company twitter url.");
			else
				log_message('error', "Failed to update settings - company twitter url!");

			// Update settings - google url
			$data           = array('content' => $company_google);
			$update_results = $this->Content->update_content($data, $company_google_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company google url.");
			else
				log_message('error', "Failed to update settings - company google url!");

			// Update settings - linkedin url
			$data           = array('content' => $company_linkedin);
			$update_results = $this->Content->update_content($data, $company_linkedin_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company linkedin url.");
			else
				log_message('error', "Failed to update settings - company linkedin url!");

			// Update settings - youtube url
			$data           = array('content' => $company_youtube);
			$update_results = $this->Content->update_content($data, $company_youtube_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company youtube url.");
			else
				log_message('error', "Failed to update settings - company youtube url!");

			// Update settings - pinterest url
			$data           = array('content' => $company_pinterest);
			$update_results = $this->Content->update_content($data, $company_pinterest_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - company pinterest url.");
			else
				log_message('error', "Failed to update settings - company pinterest url!");

			// Update settings - gated views
			$data           = array('content' => $gated_views);
			$update_results = $this->Content->update_content($data, $gated_views_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - gated views.");
			else
				log_message('error', "Failed to update settings - gated views!");

			// Update settings - site adwords embed script
			$data           = array('content' => $site_adwords);
			$update_results = $this->Content->update_content($data, $site_adwords_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - site adwords embed.");
			else
				log_message('error', "Failed to update settings - site adwords embed!");

			// Update settings - site analystics embed script
			$data           = array('content' => $site_analytics);
			$update_results = $this->Content->update_content($data, $site_analytics_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - site analytics embed.");
			else
				log_message('error', "Failed to update settings - site analytics embed!");

			// Update settings - site facebook embed script
			$data           = array('content' => $site_facebook);
			$update_results = $this->Content->update_content($data, $site_facebook_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - site facebook embed.");
			else
				log_message('error', "Failed to update settings - site facebook embed!");

			// Update settings - site bing embed script
			$data           = array('content' => $site_bing);
			$update_results = $this->Content->update_content($data, $site_bing_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - site bing embed.");
			else
				log_message('error', "Failed to update settings - site bing embed!");

			// Update settings - site misc embed script
			$data           = array('content' => $site_misc);
			$update_results = $this->Content->update_content($data, $site_misc_id);

			if($update_results)
				log_message('debug', "Successfully updated settings - site misc embed.");
			else
				log_message('error', "Failed to update settings - site misc embed!");
		}

		$settings               = $this->Content->get_content("settings");
		$company_logo_id        = (!empty($settings['company']['logo'])) ? $settings['company']['logo'] : 4;
		$company_logo           = $this->Content->get_picture($company_logo_id);
		$company_logo_footer_id = (!empty($settings['company']['logo_footer'])) ? $settings['company']['logo_footer'] : 4;
		$company_logo_footer    = $this->Content->get_picture($company_logo_footer_id);
		$company_lead_default   = (!empty($settings['company']['lead_default'])) ? $settings['company']['lead_default'] : 0;

		$company_users = array();
		$company_user  = array();
		$agentsHtml    = "<option value=''>None</option>";

		if($role == "master")
		{
			$company_users = $this->Accounts->get_users_all();
		}
		else if($role == "company owner")
			$company_users = $this->Accounts->get_users_by_company($company_id);
		else if($role == "office manager")
			$company_users = $this->Accounts->get_users_by_office($office_id);
		else if($role == "team leader")
			$company_users = $this->Accounts->get_users_by_team($team_id);
		else
			$company_user = $this->Accounts->get_user_by_id($user_id);

		if(!empty($company_users))
		{
			foreach($company_users as $c_user)
			{
				if($c_user['role'] != 'master' && $c_user['role'] != 'admin')
				{
					$agent_name = $c_user['first_name'] . " " . $c_user['last_name'];

					if($company_lead_default == $c_user['user_id'])
					{
						$agentsHtml .= "<option value='{$c_user['user_id']}' selected>{$agent_name}</option>\n";
					}
					else
					{
						$agentsHtml .= "<option value='{$c_user['user_id']}'>{$agent_name}</option>\n";
					}
				}
			}
		}
		else if(!empty($company_user))
		{
			if($company_user['role'] != 'master' && $company_user['role'] != 'admin')
			{
				$agent_name = $company_user['first_name'] . " " . $company_user['last_name'];
				$agentsHtml .= "<option value='' selected>{$agent_name}</option>\n";
			}
		}

		$data = array(
			'company_name'         => $settings['company']['name'],
			'site_name'            => $settings['site']['name'],
			'company_street'       => $settings['company']['street'],
			'company_city'         => $settings['company']['city'],
			'company_state'        => $settings['company']['state'],
			'company_zipcode'      => $settings['company']['zipcode'],
			'company_phone'        => $settings['company']['phone'],
			'company_email'        => $settings['company']['email'],
			'company_lead_email'   => $settings['company']['lead_email'],
			'company_agents'       => $agentsHtml,
			'company_logo'         => $company_logo['thumb'],
			'company_logo_footer'  => $company_logo_footer['thumb'],
			'company_facebook'     => $settings['company']['facebook_url'],
			'company_twitter'      => $settings['company']['twitter_url'],
			'company_google'       => $settings['company']['google_url'],
			'company_linkedin'     => $settings['company']['linkedin_url'],
			'company_youtube'      => $settings['company']['youtube_url'],
			'company_pinterest'    => $settings['company']['pinterest_url'],
			'gated_views'          => $settings['site']['gated_views'],
			'site_adwords'         => $settings['site']['adwords_embed'],
			'site_analytics'       => $settings['site']['analytics_embed'],
			'site_facebook'        => $settings['site']['facebook_embed'],
			'site_bing'            => $settings['site']['bing_embed'],
			'site_misc'            => $settings['site']['misc_embed'],
			'readonly'             => $readonly,
			'settings_save_button' => $settings_save_button
		);

		// Set settings as the active navbar link
		$nav_active['settings'] = 'active';

		$data_nav = array(
			'active' => $nav_active
		);

		$data_header = array(
			'title'         => 'Lonnie Bush | Admin - Settings',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-settings',
			'user_name'     => $access_name
		);

		$data_footer = array(
			'placeholder' => ''
		);

		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/settings", $data);
		$this->load->view("admin/footer", $data_footer);
	}
}

/* End of file settings.php */
/* Location: ./application/controllers/admin/settings.php */
