<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pages extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('upload');
		$this->load->library('image_lib');
		$this->load->helper('file');
	}

	/**
	 * Index Page for this controller.
	 *
	 * We will be assuming in this function that all the content we need to pull exists
	 * in the database so we can skip the empty() checks
	 */
	public function index()
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_access('admin-pages');
		
		if(!$access_admin)
			header("Location: /admin/login/");
		
		// Grab the current user's name since we know they are online
		$access_name = $this->Accounts->get_user_name_online();
		$access_name = ($access_name) ? $access_name : "User";
		
		$data = array();
		
		// If the user can view this, but only do that, we'll hide our add/edit/delete buttons
		// and set any visiable form fields to readonly
		$access_update = $this->Accounts->can_user_update('admin-pages');
		$access_create = $this->Accounts->can_user_create('admin-pages');
		$access_delete = $this->Accounts->can_user_delete('admin-pages');
		
		$data['readonly'] = (!$access_update) ? 'disabled' : '';
		
		
		//------------------------------
		//  PAGES - REFER OWNERS
		//------------------------------
		$content = $this->Content->get_content('refer-owners');
		
		// To make our php calls on our fields we'll convert our content so we can just echo it straight
		$data['refer_owners_title'] = $content['content']['title'];
		$data['refer_owners_text']  = $content['content']['text'];
		
		$data['refer_owners_save_button'] = ($access_update) ? '<button id="pages_refer_owners_save" class="btn green-lilahmedia" type="submit">Save</button>' : '';
		
		//--------------------------------
		//  PAGES - VENDORS FAQ
		//--------------------------------
		$content = $this->Content->get_content('vendors-faq');
		
		// To make our php calls on our fields we'll convert our content so we can just echo it straight
		$data['vendors_faq_title'] = $content['content']['title'];
		$data['vendors_faq_text']  = $content['content']['text'];
		
		$data['vendors_faq_save_button'] = ($access_update) ? '<button id="pages_vendors_faq_save" class="btn btn grey-gallery" type="submit">Save</button>' : '';
		
		//--------------------------------
		//  PAGES - MEET THE TEAM
		//--------------------------------
		$content = $this->Content->get_content('team');
		
		// To make our php calls on our fields we'll convert our content so we can just echo it straight
		$data['team_title'] = $content['content']['title'];
		$data['team_text']  = $content['content']['text'];
		
		$data['team_save_button'] = ($access_update) ? '<button id="pages_team_save" class="btn green-lilahmedia" type="submit">Save</button>' : '';
		

		// Set pages as the active navbar link
		$nav_active['pages'] = 'active';
		
		$data_nav = array(
			'active' => $nav_active
		);
		
		$data_header = array(
			'title'         => 'Lonnie Bush Property Management | Admin - Pages',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'admin-pages',
			'user_name'     => $access_name
		);
		
		$data['pages_refer_owners_hide'] = '';
		$data['pages_vendors_faq_hide']  = '';
		$data['pages_team_hide']         = '';
		
		$data_footer = array(
			'placeholder' => ''
		);
		
		$this->load->view("admin/header", $data_header);
		$this->load->view("admin/navbar", $data_nav);
		$this->load->view("admin/pages", $data);
		$this->load->view("admin/footer", $data_footer);
	}
	
	public function update($section = null)
	{
		// Check to make sure the current user is allowed to be here
		$access_admin = $this->Accounts->can_user_update('admin-pages');
		
		if(!$access_admin)
			header("Location: /admin/pages/");
		
		// This function will primarily be used as an ajax handler to create, update and delete
		// content via the Pages->Home admin section.
		if(!empty($section))
		{
			if($section == "settings")
			{
				$form = $this->input->post('pages_form_submit');
				
				switch($form)
				{
					case "pages_refer_owners":
						// Grab our form variables
						$refer_owners_title = $this->input->post('refer_owners_title');
						$refer_owners_text  = $this->input->post('refer_owners_text');
						
						$pages_content = $this->Content->get_content_full('refer-owners');
						
						// We need content ids to update our info, we can grab them straight from our content array
						$refer_owners_title_id = $pages_content['content']['title']['id'];
						$refer_owners_text_id  = $pages_content['content']['text']['id'];
						
						// Update title
						$data           = array('content' => $refer_owners_title);
						$update_results = $this->Content->update_content($data, $refer_owners_title_id);
						
						if($update_results)
							log_message('debug', "Successfully updated refer owners title.");
						else
							log_message('error', "Failed to update refer owners title!");
						
						// Update text content
						$data           = array('content' => $refer_owners_text);
						$update_results = $this->Content->update_content($data, $refer_owners_text_id);
						
						if($update_results)
							log_message('debug', "Successfully updated refer owners text content.");
						else
							log_message('error', "Failed to update refer owners text content!");
						
						// Everything is updated, send the user back to the admin page
						header("Location: /admin/pages/");
						break;
					case "pages_vendors_faq":
						// Grab our form variables
						$vendors_faq_title = $this->input->post('vendors_faq_title');
						$vendors_faq_text  = $this->input->post('vendors_faq_text');
						
						$pages_content = $this->Content->get_content_full('vendors-faq');
						
						// We need content ids to update our info, we can grab them straight from our home content array
						$vendors_faq_title_id = $pages_content['content']['title']['id'];
						$vendors_faq_text_id  = $pages_content['content']['text']['id'];
						
						// Update title
						$data           = array('content' => $vendors_faq_title);
						$update_results = $this->Content->update_content($data, $vendors_faq_title_id);
						
						if($update_results)
							log_message('debug', "Successfully updated vendors faq title.");
						else
							log_message('error', "Failed to update vendors faq title!");
						
						// Update text content
						$data           = array('content' => $vendors_faq_text);
						$update_results = $this->Content->update_content($data, $vendors_faq_text_id);
						
						if($update_results)
							log_message('debug', "Successfully updated vendors faq text content.");
						else
							log_message('error', "Failed to update vendors faq text content!");
						
						// Everything is updated, send the user back to the admin page
						header("Location: /admin/pages/");
						break;
					case "pages_team":
						// Grab our form variables
						$team_title = $this->input->post('team_title');
						$team_text  = $this->input->post('team_text');
						
						$pages_content = $this->Content->get_content_full('team');
						
						// We need content ids to update our info, we can grab them straight from our home content array
						$team_title_id = $pages_content['content']['title']['id'];
						$team_text_id  = $pages_content['content']['text']['id'];
						
						// Updatetitle
						$data           = array('content' => $team_title);
						$update_results = $this->Content->update_content($data, $team_title_id);
						
						if($update_results)
							log_message('debug', "Successfully updated team title.");
						else
							log_message('error', "Failed to update team title!");
						
						// Updattext content
						$data           = array('content' => $team_text);
						$update_results = $this->Content->update_content($data, $team_text_id);
						
						if($update_results)
							log_message('debug', "Successfully updated team text content.");
						else
							log_message('error', "Failed to update team text content!");
						
						// Everything is updated, send the user back to the admin page
						header("Location: /admin/pages/");
						break;

					default:
						header("Location: /admin/pages/");
						break;
				}
			}
		}
		else
			header("Location: /admin/pages/");
	}
	
	public function add($section = null)
	{
		header("Location: /admin/pages/");
	}
	
	public function delete($section = null)
	{
		header("Location: /admin/pages/");
	}
}

/* End of file pages.php */
/* Location: ./application/controllers/admin/pages.php */
