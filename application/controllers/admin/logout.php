<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// There are no real checks needed here, just call the logout method and we're good
		$result = $this->Accounts->logout();

		header("Location: /admin/login/");
	}
}

/* End of file home.php */
/* Location: ./application/controllers/admin/home.php */
