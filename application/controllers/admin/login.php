<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library("email");
		$this->load->helper("string");

		$config['protocol']  = 'smtp';
		$config['smtp_host'] = 'mail.hamptonroadspropertyowners.com';
		$config['smtp_user'] = 'mail-server@hamptonroadspropertyowners.com';
		$config['smtp_pass'] = 'e^N{lU%0)*4D';
		$config['smtp_crypto'] = 'ssl';
		$config['smtp_port'] = '465';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype']  = 'html';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = 'TRUE';

		$this->email->initialize($config);
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Check user access. The can_user_access method also checks for online status
		$access_admin       = $this->Accounts->can_user_access('admin-dashboard');

		if($access_admin)
			header("Location: /admin/");

		$postback = $this->input->post('form_submit');

		$flash_class = "";
		$flash_text  = "";

		if($postback)
		{
			if($postback == "login")
			{
				$username = $this->input->post('username');
				$password = $this->input->post('password');

				$result = $this->Accounts->login($username, $password, "user");

				if($result)
				{
					$allowed_in       = $this->Accounts->can_user_access('admin-dashboard');

					if($allowed_in)
						header("Location: /admin/");
					else
					{
						$flash_class = 'alert-danger';
						$flash_text  = 'This account does not have access to this area.';
					}
				}
				else
				{
					$flash_class = 'alert-danger';
					$flash_text  = 'Invalid username or password!';
				}
			}
		}
		else
		{
			$flash_class = 'alert-danger display-hide';
			$flash_text  = 'Enter username and password';
		}

		$data['title']         = 'Lonnie Bush | Admin - Login';
		$data['meta_desc']     = '';
		$data['meta_keywords'] = '';
		$data['body_class']    = 'login admin-login';
		$data['flash_class']   = $flash_class;
		$data['flash_text']    = $flash_text;

		$this->load->view("admin/login", $data);
	}

	public function reset()
	{
		// Check our posted data for a username (email address)
		$username = $this->input->post('email');

		if($username)
		{
			$username =
				((preg_match('/^\S+@\S+\.\S+$/', $username) !== false) || ($username == "admin")) ? $username : "invalid";

			if($username == "invalid")
			{
				$flash_class = "alert alert-danger";
				$flash_text  = "Failed! Invalid username or password!";
			}
			else
			{
				$user = $this->Accounts->get_user($username);

				if($user)
				{
					// User exists, create a reset code and datetime 30 minutes from now
					$reset_code    = random_string('alnum', 32);
					$reset_expires = date("Y-m-d h:i:s", strtotime("+30 mins"));

					// Insert our reset data into the user record for the supplied username
					$data           = array(
						'reset_code'    => $reset_code,
						'reset_expires' => $reset_expires
					);
					$update_results = $this->Accounts->update_user($data, $user['user_id']);

					if($update_results)
					{
						// We are now ready to create the reset password email and send it to the
						// user
						$email_body    = "";
						$email_to      = $user['email'];
						$email_from    = $this->config->item('default_from_email_address');
						$email_subject = "Password Reset Request from Lonnie Bush";

						$email_body .= "<!DOCTYPE html>
						<html lang='en-US'>
						<head></head>
						<body>
						<table style=''>
							<tr>
								<td style='text-align: center;'>
									<img src='".base_url()."includes/img/logo-scs-1.png' alt='logo'><br>
									<img src='".base_url()."includes/img/nav-border-1.png' alt='border'>
								</td>
							</tr>
							<tr>
								<td style='padding: 15px 25px;'>
									Dear {$user['first_name']},<br><br>
									A reset password request was submitted on Lonnie Bush. To reset your password,
									please click on the link below and follow the instructions. This link will only be active for
									the next 30 minutes.
								</td>
							</tr>
							<tr>
								<td style='padding: 15px 25px;'>
									<a href='".base_url()."reset/?rc={$reset_code}' alt='Reset Password'>
										".base_url()."reset/?rc={$reset_code}
									</a>
								</td>
							</tr>
							<tr>
								<td style='padding: 0px 25px;'>
									Respectfully,<br><br> Lonnie Bush Team<br><br>
								</td>
							</tr>
							<tr>
								<td style='text-align: center;'>
									<img src='".base_url()."includes/img/nav-border-1.png' alt='border'><br>
									<p style='font-size: 12px;'>If you did not initiate this password reset, you do not need to
										do anything else at this time.</p>
									<img src='".base_url()."includes/img/contact-scs-1.jpg' alt='contact info'>
								</td>
							</tr>
						</table>
						</body>
						</html>\n";

						$this->email->from($email_from, "Lonnie Bush");
						$this->email->to($email_to);
						$this->email->bcc($email_bcc, "Lonnie Bush Admin");

						$this->email->subject($email_subject);
						$this->email->message($email_body);

						$email_results = $this->email->send();

						if($email_results)
						{
							// Successfully sent password reset email
							log_message('error', "Sent password reset email to $email_to.");

							$flash_class = "alert alert-success";
							$flash_text  = "Success! An email with instructions to reset
							your password has been sent to your registered email address.";
						}
						else
						{
							// Failed to send password reset email
							log_message('error', "Failed to send password reset email to $email_to!");

							$flash_class = "alert alert-danger";
							$flash_text  = "Failed! Password reset is not available at this time.";
						}
					}
					else
					{
						$flash_class = "alert alert-danger";
						$flash_text  = "Failed! Password reset is not available at this time.";
					}
				}
				else
				{
					$flash_class = "alert alert-danger";
					$flash_text  = "Failed! User does not exist in the system.";
				}
			}
		}
		else
		{
			$flash_class = "alert alert-danger";
			$flash_text  = "A valid user email address is required.";
		}

		$data = array(
			'title'         => 'Lonnie Bush | Admin - Login',
			'meta_desc'     => '',
			'meta_keywords' => '',
			'body_class'    => 'login admin-login',
			'flash_class'   => $flash_class,
			'flash_text'    => $flash_text
		);

		$this->load->view("admin/login", $data);
	}




	public function mdpass()
	{
		echo md5('password');
		exit;
	}
}

/* End of file home.php */
/* Location: ./application/controllers/admin/home.php */
