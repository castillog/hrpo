<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Privacy extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Set about as the active navbar link
		$nav_active['home'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Privacy Policy | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-privacy',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('privacy', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file privacy.php */
/* Location: ./application/controllers/privacy.php */
