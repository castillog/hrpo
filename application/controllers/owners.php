<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Owners extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab our company settings content data
		$content = $this->Content->get_content("settings");
		$company_lead_email    = $content['company']['lead_email'];

		$flash_class = "display-hide";
		$flash_msg = "";
		$footer_script = "";

		// Set owners as the active navbar link
		$nav_active['owners'] = 'active';

		$data_header = array(
			'title'         => 'Owners | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-owners',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('owners', $data);
		$this->load->view('footer', $data_footer);
	}

	public function guaranteed()
	{
		// Grab our company settings content data
		$content = $this->Content->get_content("settings");
		$company_lead_email    = $content['company']['lead_email'];

		$flash_class = "display-hide";
		$flash_msg = "";
		$footer_script = "";

		// Set owners as the active navbar link
		$nav_active['owners'] = 'active';

		$data_header = array(
			'title'         => 'Guaranteed Rent Program | Lonnie Bush Property Management',
			'description'   => 'Hampton Roads Property Management with Lonnie Bush Property Management. Area leads in professional property management services',
			'keywords'      => 'Guaranteed Rent Program, Hampton Roads Rentals, Property Management, Hampton Roads',
			'bodyClass'     => 'page-owners-guaranteed',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('owners-guaranteed', $data);
		$this->load->view('footer', $data_footer);
	}

	public function faq()
	{
		// Grab our company settings content data
		$content = $this->Content->get_content("settings");
		$company_lead_email    = $content['company']['lead_email'];

		$flash_class = "display-hide";
		$flash_msg = "";
		$footer_script = "";

		// Set owners as the active navbar link
		$nav_active['owners'] = 'active';

		$data_header = array(
			'title'         => 'Owner FAQ | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-owners-faq',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('owners-faq', $data);
		$this->load->view('footer', $data_footer);
	}

	public function login()
	{
		// Set owners as the active navbar link
		$nav_active['owners'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Owner Login | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-owners-login',
			'nav_active'    => $nav_active
		);

		$data        = array();

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('owners-login', $data);
		$this->load->view('footer', $data_footer);
	}

	public function rentready()
	{
		// Grab our company settings content data
		$content = $this->Content->get_content("settings");
		$company_lead_email    = $content['company']['lead_email'];

		$flash_class = "display-hide";
		$flash_msg = "";
		$footer_script = "";

		// Set owners as the active navbar link
		$nav_active['owners'] = 'active';

		$data_header = array(
			'title'         => 'Rent Ready | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-owners-rentready',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('owners-rentready', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file owners.php */
/* Location: ./application/controllers/owners.php */
