<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Tenants extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
		$this->load->library('email');

		$this->email->initialize();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Don't need this route for the moment
		header("Location: /404/");

		// Set tenants as the active navbar link
		$nav_active['tenants'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'       => 'Tenants | Lonnie Bush',
			'description' => '',
			'keywords'    => '',
			'bodyClass'   => 'page-tenants',
			'nav_active'  => $nav_active
		);

		$data = array(
			'flash_class' => '',
			'flash_msg' => ''
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('tenants', $data);
		$this->load->view('footer', $data_footer);
	}

	public function faq()
	{
		// Set tenants as the active navbar link
		$nav_active['tenants'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'       => 'Tenant FAQ | Lonnie Bush',
			'description' => '',
			'keywords'    => '',
			'bodyClass'   => 'page-tenants-faq',
			'nav_active'  => $nav_active
		);

		$data = array(
			'flash_class' => '',
		   'flash_msg' => ''
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('tenants-faq', $data);
		$this->load->view('footer', $data_footer);
	}

	public function maintenance()
	{
		// Set tenants as the active navbar link
		$nav_active['tenants'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'       => 'Maintenance | Lonnie Bush',
			'description' => '',
			'keywords'    => '',
			'bodyClass'   => 'page-tenants-maintenance',
			'nav_active'  => $nav_active
		);

		$data = array(
			'flash_class' => '',
			'flash_msg' => ''
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('tenants-maintenance', $data);
		$this->load->view('footer', $data_footer);
	}

	public function portal()
	{
		$flash_class = "";
		$flash_msg   = "";
		$footer_script = "";

		$postback = $this->input->post("form-submit");

		if($postback)
		{
			// Grab our company settings content data
			$content            = $this->Content->get_content("settings");
			$company_lead_email = $content['company']['lead_email'];

			$lead_email = (!empty($company_lead_email)) ? $company_lead_email : $this->config->item('default_email_address');

			// Grab all of our posted fields
			$firstname = $this->input->post('first-name');
			$firstname = (!empty($firstname)) ? $firstname : "invalid";

			$lastname = $this->input->post('last-name');
			$lastname = (!empty($lastname)) ? $lastname : "invalid";

			$email = $this->input->post('email');
			$email = (!empty($email) && preg_match('/^\S+@\S+\.\S+$/', $email) !== false) ? $email : "invalid";

			//log_message('error', "Name: " .$name . ", Email: " . $email . ", Phone: " . $phone . ", Message: " . $message);

			if($firstname == "invalid")
			{
				$flash_class = "alert alert-danger";
				$flash_msg   = "Failed! You must enter a valid first name.";
			}

			if($lastname == "invalid")
			{
				$flash_class = "alert alert-danger";
				$flash_msg   = "Failed! You must enter a valid last name.";
			}

			if($email == "invalid")
			{
				$flash_class = "alert alert-danger";
				$flash_msg   = "Failed! You must provide a valid email address.";
			}

			$email_body = "";
			$email_to   = $lead_email;
			$email_from    = $this->config->item('default_from_email_address');
			$email_subject = "Activation Link Request from Lonnie Bush";

			$email_body .= "A visitor to Lonnie Bush has requested an activiation link:<br><br>
				First Name: {$firstname}<br>
				Last Name: {$lastname}<br>
				Email: {$email}<br>
				- Lonnie Bush Property Management";

			$this->email->from($email_from, "Lonnie Bush");
			$this->email->to($email_to);

			$this->email->subject($email_subject);
			$this->email->message($email_body);

			$email_results = $this->email->send();

			if($email_results)
			{
				$flash_class = "alert alert-success";
				$flash_msg   = "Success! Your request has been submitted.";

				$footer_script .=
					"<!-- Google Code for lead Conversion Page -->
					<script type='text/javascript'>
					/* <![CDATA[ */
					var google_conversion_id = 976918088;
					var google_conversion_language = 'en';
					var google_conversion_format = '3';
					var google_conversion_color = 'ffffff';
					var google_conversion_label = 'pZkOCIiyhwgQyKzq0QM';
					var google_conversion_value = 0;
					var google_remarketing_only = false;
					/* ]]> */
					</script>
					<script type='text/javascript' src='//www.googleadservices.com/pagead/conversion.js'>
					</script>
					<noscript>
					<div style='display:inline;'>
					<img height='1' width='1' style='border-style:none;' alt='' src='//www.googleadservices.com/pagead/conversion/976918088/?value=0&amp;label=pZkOCIiyhwgQyKzq0QM&amp;guid=ON&amp;script=0'/>
					</div>
					</noscript>
					\n";
			}
			else
			{
				$flash_class = "alert alert-danger";
				$flash_msg   = "Failed! Unable to submit your request at this time.";
			}
		}

		// Set tenants as the active navbar link
		$nav_active['tenants'] = 'active';

		$data_header = array(
			'title'       => 'Tenant Portal | Lonnie Bush',
			'description' => '',
			'keywords'    => '',
			'bodyClass'   => 'page-tenants-portal',
			'nav_active'  => $nav_active
		);

		$data = array(
			'flash_class' => $flash_class,
			'flash_msg'   => $flash_msg
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('tenants-portal', $data);
		$this->load->view('footer', $data_footer);
	}

	public function login()
	{
		// Set tenants as the active navbar link
		$nav_active['tenants'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'       => 'Tenant Login | Lonnie Bush',
			'description' => '',
			'keywords'    => '',
			'bodyClass'   => 'page-tenants-login',
			'nav_active'  => $nav_active
		);

		$data = array(
			'flash_class' => '',
			'flash_msg' => ''
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('tenants-login', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file tenants.php */
/* Location: ./application/controllers/tenants.php */
