<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class About extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab our about page content data
		$content = $this->Content->get_content("about");

		$title = $content['content']['title'];
		$byline = $content['content']['byline'];
		$text = $content['content']['text'];

		// Set about as the active navbar link
		$nav_active['about'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'About Us | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-about',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'title' => $title,
			'byline' => $byline,
		   'text' => $text
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('about', $data);
		$this->load->view('footer', $data_footer);
	}
	
	public function covid_19()
	{
		// Grab our about page content data
		$content = $this->Content->get_content("about");
		
		$title = $content['content']['title'];
		$byline = $content['content']['byline'];
		$text = $content['content']['text'];
		
		// Set about as the active navbar link
		$nav_active['about'] = 'active';
		
		$footer_script = "";
		
		$data_header = array(
			'title'         => 'COVID-19 Resources | Lonnie Bush',
			'description'   => 'The COVID-19 crisis requires updates to Virginia property management protocols. Click here to stay up-to-date with Lonnie Bush Property Management.',
			'keywords'      => '',
			'bodyClass'     => 'page-covid',
			'nav_active'    => $nav_active
		);
		
		$data        = array(
			'title' => $title,
			'byline' => $byline,
			'text' => $text
		);
		
		$data_footer = array(
			'footer_script' => $footer_script
		);
		
		$this->load->view('header', $data_header);
		$this->load->view('covid-19', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file about.php */
/* Location: ./application/controllers/about.php */
