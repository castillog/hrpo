<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Agents extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		header('Location: /agents/refer/');
	}
	
	public function refer()
	{
		// Grab our about page content data
		$content = $this->Content->get_content("refer-owners");
		
		$title = $content['content']['title'];
		$byline = $content['content']['byline'];
		$text = $content['content']['text'];
		
		// Set about as the active navbar link
		$nav_active['agents'] = 'active';
		
		$footer_script = "";
		
		$data_header = array(
			'title'         => 'Refer Owners | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-agents',
			'nav_active'    => $nav_active
		);
		
		$data        = array(
			'title' => $title,
			'byline' => $byline,
			'text' => $text
		);
		
		$data_footer = array(
			'footer_script' => $footer_script
		);
		
		$this->load->view('header', $data_header);
		$this->load->view('agents-refer', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file agents.php */
/* Location: ./application/controllers/agents.php */
