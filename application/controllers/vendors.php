<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Vendors extends CI_Controller
{
	/**
	 * Constructor for this controller
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Index Page for this controller.
	 */
	public function index()
	{
		// Grab a list of our current preferred vendors
		$vendors = $this->Content->get_vendors_all();

		$vendorHtml = "";

		if(!empty($vendors))
		{
			for($i = 0; $i < count($vendors); $i++)
			{
				$vendor = $vendors[$i];

				$vendorHtml .=
					"<div class='col-md-6 block'>
						<div class='vendor-box'>
							<div class='vendor-logo'><img src='{$vendor['logo']}'></div>
							<div class='vendor-name'>{$vendor['name']}</div>
							<div class='vendor-contact'>{$vendor['contact']}</div>
							<div class='vendor-phone'>{$vendor['phone']}</div>
							<div class='vendor-email'><a href='{$vendor['email']}'>{$vendor['email']}</a></div>
							<div class='vendor-website'><a href='{$vendor['website']}'>{$vendor['website']}</a></div>
						</div>
					</div>";
			}
		}

		// Set about as the active navbar link
		$nav_active['vendors'] = 'active';

		$footer_script = "";

		$data_header = array(
			'title'         => 'Preferred Vendors | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-vendors',
			'nav_active'    => $nav_active
		);

		$data        = array(
			'vendorHtml' => $vendorHtml
		);

		$data_footer = array(
			'footer_script' => $footer_script
		);

		$this->load->view('header', $data_header);
		$this->load->view('vendors', $data);
		$this->load->view('footer', $data_footer);
	}
	
	public function faq()
	{
		// Grab our about page content data
		$content = $this->Content->get_content("vendors-faq");
		
		$title = $content['content']['title'];
		$byline = $content['content']['byline'];
		$text = $content['content']['text'];
		
		// Set about as the active navbar link
		$nav_active['about'] = 'active';
		
		$footer_script = "";
		
		$data_header = array(
			'title'         => 'About Us | Lonnie Bush',
			'description'   => '',
			'keywords'      => '',
			'bodyClass'     => 'page-vendors',
			'nav_active'    => $nav_active
		);
		
		$data        = array(
			'title' => $title,
			'byline' => $byline,
			'text' => $text
		);
		
		$data_footer = array(
			'footer_script' => $footer_script
		);
		
		$this->load->view('header', $data_header);
		$this->load->view('vendors-faq', $data);
		$this->load->view('footer', $data_footer);
	}
}

/* End of file Vendors.php */
/* Location: ./application/controllers/Vendors.php */
