<?php if(!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 * Class Accounts
 *
 * Manages user accounts, access rights, organizations, leads, and activity logs
 */
class Accounts extends CI_Model
{
	/**
	 * Constructor
	 */
	function __construct()
	{
		// Load parent's constructor
		parent::__construct();
	}

	//---------------------
	//  GET USERS
	//---------------------
	/**
	 * Get a user
	 *
	 * @param string $username
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_user($username)
	{
		// Make sure we have a username and it only contains valid characters
		if(!empty($username) && preg_match('/[\w-@\.]+/', $username))
		{
			$criteria = array(
				'username' => $username
			);

			$result = $this->db_fetch('users', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get a user
	 *
	 * @param int|string $user_id
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_user_by_id($user_id)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($user_id) && preg_match('/\d+/', $user_id))
		{
			$criteria = array(
				'user_id' => $user_id
			);
			$result   = $this->db_fetch('users', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get a user by reset code
	 *
	 * @param string $reset_code
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_user_by_reset($reset_code)
	{
		// Make sure we have a username and it only contains valid characters
		if(!empty($reset_code) && preg_match('/[\w\.]+/', $reset_code))
		{
			$criteria = array(
				'reset_code' => $reset_code
			);

			$result = $this->db_fetch('users', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get users by team
	 *
	 * @param int|string $team_id
	 *
	 * @return mixed|bool Users data or false on search failure
	 */
	public function get_users_by_team($team_id)
	{
		// Make sure we have a team_id and it only contains valid characters
		if(!empty($team_id) && preg_match('/\d+/', $team_id))
		{
			$criteria = array(
				'team_id' => $team_id
			);
			$result   = $this->db_fetch('users', $criteria, "first_name ASC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get users by office
	 *
	 * @param int|string $office_id
	 *
	 * @return mixed|bool Users data or false on search failure
	 */
	public function get_users_by_office($office_id)
	{
		// Make sure we have a office_id and it only contains valid characters
		if(!empty($office_id) && preg_match('/\d+/', $office_id))
		{
			$criteria = array(
				'office_id' => $office_id
			);
			$result   = $this->db_fetch('users', $criteria, "first_name ASC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get users by company
	 *
	 * @param int|string $company_id
	 *
	 * @return mixed|bool Users data or false on search failure
	 */
	public function get_users_by_company($company_id)
	{
		// Make sure we have a company_id and it only contains valid characters
		if(!empty($company_id) && preg_match('/\d+/', $company_id))
		{
			$criteria = array(
				'company_id' => $company_id
			);
			$result   = $this->db_fetch('users', $criteria, "first_name ASC");

			return $result;
		}
		else
			return false;
	}
	
	/**
	 * Get users data
	 *
	 * @param mixed $criteria  Search criteria pre-formatted into an Active Record array
	 * @param string $sort Sort field and direction
	 * @param int|string $limit Limit of results returned
	 * @param int|string offset Record offset for results returned
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_users($criteria, $sort = null, $limit = null, $offset = null)
	{
		// Make sure we have criteria
		if(!empty($criteria))
		{
			$result = $this->db_fetch('users', $criteria, $sort, $limit, $offset);
			
			if(!empty($result))
			{
				return $result;
			}
			else
				return false;
		}
		else
			return false;
	}
	
	/**
	 * Get all users
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_users_all()
	{
		$criteria = array();
		$result   = $this->db_fetch('users', $criteria, "first_name ASC");

		return $result;
	}

	//---------------------
	//  USER ACCESS
	//---------------------
	/**
	 * Get user online
	 *
	 * @return int user id if logged in, false if not
	 */
	public function get_user_online()
	{
		// Grab the user status session variable
		$userStatus = $this->session->userdata('status');

		if(!empty($userStatus) && $userStatus == "online")
		{
			$user_id = $this->session->userdata('user');

			if(!empty($user_id))
				return $user_id;
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get last user online
	 *
	 * @return int last user id if logged in, false if not
	 */
	public function get_last_user_online()
	{
		$last_user_id = $this->session->userdata('last_user');

		if(!empty($last_user_id))
			return $last_user_id;
		else
			return false;
	}

	/**
	 * Get last user type online
	 *
	 * @return string lead|user, false if not set
	 */
	public function get_last_user_type_online()
	{
		$last_user_type = $this->session->userdata('last_user_type');

		if(!empty($last_user_type))
			return $last_user_type;
		else
			return false;
	}

	/**
	 * Get user's first and last name online
	 *
	 * @return string first and last name if logged in, false if not
	 */
	public function get_user_name_online()
	{
		// Grab the user status session variable
		$userStatus = $this->session->userdata('status');

		if(!empty($userStatus) && $userStatus == "online")
		{
			$user_id   = $this->session->userdata('user');
			$user_type = $this->session->userdata('type');

			if(!empty($user_id))
			{
				if($user_type == 'lead')
					$user = $this->get_lead_by_id($user_id);
				else
					$user = $this->get_user_by_id($user_id);

				if($user)
					return $user['first_name'] . " " . $user['last_name'];
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get user type online
	 *
	 * @return string lead|user, false if not set
	 */
	public function get_user_type_online()
	{
		// Grab the user status session variable
		$userStatus = $this->session->userdata('status');

		if(!empty($userStatus) && $userStatus == "online")
		{
			$user_type = $this->session->userdata('type');

			if(!empty($user_type))
				return $user_type;
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Is user online
	 *
	 * @return bool true if logged in, false if not
	 */
	public function is_user_online()
	{
		// Grab the user status session variable
		$userStatus = $this->session->userdata('status');

		if(!empty($userStatus) && $userStatus == "online")
			return true;
		else
			return false;
	}

	/**
	 * Can user access page
	 *
	 * @param string $page
	 *
	 * @return bool true if has access rights, false if not
	 */
	public function can_user_access($page)
	{
		// Grab our currently logged in user id
		$user_id   = $this->get_user_online();
		$user_type = $this->get_user_type_online();

		if(!empty($user_id))
		{
			$access = $this->get_access_rights($user_id, $user_type);

			if(!empty($access))
			{
				// Now check the current user's access rights and compare against the provided page
				foreach($access as $right)
				{
					if(($right['page'] == $page || $right['page'] == 'all') && $right['enabled'] && $right['read'])
						return true;
				}

				return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Can user access microsite
	 *
	 * @param int|string $site_id
	 *
	 * @return bool true if has access rights, false if not
	 */
	public function can_user_access_site($site_id)
	{
		$site = array();

		// Make sure we have a site_id and it only contains valid characters
		if(!empty($site_id) && preg_match('/\d+/', $site_id))
		{
			$criteria = array(
				'site_id' => $site_id
			);

			$site = $this->db_fetch('sites', $criteria);
			$site = ($site) ? current($site) : array();
		}
		else
			return false;

		// Grab the assinged microsite company id
		$site_company_id = (!empty($site)) ? $site['company_id'] : 0;

		// Grab our currently logged in user id
		$user_id    = $this->get_user_online();
		$user       = $this->get_user_by_id($user_id);
		$company_id = $user['company_id'];

		if(!empty($site_company_id))
		{
			if(($site_company_id == $company_id) || ($user['role'] == "master"))
				return true;
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Can user create on page
	 *
	 * @param string $page
	 *
	 * @return bool true if has access rights, false if not
	 */
	public function can_user_create($page)
	{
		// Grab our currently logged in user id
		$user_id   = $this->get_user_online();
		$user_type = $this->get_user_type_online();

		if(!empty($user_id))
		{
			$access = $this->get_access_rights($user_id, $user_type);

			if(!empty($access))
			{
				// Now check the current user's access rights and compare against the provided page
				foreach($access as $right)
				{
					if(($right['page'] == $page || $right['page'] == 'all') && $right['enabled'] && $right['create'])
						return true;
				}

				return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Can user update on page
	 *
	 * @param string $page
	 *
	 * @return bool true if has access rights, false if not
	 */
	public function can_user_update($page)
	{
		// Grab our currently logged in user id
		$user_id   = $this->get_user_online();
		$user_type = $this->get_user_type_online();

		if(!empty($user_id))
		{
			$access = $this->get_access_rights($user_id, $user_type);

			if(!empty($access))
			{
				// Now check the current user's access rights and compare against the provided page
				foreach($access as $right)
				{
					if(($right['page'] == $page || $right['page'] == 'all') && $right['enabled'] && $right['update'])
						return true;
				}

				return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Can user delete on page
	 *
	 * @param string $page
	 *
	 * @return bool true if has access rights, false if not
	 */
	public function can_user_delete($page)
	{
		// Grab our currently logged in user id
		$user_id   = $this->get_user_online();
		$user_type = $this->get_user_type_online();

		if(!empty($user_id))
		{
			$access = $this->get_access_rights($user_id, $user_type);

			if(!empty($access))
			{
				// Now check the current user's access rights and compare against the provided page
				foreach($access as $right)
				{
					if(($right['page'] == $page || $right['page'] == 'all') && $right['enabled'] && $right['delete'])
						return true;
				}

				return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Has visited page
	 *
	 * @param string $page
	 *
	 * @return bool true if visited once before, false if not
	 */
	public function has_visited($page)
	{
		// Grab the user status session variable
		$visitPage = 'visit_' . $page;
		$visited   = $this->session->userdata($visitPage);
		$visited   = ($visited == "yes") ? true : false;

		// Setting the page as visited below will trigger any page refresh or revisit
		// to run or not run the code that follows it
		$this->session->set_userdata(array($visitPage => "yes"));

		return $visited;
	}

	/**
	 * Login user
	 *
	 * @param string $username
	 * @param string $password
	 * @param string $type
	 *
	 * @return true if user successfully logs in, false if not
	 */
	public function login($username, $password, $type = "user")
	{
		if(!empty($username) && !empty($password))
		{
			$user = $this->get_user($username);

			if(!empty($lead))
			{
				$password = md5($password);
				$valid    = ($password == $lead['password']);

				if($valid)
				{
					$user_data = array(
						'user'   => $lead['lead_id'],
						'type'   => 'lead',
						'status' => 'online'
					);
					$this->session->set_userdata($user_data);

					return true;
				}
				else
					return false;
			}
			else if(!empty($lender))
			{
				$password = md5($password);
				$valid    = ($password == $lender['password']);

				if($valid)
				{
					$user_data = array(
						'user'   => $lender['lender_id'],
						'type'   => 'lender',
						'status' => 'online'
					);
					$this->session->set_userdata($user_data);

					return true;
				}
				else
					return false;
			}
			else if(!empty($user))
			{
				$password = md5($password);
				$valid    = ($password == $user['password']);

				if($valid)
				{
					$user_data = array(
						'user'   => $user['user_id'],
						'type'   => 'user',
						'status' => 'online'
					);
					$this->session->set_userdata($user_data);

					return true;
				}
				else
					return false;
			}
		}
		else
			return false;
	}

	/**
	 * Logoff user
	 *
	 * @return true if user successfully logs off, false if not
	 */
	public function logout()
	{
		// Check to see if the user is logged in
		$userStatus = $this->session->userdata('status');

		if(!empty($userStatus) && $userStatus == "online")
		{
			// The user is logged in. Log them out by unsetting our session data
			$userData = array(
				'user'   => '',
				'type'   => '',
				'status' => ''
			);

			$this->session->unset_userdata($userData);

			return true;
		}
		else
		{
			// The user isn't logged in, nothing to do here
			return true;
		}
	}

	//---------------------
	//  GET ORGANIZATION
	//---------------------
	/**
	 * Get a company's organization
	 *
	 * @param int|string $company_id
	 *
	 * @return mixed|bool Organization data or false on search failure
	 */
	public function get_organization($company_id)
	{
		$result['companies'] = array();
		$result['offices']   = array();
		$result['teams']     = array();
		$result['users']     = array();

		// Make sure we have a company_id and it only contains valid characters
		if(!empty($company_id) && preg_match('/\d+/', $company_id))
		{
			$criteria = array(
				'company_id' => $company_id
			);

			$result['companies'] = $this->db_fetch('companies', $criteria);
			$result['offices']   = $this->db_fetch('offices', $criteria);
			$result['teams']     = $this->db_fetch('teams', $criteria);
			$result['users']     = $this->db_fetch('users', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get an office's organization
	 *
	 * @param int|string $office_id
	 *
	 * @return mixed|bool Organization data or false on search failure
	 */
	public function get_organization_by_office($office_id)
	{
		$result['companies'] = array();
		$result['offices']   = array();
		$result['teams']     = array();
		$result['users']     = array();

		// Make sure we have a user_id and it only contains valid characters
		if(!empty($office_id) && preg_match('/\d+/', $office_id))
		{
			$criteria = array(
				'office_id' => $office_id
			);
			$users    = $this->db_fetch('users', $criteria);
			$teams    = $this->db_fetch('teams', $criteria);
			$offices  = $this->db_fetch('offices', $criteria);

			if(!empty($users))
			{
				$result['users'] = $users;

				$current = current($users);

				$criteria  = array(
					'company_id' => $current['company_id']
				);
				$companies = $this->db_fetch('companies', $criteria);

				if(!empty($companies))
					$result['companies'] = $companies;
			}

			if(!empty($teams))
				$result['teams'] = $teams;

			if(!empty($offices))
				$result['offices'] = $offices;

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a team's organization
	 *
	 * @param int|string $team_id
	 *
	 * @return mixed|bool Organization data or false on search failure
	 */
	public function get_organization_by_team($team_id)
	{
		$result['companies'] = array();
		$result['offices']   = array();
		$result['teams']     = array();
		$result['users']     = array();

		// Make sure we have a user_id and it only contains valid characters
		if(!empty($team_id) && preg_match('/\d+/', $team_id))
		{
			$criteria = array(
				'team_id' => $team_id
			);
			$users    = $this->db_fetch('users', $criteria);
			$teams    = $this->db_fetch('teams', $criteria);

			if(!empty($users))
			{
				$result['users'] = $users;

				$current = current($users);

				$criteria = array(
					'office_id' => $current['office_id']
				);
				$offices  = $this->db_fetch('offices', $criteria);

				if(!empty($offices))
					$result['offices'] = $offices;

				$criteria  = array(
					'company_id' => $current['company_id']
				);
				$companies = $this->db_fetch('companies', $criteria);

				if(!empty($companies))
					$result['companies'] = $companies;
			}

			if(!empty($teams))
				$result['teams'] = $teams;

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a user's organization
	 *
	 * @param int|string $user_id
	 *
	 * @return mixed|bool Organization data or false on search failure
	 */
	public function get_organization_by_user($user_id)
	{
		$result['companies'] = array();
		$result['offices']   = array();
		$result['teams']     = array();
		$result['users']     = array();

		// Make sure we have a user_id and it only contains valid characters
		if(!empty($user_id) && preg_match('/\d+/', $user_id))
		{
			$criteria = array(
				'user_id' => $user_id
			);
			$users    = $this->db_fetch('users', $criteria);

			if(!empty($users))
			{
				$current    = current($users);
				$team_id    = $current['team_id'];
				$office_id  = $current['office_id'];
				$company_id = $current['company_id'];

				$result['users'] = $users;

				$criteria = array(
					'team_id' => $team_id
				);
				$teams    = $this->db_fetch('teams', $criteria);

				if(!empty($teams))
					$result['teams'] = $teams;

				$criteria = array(
					'office_id' => $office_id
				);
				$offices  = $this->db_fetch('offices', $criteria);

				if(!empty($offices))
					$result['offices'] = $offices;

				$criteria  = array(
					'company_id' => $company_id
				);
				$companies = $this->db_fetch('companies', $criteria);

				if(!empty($companies))
					$result['companies'] = $companies;

				return $result;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get all company organizations
	 *
	 * @return mixed|bool Organization data or false on search failure
	 */
	public function get_organization_all()
	{
		$result['companies'] = array();
		$result['offices']   = array();
		$result['teams']     = array();
		$result['users']     = array();

		$criteria            = array();
		$result['companies'] = $this->db_fetch('companies', $criteria);
		$result['offices']   = $this->db_fetch('offices', $criteria);
		$result['teams']     = $this->db_fetch('teams', $criteria);
		$result['users']     = $this->db_fetch('users', $criteria);

		return $result;
	}

	/**
	 * Get team
	 *
	 * @param int|string $team_id
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_team($team_id)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($team_id) && preg_match('/\d+/', $team_id))
		{
			$criteria = array(
				'team_id' => $team_id
			);
			$result   = $this->db_fetch('teams', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get all teams
	 *
	 * @return mixed|bool Team data or false on search failure
	 */
	public function get_teams_all()
	{
		$criteria = array();
		$result   = $this->db_fetch('teams', $criteria);

		return $result;
	}

	/**
	 * Get office
	 *
	 * @param int|string $office_id
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_office($office_id)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($office_id) && preg_match('/\d+/', $office_id))
		{
			$criteria = array(
				'office_id' => $office_id
			);
			$result   = $this->db_fetch('offices', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get all offices
	 *
	 * @return mixed|bool Team data or false on search failure
	 */
	public function get_offices_all()
	{
		$criteria = array();
		$result   = $this->db_fetch('offices', $criteria);

		return $result;
	}

	/**
	 * Get company
	 *
	 * @param int|string $company_id
	 *
	 * @return mixed|bool User data or false on search failure
	 */
	public function get_company($company_id)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($company_id) && preg_match('/\d+/', $company_id))
		{
			$criteria = array(
				'company_id' => $company_id
			);
			$result   = $this->db_fetch('companies', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get all companies
	 *
	 * @return mixed|bool Team data or false on search failure
	 */
	public function get_companies_all()
	{
		$criteria = array();
		$result   = $this->db_fetch('companies', $criteria);

		return $result;
	}

	//---------------------
	//  GET ACCESS RIGHTS
	//---------------------
	/**
	 * Get a user's access rights
	 *
	 * @param int|string $user_id
	 *
	 * @return mixed|bool Access rights data or false on search failure
	 */
	public function get_access_rights($user_id, $type = "user")
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($user_id) && preg_match('/\d+/', $user_id))
		{
			if($type == 'lead')
				$user = $this->get_lead_by_id($user_id);
			else if($type == 'lender')
				$user = $this->get_lender_by_id($user_id);
			else
				$user = $this->get_user_by_id($user_id);

			if(!empty($user))
			{
				$role = (!empty($user['role'])) ? $user['role'] : "";

				$acl = $this->get_access_rights_by_role($role);

				if($acl)
					return $acl;
				else
					return false;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get a access right by id
	 *
	 * @param int|string $access_id
	 *
	 * @return mixed|bool Access rights data or false on search failure
	 */
	public function get_access_rights_by_id($access_id)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($access_id) && preg_match('/\d+/', $access_id))
		{
			$criteria = array(
				'access_id' => $access_id
			);
			$result   = $this->db_fetch('acl', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get access rights by role
	 *
	 * @param int|string $role
	 *
	 * @return mixed|bool Access rights data or false on search failure
	 */
	public function get_access_rights_by_role($role)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($role) && preg_match('/\w+/', $role))
		{
			$criteria = array(
				'role' => $role
			);
			$result   = $this->db_fetch('acl', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get all  access rights
	 *
	 * @return mixed|bool Access rights data or false on search failure
	 */
	public function get_access_rights_all()
	{
		$criteria = array();

		$result = $this->db_fetch('acl', $criteria);

		return $result;
	}

	//---------------------
	//  GET SEARCHES
	//---------------------
	/**
	 * Get a saved search
	 *
	 * @param int|string $search_id
	 *
	 * @return mixed|bool Favorite data or false on search failure
	 */
	public function get_search($search_id)
	{
		// Make sure we have a search_id and it only contains valid characters
		if(!empty($search_id) && preg_match('/\d+/', $search_id))
		{
			$criteria = array(
				'search_id' => $search_id
			);
			$result   = $this->db_fetch('searches', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get searches
	 *
	 * @param mixed $criteria
	 * @param string $sort
	 * @param int|string $limit
	 * @param int|string $offset
	 *
	 * @return mixed|bool Search data or false on search failure
	 */
	public function get_searches($criteria, $sort = null, $limit = null, $offset = null)
	{
		$result = $this->db_fetch('searches', $criteria, $sort, $limit, $offset);

		if($result)
			return $result;
		else
			return false;
	}

	/**
	 * Get all searches
	 *
	 * @return mixed|bool Search data or false on search failure
	 */
	public function get_searches_all()
	{
		$criteria = array();

		$result = $this->db_fetch('searches', $criteria);

		if($result)
			return $result;
		else
			return false;
	}

	//---------------------
	//  GET NEWS
	//---------------------
	/**
	 * Get a saved article
	 *
	 * @param int|string $news_id
	 *
	 * @return mixed|bool Favorite data or false on news failure
	 */
	public function get_article($news_id)
	{
		// Make sure we have a news_id and it only contains valid characters
		if(!empty($news_id) && preg_match('/\d+/', $news_id))
		{
			$criteria = array(
				'news_id' => $news_id
			);
			$result   = $this->db_fetch('news', $criteria);

			if($result)
				return current($result);
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get news
	 *
	 * @param mixed $criteria
	 * @param string $sort
	 * @param int|string $limit
	 * @param int|string $offset
	 *
	 * @return mixed|bool News data or false on news failure
	 */
	public function get_news($criteria, $sort = null, $limit = null, $offset = null)
	{
		$result = $this->db_fetch('news', $criteria, $sort, $limit, $offset);

		if($result)
			return $result;
		else
			return false;
	}

	/**
	 * Get all news
	 *
	 * @return mixed|bool Search data or false on news failure
	 */
	public function get_news_all()
	{
		$criteria = array();

		$result = $this->db_fetch('news', $criteria);

		if($result)
			return $result;
		else
			return false;
	}

	//---------------------
	//  GET TESTIMONIALS
	//---------------------
	/**
	 * Get testimonial data
	 *
	 * @param int|string $testimonial_id
	 *
	 * @return mixed|bool Community data or false on search failure
	 */
	public function get_testimonial($testimonial_id)
	{
		// Make sure we have a picture_id and it only contains valid characters
		if(!empty($testimonial_id) && preg_match('/\d+/', $testimonial_id))
		{
			$criteria = array(
				'testimonial_id' => $testimonial_id
			);
			$result = $this->db_fetch('testimonials', $criteria);

			if(!empty($result))
			{
				return current($result);
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get testimonials data
	 *
	 * @param mixed $criteria  Search criteria pre-formatted into an Active Record array
	 * @param string $sort Sort field and direction
	 * @param int|string $limit Limit of results returned
	 * @param int|string offset Record offset for results returned
	 *
	 * @return mixed|bool Community data or false on search failure
	 */
	public function get_testimonials($criteria, $sort = null, $limit = null, $offset = null)
	{
		// Make sure we have criteria
		if(!empty($criteria))
		{
			$result = $this->db_fetch('testimonials', $criteria, $sort, $limit, $offset);

			if(!empty($result))
			{
				return $result;
			}
			else
				return false;
		}
		else
			return false;
	}

	/**
	 * Get all testimonials data
	 *
	 * @param string $sort Sort field and direction
	 * @param int|string $limit Limit of results returned
	 * @param int|string offset Record offset for results returned
	 *
	 * @return mixed|bool Community data or false on search failure
	 */
	public function get_testimonials_all($sort = null, $limit = null, $offset = null)
	{
		$criteria = array(
			'testimonial_id >' => 0
		);

		$result = $this->db_fetch('testimonials', $criteria, $sort, $limit, $offset);

		if(!empty($result))
		{
			return $result;
		}
		else
			return false;
	}

	//---------------------
	//  GET CONTACT REQUESTS
	//---------------------
	/**
	 * Get a user's contact requests
	 *
	 * @param int|string $user_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_by_user($user_id, $start = null, $end = null)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($user_id) && preg_match('/\d+/', $user_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'user_id'     => $user_id,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a lead's contact requests
	 *
	 * @param int|string $lead_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_by_lead($lead_id, $start = null, $end = null)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($lead_id) && preg_match('/\d+/', $lead_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'lead_id'     => $lead_id,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a teams's contact requests
	 *
	 * @param int|string $team_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_by_team($team_id, $start = null, $end = null)
	{
		// Make sure we have a team_id and it only contains valid characters
		if(!empty($team_id) && preg_match('/\d+/', $team_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'team_id'     => $team_id,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get an office's contact requests
	 *
	 * @param int|string $office_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_by_office($office_id, $start = null, $end = null)
	{
		// Make sure we have an office_id and it only contains valid characters
		if(!empty($office_id) && preg_match('/\d+/', $office_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'office_id'   => $office_id,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a company's contact requests
	 *
	 * @param int|string $company_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_by_company($company_id, $start = null, $end = null)
	{
		// Make sure we have a company_id and it only contains valid characters
		if(!empty($company_id) && preg_match('/\d+/', $company_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'company_id'  => $company_id,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get all users' contact requests
	 *
	 * @param string $type
	 * @param string $start Start date range
	 * @param string $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_by_all($start = null, $end = null)
	{
		$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
		$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
		$criteria = array(
			'_created >=' => $start,
			'_created <=' => $end
		);
		$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

		return $result;
	}

	/**
	 * Get full contact requests
	 *
	 * @param string $start Start date range
	 * @param string $end   End date range
	 *
	 * @return mixed|bool contacts data or false on search failure
	 */
	public function get_contacts_all($start = null, $end = null)
	{
		$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
		$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
		$criteria = array(
			'_created >=' => $start,
			'_created <=' => $end
		);
		$result   = $this->db_fetch('contacts', $criteria, "_created DESC");

		return $result;
	}

	//---------------------
	//  GET ACTIVITY LOGS
	//---------------------
	/**
	 * Get a user's activity log
	 *
	 * @param int|string $user_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_user($user_id, $type = "user", $start = null, $end = null)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($user_id) && preg_match('/\d+/', $user_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'user_id'     => $user_id,
				'type'        => $type,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('activity', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a lead's activity log
	 *
	 * @param int|string $lead_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_lead($lead_id, $type = "lead", $start = null, $end = null)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($lead_id) && preg_match('/\d+/', $lead_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'lead_id'     => $lead_id,
				'type'        => $type,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('activity', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a lender's activity log
	 *
	 * @param int|string $lender_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_lender($lender_id, $type = "lender", $start = null, $end = null)
	{
		// Make sure we have a lkender_id and it only contains valid characters
		if(!empty($lender_id) && preg_match('/\d+/', $lender_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'lender_id'     => $lender_id,
				'type'        => $type,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('activity', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a teams's activity log
	 *
	 * @param int|string $team_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_team($team_id, $type = "user", $start = null, $end = null)
	{
		// Make sure we have a team_id and it only contains valid characters
		if(!empty($team_id) && preg_match('/\d+/', $team_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'team_id'     => $team_id,
				'type'        => $type,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('activity', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get an office's activity log
	 *
	 * @param int|string $office_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_office($office_id, $type = "user", $start = null, $end = null)
	{
		// Make sure we have an office_id and it only contains valid characters
		if(!empty($office_id) && preg_match('/\d+/', $office_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'office_id'   => $office_id,
				'type'        => $type,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('activity', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get a company's activity log
	 *
	 * @param int|string $company_id
	 * @param string     $type
	 * @param string     $start Start date range
	 * @param string     $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_company($company_id, $type = "user", $start = null, $end = null)
	{
		// Make sure we have a company_id and it only contains valid characters
		if(!empty($company_id) && preg_match('/\d+/', $company_id))
		{
			$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
			$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
			$criteria = array(
				'company_id'  => $company_id,
				'type'        => $type,
				'_created >=' => $start,
				'_created <=' => $end
			);
			$result   = $this->db_fetch('activity', $criteria, "_created DESC");

			return $result;
		}
		else
			return false;
	}

	/**
	 * Get all users' activity log
	 *
	 * @param string $type
	 * @param string $start Start date range
	 * @param string $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_by_all($type = "user", $start = null, $end = null)
	{
		$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
		$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
		$criteria = array(
			'type'        => $type,
			'_created >=' => $start,
			'_created <=' => $end
		);
		$result   = $this->db_fetch('activity', $criteria, "_created DESC");

		return $result;
	}

	/**
	 * Get full activity log
	 *
	 * @param string $start Start date range
	 * @param string $end   End date range
	 *
	 * @return mixed|bool Activity data or false on search failure
	 */
	public function get_activity_all($start = null, $end = null)
	{
		$start    = (!empty($start)) ? $start : "1900-01-01 00:00:00";
		$end      = (!empty($end)) ? $end : "2200-01-01 00:00:00";
		$criteria = array(
			'_created >=' => $start,
			'_created <=' => $end
		);
		$result   = $this->db_fetch('activity', $criteria, "_created DESC");

		return $result;
	}

	//---------------------
	//  CREATE RECORDS
	//---------------------
	/**
	 * Create a new user
	 *
	 * @param mixed $data New user data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_user($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('users', $data);
		else
			return false;
	}

	/**
	 * Create a new team
	 *
	 * @param mixed $data New team data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_team($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('teams', $data);
		else
			return false;
	}

	/**
	 * Create a new office
	 *
	 * @param mixed $data New office data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_office($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('offices', $data);
		else
			return false;
	}

	/**
	 * Create a new company
	 *
	 * @param mixed $data New company data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_company($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('companies', $data);
		else
			return false;
	}

	/**
	 * Create a new access right
	 *
	 * @param mixed $data New access right data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_access_right($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('acl', $data);
		else
			return false;
	}

	/**
	 * Create a new contact request
	 *
	 * @param mixed $data New contact request data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_contact($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('contacts', $data);
		else
			return false;
	}

	/**
	 * Create a new activity
	 *
	 * @param mixed $data New activity data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_activity($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('activity', $data);
		else
			return false;
	}

	/**
	 * Create a new saved search
	 *
	 * @param mixed $data New search data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_search($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('searches', $data);
		else
			return false;
	}

	/**
	 * Create a new saved news
	 *
	 * @param mixed $data New news data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_news($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('news', $data);
		else
			return false;
	}

	/**
	 * Create a new testimonial
	 *
	 * @param mixed $data New testimonial data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function create_testimonial($data)
	{
		// Make sure we actually have data to insert
		if(!empty($data))
			return $this->db_insert('testimonials', $data);
		else
			return false;
	}

	//---------------------
	//  UPDATE RECORDS
	//---------------------
	/**
	 * Update a user
	 *
	 * @param mixed $data    User data pre-formatted into an Active Record criteria array
	 * @param mixed $user_id User id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_user($data, $user_id)
	{
		// Make sure we actually have the criteria to update
		if(!empty($data) && !empty($user_id))
		{
			$id = array('user_id' => $user_id);

			return $this->db_update('users', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update a team
	 *
	 * @param mixed $data    Team data pre-formatted into an Active Record criteria array
	 * @param mixed $team_id Team id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_team($data, $team_id)
	{
		// Make sure we actually have  the criteria to update
		if(!empty($data) && !empty($team_id))
		{
			$id = array('team_id' => $team_id);

			return $this->db_update('teams', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update an office
	 *
	 * @param mixed $data      Office data pre-formatted into an Active Record criteria array
	 * @param mixed $office_id Office id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_office($data, $office_id)
	{
		// Make sure we actually have  the criteria to update
		if(!empty($data) && !empty($office_id))
		{
			$id = array('office_id' => $office_id);

			return $this->db_update('offices', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update a company
	 *
	 * @param mixed $data       Company data pre-formatted into an Active Record criteria array
	 * @param mixed $company_id Company id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_company($data, $company_id)
	{
		// Make sure we actually have  the criteria to update
		if(!empty($data) && !empty($company_id))
		{
			$id = array('company_id' => $company_id);

			return $this->db_update('companies', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update an access right
	 *
	 * @param mixed $data      Access Right data pre-formatted into an Active Record criteria array
	 * @param mixed $access_id Access id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_access_right($data, $access_id)
	{
		// Make sure we actually have  the criteria to update
		if(!empty($data) && !empty($access_id))
		{
			$id = array('access_id' => $access_id);

			return $this->db_update('acl', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update an contact request
	 *
	 * @param mixed $data       Contact request data pre-formatted into an Active Record criteria array
	 * @param mixed $contact_id Contact request id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_contact($data, $contact_id)
	{
		// Make sure we actually have the criteria to update
		if(!empty($data) && !empty($contact_id))
		{
			$id = array('contact_id' => $contact_id);

			return $this->db_update('contacts', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update an activity
	 *
	 * @param mixed $data        Activity data pre-formatted into an Active Record criteria array
	 * @param mixed $activity_id Activity id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_activity($data, $activity_id)
	{
		// Make sure we actually have  the criteria to update
		if(!empty($data) && !empty($activity_id))
		{
			$id = array('activity_id' => $activity_id);

			return $this->db_update('activity', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update a saved search
	 *
	 * @param mixed $data Search data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_search($data, $search_id)
	{
		// Make sure we actually have data to update
		if(!empty($data) && !empty($search_id))
		{
			$id = array('search_id' => $search_id);

			return $this->db_update('searches', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update a saved news
	 *
	 * @param mixed $data Search data pre-formatted into an Active Record array to insert
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_news($data, $news_id)
	{
		// Make sure we actually have data to update
		if(!empty($data) && !empty($news_id))
		{
			$id = array('news_id' => $news_id);

			return $this->db_update('news', $data, $id);
		}
		else
			return false;
	}

	/**
	 * Update a testimonial
	 *
	 * @param mixed $data testimonial data pre-formatted into an Active Record criteria array
	 * @param mixed $testimonial_id testimonial id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	public function update_testimonial($data, $testimonial_id)
	{
		// Make sure we actually have the criteria to update
		if(!empty($data) && !empty($testimonial_id))
		{
			$id = array('testimonial_id' => $testimonial_id);

			return $this->db_update('testimonials', $data, $id);
		}

		else
			return false;
	}

	//---------------------
	//  DELETE RECORDS
	//---------------------
	/**
	 * Delete a user
	 *
	 * @param int|string $user_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_user($user_id)
	{
		// Make sure we have a user_id and it only contains valid characters
		if(!empty($user_id) && preg_match('/\d+/', $user_id))
		{
			$criteria = array(
				'user_id' => $user_id
			);
			$result   = $this->db_delete('users', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete a team
	 *
	 * @param int|string $team_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_team($team_id)
	{
		// Make sure we have a team_id and it only contains valid characters
		if(!empty($team_id) && preg_match('/\d+/', $team_id))
		{
			$criteria = array(
				'team_id' => $team_id
			);
			$result   = $this->db_delete('teams', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete an office
	 *
	 * @param int|string $office_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_office($office_id)
	{
		// Make sure we have a office_id and it only contains valid characters
		if(!empty($office_id) && preg_match('/\d+/', $office_id))
		{
			$criteria = array(
				'office_id' => $office_id
			);
			$result   = $this->db_delete('offices', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete a company
	 *
	 * @param int|string $company_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_company($company_id)
	{
		// Make sure we have a company_id and it only contains valid characters
		if(!empty($company_id) && preg_match('/\d+/', $company_id))
		{
			$criteria = array(
				'company_id' => $company_id
			);
			$result   = $this->db_delete('companies', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete an access right
	 *
	 * @param int|string $access_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_access_right($access_id)
	{
		// Make sure we have a access_id and it only contains valid characters
		if(!empty($access_id) && preg_match('/\d+/', $access_id))
		{
			$criteria = array(
				'access_id' => $access_id
			);
			$result   = $this->db_delete('acl', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete a contact request
	 *
	 * @param int|string $contact_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_contact($contact_id)
	{
		// Make sure we have a contact_id and it only contains valid characters
		if(!empty($contact_id) && preg_match('/\d+/', $contact_id))
		{
			$criteria = array(
				'contact_id' => $contact_id
			);
			$result   = $this->db_delete('contacts', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete an activity
	 *
	 * @param int|string $activity_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_activity($activity_id)
	{
		// Make sure we have a activity_id and it only contains valid characters
		if(!empty($activity_id) && preg_match('/\d+/', $activity_id))
		{
			$criteria = array(
				'activity_id' => $activity_id
			);
			$result   = $this->db_delete('activity', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete a saved search
	 *
	 * @param int|string $search_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_search($search_id)
	{
		// Make sure we have a search_id and it only contains valid characters
		if(!empty($search_id) && preg_match('/\d+/', $search_id))
		{
			$criteria = array(
				'search_id' => $search_id
			);
			$result   = $this->db_delete('searches', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete a saved news
	 *
	 * @param int|string $news_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_news($news_id)
	{
		// Make sure we have a news_id and it only contains valid characters
		if(!empty($news_id) && preg_match('/\d+/', $news_id))
		{
			$criteria = array(
				'news_id' => $news_id
			);
			$result   = $this->db_delete('news', $criteria);

			return $result;
		}
		else
			return false;
	}

	/**
	 * Delete a testimonial
	 *
	 * @param int|string $testimonial_id
	 *
	 * @return bool True on success and false on failure
	 */
	public function delete_testimonial($testimonial_id)
	{
		// Make sure we have a document_id and it only contains valid characters
		if(!empty($testimonial_id) && preg_match('/\d+/', $testimonial_id))
		{
			$criteria = array(
				'testimonial_id' => $testimonial_id
			);
			$result = $this->db_delete('testimonials', $criteria);

			return $result;
		}
		else
			return false;
	}

	//-------------------------
	//  DB FUNCTIONS (PRVIATE)
	//-------------------------
	/**
	 * Fetch records from the database
	 *
	 * @param string     $table
	 * @param mixed      $criteria Search criteria pre-formatted into an Active Record array
	 * @param string     $sort     Sort field and direction
	 * @param int|string $limit    Limit of results returned
	 * @param int|string offset    Record offset for results returned
	 *
	 * @return mixed|bool Selected data or false on failure
	 */
	private function db_fetch($table, $criteria, $sort = null, $limit = null, $offset = null)
	{
		if(!empty($criteria))
		{
			$where_in = array();

			foreach($criteria as $key => $value)
			{
				if(is_array($value))
				{
					$where_in[$key] = $value;
					unset($criteria[$key]);
				}
			}

			$this->db->select('*')->from($table)->where($criteria);

			if(!empty($where_in))
			{
				foreach($where_in as $field => $selection)
				{
					$this->db->where_in($field, $selection);
				}
			}
		}
		else
			$this->db->select('*')->from($table);

		if(!empty($sort))
			$this->db->order_by($sort);

		if(!empty($limit) && !(empty($offset)))
			$this->db->limit($limit, $offset);
		else if(!empty($limit))
			$this->db->limit($limit);

		/** @var CI_DB_mysqli_result $result */
		$result = $this->db->get();

		if($result->num_rows() > 0)
		{
			return $result->result_array();
		}
		else
			return false;
	}

	/**
	 * Insert records in the database
	 *
	 * @param string $table
	 * @param mixed  $data Data pre-formatted into an Active Record array
	 *
	 * @return bool True on success and false on failure
	 */
	private function db_insert($table, $data)
	{
		$data['_created']  = date('Y-m-d H:i:s');
		$data['_modified'] = date('Y-m-d H:i:s');

		$this->db->insert($table, $data);
		$insert_id = $this->db->insert_id();

		if($this->db->affected_rows() < 1)
			return false;
		else
			return $insert_id;
	}

	/**
	 * Update records in the database
	 *
	 * @param string $table
	 * @param mixed  $data Data pre-formatted into an Active Record array
	 * @param mixed  $id   Record id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	private function db_update($table, $data, $id)
	{
		$data['_modified'] = date('Y-m-d H:i:s');

		$this->db->update($table, $data, $id);

		if($this->db->affected_rows() < 1)
			return false;
		else
			return true;
	}

	/**
	 * Delete records from the database
	 *
	 * @param string $table
	 * @param mixed  $id Record id pre-formatted into an Active Record criteria array
	 *
	 * @return bool True on success and false on failure
	 */
	private function db_delete($table, $id)
	{
		$this->db->delete($table, $id);

		if($this->db->affected_rows() < 1)
			return false;
		else
			return true;
	}
}

/* End of file accounts.php */
/* Location: ./application/models/accounts.php */
